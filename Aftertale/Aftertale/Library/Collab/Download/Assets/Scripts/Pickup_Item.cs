﻿using UnityEngine;
using System.Collections;

public class Pickup_Item : Popup_text_fonts {

    public string item;

    public string inventoryfull;

    private bool auxiliarbool;

    void Start()
    {
        originalfont = displaytext.font;
        auxiliarbool = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (state == messages.Length - 2 && AutoTextScript.isinteracting == false && caninteract == true)
            {
                if (player_data.playerdata.items.Count < 7)
                {
                    state++;
                    AutoTextScript.isinteracting = true;
                    player.GetComponent<movement>().can_move = false;
                    player.GetComponent<Animator>().SetBool("moving", false);
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = "";
                    displaytext.font = fonts[state];
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, dialoguesounds[state]);
                    auxiliarbool = true;

                }
                else
                {
                    state++;
                    AutoTextScript.isinteracting = true;
                    player.GetComponent<movement>().can_move = false;
                    player.GetComponent<Animator>().SetBool("moving", false);
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = "";
                    displaytext.font = fonts[state];
                    AutoTextScript.TypeText(displaytext, inventoryfull, 0.03f, dialoguesounds[state]);
                    auxiliarbool = false;
                }

            }
            if (state == messages.Length - 1 && AutoTextScript.isinteracting == false)
            {
                player.GetComponent<movement>().can_move = true;
                dialoguebox.gameObject.SetActive(false);
                displaytext.gameObject.SetActive(false);
                AutoTextScript.isfinished = false;
                AutoTextScript.isinteracting = false;
                displaytext.font = originalfont;
                state = -1;
                if (auxiliarbool == true)
                {
                    if (player_data.playerdata.items[0] == "Nothing")
                    {
                        player_data.playerdata.items.RemoveAt(0);
                    }
                    player_data.playerdata.items.Add(item);
                    Destroy(gameObject);
                    if (variable != 0 || value != 0)
                    {
                        player_data.playerdata.global_variables[variable] = value;
                    }
                }
            }
            else if (caninteract == true && AutoTextScript.isinteracting == false)
            {
                state++;
                AutoTextScript.isinteracting = true;
                player.GetComponent<movement>().can_move = false;
                player.GetComponent<Animator>().SetBool("moving", false);
                dialoguebox.gameObject.SetActive(true);
                displaytext.gameObject.SetActive(true);
                displaytext.text = "";
                displaytext.font = fonts[state];
                AutoTextScript.TypeText(displaytext, messages[state], 0.03f, dialoguesounds[state]);

            } 
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (caninteract == true)
            {
                if (state >= 0)
                {
                    AutoTextScript.StopText(displaytext);
                    messages[state] = messages[state].Replace("ç", "\n");
                    displaytext.text = messages[state];
                    AutoTextScript.isfinished = true;
                    AutoTextScript.isinteracting = false;
                }

            }
        }
    }
}
