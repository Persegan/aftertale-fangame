﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADP_Calculator : MonoBehaviour {

    public bool type; //if type = false we do the 3 things for the first month, if true, we don't

    protected float Result;
    public float F_Factor;
    public float initial_Retention;
    public int number_of_months;

    public float retention1;
    public float retention2;
    public float retention3;

    protected float last_retention;

	// Use this for initialization
	void Start () {
        Result = 0.0f;
        last_retention = initial_Retention;
        for (int i = 0; i < number_of_months; i++)
        {
            if (type == false)
            {
                if (i == 0)
                {
                    Result += 1.0f + (1.0f * retention1) + (6.0f * retention2) + (23.0f * retention3);
                    last_retention = retention3;
                }
                else
                {
                    last_retention = last_retention * F_Factor;
                    Result += (30.0f * last_retention);
                }
            }
            else
            {
                Result += (30.0f * last_retention);
                last_retention = last_retention * F_Factor;
            }
        }
        Debug.Log(Result);
		
	}
	
	// Update is called once per frame
	void Update () {

		
	}
}
