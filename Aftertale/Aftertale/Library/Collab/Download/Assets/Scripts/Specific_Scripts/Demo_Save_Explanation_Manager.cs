﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Demo_Save_Explanation_Manager : MonoBehaviour {

    public string scene_to_load;

    public GameObject black_background;

    // Use this for initialization
    void Start () {
        StartCoroutine(Demo_Save_Explanation_Execution());

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Demo_Save_Explanation_Execution()
    {
        black_background.SetActive(true);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(1.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(1.0f);
        yield return new WaitForSeconds(5.0f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(1.0f, 0.5f, false);
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene(scene_to_load);


    }
}
