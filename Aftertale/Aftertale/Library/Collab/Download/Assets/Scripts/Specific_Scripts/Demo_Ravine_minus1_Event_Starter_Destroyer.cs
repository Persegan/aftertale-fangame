﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_minus1_Event_Starter_Destroyer : MonoBehaviour {

    private GameObject Player;

    
	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");

        //We check whether this is the player's first time in ravine -1 and thus we need to lerp the camera
        if (player_data.playerdata.global_variables[40] == 0)
        {
            StartCoroutine(initial_transition());
        }

        //wew change variable 14 so that the parallax trees in ravine 0 work properly
        player_data.playerdata.global_variables[14] = 1;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator initial_transition()
    {
        Camera.main.gameObject.GetComponent<camera>().enabled = false;
        Camera.main.gameObject.transform.position = new Vector3(1.339f, 4.01f, -10.0f);
        Player.GetComponent<movement>().can_dash = false;
        Player.GetComponent<movement>().can_interact = false;
        Player.GetComponent<movement>().can_move = false;
        Player.GetComponent<Animator>().Play("Pumpkin_Getting_Up", 1);
        yield return new WaitForEndOfFrame();
        Player.GetComponent<Animator>().enabled = false;

        yield return StartCoroutine(Level_Transition.move_lerping(Camera.main.gameObject, new Vector3(1.339f, -4.48f, -10.0f), 0.5f, 0.01f));
        Camera.main.gameObject.transform.position = new Vector3(1.339f, -4.48f, -10.0f);
        Player.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).length / Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).speedMultiplier);
        Debug.Log(Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).length / Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).speedMultiplier);
        Camera.main.gameObject.GetComponent<camera>().enabled = true;
        Camera.main.gameObject.GetComponent<camera>().speed = 100000;
        Camera.main.gameObject.GetComponent<camera>().Reposition();
        Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_interact = true;
        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<Animator>().Play("Blank_State", 1);
        yield return new WaitForEndOfFrame();
        //Camera.main.gameObject.GetComponent<camera>().speed = 100;
        player_data.playerdata.global_variables[40] = 1;
    }
}
