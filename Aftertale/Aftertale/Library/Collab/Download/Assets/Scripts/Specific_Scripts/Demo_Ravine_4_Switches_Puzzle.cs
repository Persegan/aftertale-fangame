﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Demo_Ravine_4_Switches_Puzzle : MonoBehaviour {

    public GameObject Self_Switch_Pressed;
    public GameObject Self_Switch_Unpressed;
    public GameObject crossing_bridge;
    public GameObject River_Animation;

    public GameObject Bridge_Collisions;

    public List<SpriteRenderer> Affected_Switches;

    public List<SpriteRenderer> All_Switches;



    private Color light_red = new Color(210 / 255.0f, 65 / 255.0f, 65 / 255.0f);
    private Color light_green = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {

                Self_Switch_Unpressed.GetComponent<SpriteRenderer>().enabled = false;
                Self_Switch_Pressed.GetComponent<SpriteRenderer>().enabled = true;
         
                ChangeColor();
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            Self_Switch_Unpressed.GetComponent<SpriteRenderer>().enabled = true;
            Self_Switch_Pressed.GetComponent<SpriteRenderer>().enabled = false;
        }
    }


    public void ChangeColor()
    {
        for (int i = 0; i < Affected_Switches.Count; i++)
        {
            if (Affected_Switches[i].color == light_red)
            {
                Affected_Switches[i].color = light_green;
            }
            else
            {
                Affected_Switches[i].color = light_red;
            }
        }
        int auxiliar_count = 0;
        for (int i = 0; i < All_Switches.Count; i++)
        {
            if (All_Switches[i].color == light_green)
            {
                auxiliar_count++;
            }
        }
        if (auxiliar_count == All_Switches.Count)
        {
            StartCoroutine(EndPuzzle());
        }

    }

    public IEnumerator EndPuzzle()
    {
        player_data.playerdata.global_variables[7] = 1;
        crossing_bridge.SetActive(true);
        Destroy(River_Animation);
        Destroy(Bridge_Collisions);

        transform.parent.gameObject.SetActive(false);

        yield return 0;
    }
}
