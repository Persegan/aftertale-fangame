﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_5_Water_Ripples : MonoBehaviour {

    public AudioSource water_splash_sound;
    public float splash_sound_delay;

    public GameObject water_ripple;
    public float water_ripple_delay;

    private GameObject Player;
    private bool can_sound;
    private bool can_ripple;

	// Use this for initialization
	void Start () {
        can_sound = true;
        can_ripple = true;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D collider)
    {
       if (collider.tag == "Player")
        {
            if (Player == null)
            {
                Player = collider.gameObject;
            }

            if (Player.GetComponent<Rigidbody2D>().velocity.magnitude != 0)
            {
                if (can_ripple == true)
                {
                    StartCoroutine(CreateRipple());
                }
                if (can_sound == true)
                {
                    StartCoroutine(PlaySound());
                }
            }
        }
    }

    public IEnumerator CreateRipple()
    {
        can_ripple = false;
        GameObject temporal = Instantiate(water_ripple);
        temporal.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y - 0.12f, -0.75f);
        yield return new WaitForSeconds(water_ripple_delay);
        can_ripple = true;
    }

    public IEnumerator PlaySound()
    {
        can_sound = false;
        water_splash_sound.Play();
        yield return new WaitForSeconds(splash_sound_delay);
        can_sound = true;
    }
}
