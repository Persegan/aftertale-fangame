﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Demo_Ravine_3_Conversation_2 : MonoBehaviour {

    public int state;
    public string[] messages;
    public GameObject character_portrait;
    public Sprite[] character_sprites;
    public Image dialoguebox;
    public Text displaytext;
    public Button message_owner_box;
    public GameObject player;
    public AudioSource[] character_sounds;
    public Font[] character_fonts;
    public GameObject black_background;

    public List<GameObject> private_characters = new List<GameObject>();
    public GameObject Overworld_Gaster;
    public AudioSource Gaster_Barks;

    private bool can_interact;

    public bool complete = false;

    const int GASTER_ANNOYED = 1;
    const int GASTER_CONFIDENT = 2;
    const int GASTER_DOGFACE = 3;
    const int GASTER_FRUSTRATED = 4;
    const int GASTER_NEUTRAL = 5;
    const int GASTER_SYMPATHETIC = 6;
    const int GASTER_THINKING = 7;
    const int GASTER_PUPPY = 8;

    private bool can_skip;
    // Use this for initialization
    void Start()
    {
        state = 0;
        can_skip = false;
        complete = false;
        can_interact = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Execute_Dialogue()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Coroutine());
        }
    }


    public void Skip_Dialogue()
    {
        if (state >= 0 && can_skip == true)
        {
            AutoTextScript.StopText(displaytext);
            messages[state] = messages[state].Replace("ç", "\n");
            displaytext.text = messages[state];
            AutoTextScript.isfinished = true;
            AutoTextScript.isinteracting = false;
        }
    }


    IEnumerator Execute_Dialogue_Coroutine()
    {
        if (can_interact == true)
        {
            if (AutoTextScript.isinteracting == false && can_skip == true)
            {
                state++;
            }
            switch (state)
            {
                case 0:
                    //Gaster Barks and Pumpkin Faces South
                    Gaster_Barks.Play();
                    yield return new WaitForSeconds(0.25f);
                    Gaster_Barks.Play();
                    yield return new WaitForSeconds(0.75f);
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().TTF_Overworld;
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
                    player.GetComponent<Animator>().SetInteger("direction", 4);

                    can_interact = false;
                    //Camera pans down
                    Camera.main.GetComponent<camera>().enabled = false;
                    StartCoroutine(move_without_lerping(Camera.main.gameObject, new Vector3(0.491f, -1.116f, -10f), 2.0f));

                    //Gaster is walking into position
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Walk_Right", 0);
                    yield return StartCoroutine(move_without_lerping(Overworld_Gaster, new Vector3(-0.644f, -1.491f, 0f), 0.5f));
                    yield return new WaitForSeconds(0.16f);
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Standing_Right", 0);
                    yield return new WaitForSeconds(0.8f);
                    can_interact = true;

                    player.GetComponent<movement>().can_move = false;
                    AutoTextScript.isinteracting = true;
                    yield return StartCoroutine(Place_Objects());
                    message_owner_box.gameObject.SetActive(true);
                    message_owner_box.transform.Find("Text").GetComponent<Text>().text = "???";
                    private_characters[1].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[8];
                    message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    break;

                case 1:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    break;

                case 2:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_CONFIDENT];
                    break;

                case 3:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 4:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 5:
                    can_skip = false;
                    can_interact = false;
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Walk_Down", 0);
                    yield return StartCoroutine(move_without_lerping(Overworld_Gaster, new Vector3(-1.3f, -2.328f, 0f), 0.5f));
                    Camera.main.GetComponent<camera>().enabled = true;
                    StartCoroutine(FinishEverything());
                    break;                    

                default:

                    break;
            }
        }
       
    }





    IEnumerator PauseExecution()
    {
        can_interact = false;
        can_skip = false;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Count; i++)
        {
            private_characters[i].SetActive(false);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator ResumeExecution()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        message_owner_box.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);

        for (int i = 0; i < private_characters.Count; i++)
        {
            private_characters[i].SetActive(true);
        }

        yield return new WaitForSeconds(0.5f);
        can_interact = true;
        can_skip = true;
    }





    IEnumerator Place_Objects()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);

        yield return new WaitForSeconds(0.5f);

        private_characters.Add(GameObject.Instantiate(character_portrait));
        private_characters.Add(GameObject.Instantiate(character_portrait));

        /*private_characters[0] = GameObject.Instantiate(character_portrait);
        private_characters[1] = GameObject.Instantiate(character_portrait);*/

        private_characters[0].transform.SetParent(GameObject.Find("Canvas").transform);
        private_characters[1].transform.SetParent(GameObject.Find("Canvas").transform);

        private_characters[0].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);
        private_characters[1].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);

        private_characters[0].transform.localPosition = new Vector3(-777, 72.5f, 0);
        private_characters[1].transform.localPosition = new Vector3(777, 72.5f, 0);


        private_characters[0].GetComponent<Image>().sprite = character_sprites[0];
        private_characters[1].GetComponent<Image>().sprite = character_sprites[1];

        private_characters[0].transform.localScale = new Vector3(1, 1, 1);
        private_characters[1].transform.localScale = new Vector3(1, 1, 1);

        private_characters[0].transform.SetAsFirstSibling();
        private_characters[1].transform.SetAsFirstSibling();
        black_background.transform.SetAsFirstSibling();

        private_characters[0].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
        private_characters[1].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);

        //StartCoroutine(move_lerping(dialoguebox.gameObject, new Vector3(0, -199, -1), 2.0f));
        //StartCoroutine(move_lerping(private_characters[0], new Vector3(-300, 23, -1), 2.0f));
        //StartCoroutine(move_lerping(private_characters[1], new Vector3(300, 23, -1), 2.0f));

        //yield return new WaitForSeconds(2.0f);

        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);
        displaytext.gameObject.transform.localPosition = new Vector3(11, -220, -1);
        private_characters[0].transform.localPosition = new Vector3(-300, 72.5f, -1);
        private_characters[1].transform.localPosition = new Vector3(235, 72.5f, -1);
        can_skip = true;
    }

    IEnumerator move_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (Vector3.Distance(target.transform.localPosition, destination) > 1.5f)
        {
            target.transform.localPosition = Vector3.Lerp(target.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForSeconds(0.00001f);
            speed += 0.02f;
        }
        yield break;
    }

    IEnumerator move_without_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (target.transform.position != destination)
        {
            target.transform.position = Vector3.MoveTowards(target.transform.position, destination, Time.deltaTime * speed);
            yield return new WaitForSeconds(0.001f);
        }
    }

    IEnumerator FinishEverything()
    {
        //player.GetComponent<movement>().can_move = true;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Count; i++)
        {
            Destroy(private_characters[i]);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        complete = true;
        player.GetComponent<movement>().can_move = true;
        player.GetComponent<movement>().can_dash = true;
        player.GetComponent<movement>().can_interact = true;
        player_data.playerdata.global_variables[13] = 1;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
    }
}
