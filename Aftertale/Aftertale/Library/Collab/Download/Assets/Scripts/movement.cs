﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class movement : MonoBehaviour {

    private float Pumpkin_trigger_collision_rescaling_auxiliar = 4.17f;

	public float movespeed = 2;
	public BoxCollider2D interactionbox;
	public bool can_move;
    public bool menu_open = false;
    public bool in_menu = false;

    public int direction = 3;

    public GameObject soul;
    public GameObject menu;
    public GameObject stat_display; 
    public GameObject item_display;
    public Button stat_display_button;
    public GameObject[] Menu_Items;
    public GameObject dialoguebox;
    public GameObject narratortext;
    public AudioClip Healing_Sound;

    public float dashspeed;
    public float dashduration;
    public float dashcooldown;
    public float dash_modify_strength;


    private GameObject selecteditem;
    private bool auxiliarbool;
    private Vector2 dashvelocity;

    public AudioSource dialogue_sound;

    private bool iswriting;
    private string message;
    public bool can_interact
    {
        get;
        set;
    }

    public bool can_dash
    {
        get;
        set;
    }

    public bool is_dashing
    {
        get;
        set;
    }

    public float previousdrag
    {
        get;
        set;
    }

    // Use this for initialization
    void Awake () {
		can_move = true;
        can_interact = true;
        can_dash = true;
        is_dashing = false;

	}
	
	// Update is called once per frame
	void Update () {
        #region MovingDuringDash
        if (is_dashing == true)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(dashvelocity.normalized.x * dashspeed, dashvelocity.normalized.y * dashspeed);
            Vector2 temporal = gameObject.GetComponent<Rigidbody2D>().velocity;

            if (dashvelocity.normalized.y == 1 && dashvelocity.normalized.x == 0)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {

                    temporal.x += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }

                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    temporal.x -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }
            }

            else if (dashvelocity.normalized.x == 1 && dashvelocity.normalized.y == 0)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {

                    temporal.y += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }

                if (Input.GetKey(KeyCode.DownArrow))
                {
                    temporal.y -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }
            }


           else if (dashvelocity.normalized.y == -1 && dashvelocity.normalized.x == 0)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {

                    temporal.x += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }

                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    temporal.x -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }
            }

            else if (dashvelocity.normalized.x == -1 && dashvelocity.normalized.y == 0)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {

                    temporal.y += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }

                if (Input.GetKey(KeyCode.DownArrow))
                {
                    temporal.y -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;

                }
            }

            else if (dashvelocity.normalized.x > 0 && dashvelocity.normalized.y > 0)
            {
                if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.UpArrow))
                {
                    temporal.y -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
                else if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.RightArrow))
                {
                    temporal.x -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
            }
            else if (dashvelocity.normalized.x > 0 && dashvelocity.normalized.y < 0)
            {
                if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.DownArrow))
                {
                    temporal.y += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
                else if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.RightArrow))
                {
                    temporal.x -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
            }
            else if (dashvelocity.normalized.x < 0 && dashvelocity.normalized.y > 0)
            {
                if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.UpArrow))
                {
                    temporal.y -= dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
                else if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftArrow))
                {
                    temporal.x += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
            }
            else if (dashvelocity.normalized.x < 0 && dashvelocity.normalized.y < 0)
            {
                if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.DownArrow))
                {
                    temporal.y += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
                else if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftArrow))
                {
                    temporal.x += dash_modify_strength;
                    gameObject.GetComponent<Rigidbody2D>().velocity = temporal;
                }
            }

        }
#endregion
        if (can_move == true) {
            if (Input.GetKeyDown (KeyCode.C))
            {
                Open_Menu();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                if (can_dash == true && gameObject.GetComponent<Rigidbody2D>().velocity.magnitude != 0)
                {
                    StartCoroutine(dash());
                }

            }
			if (Input.GetKey (KeyCode.UpArrow)) {

				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, movespeed);
				if (gameObject.GetComponent<Animator> ().GetBool ("moving") == true) {
					if (gameObject.GetComponent<Animator> ().GetInteger ("direction") == 2 || gameObject.GetComponent<Animator> ().GetInteger ("direction") == 4) {

					} else {
						gameObject.GetComponent<Animator> ().SetInteger ("direction", 1);
						interactionbox.offset = new Vector2(0, 0.22f/ Pumpkin_trigger_collision_rescaling_auxiliar);
						interactionbox.size = new Vector2 (0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.65f / Pumpkin_trigger_collision_rescaling_auxiliar);

					}


				} else {
					gameObject.GetComponent<Animator> ().SetBool ("moving", true);
					gameObject.GetComponent<Animator> ().SetInteger ("direction", 1);
					interactionbox.offset = new Vector2(0, 0.22f / Pumpkin_trigger_collision_rescaling_auxiliar);
					interactionbox.size = new Vector2 (0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.65f / Pumpkin_trigger_collision_rescaling_auxiliar);

				}

			}

	

			if (Input.GetKey (KeyCode.RightArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (movespeed, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
				if (gameObject.GetComponent<Animator> ().GetBool ("moving") == true) {
					if (gameObject.GetComponent<Animator> ().GetInteger ("direction") == 1 || gameObject.GetComponent<Animator> ().GetInteger ("direction") == 3) {

					} else {
						gameObject.GetComponent<Animator> ().SetInteger ("direction", 2);
						interactionbox.offset = new Vector2(0.41f / Pumpkin_trigger_collision_rescaling_auxiliar, -0.32f / Pumpkin_trigger_collision_rescaling_auxiliar);
						interactionbox.size = new Vector2 (0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.45f / Pumpkin_trigger_collision_rescaling_auxiliar);

					}


				} else {
					gameObject.GetComponent<Animator> ().SetBool ("moving", true);
					gameObject.GetComponent<Animator> ().SetInteger ("direction", 2);
                    interactionbox.offset = new Vector2(0.41f / Pumpkin_trigger_collision_rescaling_auxiliar, -0.32f / Pumpkin_trigger_collision_rescaling_auxiliar);
                    interactionbox.size = new Vector2(0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.45f / Pumpkin_trigger_collision_rescaling_auxiliar);
                }
			}

			if (Input.GetKey (KeyCode.DownArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, -movespeed);
				if (gameObject.GetComponent<Animator> ().GetBool ("moving") == true) {
					if (gameObject.GetComponent<Animator> ().GetInteger ("direction") == 2 || gameObject.GetComponent<Animator> ().GetInteger ("direction") == 4) {

					} else {
						gameObject.GetComponent<Animator> ().SetInteger ("direction", 3);
						interactionbox.offset = new Vector2(0f, -0.74f / Pumpkin_trigger_collision_rescaling_auxiliar);
						interactionbox.size = new Vector2 (0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.36f / Pumpkin_trigger_collision_rescaling_auxiliar);

					}
				
				
				} else {
					gameObject.GetComponent<Animator> ().SetBool ("moving", true);
					gameObject.GetComponent<Animator> ().SetInteger ("direction", 3);
					interactionbox.offset = new Vector2(0f, -0.74f / Pumpkin_trigger_collision_rescaling_auxiliar);
					interactionbox.size = new Vector2 (0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.36f / Pumpkin_trigger_collision_rescaling_auxiliar);
				}
			}

			if (Input.GetKey (KeyCode.LeftArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-movespeed, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
				if (gameObject.GetComponent<Animator> ().GetBool ("moving") == true) {
					if (gameObject.GetComponent<Animator> ().GetInteger ("direction") == 1 || gameObject.GetComponent<Animator> ().GetInteger ("direction") == 3) {

					} else {
						gameObject.GetComponent<Animator> ().SetInteger ("direction", 4);
                        interactionbox.offset = new Vector2(-0.41f / Pumpkin_trigger_collision_rescaling_auxiliar, -0.32f / Pumpkin_trigger_collision_rescaling_auxiliar);
                        interactionbox.size = new Vector2(0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.45f / Pumpkin_trigger_collision_rescaling_auxiliar); ;

					}


				} else {
					gameObject.GetComponent<Animator> ().SetBool ("moving", true);
					gameObject.GetComponent<Animator> ().SetInteger ("direction", 4);
                    interactionbox.offset = new Vector2(-0.41f / Pumpkin_trigger_collision_rescaling_auxiliar, -0.32f / Pumpkin_trigger_collision_rescaling_auxiliar);
                    interactionbox.size = new Vector2(0.5f / Pumpkin_trigger_collision_rescaling_auxiliar, 0.45f / Pumpkin_trigger_collision_rescaling_auxiliar);
                }
			} 
			if (Input.GetKeyUp (KeyCode.UpArrow)) {
				gameObject.GetComponent<Animator> ().SetBool ("moving", false);
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, 0);
			}
			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				gameObject.GetComponent<Animator> ().SetBool ("moving", false);
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (Input.GetKeyUp (KeyCode.LeftArrow)) {
				gameObject.GetComponent<Animator> ().SetBool ("moving", false);
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (Input.GetKeyUp (KeyCode.DownArrow)) {
				gameObject.GetComponent<Animator> ().SetBool ("moving", false);
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, 0);
			}
		}
        else if (menu_open == true && in_menu == false)
        {
            gameObject.GetComponent<Animator>().SetBool("moving", false);
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            if (Input.GetKeyDown(KeyCode.X) ||Input.GetKeyDown(KeyCode.C))
            {
                Close_Menu();
            }
        }
        else if (menu_open == true && in_menu ==true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                in_menu = false;
            }
        }
        else if (menu_open == false && in_menu == true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                menu_open = true;
            }
        }
        else if (iswriting == true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                AutoTextScript.StopText(narratortext.GetComponent<Text>());
                message = message.Replace("ç", "\n");
                narratortext.GetComponent<Text>().text = message;
            }
            if (AutoTextScript.isfinished == true)
            {
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    auxiliarbool = true;
                    can_move = true;
                    soul.SetActive(false);
                    menu.SetActive(false);
                    GameObject myEventSystem = GameObject.Find("EventSystem");
                    myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                    narratortext.SetActive(false);
                    dialoguebox.SetActive(false);
                    iswriting = false;
                }
            }
          
        }
		else
		{
            if (is_dashing == false && can_move == true)
            {
                gameObject.GetComponent<Animator>().SetBool("moving", false);
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }
		}
        if (auxiliarbool == true)
        {
            if (Input.GetKeyUp(KeyCode.Z))
            {
                can_interact = true;
                auxiliarbool = false;
            }
        }

	}

	void OnTriggerEnter2D(Collider2D collider)
	{
	    if (collider.tag == "Level_Transition")
        {
            can_dash = false;
            can_move = false;
            can_interact = false;
            //GameObject.Find("_Fade_In_Out_Manager").GetComponent<Level_Transition>().final_transition(collider.name);
            GameObject.Find("_Fade_In_Out_Manager").BroadcastMessage("final_transition", collider.name);
            player_data.playerdata.position_to_load = collider.gameObject.transform.GetChild(0).transform.localPosition;
            player_data.playerdata.direction_to_load = (int)collider.gameObject.transform.GetChild(0).transform.localPosition.z;
        }	
	}









    ////Helper methods
    public void Open_Menu()
    {
        can_move = false;
        can_dash = false;
        can_interact = false;
        soul.SetActive(true);
        menu_open = true;
        menu.SetActive(true);
        GameObject.Find("Item_button").GetComponent<choice_button_script>().PlayMenuHighlight();
        GameObject.Find("Item_button").GetComponent<Button>().Select();
        GameObject.Find("Player_Name_overview").GetComponent<Text>().text = player_data.playerdata.player_name;
        GameObject.Find("Player_LV_overview").GetComponent<Text>().text = "LV " + player_data.playerdata.level.ToString();
        GameObject.Find("Player_HP_overview").GetComponent<Text>().text = "HP " + player_data.playerdata.health.ToString() + "/" + player_data.playerdata.maxhealth.ToString();
        GameObject.Find("Player_G_overview").GetComponent<Text>().text = "G " + player_data.playerdata.gold.ToString();
    }
    public void Close_Menu()
    {
        can_move = true;
        can_dash = true;
        can_interact = true;
        soul.SetActive(false);
        menu.SetActive(false);
        menu_open = false;
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
    }
    public void Submit_Menu_Stat()
    {

        soul.SetActive(false);
        in_menu = true;
        stat_display.SetActive(true);
        //GameObject myEventSystem = GameObject.Find("EventSystem");
        //myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        GameObject.Find("Player_Name").GetComponent<Text>().text = player_data.playerdata.player_name;
        GameObject.Find("Player_LV").GetComponent<Text>().text = "LV " + player_data.playerdata.level.ToString();
        GameObject.Find("Player_HP").GetComponent<Text>().text = "HP " + player_data.playerdata.health.ToString() + "/" + player_data.playerdata.maxhealth.ToString();
        GameObject.Find("Player_AT").GetComponent<Text>().text = "AT " + player_data.playerdata.attack.ToString();
        GameObject.Find("Player_DF").GetComponent<Text>().text = "DF " + player_data.playerdata.defense.ToString();
        GameObject.Find("Player_EXP").GetComponent<Text>().text = "EXP: " + player_data.playerdata.exp.ToString();
        GameObject.Find("Player_Next").GetComponent<Text>().text ="NEXT: " + player_data.playerdata.next.ToString();
        GameObject.Find("Player_Weapon").GetComponent<Text>().text = "WEAPON: " + player_data.playerdata.weapon;
        GameObject.Find("Player_Armor").GetComponent<Text>().text ="ARMOR: " + player_data.playerdata.armor;
        GameObject.Find("Player_Gold").GetComponent<Text>().text = "GOLD: " + player_data.playerdata.gold.ToString();

       
        stat_display_button.Select();

    }
    public void Cancel_Menu_Stat()
    {
        stat_display.SetActive(false);
        soul.SetActive(true);
       // in_menu = false;
        GameObject.Find("Stats_button").GetComponent<Button>().Select();
    }

    public void Submit_Menu_Item()
    {
        soul.SetActive(true);
        in_menu = true;
        item_display.SetActive(true);
        for (int i = 0; i < player_data.playerdata.items.Count; i++)
        {
            Menu_Items[i].SetActive(true);
            Menu_Items[i].GetComponentInChildren<Text>().text = player_data.playerdata.items[i];
        }
        Menu_Items[0].GetComponent<Button>().Select();
    }

    public void Submit_Item_in_menu()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        selecteditem = myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().currentSelectedGameObject;
        GameObject.Find("Use").GetComponent<Button>().Select();
        menu_open = false;
    }

    public void Cancel_Use_Info_Drop()
    {
        selecteditem.GetComponent<Button>().Select();
    }

    public void Cancel_Menu_Item()
    {
        item_display.SetActive(false);
        soul.SetActive(true);
        // in_menu = false;
        GameObject.Find("Item_button").GetComponent<Button>().Select();
    }

    public void Submit_Use()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        menu_open = false;
        in_menu = false;
        item_display.SetActive(false);
        soul.SetActive(false);
        iswriting = true;
        dialoguebox.SetActive(true);
        narratortext.SetActive(true);
        narratortext.GetComponent<Text>().text = "";
        switch (selecteditem.GetComponentInChildren<Text>().text)
        {
            case "Nothing":
                message = "Using nothing doesn't appear to do anything. How peculiar.";

                break;
            case "MTT Starbar":
                message = "You eat the MTT Starbar! You recover all your health.";
                soul.GetComponent<soul_movement>().Recover_health(player_data.playerdata.maxhealth);
                break;

            case "Bnruger":
                message = "You eat the Bnruger! You recover 10 HP.";
                soul.GetComponent<soul_movement>().Recover_health(10.0f);
                break;

            case "Chocolate Donut":
                message = "You eat the Chocolate Donut! You recover 10 HP.";
                soul.GetComponent<soul_movement>().Recover_health(10.0f);
                break;

            case "Annoying Sign":
                message = "No point on doing that now.";
                break;

            case "Cinnamon Bunny":
                message = "PLACEHOLDER.";
                break;

            case "Dog Treat":
                message = "* You eat the Dog Treat! You recover 10 HP.";
                player_data.playerdata.health += 10.0f;
                if (player_data.playerdata.health > player_data.playerdata.maxhealth)
                    player_data.playerdata.health = player_data.playerdata.maxhealth;
                GetComponent<AudioSource>().PlayOneShot(Healing_Sound);
                break;

            default:
                break;
        }
        AutoTextScript.TypeText(narratortext.GetComponent<Text>(), message, 0.03f, dialogue_sound);
    }

    public void Submit_Info()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        menu_open = false;
        in_menu = false;
        item_display.SetActive(false);
        soul.SetActive(false);
        iswriting = true;
        dialoguebox.SetActive(true);
        narratortext.SetActive(true);
        narratortext.GetComponent<Text>().text = "";
        switch (selecteditem.GetComponentInChildren<Text>().text)
        {
            case "Nothing":
                message = "Nothing appears to be the opposite of something. Bet you didn't know that, huh?";

                break;

            case "MTT Starbar":
                message = "A healing item that recovers all your health.";
                break;

            case "Bnruger":
                message = "A healing item that recovers 10 HP.";
                break;

            case "Chocolate Donut":
                message = "A healing item that recovers 10 HP.";
                break;

            case "Annoying Sign":
                message = "A sign that appears to have fallen into the ravine a long time ago. Can be used as a weapon.";
                break;

            case "Cinnamon Bunny":
                message = "PLACEHOLDER.";
                break;

            case "Dog Treat":
                message = "* Smells like bacon! Eat it to recover 10 HP.";
                break;

            default:
                break;
        }
        AutoTextScript.TypeText(narratortext.GetComponent<Text>(), message, 0.03f, dialogue_sound);
    }

    public void Submit_Drop()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        menu_open = false;
        in_menu = false;
        item_display.SetActive(false);
        soul.SetActive(false);
        iswriting = true;
        dialoguebox.SetActive(true);
        narratortext.SetActive(true);
        narratortext.GetComponent<Text>().text = "";
        switch (selecteditem.GetComponentInChildren<Text>().text)
        {
            case "Nothing":
                message = "You can't drop nothing!";

                break;

            case "MTT Starbar":
                message = "You drop the MTT Starbar";
                player_data.playerdata.items.Remove("MTT Starbar");
                break;

            case "Bnruger":
                message = "You drop the Bnruger";
                player_data.playerdata.items.Remove("Bnruger");
                break;

            case "Chocolate Donut":
                message = "You drop the Chocolate Donut";
                player_data.playerdata.items.Remove("Chocolate Donut");
                break;

            case "Annoying Sign":
                message = "The sign refuses to be dropped.";
                break;

            case "Cinnamon Bunny":
                message = "PLACEHOLDER.";
                break;

            case "Dog Treat":
                message = "* You drop the Dog Treat.";
                break;

            default:
                break;
        }
        if (player_data.playerdata.items.Count == 0)
        {
            player_data.playerdata.items.Add("Nothing");
        }
        AutoTextScript.TypeText(narratortext.GetComponent<Text>(), message, 0.03f, dialogue_sound);
    }

    public IEnumerator dash()
    {
        if (gameObject.GetComponent<Rigidbody2D>().velocity.magnitude == 0)
        {
            StopCoroutine(dash());
        }
        can_dash = false;
        can_move = false;
        is_dashing = true;
        gameObject.GetComponent<Animator>().SetBool("Dashing", true);
        previousdrag = gameObject.GetComponent<Rigidbody2D>().drag;
        gameObject.GetComponent<Rigidbody2D>().drag = 0;
        dashvelocity = new Vector2(0, 0);
        if (Input.GetKey(KeyCode.UpArrow))
        {
            gameObject.GetComponent<Animator>().SetInteger("direction", 1);
            dashvelocity.y = 1;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            gameObject.GetComponent<Animator>().SetInteger("direction", 3);
            dashvelocity.y = -1;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            gameObject.GetComponent<Animator>().SetInteger("direction", 2);
            dashvelocity.x = 1;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            gameObject.GetComponent<Animator>().SetInteger("direction", 4);
            dashvelocity.x = -1;
        }
        can_move = false;
        can_interact = false;
        yield return new WaitForSeconds(dashduration);
        is_dashing = false;
        gameObject.GetComponent<Animator>().SetBool("Dashing", false);
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        gameObject.GetComponent<Rigidbody2D>().drag = previousdrag;
        can_move = true;
        can_interact = true;
        gameObject.GetComponent<Animator>().SetBool("moving", false);
        yield return new WaitForSeconds(dashcooldown);
        can_dash = true;       
    }
}
