﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class player_data : MonoBehaviour {

	public static player_data playerdata;
	public string player_name;

	public List <string> items = new List<string> ();
    public List<int> global_variables = new List<int>();

    public float attack = 0;
    public float baseattack = 0;
    public float basedefense = 0;
    public float defense = 0;
	public float health;
	public float maxhealth;
	public float level;
    public int exp;
    public int next;
    public int gold;
    public string weapon;
    public string armor;
    public string scene_to_load;
    public Vector3 position_to_load;
    public int direction_to_load;
    public float position_from_load_x;
    public float position_from_load_y;

    public GameObject Player_Soul;

	public Vector3 soul_local_position;


	void Awake()
	{
		if (playerdata == null) {
			DontDestroyOnLoad (gameObject);
			playerdata = this;
		} else if (playerdata != this) {
			Destroy (gameObject);
		}

        Reset();

        position_to_load = new Vector3(-1.525f, -0.781f, 8.6f);
		//items.Add("MTT Starbar");
        //items.Add("Chocolate Donut");
        //items.Add("Bnruger");
        //UnityEngine.Cursor.visible = false;      

	}

	public void Save()
	{
        //we set variable 41 to to value 1 so that next time we load Pumpkin does the waking up animation
        global_variables[41] = 1;
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/player_data.dat");


		PlayerData data = new PlayerData ();
		data.items = items;
        data.global_variables = global_variables;
		data.attack = attack;
        data.baseattack = baseattack;
        data.defense = defense;
        data.basedefense = basedefense;
		data.health = health;
		data.maxhealth = maxhealth;
		data.player_name = player_name;
		data.level = level;
        data.scene_to_load = scene_to_load;
        data.position_to_load_x = position_from_load_x;
        data.position_to_load_y = position_from_load_y;

        bf.Serialize (file, data);
		file.Close ();
        global_variables[41] = 0;
    }

	public void Load()
	{
		if (File.Exists (Application.persistentDataPath + "/player_data.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/player_data.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize (file);
			file.Close ();

			items = data.items;
            global_variables = data.global_variables;
			attack = data.attack;
            baseattack = data.baseattack;
            defense = data.defense;
            basedefense = data.basedefense;
			health = data.health;
			player_name = data.player_name;
			maxhealth = data.maxhealth;
			level = data.level;
            scene_to_load = data.scene_to_load;
            position_to_load = new Vector3(data.position_to_load_x, data.position_to_load_y, 1.0f);
            direction_to_load = 3;
		}

	}
	// Use this for initialization
	void Start () {
        //Save();
	
	}
	
	// Update is called once per frame
	void Update () {	
	}

	public void UseItem(string item)
	{
		StartCoroutine (UseItem_coroutine (item));
	}

    public void LoadScene()
    {
        SceneManager.LoadScene(scene_to_load);
    }

    public void TrueReset()
    {
        File.Delete(Application.persistentDataPath + "/player_data.dat");
    }

    public void Reset()
    {
        player_name = "";
        items.Clear();
        items.Add("Nothing");
        global_variables.Clear();
        attack = 0;
        baseattack = 0;
        defense = 0;
        basedefense = 0;
        health = 20;
        maxhealth = 20;
        level = 1;
        exp = 0;
        next = 10;
        gold = 0;
        weapon = "";
        armor = "";
        scene_to_load = "";
       position_to_load = new Vector3(-1.525f, -0.781f, 8.6f);



        global_variables.Add(0); //Position 0 is how many times you've checked the Ravine_0 planks
        global_variables.Add(1); //Position 1 is the states of talking with Signey in Ravine_3
        global_variables.Add(2); //Position 2 is wether you've procced Gaster's conversation 1 in Ravine_2
        global_variables.Add(3); //Position 3 is wether you've procced Gaster's conversation 3 in Ravine_2
        global_variables.Add(4); //Position 4 is the amount of standing still conversations that have happened in Ravine_2
        global_variables.Add(5); //Position 5 is wether you picked up the Ravine 0 sign or not
        global_variables.Add(6); //Position 6 is how many times you've checked the Ravine_4 sign
        global_variables.Add(7); //Position 7 is wether the puzzle in Ravine_4 has been cleared or not
        global_variables.Add(8); //Position 8 is the amount of times you've interacted with the sign in Ravine_4 after the puzzle has been cleared
        global_variables.Add(9); //Position 9 is wether you've cleared or not the 1st puzzle on Ravine_3
        global_variables.Add(10); //Position 10 is wether you've cleared or not the 2nd puzzle on Ravine_3
        global_variables.Add(11); //Position 11 is wether we need to fade in music in Ravine_0
        global_variables.Add(12); //Position 12 is wether you've procced Gaster's conversation 1 in Ravine_3
        global_variables.Add(13); //Position 13 is wether you've procced Gaster's conversation 2 in Ravine_3
        global_variables.Add(14); //Position 14 is wether you're entering Ravine_0 from the right or not, for the parallaxing trees
        global_variables.Add(15); //Position 15 is wether the timed switches puzzle in Ravine_6 has been cleared or not
        global_variables.Add(16); //Position 16 is wether you've completed the Ice_Block_Puzzle in Ravine_4 or not
        global_variables.Add(17); //Position 17 is the number of times you've checked the flowers in Ravine_5
        global_variables.Add(18); //Position 18 is the number of times you've checked the starry wall in Ravine_5
        global_variables.Add(19); //Position 19 is the highest score achieved on the ball minigame in Ravine_6
        global_variables.Add(20); //Position 20 is wether you've beaten Undyne's high score on the ball minigame in Ravine_6
        global_variables.Add(21); //Position 21 is whether you've picked up the Sackboy in Ravine04
        global_variables.Add(22); //Position 22 is whether you've done the signey tutorial in Ravine_6 and what ending did you complete it with, pacifist or genocide. 0 no completion, 1 pacifist, 2 genocide
        global_variables.Add(23); //Position 23 is the amount of times you've interacted with Signy in Ravine_6
        global_variables.Add(24); //Position 24 is the state of the first puzzle in Ravine 12
        global_variables.Add(25); //Position 25 is the state of the first switch of the first puzzle in Ravine 12
        global_variables.Add(26); //Position 26 is the state of the second switch of the first puzzle in Ravine 12
        global_variables.Add(27); //Position 27 is the state of the third switch of the first puzzle in Ravine 12
        global_variables.Add(28); //Position 28 is the state of the fourth switch of the first puzzle in Ravine 12
        global_variables.Add(29); //Position 29 is the state of the fifth switch of the first puzzle in Ravine 12
        global_variables.Add(30); //Position 30 is the state of the sixth switch of the first puzzle in Ravine 12
        global_variables.Add(31); //Position 31 is the state of the seventh switch of the first puzzle in Ravine 12
        global_variables.Add(32); //Position 32 is the state of the eighth switch of the first puzzle in Ravine 12
        global_variables.Add(33); //Position 33 is the state of Puzzle 2 in Ravine 12
        global_variables.Add(34); //Position 34 is the state of Puzzle 3 in Ravine 12
        global_variables.Add(35); //Position 35 is the state of Puzzle 4 in Ravine 12
        global_variables.Add(36); //Position 36 is the state of Puzzle 5 in Ravine 12
        global_variables.Add(37); //Position 37 is the state of Puzzle 6 in Ravine 12
        global_variables.Add(38); //Position 38 is the state of Puzzle 7 in Ravine 12
        global_variables.Add(39); //Position 39 is the state of the Secret Puzzle in Ravine 12
        global_variables.Add(40); //Position 40 is whether we need to do the camera lerp intro on Ravine -1 or not
        global_variables.Add(41); //Position 41 is whether we need to make the pumpkin waking up animation when we enter a room or not (so whether we're loading from the menu or we're entering the room normally)
        global_variables.Add(42); //Position 42 is whether the sackboy in Ravine Secret Room 2 has been picked up or not
        global_variables.Add(43); //Position 423is whether the sackboy in Ravine 6 has been picked up or not



        for (int i = 0; i < global_variables.Count; i++)
        {
            global_variables[i] = 0;
        }
    }






	IEnumerator UseItem_coroutine(string item)
	{
		yield return null;
		switch (item) {
		case "Nothing":
            Camera.main.GetComponentInChildren<battle_manager>().dialogue_box_message = "You use nothing! It's not very effective...";
            Camera.main.GetComponentInChildren<battle_manager>().SetDialogueBox ("You use nothing! It's not very effective...");
			break;
        case "MTT Starbar":
            Camera.main.GetComponentInChildren<battle_manager>().dialogue_box_message = "You eat the MTT Starbar! You recover all your health!";
            Camera.main.GetComponentInChildren<battle_manager>().SetDialogueBox("You eat the MTT Starbar! You recover all your health!");
            Player_Soul.GetComponent<soul_movement>().Recover_health(player_data.playerdata.maxhealth);
            player_data.playerdata.items.Remove("MTT Starbar");
            if (player_data.playerdata.items.Count == 0)
            {
                player_data.playerdata.items.Add("Nothing");
            }
                break;

        case "Chocolate Donut":
            Camera.main.GetComponentInChildren<battle_manager>().dialogue_box_message = "You eat the Chocolate Donut! You recover 10 HP!";
            Camera.main.GetComponentInChildren<battle_manager>().SetDialogueBox("You eat the Chocolate Donut! You recover 10 HP!");
            Player_Soul.GetComponent<soul_movement>().Recover_health(10.0f);
            player_data.playerdata.items.Remove("Chocolate Donut");
            if (player_data.playerdata.items.Count == 0)
            {
                player_data.playerdata.items.Add("Nothing");
            }
                break;

        case "Bnruger":
            Camera.main.GetComponentInChildren<battle_manager>().dialogue_box_message = "You eat the Bnruger! You recover 10 HP!";
            Camera.main.GetComponentInChildren<battle_manager>().SetDialogueBox("You eat the Bnruger! You recover 10 HP!");
            Player_Soul.GetComponent<soul_movement>().Recover_health(10.0f);
            player_data.playerdata.items.Remove("Bnruger");
            if (player_data.playerdata.items.Count == 0)
            {
                player_data.playerdata.items.Add("Nothing");
            }
                break;

            case "Cinnamon Bunny":
                Camera.main.GetComponentInChildren<battle_manager>().dialogue_box_message = "PLACEHOLDER";
                Camera.main.GetComponentInChildren<battle_manager>().SetDialogueBox("PLACEHOLDER");
                Player_Soul.GetComponent<soul_movement>().Recover_health(22.0f);
                player_data.playerdata.items.Remove("Cinnamon Bunny");
                if (player_data.playerdata.items.Count == 0)
                {
                    player_data.playerdata.items.Add("Nothing");
                }
                break;
            case "Dog Treat":
                Camera.main.GetComponentInChildren<battle_manager>().dialogue_box_message = "You eat the Dog Treat! It tastes like bacon.";
                Camera.main.GetComponentInChildren<battle_manager>().SetDialogueBox("You eat the Dog Treat! It tastes like bacon.");
                Player_Soul.GetComponent<soul_movement>().Recover_health(10.0f);
                player_data.playerdata.items.Remove("Dog Treat");
                if (player_data.playerdata.items.Count == 0)
                {
                    player_data.playerdata.items.Add("Nothing");
                }

                break;



            default:
				break;

				}
	}

    
}
	

[System.Serializable]
class PlayerData
{
	public List <string> items = new List<string> ();
    public List<int> global_variables = new List<int>();
    public string player_name;
	public float attack;
    public float baseattack;
    public float defense;
    public float basedefense;
	public float health;
	public float maxhealth;
	public float level;
    public string scene_to_load;
    public float position_to_load_x;
    public float position_to_load_y;
}