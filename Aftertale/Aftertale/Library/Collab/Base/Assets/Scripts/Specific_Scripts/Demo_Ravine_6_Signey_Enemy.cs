﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_6_Signey_Enemy : MonoBehaviour
{

    public battle_manager battle_manager;
    public GameObject speech_bubble;
    private bool finishedspeaking;
    private bool isspeaking;
    private string speakingmessage;
    public Text speech_bubble_text;
    public AudioClip speech_sound;
    
    //Stuff to end the fight
    public GameObject soul;
    public GameObject black_background;
    public GameObject Battle_Canvas;
    public GameObject Overworld_Signy;
    public Sprite Damaged_Signy_1;
    public Sprite Damaged_Signy_2;
    public Sprite Damaged_Signy_3;


    //Stuff for Signey
    private int interaction_dialogue;
    private bool act1_give_memory = true;
    private bool act2_give_memory = true;
    private bool act3_give_memory = true;
    private bool act3_phase = false;
    private bool fight_begins = true;
    private bool black_memories = true;


    //Stuff to 




    //Signey Stuff
    public GameObject Signey_Attack;

    // Use this for initialization
    void Start()
    {
        //general stuff
        battle_manager = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<battle_manager>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (finishedspeaking == true)
            {
                finishedspeaking = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (isspeaking == true)
            {
                finishedspeaking = true;
                AutoTextScript.isfinished = true;
                speech_bubble_text.StopAllCoroutines();
                speakingmessage = speakingmessage.Replace("ç", "\n");
                speech_bubble_text.text = speakingmessage;
            }

        }

    }



    ///Auxiliar methods
    IEnumerator ShowDialogue(string message)
    {
        speakingmessage = message;
        isspeaking = true;
        speech_bubble.SetActive(true);
        speech_bubble_text.gameObject.SetActive(true);
        speech_bubble_text.text = "";

        AutoTextScript.TypeText(speech_bubble_text, message, 0.03f, null);
        while (AutoTextScript.isfinished == false)
        {
            //speech_sound.pitch = Random.Range (0.3f, 1.9f);
            GetComponent<AudioSource>().clip = speech_sound;
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.1f);
        }
        finishedspeaking = true;
        while (finishedspeaking == true)
        {
            yield return new WaitForEndOfFrame();
        }
        //yield return new WaitForSeconds (2.0f);
        speech_bubble.SetActive(false);
        speech_bubble_text.gameObject.SetActive(false);
    }

    public void attack(int turn)
    {
        StartCoroutine(attack_coroutine(turn));
    }

    public void dialogue(int turn)
    {
        StartCoroutine(dialogue_coroutine(turn));
    }

    public void ExecuteInteraction(int number)
    {
        StartCoroutine(ExecuteInteraction_coroutine(number));
    }

    public void ExecuteMemory(string name)
    {
        StartCoroutine(ExecuteMemory_coroutine(name));
    }



    IEnumerator attack_coroutine(int turn)
    {
        //We change the sprite if Signy is damaged
        if (GetComponent<enemy_data>().health > GetComponent<enemy_data>().max_health * 50 / 100)
        {
            
        }

        else if (GetComponent<enemy_data>().health <= GetComponent<enemy_data>().max_health * 50 / 100 && GetComponent<enemy_data>().health > GetComponent<enemy_data>().max_health * 25 / 100)
        {
            transform.GetChild(0).gameObject.GetComponent<Image>().sprite = Damaged_Signy_1;
        }

        else if (GetComponent<enemy_data>().health <= GetComponent<enemy_data>().max_health * 25 / 100 && GetComponent<enemy_data>().health > 0)
        {
            transform.GetChild(0).gameObject.GetComponent<Image>().sprite = Damaged_Signy_2;
        }

        else
        {

            if (black_memories == true)
            {
                StartCoroutine(AudioManager.FadeOut(AudioManager.Audio_Manager.GetComponent<AudioSource>(), 7.5f));
                GetComponent<enemy_data>().defense = 100;
                GetComponent<enemy_data>().interactions[0] = "* Check";
                GetComponent<enemy_data>().interactions[1] = null;
                GetComponent<enemy_data>().interactions[2] = null;
                transform.GetChild(0).gameObject.GetComponent<Image>().sprite = Damaged_Signy_3;
                battle_manager.ResetMemoryPortions();
                yield return new WaitForEndOfFrame();
                GetComponent<Animator>().enabled = false;
                battle_manager.GiveMemoryPortion(gameObject, Color.black, new Color (0,0,0,200/255f));
                yield return new WaitForSeconds(2.5f);
                battle_manager.GiveMemoryPortion(gameObject, Color.black, new Color(0, 0, 0, 200 / 255f));
                yield return new WaitForSeconds(2.5f);
                battle_manager.GiveMemoryPortion(gameObject, Color.black, new Color(0, 0, 0, 200 / 255f));
                yield return new WaitForSeconds(2.5f);
                battle_manager.StartPlayerTurn();

                black_memories = false;
                yield break;
            }
           
        }
        yield return new WaitForEndOfFrame();

        if (fight_begins == true)
        {
            yield return ShowDialogue("* No need to be alarmed by the change ofçscenery.");
            yield return ShowDialogue("* This is traditionally called a battle, butçit's nothing to beçafraid of.");
            yield return ShowDialogue("* You can move aroundçhere, see? Same asçanywhere else.");
            yield return ShowDialogue("* Just don't run intoçme, or you might hurtçyourself.");
            fight_begins = false;
        }

        if (interaction_dialogue == 1)
        {
            yield return ShowDialogue("* Puzzles are great,çaren't they?");
            yield return ShowDialogue("* The only reason Içexist is to help othersçlearn about puzzles.");
            yield return ShowDialogue("* Watch them solveçpuzzles. Congratulateçthem on solvingçpuzzles.");
            yield return ShowDialogue("* I may not be a realçperson, but I can'tçhelp but be envious.");
            yield return ShowDialogue("* When will it be myçturn to solve a puzzle?");
            interaction_dialogue = 0;
        }

        if (interaction_dialogue == 2)
        {
            yield return ShowDialogue("* Life is full ofçconfrontations.");
            yield return ShowDialogue("* You're going to meetçall sorts of people asçyou get older.");
            yield return ShowDialogue("* People like you, withçtheir own hopes andçdreams.");
            yield return ShowDialogue("* And with their ownçhurt, too.");
            yield return ShowDialogue("* Someday, you mightçfind yourself on theçother side of a battleçwith your friends.");
            yield return ShowDialogue("* But don't give up onçthem.");
            yield return ShowDialogue("* If you're strongçenough, you can makeçall the difference.");
            interaction_dialogue = 0;
        }

        if (interaction_dialogue == 3)
        {
            yield return ShowDialogue("* Well. Didn't thinkçI'd have to explainçthat.");
            yield return ShowDialogue("* You do have legs,çright?");
            yield return ShowDialogue("* You know, those fancyçpuzzle-solving noodlesçbeneath your body?");
            yield return ShowDialogue("* Well, pick açdirection and walk inçit.");
            yield return ShowDialogue("* If you didnt have itçgood enough already,çyou can even press X toçdash while moving.");
            yield return ShowDialogue("* ...");
            yield return ShowDialogue("* I'm sorry. I'm just açlittle frustrated.");
            yield return ShowDialogue("* If I had legs, Içcould solve puzzlesçtoo.");
            interaction_dialogue = 0;
        }

        if (interaction_dialogue == 4)
        {
            yield return ShowDialogue("* Maybe it's not soçbad.");
            yield return ShowDialogue("* I might not beçsolving the puzzle, butçI still contribute.");
            yield return ShowDialogue("* Without you, theçpuzzle is incomplete..");
            yield return ShowDialogue("* And without me, theçpuzzle is incomplete.");
            yield return ShowDialogue("* I might be ançinanimate object, butçin my own way, I'mçsolving puzzles too.");
            yield return ShowDialogue("* Thanks, pal.");
            interaction_dialogue = 0;
        }

        if (interaction_dialogue == 5)
        {
            yield return ShowDialogue("* Tell you what, Pal. You're not just a good puzzle solver.");
            yield return ShowDialogue("* You're a good friend, too.");
            interaction_dialogue = 0;
            ExecutePacifistEnding();
            yield break;
        }


        //We execute his only attack
        Vector3 spawn_helper = new Vector3(94, -153, 1f);
        GameObject temp = Instantiate(Signey_Attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

        


        //We show dialogue depending on the act choice


        yield return new WaitForSeconds(1.2f);
        Destroy(temp);
        battle_manager.StartPlayerTurn();
    }

    IEnumerator dialogue_coroutine(int turn)
    {
        if (GetComponent<enemy_data>().health > GetComponent<enemy_data>().max_health * 50 / 100)
        {
            int random_number = Random.Range(0, 3);
            switch (random_number)
            {
                case 0:
                    battle_manager.dialogue_box_message = "* ( This sign is unusually self aware for ançinanimate object. )";
                    break;

                case 1:
                    battle_manager.dialogue_box_message = "* ( Smells like reliable and trustworthyçinformation. )";
                    break;

                case 2:
                    battle_manager.dialogue_box_message = "* ( Someone wrote a lot of text on this sign. )";
                    break;

                default:

                    break;
            }
        }

        else if (GetComponent<enemy_data>().health <= GetComponent<enemy_data>().max_health * 50 / 100 && GetComponent<enemy_data>().health > GetComponent<enemy_data>().max_health * 25 / 100)
        {
            battle_manager.dialogue_box_message = "* ( Someone put a lot of effort into this sign. )";
        }

        else if (GetComponent<enemy_data>().health <= GetComponent<enemy_data>().max_health * 25 / 100 && GetComponent<enemy_data>().health > 0)
        {
            battle_manager.dialogue_box_message = "* ( Someone who just wanted to help other people. )";
        }

        else
        {
            battle_manager.dialogue_box_message = "* ( This sign couldn't ever help you. )";
        }
            yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteInteraction_coroutine(int number)
    {
        switch (number)
        {
            case 0:
                if (GetComponent<enemy_data>().health > 0)
                {
                    battle_manager.dialogue_box_message = "* You scan Signy for puzzle instructions.";
                    battle_manager.SetDialogueBox("* You scan Signy for puzzle instructions.");
                    if (act1_give_memory == true)
                    {
                        battle_manager.GiveMemoryPortion(this.gameObject, Color.white, Color.white);
                        act1_give_memory = false;
                    }
                    interaction_dialogue = 1;
                }
              else
                {
                    battle_manager.dialogue_box_message = "* A pile of wood.";
                    battle_manager.SetDialogueBox("* A pile of wood.");
                }

                break;

            case 1:
                battle_manager.dialogue_box_message = "* There's a paragraph here about battles.";
                battle_manager.SetDialogueBox("* There's a paragraph here about battles.");
                if (act2_give_memory == true)
                {
                    battle_manager.GiveMemoryPortion(this.gameObject, Color.white, Color.white);
                    act2_give_memory = false;
                }
                interaction_dialogue = 2;

                break;

            case 2:
                if (act3_phase == false)
                {
                    battle_manager.dialogue_box_message = "* You find some information about movement onçSigny.";
                    battle_manager.SetDialogueBox("* You find some information about movement onçSigny.");
                    GetComponent<enemy_data>().interactions[2] = "*Comfort Signy";
                    act3_phase = true;
                    interaction_dialogue = 3;
                }
                else
                {
                    battle_manager.dialogue_box_message = "* You thank Signy for being an essential part ofçthese puzzles.";
                    battle_manager.SetDialogueBox("* You thank Signy for being an essential part ofçthese puzzles.");
                    if (act3_give_memory == true)
                    {
                        battle_manager.GiveMemoryPortion(this.gameObject, Color.white, Color.white);
                        act3_give_memory = false;
                    }
                    
                    interaction_dialogue = 4;
                }
               
                break;

            default:
                break;
        }
        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteMemory_coroutine(string name)
    {
        yield return new WaitForEndOfFrame();
        Memory auxiliar;

        for (int i = 0; i < GetComponent<enemy_data>().memories.Count; i++)
        {
            if (GetComponent<enemy_data>().memories[i].name == name)
            {
                auxiliar = GetComponent<enemy_data>().memories[i];
                battle_manager.dialogue_box_message = auxiliar.message;
                battle_manager.SetDialogueBox(auxiliar.message);
                break;
            }
        }

        switch (name)
        {
            case "Puzzles with Signy":
                //battle_manager.battle_finished_pacifist = true;        
                interaction_dialogue = 5;      
                break;

            case "Destroyed Sign":
                battle_manager.battle_finished_genocide = true;
                break;

            default:
                break;
        }
    }

    public void ExecutePacifistEnding()
    {
        Overworld_Signy.transform.parent.gameObject.SetActive(true);
        Overworld_Signy.SetActive(true);
        Overworld_Signy.GetComponent<Demo_Ravine_6_Signey_Overworld_Fight>().Finish_Battle_Pacifist();
        soul.SetActive(false);
        interaction_dialogue = 5;
    }

    public void ExecuteGenocideEnding()
    {
        Overworld_Signy.transform.parent.gameObject.SetActive(true);
        Overworld_Signy.SetActive(true);
        Overworld_Signy.GetComponent<Demo_Ravine_6_Signey_Overworld_Fight>().Finish_Battle_Genocide();
        soul.SetActive(false);
        interaction_dialogue = 6;
    }
}
