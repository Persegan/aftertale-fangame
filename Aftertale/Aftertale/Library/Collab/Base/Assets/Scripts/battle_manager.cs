﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class battle_manager : MonoBehaviour {

    const int MEMORY_FAILED = 20;
    const int BEFORE_ENEMY_TURN = 7;


    public Text dialogue_box_text;
	public AudioClip dialogue_sound;
	public AudioClip menu_select;
	public AudioClip menu_choose;
	public AudioClip attack;
	public AudioClip damage_enemy_sound;
	public Button fight;
	public Button act;
	public Button item;
	public Button memory;
	public Image dialogue_box;
	public Image attack_indicator;
	public List <GameObject> enemies = new List<GameObject> ();
	public Button[] choices;
    public Image[,] memory_colors;
    public Image[] memory_portions;
    public ParticleSystem memory_particle_system;
    public GameObject memory_particle_Attractor;

    public Image[] auxiliar_memory_colors;
	public GameObject player;
	public GameObject soul;
	public string dialogue_box_message;
	public GameObject Name_UI;
	public GameObject Level_UI;
	public GameObject HPnumber_UI;
	public GameObject HPbar_UI;

	public Sprite Dialogue_box_fight;
	public Sprite Dialogue_box_sprite;
	public bool playerturn;

	public GameObject damage_animation;
	public GameObject damage_number;

	private int action;
	private int turn;
	public float damage;
	private GameObject enemy_target;

    public bool battle_finished_pacifist = false;
    public bool battle_finished_genocide = false;


    // Use this for initialization
    void Start () {
		Name_UI.GetComponent<Text>().text = player_data.playerdata.player_name;
		Level_UI.GetComponent<Text> ().text = "LV ";
		Level_UI.GetComponent<Text> ().text += player_data.playerdata.level.ToString();
		Text Temporal = HPnumber_UI.GetComponent<Text> ();
		Temporal.text = player_data.playerdata.health.ToString ();
		Temporal.text += "/";
		Temporal.text += player_data.playerdata.maxhealth.ToString ();
		HPbar_UI.GetComponent<Slider> ().maxValue = player_data.playerdata.maxhealth;
		HPbar_UI.GetComponent<Slider> ().value = player_data.playerdata.health;

        memory_colors = new Image[4, 3];

        memory_colors[0,0] = auxiliar_memory_colors[0];
        memory_colors[0,1] = auxiliar_memory_colors[1];
        memory_colors[0,2] = auxiliar_memory_colors[2];

        memory_colors[1,0] = auxiliar_memory_colors[3];
        memory_colors[1,1] = auxiliar_memory_colors[4];
        memory_colors[1,2] = auxiliar_memory_colors[5];

        memory_colors[2,0] = auxiliar_memory_colors[6];
        memory_colors[2,1] = auxiliar_memory_colors[7];
        memory_colors[2,2] = auxiliar_memory_colors[8];

        memory_colors[3,0] = auxiliar_memory_colors[9];
        memory_colors[3,1] = auxiliar_memory_colors[10];
        memory_colors[3,2] = auxiliar_memory_colors[11];
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Z)) {
            if (action == MEMORY_FAILED && AutoTextScript.isinteracting == false)
            {
                dialogue_box_text.text = "";
                action = -1;
                StartPlayerTurn();
            }
			if (action == 7 && AutoTextScript.isinteracting == false) {
				dialogue_box_text.text = "";
				action = -1;
				StartEnemyTurn ();
			}
		}
			if (Input.GetKeyDown (KeyCode.X)) {


			if (action == 7 || action == MEMORY_FAILED) {
				AutoTextScript.isinteracting = false;
				dialogue_box_text.StopAllCoroutines ();
				dialogue_box_text.text = dialogue_box_message;
			}
		}

	}	

	public void SetSoulPosition()
	{
		GameObject myEventSystem = GameObject.Find ("EventSystem");
		GameObject currentbutton = myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem> ().currentSelectedGameObject;
		soul.transform.position = new Vector3 (currentbutton.transform.position.x - 0.19f, currentbutton.transform.position.y, soul.transform.position.z);
	}

	public void RestoreButtons()
	{
		fight.GetComponent<Image> ().sprite = fight.GetComponent<Button> ().spriteState.disabledSprite;
		act.GetComponent<Image> ().sprite = act.GetComponent<Button> ().spriteState.disabledSprite;
		item.GetComponent<Image> ().sprite = item.GetComponent<Button> ().spriteState.disabledSprite;
		memory.GetComponent<Image> ().sprite = memory.GetComponent<Button> ().spriteState.disabledSprite;
	}

	public void SetDialogueBox(string message)
	{
        AutoTextScript.TypeText(dialogue_box_text, message, 0.03f, GameObject.Find("Generic_Text_Noise").GetComponent<AudioSource>());
	}

	public void Play_Menu_Choose()
	{
        GetComponent<AudioSource>().clip = menu_choose;
        GetComponent<AudioSource>().Play ();
	}

	public void Player_Menu_Select()
	{
        GetComponent<AudioSource>().clip = menu_select;
        GetComponent<AudioSource>().Play();
    }

	public void Submit_Fight()
	{
		//fight.interactable = false;
		fight.GetComponent<Image> ().sprite = fight.GetComponent<Button> ().spriteState.highlightedSprite;
		dialogue_box_text.StopAllCoroutines ();
		dialogue_box_text.text = "";

		action = 1;

		if (enemies.Count <= 4) {
			for (int i = 0; i < enemies.Count; i++) {
				choices [i].gameObject.SetActive(true);
				choices[i].gameObject.GetComponentInChildren<Text>().text = enemies [i].GetComponent<enemy_data> ().name;
			}
		} else {
			for (int i = 0; i < 4; i++) {
				choices [i].gameObject.SetActive(true);
				enemy_data temporal = enemies [i].GetComponent (typeof(enemy_data)) as enemy_data;
				choices [i].gameObject.GetComponentInChildren<Text> ().text = temporal.enemy_name;
			}
			choices [5].gameObject.GetComponentInChildren<Text> ().text = "Next Page";

		}

		fight.Select ();
		choices [0].Select ();

	}

	public void Submit_Act()
	{
		act.GetComponent<Image> ().sprite = act.GetComponent<Button> ().spriteState.highlightedSprite;
		dialogue_box_text.StopAllCoroutines ();
		//act.interactable = false;
		dialogue_box_text.text = "";
		action = 2;

		for (int i = 0; i < enemies.Count; i++) {
			choices [i].gameObject.SetActive(true);
			choices[i].gameObject.GetComponentInChildren<Text>().text = enemies [i].GetComponent<enemy_data> ().name;
		}

		choices [0].Select ();

	}

	public void Submit_Item()
	{
		item.GetComponent<Image> ().sprite = item.GetComponent<Button> ().spriteState.highlightedSprite;
		dialogue_box_text.StopAllCoroutines ();
		//item.interactable = false;
		dialogue_box_text.text = "";
		if (player_data.playerdata.items.Count <= 4) {
			for (int i = 0; i < player_data.playerdata.items.Count; i++) {
				choices [i].gameObject.SetActive (true);
				choices [i].gameObject.GetComponentInChildren<Text> ().text = player_data.playerdata.items[i];
			}
		} else {
			for (int i = 0; i < 4; i++) {
				choices [i].gameObject.SetActive (true);
				choices [i].gameObject.GetComponent<Text> ().text = player_data.playerdata.items [i];
			}
			choices [5].gameObject.SetActive (true);
			choices [5].gameObject.GetComponent<Text> ().text = "PAGE 1";
			choices [6].gameObject.SetActive (true);
		}
		choices [0].Select ();
		action = 4;
	}

	public void ChangeItemPage()
	{
		for (int i = 0; i < 4; i++) {
			choices [i].gameObject.SetActive (false);
		}
		if (choices [6].gameObject.GetComponent<Text> ().text == "PAGE 1") {
			int j = 0;
			for (int i = 4; i < player_data.playerdata.items.Count; i++) {
				choices [j].gameObject.SetActive (true);
				choices [j].GetComponent<Text> ().text = player_data.playerdata.items [i];
				j++;
			}
			choices [0].Select ();
			choices [6].GetComponent<Text> ().text = "PAGE 2";
		} else if (choices [6].gameObject.GetComponent<Text> ().text == "PAGE 2") {
			int j = 0;
			for (int i = 0; i < 4; i++) {
				choices [j].gameObject.SetActive (true);
				choices [j].GetComponent<Text> ().text = player_data.playerdata.items [i];
				j++;
			}
			choices [0].Select ();
			choices [6].GetComponent<Text> ().text = "PAGE 1";
		}
	}

	public void Submit_Memory()
	{
        {
            memory.GetComponent<Image>().sprite = memory.GetComponent<Button>().spriteState.highlightedSprite;
            dialogue_box_text.StopAllCoroutines();
            dialogue_box_text.text = "";
            action = 9;
            for (int i = 0; i < enemies.Count; i++)
            {
                choices[i].gameObject.SetActive(true);
                choices[i].gameObject.GetComponentInChildren<Text>().text = enemies[i].GetComponent<enemy_data>().name;
            }

            choices[0].Select();
        }
     

    }

	public void Submit_Choice_general(int number)
	{
		switch (action) {
		case 1:
			for (int i = 0; i < choices.Length; i++) {
				choices [i].gameObject.SetActive (false);
			}
			soul.SetActive (false);
			RestoreButtons ();
			dialogue_box.gameObject.GetComponent<Image> ().sprite = Dialogue_box_fight;
			GameObject temporal = Instantiate (attack_indicator.gameObject) as GameObject;
			temporal.transform.SetParent (gameObject.transform, false);
			temporal.transform.localPosition = (new Vector3 (-520f, -115f, 0));
			temporal.GetComponent<attack_indicator> ().battle_manager = gameObject;
			temporal.GetComponent<attack_indicator> ().enemy = enemies [number].gameObject;
            soul.GetComponent<soul_movement>().fight_chosen = true;
			break;
		case 2:
			for (int i = 0; i < enemies [number].GetComponent<enemy_data> ().interactions.Count; i++) {
				choices [i].gameObject.SetActive (true);
				choices [i].gameObject.GetComponentInChildren<Text> ().text = enemies [number].GetComponent<enemy_data> ().interactions [i];
			}
			action = 3;
			enemy_target = enemies [number];
			break;
		case 3:
			for (int i = 0; i < choices.Length; i++) {
				choices [i].gameObject.SetActive (false);
			}
			soul.SetActive (false);
			RestoreButtons ();
			enemy_target.GetComponent<enemy_data> ().ExecuteInteraction_abstract (number);
			AutoTextScript.isinteracting = true;
			action = 7;
			//enemy_target = null;
            soul.GetComponent<soul_movement>().act_chosen = true;
			break;
		case 4:
			for (int i = 0; i < choices.Length; i++) {
				choices [i].gameObject.SetActive (false);
			}
			AutoTextScript.isinteracting = true;
			action = 7;
			soul.SetActive (false);
			RestoreButtons ();
			player_data.playerdata.UseItem (player_data.playerdata.items [number]);
            soul.GetComponent<soul_movement>().item_chosen = true;
            break;
        case 9:
            for (int i = 0; i < enemies[number].GetComponent<enemy_data>().memories.Count; i++)
            {
                choices[i].gameObject.SetActive(true);
                choices[i].gameObject.GetComponentInChildren<Text>().text = enemies[number].GetComponent<enemy_data>().memories[i].name;

                for (int j = 0; j < 3; j++)
                {
                   memory_colors[i, 0].gameObject.transform.parent.gameObject.SetActive(true);
                   memory_colors[i,j].color = enemies[number].GetComponent<enemy_data>().memories[i].colors[j];
                }
            }

            action = 12;
            enemy_target = enemies[number];
            break;

        case 12:

            float r = memory_portions[0].color.r + memory_portions[1].color.r + memory_portions[2].color.r;
            float g = memory_portions[0].color.g + memory_portions[1].color.g + memory_portions[2].color.g;
            float b = memory_portions[0].color.b + memory_portions[1].color.b + memory_portions[2].color.b;

            float r2 = enemy_target.GetComponent<enemy_data>().memories[number].colors[0].r + enemy_target.GetComponent<enemy_data>().memories[number].colors[1].r + enemy_target.GetComponent<enemy_data>().memories[number].colors[2].r;
            float g2 = enemy_target.GetComponent<enemy_data>().memories[number].colors[0].g + enemy_target.GetComponent<enemy_data>().memories[number].colors[1].g + enemy_target.GetComponent<enemy_data>().memories[number].colors[2].g;
            float b2 = enemy_target.GetComponent<enemy_data>().memories[number].colors[0].b + enemy_target.GetComponent<enemy_data>().memories[number].colors[1].b + enemy_target.GetComponent<enemy_data>().memories[number].colors[2].b;


            if (Mathf.Abs(r-r2) < 0.1 && Mathf.Abs(g - g2) < 0.1 && Mathf.Abs(b - b2) < 0.1)
            {
                for (int i = 0; i < choices.Length; i++)
                {
                    choices[i].gameObject.SetActive(false);
                }
                soul.SetActive(false);
                RestoreButtons();
                enemy_target.GetComponent<enemy_data>().ExecuteMemory_abstract(enemy_target.GetComponent<enemy_data>().memories[number].name);
                AutoTextScript.isinteracting = true;
                action = 7;
                for (int i = 0; i < 4; i++)
                {
                    memory_colors[i, 0].gameObject.transform.parent.gameObject.SetActive(false);

                }
                soul.GetComponent<soul_movement>().memory_chosen = true;
               // enemy_target = null;

                Color default_color = new Color(109 / 255f, 106 / 255f, 134 / 255f);
                memory_portions[0].color = default_color;
                memory_portions[1].color = default_color;
                memory_portions[2].color = default_color;
                break;

            }
            else
            {
                for (int i = 0; i < choices.Length; i++)
                {
                    choices[i].gameObject.SetActive(false);
                }
                 soul.SetActive(false);
                RestoreButtons();
                AutoTextScript.isinteracting = true;
                action = MEMORY_FAILED;
                soul.GetComponent<soul_movement>().memory_chosen = true;
               // enemy_target = null;
                dialogue_box_message = "You try to remember, but fail to do so.";
                SetDialogueBox("You try to remember, but fail to do so.");
                for (int i = 0; i < 4; i++)
                {
                    memory_colors[i, 0].gameObject.transform.parent.gameObject.SetActive(false);

                }
                break;
            }
          

            default:
			break;
		}
	}
	public void Submit_Choice_1()
	{
		Submit_Choice_general (0);
	}
		
	public void Submit_Choice_2()
	{
		Submit_Choice_general (1);
	}
	public void Submit_Choice_3()
	{
		Submit_Choice_general (2);
	}
	public void Submit_Choice_4()
	{
		Submit_Choice_general (3);
	}

	public void Cancel_Choice_General()
	{
		for (int i = 0; i < 5; i++) {
			choices [i].gameObject.SetActive(false);
		}
        for (int i = 0; i < 4; i++)
        {
            memory_colors[i, 0].gameObject.transform.parent.gameObject.SetActive(false);
            
        }
		if (action == 1) {
			action = 0;
			RestoreButtons ();
			//fight.interactable = true;
			fight.Select ();
			SetDialogueBox (dialogue_box_message);
		} else if (action == 2) {
			action = 0;
			RestoreButtons ();
			//act.interactable = true;
			act.Select ();
			SetDialogueBox (dialogue_box_message);
		} else if (action == 8|| action == 3) {
			//act.interactable = false;
			dialogue_box_text.text = "";

			for (int i = 0; i < enemies.Count; i++) {
				choices [i].gameObject.SetActive(true);
				choices[i].gameObject.GetComponentInChildren<Text>().text = enemies [i].GetComponent<enemy_data> ().name;
			}
			action = 2;

			choices [0].Select ();
			
		} else if (action == 5 || action == 4) {
			action = 0;
			RestoreButtons ();
			//item.interactable = true;
			item.Select ();
			SetDialogueBox (dialogue_box_message);
		}
        else if (action == 9 || action == 10)
        {
            action = 0;
            RestoreButtons();
            memory.Select();
            SetDialogueBox(dialogue_box_message);
        }
        else if (action == 12 ||action == 11)
        {
            dialogue_box_text.text = "";

            for (int i = 0; i < enemies.Count; i++)
            {
                choices[i].gameObject.SetActive(true);
                choices[i].gameObject.GetComponentInChildren<Text>().text = enemies[i].GetComponent<enemy_data>().name;
            }
            if (action == 12)
            {
                action = 9;
            }
            if (action == 11)
            {
                action = 10;
            }
            choices[0].Select();
        }
	}

	public void damage_enemy(GameObject enemy)
	{
		StartCoroutine (damage_enemy_coroutine (enemy));
	}

	IEnumerator damage_enemy_coroutine(GameObject enemy)
	{
		GameObject temporal = Instantiate (damage_animation.gameObject, enemy.transform.position, enemy.transform.rotation) as GameObject;
        GetComponent<AudioSource>().clip = attack;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds (0.35f);
		enemy.GetComponent<Animator> ().SetInteger ("state", 1); //We execute the enemy being damaged animation;
        GetComponent<AudioSource>().clip = damage_enemy_sound;
        GetComponent<AudioSource>().Play();
        enemy.GetComponent<enemy_data> ().health_bar.gameObject.SetActive (true);
		temporal = Instantiate (damage_number.gameObject, enemy.transform.position, enemy.transform.rotation) as GameObject;
		temporal.transform.SetParent (gameObject.transform, false);
        //Debug.Log(damage);
        damage = Mathf.Floor((player_data.playerdata.attack - enemy.GetComponent<enemy_data>().defense) * damage);
        if (damage < 0)
        {
            damage = 0;
        }
		temporal.GetComponent<Text> ().text = damage.ToString();
        temporal.transform.GetChild(0).GetComponent<Text>().text = damage.ToString();
        float i = enemy.GetComponent<enemy_data>().health;
        float j = enemy.GetComponent<enemy_data>().health - damage;
        if (j < 0.0f )
        {
            j = 0.0f;
        }
        float progress = Time.deltaTime * 2;
        while (enemy.GetComponent<enemy_data>().health_bar.value > j) {
            progress += Time.deltaTime * 2;
            enemy.GetComponent<enemy_data>().health_bar.value = Mathf.Lerp(i, j, progress);
			yield return new WaitForSeconds (0.003f);
		}
		yield return new WaitForSeconds (1.5f);
		Destroy (GameObject.Find ("Attack_Indicator(Clone)"));
		Destroy (temporal);
		enemy.GetComponent<enemy_data> ().health = enemy.GetComponent<enemy_data> ().health_bar.value;
		enemy.GetComponent<enemy_data> ().health_bar.gameObject.SetActive (false);
		dialogue_box.gameObject.GetComponent<Image> ().sprite = Dialogue_box_sprite;
		damage = 0;
		soul.SetActive (true);
        enemy.GetComponent<Animator>().SetInteger("state", 0);
        StartEnemyTurn ();
	}

	public void miss_enemy(GameObject enemy)
	{
		StartCoroutine (miss_enemy_coroutine (enemy));
	}

	IEnumerator miss_enemy_coroutine(GameObject enemy)
	{
        yield return new WaitForSeconds(1.0f);
		GameObject temporal = Instantiate (damage_number.gameObject, enemy.transform.position, enemy.transform.rotation) as GameObject;
		temporal.GetComponent<Text> ().text = "0";
        temporal.transform.GetChild(0).GetComponent<Text>().text = damage.ToString();
        temporal.transform.SetParent (gameObject.transform, false);
		yield return new WaitForSeconds (1.5f);
		Destroy (temporal);
		dialogue_box.gameObject.GetComponent<Image> ().sprite = Dialogue_box_sprite;
		StartEnemyTurn ();
	}


	public void StartEnemyTurn()
	{
        if (battle_finished_pacifist == true)
        {
            enemy_target.GetComponent<enemy_data>().ExecutePacifistEnding_abstract();
        }

        else if (battle_finished_genocide == true)
        {
            enemy_target.GetComponent<enemy_data>().ExecuteGenocideEnding_abstract();
        }


        else
        {
            RestoreButtons();
            BoxCollider2D[] boxlist;
            boxlist = dialogue_box.gameObject.GetComponents<BoxCollider2D>();
            foreach (BoxCollider2D box in boxlist)
            {
                box.enabled = true;
            }
            //fight.interactable = true;
            //memory.interactable = true;
            //item.interactable = true;
            //act.interactable = true;	
            dialogue_box_text.text = "";
            StartCoroutine(resizebox(dialogue_box, dialogue_box.GetComponent<RectTransform>().sizeDelta.x, dialogue_box.GetComponent<RectTransform>().sizeDelta.y, 300, 207));
            playerturn = false;
            soul.SetActive(true);
            soul.GetComponent<BoxCollider2D>().enabled = true;
            soul.GetComponent<soul_movement>().can_move = true;
            soul.GetComponent<soul_movement>().can_dash = true;
            soul.transform.position = new Vector3(dialogue_box.transform.position.x, dialogue_box.transform.position.y, soul.transform.position.z);
            if (enemies.Count == 1)
            {
                enemies[0].GetComponent<enemy_data>().attack_abstract(turn);
            }
        }	
	}

	public void StartPlayerTurn()
	{
		StartCoroutine (StartPlayerTurn_coroutine ());

	}

    public void GiveMemoryPortion(GameObject target, Color color, Color particlecolor)
    {
        StartCoroutine(GiveMemoryPortion_Coroutine(target, color, particlecolor));
    }

    public void ResetMemoryPortions()
    {
        Color default_color = new Color(109 / 255f, 106 / 255f, 134 / 255f);
        memory_portions[0].color = default_color;
        memory_portions[1].color = default_color;
        memory_portions[2].color = default_color;
    }

	IEnumerator StartPlayerTurn_coroutine()
	{
		RestoreButtons ();
        soul.SetActive(true);
		BoxCollider2D[] boxlist;
		boxlist = dialogue_box.gameObject.GetComponents<BoxCollider2D> ();
		foreach (BoxCollider2D box in boxlist) {
			box.enabled = false;
		}
		enemies [Random.Range (0, enemies.Count-1)].GetComponent<enemy_data> ().dialogue_abstract(turn);
        soul.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        soul.GetComponent<soul_movement>().StopAllCoroutines();
        soul.GetComponent<soul_movement> ().can_move = false;
        soul.GetComponent<soul_movement>().can_dash = false;
        soul.GetComponent<soul_movement>().invulnerable = false;
        soul.GetComponent<BoxCollider2D> ().enabled = false;
		soul.GetComponent<Rigidbody2D>().velocity = new Vector3 (0, 0, 0);
		Sprite temporal = soul.GetComponent<SpriteRenderer> ().sprite;
		soul.GetComponent<SpriteRenderer> ().sprite = null;
		turn++;
		playerturn = true;
		StartCoroutine(resizebox (dialogue_box, dialogue_box.GetComponent<RectTransform>().sizeDelta.x, dialogue_box.GetComponent<RectTransform>().sizeDelta.y, 1057, 207));
		yield return new WaitForSeconds (0.5f);
		action = 0;
		SetDialogueBox (dialogue_box_message);
		soul.GetComponent<SpriteRenderer> ().sprite = temporal;
		//fight.interactable = true;
        if (soul.GetComponent<soul_movement>().fight_chosen == true)
        {
            fight.Select();
            soul.GetComponent<soul_movement>().fight_chosen = false;
        }
        else if (soul.GetComponent<soul_movement>().act_chosen == true)
        {
            act.Select();
            soul.GetComponent<soul_movement>().act_chosen = false;
        }
        else if (soul.GetComponent<soul_movement>().item_chosen == true)
        {
            item.Select();
            soul.GetComponent<soul_movement>().item_chosen = false;
        }
        else if (soul.GetComponent<soul_movement>().memory_chosen == true)
        {
            act.Select();
            soul.GetComponent<soul_movement>().memory_chosen = false;
        }
        else
        {
            fight.Select();
        }
    }

	IEnumerator resizebox(Image _transform, float initialsizex, float initialsizey, float endsizex, float endsizey)
	{
		if (initialsizex > endsizex) {
			
			for (float i = initialsizex; i >= endsizex; i = i - 20f) {
				_transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (i, _transform.GetComponent<RectTransform>().sizeDelta.y);
				yield return new WaitForSeconds (0.01f);

			}
		} else if (initialsizex < endsizex) {
			for (float i = initialsizex; i <= endsizex; i = i + 20f) {
				_transform.GetComponent<RectTransform>().sizeDelta= new Vector2 (i, _transform.GetComponent<RectTransform>().sizeDelta.y);
				yield return new WaitForSeconds (0.01f);

			}
		}
		if (initialsizey > endsizey) {
			for (float i = initialsizey; i >= endsizey; i = i - 20f) {
				_transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (_transform.GetComponent<RectTransform>().sizeDelta.x,i);
				yield return new WaitForSeconds (0.01f);

			}

		}
		else if (initialsizey < endsizey) {
			for (float i = initialsizey; i <= endsizey; i = i + 20f) {
				_transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (_transform.GetComponent<RectTransform>().sizeDelta.x,i);
				yield return new WaitForSeconds (0.01f);
		
			}

		}
	}

    IEnumerator GiveMemoryPortion_Coroutine(GameObject target, Color color, Color particlecolor)
    {
        float speed = 1.0f; 
        Color default_color = new Color(109 / 255f, 106 / 255f, 134 / 255f);
        Color temporal_color;

        memory_particle_system.gameObject.transform.position = target.transform.position;

        var main = memory_particle_system.main;
        main.startColor = particlecolor;

        if (memory_portions[0].color == default_color)
        {
            memory_particle_Attractor.transform.position = memory_portions[0].gameObject.transform.position;
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_particle_system.Play();
            yield return new WaitForSeconds(1.5f);
            memory_particle_system.Stop();
            memory_particle_Attractor.gameObject.SetActive(true);

            float t = 0;
            temporal_color = memory_portions[0].color;
            while (memory_portions[0].color != color)
            {
                memory_portions[0].color = Color.Lerp(temporal_color, color, t);
                t += Time.deltaTime * speed;
                yield return new WaitForEndOfFrame();
            }
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_portions[0].color = color;
        }

        else if (memory_portions[1].color == default_color)
        {

            memory_particle_Attractor.transform.position = memory_portions[1].gameObject.transform.position;
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_particle_system.Play();
            yield return new WaitForSeconds(1.5f);
            memory_particle_system.Stop();
            memory_particle_Attractor.gameObject.SetActive(true);

            temporal_color = memory_portions[1].color;
            float t = 0;
            while (memory_portions[1].color != color)
            {
                memory_portions[1].color = Color.Lerp(temporal_color, color, t);
                t += Time.deltaTime * speed;
                yield return new WaitForEndOfFrame();
            }
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_portions[1].color = color;
        }

        else if (memory_portions[2].color == default_color)
        {
            memory_particle_Attractor.transform.position = memory_portions[2].gameObject.transform.position;
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_particle_system.Play();
            yield return new WaitForSeconds(1.5f);
            memory_particle_system.Stop();
            memory_particle_Attractor.gameObject.SetActive(true);

            float t = 0;
            temporal_color = memory_portions[2].color;
            while (memory_portions[2].color != color)
            {
                memory_portions[2].color = Color.Lerp(temporal_color, color, t);
                t += Time.deltaTime * speed;
                yield return new WaitForEndOfFrame();
            }
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_portions[2].color = color;
        }
    
        else
        {
            memory_particle_Attractor.transform.position = memory_portions[0].gameObject.transform.position;
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_particle_system.Play();
            yield return new WaitForSeconds(1.5f);
            memory_particle_system.Stop();
            memory_particle_Attractor.gameObject.SetActive(true);

            float t = 0;
            temporal_color = memory_portions[0].color;
            while (memory_portions[0].color != color)
            {
                memory_portions[0].color = Color.Lerp(temporal_color, color, t);
                t += Time.deltaTime * speed;
                yield return new WaitForEndOfFrame();
            }
            memory_particle_Attractor.gameObject.SetActive(false);
            memory_portions[0].color = color;
            memory_portions[1].color = default_color;
            memory_portions[2].color = default_color;
        }


    }
}


