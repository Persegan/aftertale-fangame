﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Demo_Ravine_7_Snowlder_Enemy : MonoBehaviour {



    public battle_manager battle_manager;
    public GameObject speech_bubble;
    private bool finishedspeaking;
    private bool isspeaking;
    private string speakingmessage;
    public Text speech_bubble_text;

    public AudioClip speech_sound;


    //Stuff for the attack patterns
    public GameObject snowflake_attack;
    public GameObject snowball_attack;
    public GameObject iceshard_attack;
    public GameObject wind_attack;
    public GameObject wiretrap_attack;

    public GameObject soul;


    //Stuff for building the snowman
    private int head = 0;
    private int body = 0;
    private int arms = 0;
    private bool head1 = false;
    private bool head2 = false;
    private bool head3 = false;
    private bool body1 = false;
    private bool body2 = false;
    private bool body3 = false;
    private bool arms1 = false;
    private bool arms2 = false;
    private bool arms3 = false;

    public GameObject Snowlder_Arm_Left;
    public GameObject Snowlder_Arm_Right;
    public GameObject Snowlder_Body;
    public GameObject Snowlder_Head;
    public GameObject Snowlder_Snowball;

    public Sprite head0_sprite;
    public Sprite head1_sprite;
    public Sprite head2_sprite;
    public Sprite head3_sprite;

    public Sprite body0_sprite;
    public Sprite body1_sprite;
    public Sprite body2_sprite;
    public Sprite body3_sprite;

    public Sprite arms1_sprite_left;
    public Sprite arms1_sprite_right;
    public Sprite arms2_sprite_left;
    public Sprite arms2_sprite_right;
    public Sprite arms3_sprite_left;
    public Sprite arms3_sprite_right;

    private bool snowperson_complete = false;
    private bool weirdhead_dialogue = false;
    private bool temmiehead_musclebody = false;



    // Use this for initialization
    void Start()
    {
        battle_manager = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<battle_manager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (finishedspeaking == true)
            {
                finishedspeaking = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (isspeaking == true)
            {
                finishedspeaking = true;
                AutoTextScript.isfinished = true;
                speech_bubble_text.StopAllCoroutines();
                speakingmessage = speakingmessage.Replace("ç", "\n");
                speech_bubble_text.text = speakingmessage;
            }

        }

    }



    ///Auxiliar methods
    IEnumerator ShowDialogue(string message)
    {
        speakingmessage = message;
        isspeaking = true;
        speech_bubble.SetActive(true);
        speech_bubble_text.gameObject.SetActive(true);
        speech_bubble_text.text = "";
        AutoTextScript.TypeText(speech_bubble_text, message, 0.03f, null);
        while (AutoTextScript.isfinished == false)
        {
            //speech_sound.pitch = Random.Range (0.3f, 1.9f);
            GetComponent<AudioSource>().clip = speech_sound;
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.1f);
        }
        finishedspeaking = true;
        while (finishedspeaking == true)
        {
            yield return new WaitForSeconds(0.1f);
        }
        //yield return new WaitForSeconds (2.0f);
        speech_bubble.SetActive(false);
        speech_bubble_text.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.0f);
    }

    public void attack(int turn)
    {
        StartCoroutine(attack_coroutine(turn));
    }

    public void dialogue(int turn)
    {
        StartCoroutine(dialogue_coroutine(turn));
    }

    public void ExecuteInteraction(string name)
    {
        StartCoroutine(ExecuteInteraction_coroutine(name));
    }

    public void ExecuteMemory(string name)
    {
        StartCoroutine(ExecuteMemory_coroutine(name));
    }



    IEnumerator attack_coroutine(int turn)
    {
        if (snowperson_complete == true)
        {
            if (head == 1)
            {
                yield return (ShowDialogue("MY SINCERE APOLOGIES, HUMAN."));
                yield return (ShowDialogue("I'VE MADE MYSELF QUITE AN INCONVENIENCE."));
                snowperson_complete = false;
            }
            else if (head == 2)
            {
                yield return (ShowDialogue("!!!wher tEMMIE mannrs??"));
                yield return (ShowDialogue("tEMMIE go nOW!!!"));
                snowperson_complete = false;
            }
            else if (head == 3)
            {
                yield return (ShowDialogue("... Hmm..."));
                yield return (ShowDialogue(".... K."));
                snowperson_complete = false;
            }
        }

        if (weirdhead_dialogue == true)
        {
            yield return (ShowDialogue("... Hey."));
            yield return (ShowDialogue("... I'm not bothering you, am I?"));
            yield return (ShowDialogue("... So what's the deal with us, anyways?."));
            weirdhead_dialogue = false;
        }

        if (temmiehead_musclebody == true)
        {
            yield return (ShowDialogue("!!!nNO!MUSCLES r NOt CUTE!!"));
            temmiehead_musclebody = false;
        }

       // StartCoroutine(attack_wiretrap(false, new Vector3(-71, -107, 0)));
        yield return new WaitForSeconds(2.0f);
        battle_manager.StartPlayerTurn();        
    }

    IEnumerator dialogue_coroutine(int turn)
    {
        int temporal = Random.Range(1, 4);
        switch (temporal)
        {
            case 1:
                battle_manager.dialogue_box_message = "* ( It's a Snowlder. That is to say, a boulder made out of snow. )";
                break;
            case 2:
                battle_manager.dialogue_box_message = "* Snowlder blocks the way!";
                break;
            case 3:
                battle_manager.dialogue_box_message = "* Snowlder is totally ignoring you!";
                break;

        }

        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteInteraction_coroutine(string name)
    {
        for (int i = 0; i < GetComponent<enemy_data>().interactions.Count; i++)
        {
            if (name == GetComponent<enemy_data>().interactions[i].name)
            {
                GetComponent<enemy_data>().interactions[i].unread = false;
                battle_manager.dialogue_box_message = GetComponent<enemy_data>().interactions[i].message;
                battle_manager.SetDialogueBox(GetComponent<enemy_data>().interactions[i].message);
            }
        }
        switch (name)
        {
            //First we account for the original options in phase 1
            case "> Ask Snowlder to move":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Ask Snowlder to move", "* You impatiently ask the Snowlder to move. But it refused.",false); 
                break;

            case "> Tell the Snowlder a joke":

                break;


            case "> Play in the snow":

                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Roll a Snowball", "* ( Snow is no problem, you decide to just roll with it. )", true);

                break;

            case "> Roll a Snowball":
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Build a Snowperson", "* ( Inspiration strikes! This Snowlder is begging to be sculpted into a Snowperson. )", true);
                Snowlder_Snowball.SetActive(true);
                break;

            
            //This option procs phase 2
            case "> Build a Snowperson":

                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You can sense a beautiful body hiding inside this lump of snow. )", true);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", "* ( Without a proper face, there is no Snowperson. )", true);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", "* ( No Snowperson could be complete without a set of friendly arms. )", true);
                Snowlder_Snowball.SetActive(false);
                Snowlder_Head.SetActive(true);


                break;



            #region Options for phase 2

            case "> Sculpt the Body":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Muscular Physique", "* You're feeling generous, and chisel out some sweet abs for the Snowperson.", (body1 == false) ? true:false);   
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Tiny and Cute", "* Small. Adorable. The body of an intellectual.", (body2 == false) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Keep it Traditional", "* You decide to go the extra mile and carve some buttons.", (body3 == false) ? true : false);
                break;

            case "> Work on the head":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> A Friendly Face", "* You carve out Gaster Dog's face from memory. Not half bad!", (head1 == false) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> A Heroic Visage", "* You vaguely recall an epic mural, and model the face after the hero.", (head2 == false) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Keep it Plain", "* You try for a traditional Snowperson face, but... uh...", (head3 == false) ? true : false);
                break;

            case "> Give it some arms":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Strong Arms", "* Strong arms. Powerful arms. Friendly arms.", (arms1 == false) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Sexy Arms", "* Because this Snowperson deserves to feel fabulous!", (arms2 == false) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Some Sticks Will Do", "* Wood is, afterall, a trustworthy and reliable source of arms.", (arms3 == false) ? true : false);
                break;
            #endregion





            #region Options for when you sculpt the body

            case "> Muscular Physique":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You decide to take another pass at the body. )", false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", (head == 0) ? "* ( Without a proper face, there is no Snowperson. )" : "*(Perhaps a different face is in order. )", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", (arms == 0) ? "* ( No Snowperson could be complete without a set of friendly arms. )" : "* ( You decide to settle on a different set of arms. )" , (arms == 0) ? true : false);
                body = 1;
                body1 = true;

                if (head == 2)
                {
                    temmiehead_musclebody = true;
                }

                Snowlder_Body.GetComponent<Image>().sprite = body1_sprite;
                Snowlder_Body.GetComponent<Image>().SetNativeSize();
                break;

            case "> Tiny and Cute":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You decide to take another pass at the body. )", false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", (head == 0) ? "* ( Without a proper face, there is no Snowperson. )" : "*(Perhaps a different face is in order. )", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", (arms == 0) ? "* ( No Snowperson could be complete without a set of friendly arms. )" : "* ( You decide to settle on a different set of arms. )", (arms == 0) ? true : false);
                body = 2;
                body2 = true;
                Snowlder_Body.GetComponent<Image>().sprite = body2_sprite;
                Snowlder_Body.GetComponent<Image>().SetNativeSize();
                break;

            case "> Keep it Traditional":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You decide to take another pass at the body. )", false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", (head == 0) ? "* ( Without a proper face, there is no Snowperson. )" : "*(Perhaps a different face is in order. )", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", (arms == 0) ? "* ( No Snowperson could be complete without a set of friendly arms. )" : "* ( You decide to settle on a different set of arms. )", (arms == 0) ? true : false);
                body = 3;
                body3 = true;
                Snowlder_Body.GetComponent<Image>().sprite = body3_sprite;
                Snowlder_Body.GetComponent<Image>().SetNativeSize();
                break;
            #endregion



            #region Options for when you work on the head
            case "> A Friendly Face":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding inside this lump of snow. )" : "* ( You decide to take another pass at the body. )" , (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", "* ( Perhaps a different face is in order. )", false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", (arms == 0) ? "* ( No Snowperson could be complete without a set of friendly arms. )" : "* ( You decide to settle on a different set of arms. )", (arms == 0) ? true : false);
                head = 1;
                head1 = true;
                Snowlder_Head.GetComponent<Image>().sprite = head1_sprite;
                Snowlder_Head.GetComponent<Image>().SetNativeSize();
                break;

            case "> A Heroic Visage":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding inside this lump of snow. )" : "* ( You decide to take another pass at the body. )", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", "* ( Perhaps a different face is in order. )", false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", (arms == 0) ? "* ( No Snowperson could be complete without a set of friendly arms. )" : "* ( You decide to settle on a different set of arms. )", (arms == 0) ? true : false);
                head = 2;
                head2 = true;
                if (body == 1)
                {
                    temmiehead_musclebody = true;
                }
                Snowlder_Head.GetComponent<Image>().sprite = head2_sprite;
                Snowlder_Head.GetComponent<Image>().SetNativeSize();
                break;

            case "> Keep it Plain":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding inside this lump of snow. )" : "* ( You decide to take another pass at the body. )", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", "* ( Perhaps a different face is in order. )", false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", (arms == 0) ? "* ( No Snowperson could be complete without a set of friendly arms. )" : "* ( You decide to settle on a different set of arms. )", (arms == 0) ? true : false);
                head = 3;
                head3 = true;
                weirdhead_dialogue = true;
                Snowlder_Head.GetComponent<Image>().sprite = head3_sprite;
                Snowlder_Head.GetComponent<Image>().SetNativeSize();
                break;
            #endregion


            #region Options for when you give it some arms
            case "> Strong Arms":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding inside this lump of snow. )" : "* ( You decide to take another pass at the body. )", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", (head == 0) ? "* ( Without a proper face, there is no Snowperson. )" : "*(Perhaps a different face is in order. )", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", "* ( You decide to settle on a different set of arms. )", false);
                arms = 1;
                arms1 = true;
                Snowlder_Arm_Left.SetActive(true);
                Snowlder_Arm_Left.GetComponent<Image>().sprite = arms1_sprite_left;
                Snowlder_Arm_Left.GetComponent<Image>().SetNativeSize();
                Snowlder_Arm_Right.SetActive(true);
                Snowlder_Arm_Right.GetComponent<Image>().sprite = arms1_sprite_right;
                Snowlder_Arm_Right.GetComponent<Image>().SetNativeSize();
                break;

            case "> Sexy Arms":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding inside this lump of snow. )" : "* ( You decide to take another pass at the body. )", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", (head == 0) ? "* ( Without a proper face, there is no Snowperson. )" : "*(Perhaps a different face is in order. )", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", "* ( You decide to settle on a different set of arms. )", false);
                arms = 2;
                arms2 = true;
                Snowlder_Arm_Left.SetActive(true);
                Snowlder_Arm_Left.GetComponent<Image>().sprite = arms2_sprite_left;
                Snowlder_Arm_Left.GetComponent<Image>().SetNativeSize();
                Snowlder_Arm_Right.SetActive(true);
                Snowlder_Arm_Right.GetComponent<Image>().sprite = arms2_sprite_right;
                Snowlder_Arm_Right.GetComponent<Image>().SetNativeSize();
                break;

            case "> Some Sticks Will Do":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding inside this lump of snow. )" : "* ( You decide to take another pass at the body. )", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the head", (head == 0) ? "* ( Without a proper face, there is no Snowperson. )" : "*(Perhaps a different face is in order. )", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some arms", "* ( You decide to settle on a different set of arms. )", false);
                arms = 3;
                arms3 = true;
                Snowlder_Arm_Left.SetActive(true);
                Snowlder_Arm_Left.GetComponent<Image>().sprite = arms3_sprite_left;
                Snowlder_Arm_Left.GetComponent<Image>().SetNativeSize();
                Snowlder_Arm_Right.SetActive(true);
                Snowlder_Arm_Right.GetComponent<Image>().sprite = arms3_sprite_right;
                Snowlder_Arm_Right.GetComponent<Image>().SetNativeSize();
                break;
            #endregion


            case "> Ask Snowperson to Move":
                snowperson_complete = true;
                break;

                
        }

        if (head != 0 && body != 0 && arms != 0)
        {
            if (GetComponent<enemy_data>().interactions.Count == 4)
            {
                GetComponent<enemy_data>().interactions[3] = new enemy_interaction("> Ask Snowperson to Move", "*You politely ask the Snowperson to move.", true);
            }     
            else
            {
                GetComponent<enemy_data>().interactions.Add( new enemy_interaction("> Ask Snowperson to Move", "*You politely ask the Snowperson to move.", true));
            }
        }

        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteMemory_coroutine(string name)
    {
        yield return new WaitForEndOfFrame();
        Memory auxiliar;

        for (int i = 0; i < GetComponent<enemy_data>().memories.Count; i++)
        {
            if (GetComponent<enemy_data>().memories[i].name == name)
            {
                auxiliar = GetComponent<enemy_data>().memories[i];
                battle_manager.dialogue_box_message = auxiliar.message;
                battle_manager.SetDialogueBox(auxiliar.message);
                break;
            }
        }



        switch (name)
        {
            case "Patient Memory":

                break;

            default:
                break;
        }
    }

    public void ExecutePacifistEnding()
    {
        /*Overworld_Signy.transform.parent.gameObject.SetActive(true);
        Overworld_Signy.SetActive(true);
        Overworld_Signy.GetComponent<Demo_Ravine_6_Signey_Overworld_Fight>().Finish_Battle_Pacifist();
        soul.SetActive(false);*/
    }

    public void ExecuteGenocideEnding()
    {

    }



    //Auxiliary attack coroutines

    IEnumerator attack_snowflakes(int number_of_snowflakes)
    {
        Vector3 spawn_helper = new Vector3(0, 101, 1);
        for (int i = 0; i < number_of_snowflakes; i++)
        {
            int repetition_safe = Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position;
            while (Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position == repetition_safe)
            {
                Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position = Random.Range(1, 6);
            }   
            switch(Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position)
            {
                case 1:
                    spawn_helper = new Vector3(-117, spawn_helper.y, 1);
                    break;

                case 2:
                    spawn_helper = new Vector3(-57f, spawn_helper.y, 1);
                    break;

                case 3:
                    spawn_helper = new Vector3(3, spawn_helper.y, 1);
                    break;

                case 4:
                    spawn_helper = new Vector3(60f, spawn_helper.y, 1);
                    break;

                case 5:
                    spawn_helper = new Vector3(120, spawn_helper.y, 1);
                    break;

                default:
                    break;
            }
            GameObject temp = Instantiate(snowflake_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localPosition = spawn_helper;
            yield return new WaitForSeconds(1.5f);
        }
    }

    IEnumerator attack_snowballs()
    {
        Vector3 spawn_helper = new Vector3(127, -180, 1);
        GameObject temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = false;

        spawn_helper = new Vector3(127, -90, 1);
        temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = false;

        spawn_helper = new Vector3(-118, -135, 1);
        temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = true;

        spawn_helper = new Vector3(-118, -45, 1);
        temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = true;

        yield return new WaitForEndOfFrame();
    }

    IEnumerator attack_shards(int number_of_shards, float time_between_shards)
    {
        float range = 35f;

        for (int i = 0; i < number_of_shards; i++)
        {
            Vector3 randomPos = Random.insideUnitSphere * range;
            randomPos.z = 0;
            Vector3 spawn_helper = GameObject.Find("Battle_Canvas").transform.InverseTransformPoint(soul.transform.position)+ randomPos;
            GameObject temp = Instantiate(iceshard_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localPosition = spawn_helper;
            yield return new WaitForSeconds(time_between_shards);
        }
        
    }


    //Direction goes from 0 to 7 and indicates the direction you're pushing the player in, starting in "down" and going clockwise..
    //initial strength is the initial strength of the wind, the one it has upon instantiation
    //final strength is th maximum strength it can achieve during its lifetime
    //increment is how fast it gains strength from intial strength to final strength per second
    //duration is how long the wind lasts. 
    IEnumerator attack_wind(int direction, float initial_strength, float final_strength, float increment, float duration)
    {
        Vector3 spawn_helper = new Vector3();
        Vector2 push_direction = new Vector2();

        switch(direction)
        {
            case 0:
                spawn_helper = new Vector3(14, 74, 0);
                push_direction = Vector2.down;
                break;
            case 1:
                spawn_helper = new Vector3(239, 74, 0);
                push_direction = new Vector2(-0.5f, -0.5f);
                break;
            case 2:
                spawn_helper = new Vector3(239, -112, 0);
                push_direction = Vector2.left;
                break;
            case 3:
                spawn_helper = new Vector3(239, -286, 0);
                push_direction = new Vector2(-0.5f, 0.5f);
                break;
            case 4:
                spawn_helper = new Vector3(0, -286, 0);
                push_direction = Vector2.up;
                break;
            case 5:
                spawn_helper = new Vector3(239, -286, 0);
                push_direction = new Vector2(0.5f, 0.5f);
                break;
            case 6:
                spawn_helper = new Vector3(239, -112, 0);
                push_direction = Vector2.right;
                break;
            case 7:
                spawn_helper = new Vector3(239, 58, 0);
                push_direction = new Vector2(0.5f, -0.5f);
                break;
            default:
                break;
        }

        GameObject temp = Instantiate(wind_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().push_direction = push_direction;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().initial_strength = initial_strength;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().final_strength = final_strength;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().increment = increment;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().duration = duration;
        temp.transform.localPosition = spawn_helper;

        yield return new WaitForEndOfFrame();
    }



    //State is wether the wire is vertical or horizontal. False for vertical, true for horizontal. Position is the position the wire is instantiated in.
    //Wires are hard coded, and they're always the same. if you want to change their behaviour you need to go to their own script.
    IEnumerator attack_wiretrap(bool state, Vector3 position)
    {
        if (state == false)
        {
            GameObject temp = Instantiate(wiretrap_attack, position, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localPosition = position;
        }

        else
        {
            GameObject temp = Instantiate(wiretrap_attack, position, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localRotation = Quaternion.Euler(0, 0, 90);
            temp.GetComponent<RectTransform>().sizeDelta = new Vector2(temp.GetComponent<RectTransform>().sizeDelta.x, 315);
            temp.transform.localPosition = position;
        }
        yield return new WaitForEndOfFrame();
    }

}
