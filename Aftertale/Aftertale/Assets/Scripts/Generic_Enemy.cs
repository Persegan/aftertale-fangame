﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Generic_Enemy : MonoBehaviour {


    public battle_manager battle_manager;
    public GameObject speech_bubble;
    private bool finishedspeaking;
    private bool isspeaking;
    private string speakingmessage;
    public Text speech_bubble_text;

    public AudioClip speech_sound;

    public GameObject dialogue_box;


    // Use this for initialization
    void Start()
    {
        battle_manager = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<battle_manager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (finishedspeaking == true)
            {
                finishedspeaking = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (isspeaking == true)
            {
                finishedspeaking = true;
                AutoTextScript.isfinished = true;
                speech_bubble_text.StopAllCoroutines();
                speakingmessage = speakingmessage.Replace("ç", "\n");
                speech_bubble_text.text = speakingmessage;
            }

        }

    }



    ///Auxiliar methods
    IEnumerator ShowDialogue(string message)
    {
        speakingmessage = message;
        isspeaking = true;
        speech_bubble.SetActive(true);
        speech_bubble_text.gameObject.SetActive(true);
        speech_bubble_text.text = "";
        AutoTextScript.TypeText(speech_bubble_text, message, 0.03f, null);
        while (AutoTextScript.isfinished == false)
        {
            //speech_sound.pitch = Random.Range (0.3f, 1.9f);
            GetComponent<AudioSource>().clip = speech_sound;
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.1f);
        }
        finishedspeaking = true;
        while (finishedspeaking == true)
        {
            yield return new WaitForEndOfFrame();
        }
        //yield return new WaitForSeconds (2.0f);
        speech_bubble.SetActive(false);
        speech_bubble_text.gameObject.SetActive(false);
    }

    public void attack(int turn)
    {
        StartCoroutine(attack_coroutine(turn));
    }

    public void dialogue(int turn)
    {
        StartCoroutine(dialogue_coroutine(turn));
    }

    public void ExecuteInteraction(string name)
    {
        StartCoroutine(ExecuteInteraction_coroutine(name));
    }

    public void ExecuteMemory(string name)
    {
        StartCoroutine(ExecuteMemory_coroutine(name));
    }



    IEnumerator attack_coroutine(int turn)
    {
        yield return new WaitForSeconds(3.0f);
        battle_manager.StartPlayerTurn();
    }

    IEnumerator dialogue_coroutine(int turn)
    {
        battle_manager.dialogue_box_message = "What an informative Sign.";
        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteInteraction_coroutine(string name)
    {
        for (int i = 0; i < GetComponent<enemy_data>().interactions.Count; i++)
        {
            if (name == GetComponent<enemy_data>().interactions[i].name)
            {
                GetComponent<enemy_data>().interactions[i].unread = false;
                battle_manager.dialogue_box_message = GetComponent<enemy_data>().interactions[i].message;
                battle_manager.SetDialogueBox(GetComponent<enemy_data>().interactions[i].message);
            }
        }

        switch (name)
        {
            case "0":
                battle_manager.dialogue_box_message = "Placeholder";
                battle_manager.SetDialogueBox("Placeholder");
                battle_manager.GiveMemoryPortion(this.gameObject, new Color(128 / 255f, 202 / 255f, 255 / 255f), new Color(128 / 255f, 202 / 255f, 255 / 255f));
                break;
            default:
                break;
        }

        

        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteMemory_coroutine(string name)
    {
        yield return new WaitForEndOfFrame();
        Memory auxiliar;

        for (int i = 0; i < GetComponent<enemy_data>().memories.Count; i++)
        {
            if (GetComponent<enemy_data>().memories[i].name == name)
            {
                auxiliar = GetComponent<enemy_data>().memories[i];
                battle_manager.dialogue_box_message = auxiliar.message;
                battle_manager.SetDialogueBox(auxiliar.message);
                break;
            }
        }



        switch (name)
        {
            case "Patient Memory":

                break;

            default:
                break;
        }
    }

    public void ExecutePacifistEnding()
    {
        /*Overworld_Signy.transform.parent.gameObject.SetActive(true);
        Overworld_Signy.SetActive(true);
        Overworld_Signy.GetComponent<Demo_Ravine_6_Signey_Overworld_Fight>().Finish_Battle_Pacifist();
        soul.SetActive(false);*/
    }

    public void ExecuteGenocideEnding()
    {

    }




    IEnumerator resizedialoguebox(Vector3 newscale, float speed)
    {
        //Remember to return the scale to Vector3(1,1,1) at the end of each attack!
        //Example code:
        //At the beginning of an attack:
        /*
         yield return new WaitForSeconds(1.0f) //So that we wait until the dialogue box has been resized once
         resizedialoguebox(Vector3 (0.2f, 0.3f, 1.0F); //We input the desired new size
         resizedialoguebox(Vector3 (1.0f,1.0f,1.0f); //We reset the scale of the box at the end of the turn;
          */
        while (Vector3.Distance(dialogue_box.transform.localScale, newscale) > 0.05f)
        {
            dialogue_box.transform.localScale = Vector3.Lerp(dialogue_box.transform.localScale, newscale, speed * Time.deltaTime);

            // If you don't want an eased scaling, replace the above line with the following line
            //   and change speed to suit:
            // transform.localScale = Vector3.MoveTowards (transform.localScale, newscale, speed * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }

    }
}

