﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Memory_Particle_Attractor : MonoBehaviour {

    public ParticleSystem p;
    public ParticleSystem.Particle[] particles;
    public float speed;



    void Start()
    {
        p.GetParticles(particles);
    }


    void LateUpdate()
    {


        InitializeIfNeeded();

        if (particles.Length < 1)
        {
            return;
        }

        int count = p.GetParticles(particles);
        for (int i = 0; i < count; i++)
        {
            //particles[i].position = Vector3.Lerp(particles[i].position, transform.position, Time.deltaTime * speed);
            particles[i].position = Vector3.MoveTowards(particles[i].position, transform.position, Time.deltaTime * speed);

        }

        // Apply the particle changes to the particle system
        p.SetParticles(particles, count);
    }


    void InitializeIfNeeded()
    {

        if (particles == null || particles.Length < p.main.maxParticles)
        {
            particles = new ParticleSystem.Particle[p.main.maxParticles];
        }
    }
}
