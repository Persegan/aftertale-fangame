﻿using UnityEngine;
using System.Collections;

public class Change_Layer_For_Character : MonoBehaviour {

    public GameObject character;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (character.transform.position.y < gameObject.transform.position.y)
        {
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
	
	}
}
