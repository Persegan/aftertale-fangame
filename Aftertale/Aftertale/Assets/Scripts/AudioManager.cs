﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

    public static AudioManager Audio_Manager;

    public AudioClip OST_Nostalgia;
    public AudioClip TTF_Overworld;
    public AudioClip Signy_Battle_Theme;
    public AudioClip Snowlder_Battle_Theme;
    public AudioClip Menu_Song;
    public AudioClip OST_Stand_Again;
    public AudioClip Thundernsail_Remix;

    void Awake()
    {
        if (Audio_Manager == null)
        {
            DontDestroyOnLoad(gameObject);
            Audio_Manager = this;
            SceneManager.sceneLoaded += OnSceneWasLoaded;
        }
        else if (Audio_Manager != this)
        {
            Destroy(gameObject);
        }
    }

    void OnSceneWasLoaded(Scene scene, LoadSceneMode mode)
    {

        switch (scene.name)
        {
            case ("Demo_Menu"):
                GetComponent<AudioSource>().volume = 0.0f;
                GetComponent<AudioSource>().clip = Menu_Song;
                StartCoroutine(FadeIn(GetComponent<AudioSource>(), 1.0f));
                break;
            case ("Demo_Ravine_0"):
            case ("Demo_Ravine_2"):
            case ("Demo_Ravine_3"):
            case ("Demo_Ravine_4"):
            case ("Demo_Ravine_6"):
            case ("Demo_Ravine_7"):
            case ("Demo_Ravine_Secret_Room_1"):
            case ("Demo_Ravine_Secret_Room_2"):
                StopAllCoroutines();
                if (GetComponent<AudioSource>().clip == OST_Nostalgia && GetComponent<AudioSource>().isPlaying && GetComponent<AudioSource>().volume == 1.0f)
                {

                }
                else
                {
                    GetComponent<AudioSource>().volume = 1.0f;
                    GetComponent<AudioSource>().clip = OST_Nostalgia;
                    GetComponent<AudioSource>().Play();
                }
                break;

            case ("Demo_Ravine_5"):
                {
                    StartCoroutine(FadeOut(GetComponent<AudioSource>(), 3.0f));
                }

                break;
            default:

                break;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
    }


    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return new WaitForEndOfFrame();

        }

        audioSource.Stop();
        //audioSource.volume = startVolume;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;
        audioSource.Play();
        while (audioSource.volume < 1)
        {
            audioSource.volume += 1 * Time.deltaTime / FadeTime;

            yield return null;
        }

        //audioSource.Stop();
        //audioSource.volume = startVolume;
    }
}
