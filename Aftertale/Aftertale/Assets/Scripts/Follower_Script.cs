﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Follower_Script : MonoBehaviour {

    public GameObject target;
    public List<Vector3> positions;
    public int distance_permitted;
    public float speed;

    //private float distance;
    private Vector3 lastLeaderPosition;

	// Use this for initialization
	void Start () {
        positions.Add(target.transform.position);

    }
	
	// Update is called once per frame
	void Update () {
        if (lastLeaderPosition != positions[positions.Count - 1])
        {
            positions.Add(target.transform.position);
        }        
       /* distance = 0;
        distance += Mathf.Abs(gameObject.transform.position.magnitude - positions[0].magnitude);
        for (int i = 0; i < positions.Count - 1; i++)
        {
            distance += Mathf.Abs(positions[i].magnitude - positions[i + 1].magnitude);
        }*/
        if (positions.Count >= distance_permitted)//distance > distance_permitted)
        {
            if (gameObject.transform.position != positions[0])//Mathf.Abs(gameObject.transform.position.magnitude - positions[0].magnitude) > 0.002
            {
                //gameObject.GetComponent<Rigidbody2D>().velocity = ((positions[0] - gameObject.transform.position).normalized) * speed;
                transform.position = Vector3.MoveTowards(transform.position, positions[0], Time.deltaTime * speed);
                //transform.position = Vector3.SmoothDamp(transform.position, positions[0], ref velocity, 0.0003f);
                //transform.position = Vector3.Lerp(transform.position, positions[0], Time.deltaTime * speed);
                //gameObject.transform.position += (positions[0] - gameObject.transform.position).normalized * speed * Time.deltaTime;
            }
            else
            {
                positions.Remove(positions[0]);
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

            }
        }
        lastLeaderPosition = target.transform.position;

    }
}
