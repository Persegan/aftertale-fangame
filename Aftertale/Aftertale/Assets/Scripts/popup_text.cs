﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class popup_text : MonoBehaviour {
	public string[] messages;
	public Image dialoguebox;
	public Text displaytext;
	public GameObject player;
	public AudioSource sound;
    public int variable;
    public int value;

    public int[] variables;
    public int[] values;

    public int state = -1;

    protected bool caninteract = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0.000000000000000000001f, 0);
		//gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (-0.000000000000000000001f, 0);

		if (Input.GetKeyDown (KeyCode.Z))
        {
            if (state == messages.Length-1 && AutoTextScript.isinteracting == false)
            {
                player.GetComponent<movement>().can_move = true;
                dialoguebox.gameObject.SetActive(false);
                displaytext.gameObject.SetActive(false);
                AutoTextScript.isfinished = false;
                AutoTextScript.isinteracting = false;
                if (variable != 0 || value != 0)
                {
                    player_data.playerdata.global_variables[variable] = value;
                }
                for (int i = 0; i < variables.Length; i++)
                {
                    if (variables[i] != 0)
                        variables[i] = values[i];
                }
                state = -1;
            }
            else if (caninteract == true && AutoTextScript.isinteracting == false) {
                state++;
				AutoTextScript.isinteracting = true;
				player.GetComponent<movement> ().can_move = false;
                player.GetComponent<Animator>().SetBool("moving", false);
                dialoguebox.gameObject.SetActive (true);
				displaytext.gameObject.SetActive (true);
				displaytext.text = "";
				AutoTextScript.TypeText (displaytext, messages[state], 0.03f, sound);
			} /*else if (state == 3) {
				player.GetComponent<movement> ().can_move = true;
				dialoguebox.gameObject.SetActive (false);
				displaytext.gameObject.SetActive (false);
				AutoTextScript.isfinished = false;
				AutoTextScript.isinteracting = false;
				state = 0;
			}*/
           

		}	
		if (Input.GetKeyDown (KeyCode.X)) {
			if (caninteract == true && AutoTextScript.isinteracting == true) {
                if (state >= 0)
                {
                    AutoTextScript.StopText(displaytext);
                    messages[state] = messages[state].Replace("ç", "\n");
                    messages[state] = messages[state].Replace("ñ", player_data.playerdata.player_name);
                    displaytext.text = messages[state];
                    AutoTextScript.isfinished = true;
                    AutoTextScript.isinteracting = false;
                }

            }
		}
	}

	void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.tag == "Player_Collision") 
		{
            if(collision.gameObject.GetComponentInParent<movement>().menu_open == false && collision.gameObject.GetComponentInParent<movement>().in_menu == false && collision.gameObject.GetComponentInParent<movement>().can_interact == true)
            {
                caninteract = true;
               // AutoTextScript.isfinished = true;
            }
            else
            {
                caninteract = false;
               // AutoTextScript.isfinished = false;
            }
            /*
			if (AutoTextScript.isinteracting == false) {				
				state = 1;
			} else if (AutoTextScript.isinteracting == true) {
					state = 2;	
			}
			if (AutoTextScript.isfinished == true) {
				state = 3;
			}*/
        }
	}

	void OnTriggerExit2D()
	{
        caninteract = false;
        AutoTextScript.isfinished = false;
    }

	/*void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.collider.tag == "Player_Collision") 
		{
			Debug.Log ("yes");
			if (AutoTextScript.isinteracting == false) {
				if (Input.GetKeyDown (KeyCode.Z)) {
					AutoTextScript.isinteracting = true;
					player.GetComponent<movement> ().can_move = false;
					dialoguebox.gameObject.SetActive (true);
					displaytext.gameObject.SetActive (true);
					displaytext.text = "";
					AutoTextScript.TypeText (displaytext, message, 0.03f, sound);
				}
			} else if (AutoTextScript.isinteracting == true) {
				if (Input.GetKeyDown (KeyCode.Z)) {
					displaytext.text = message;
					StopCoroutine ("SetText");
					AutoTextScript.isfinished = true;
				}

			}
			if (AutoTextScript.isfinished == true) {
				if (Input.GetKeyDown (KeyCode.Z)) {
					player.GetComponent<movement> ().can_move = true;
					dialoguebox.gameObject.SetActive (false);
					displaytext.gameObject.SetActive (false);
					AutoTextScript.isfinished = false;
					AutoTextScript.isinteracting = false;
				}
			}
		}
	}*/

}
