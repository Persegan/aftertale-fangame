﻿using UnityEngine;
using System.Collections;

public class Check_Global_Variables : MonoBehaviour {

    public Variable_Object[] Objects;

	// Use this for initialization
	void Start () {
        UpdateStatus();
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < Objects.Length; i++)
        {
            Objects[i].currentumber = 0;
        }

         
        for (int i = 0; i < Objects.Length; i++)
        {
           for (int j = 0; j < Objects[i].variables.Length; j++)
            {
                if (player_data.playerdata.global_variables[Objects[i].variables[j]] == Objects[i].values[j])
                {
                    Objects[i].currentumber++;
                }
            }
        }

        for (int i = 0; i < Objects.Length; i++)
        {
            if (Objects[i].currentumber >= Objects[i].requirednumber)
            {
                Objects[i].target.SetActive(true);
            }
            else
            {
                Objects[i].target.SetActive(false);
            }
        }
	
	}

    public void UpdateStatus()
    {

        for (int i = 0; i < Objects.Length; i++)
        {
            Objects[i].currentumber = 0;
        }
        for (int i = 0; i < Objects.Length; i++)
        {
            for (int j = 0; j < Objects[i].variables.Length; j++)
            {
                if (player_data.playerdata.global_variables[Objects[i].variables[j]] == Objects[i].values[j])
                {
                    Objects[i].currentumber++;
                }
            }
        }

        for (int i = 0; i < Objects.Length; i++)
        {
            if (Objects[i].currentumber >= Objects[i].requirednumber)
            {
                Objects[i].target.SetActive(true);
            }
            else
            {
                Objects[i].target.SetActive(false);
            }
        }
    }
}
