﻿using UnityEngine;
using System.Collections;

public class Soul_Collision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D collider)
    {
        /*if (collider.tag == "memory")
        {
            //gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 2, gameObject.transform.localScale.y + 2, gameObject.transform.localScale.z);
            gameObject.GetComponentInParent<soul_movement>().memoryfragments += 1;
        }*/

        if (collider.tag == "enemy")
        {
            if (gameObject.GetComponentInParent<soul_movement>().invulnerable== false)
            {
                gameObject.GetComponentInParent<soul_movement>().Take_Damage(collider.GetComponent<enemy_data>().attack);
            }
        }
        else if (collider.tag == "Blue_Enemy")
        {
            if (gameObject.GetComponentInParent<soul_movement>().invulnerable == false && gameObject.GetComponentInParent<soul_movement>().is_dashing == false)
            {
                gameObject.GetComponentInParent<soul_movement>().Take_Damage(collider.GetComponent<enemy_data>().attack);
            }
        }

    }
}
