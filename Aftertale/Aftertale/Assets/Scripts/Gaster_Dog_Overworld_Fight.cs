﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Gaster_Dog_Overworld_Fight : MonoBehaviour {
	//To make him do different stuff depending on the side you're on, see transform.inversetransformpoint. Search google for How to determine if enemy is on right or left side.
	public AudioSource battle1;
	public AudioSource battle2;
	public AudioSource battle3;

	public GameObject black_background;
	public GameObject soul;
	public GameObject Gaster_Soul;
	public GameObject Battle_Canvas;
	public GameObject Pumpkin;
	public GameObject Dialogue_box;
	public GameObject[] memory_fragments;




    private int state = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Z)) {
			if (state == 1) {
				state = 2;
				Start_Gaster_Battle ();

			}
		}

		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0, 0);
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0, 0);

	
	}
	void OnTriggerStay2D(Collider2D collision)
	{
		if (state == 2) {

		}
		else if (collision.tag == "Player_Collision") {
            if (collision.gameObject.GetComponentInParent<movement>().menu_open == false && collision.gameObject.GetComponentInParent<movement>().in_menu == false && collision.gameObject.GetComponentInParent<movement>().can_interact == true)
            {
                state = 1;
            }
            else
            {
                state = 0;
            }
        }
     
	
	}

	void OnTriggerExit2D()
	{
		state = 0;
	}

	public void Start_Gaster_Battle()
	{
		StartCoroutine (Start_Gaster_Battle_coroutine ());
	}

    //Comment from line 87 to 155
	IEnumerator Start_Gaster_Battle_coroutine()
	{
        BoxCollider2D[] boxlist;
        boxlist = soul.gameObject.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in boxlist)
        {
            box.enabled = false;
        }
        Pumpkin.GetComponent<movement> ().can_move = false;
		battle1.Play ();
		yield return new WaitForSeconds (0.4f);
        
		black_background.SetActive (true);
        black_background.transform.SetAsLastSibling();
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 1f);
        Gaster_Soul.transform.position = gameObject.transform.position;
		Gaster_Soul.SetActive (true);
		GameObject temporal = soul;
		temporal.transform.position = Pumpkin.transform.position;
		temporal.SetActive (true);
		battle2.Play ();
		yield return new WaitForSeconds (0.07f);

		black_background.SetActive (false);
		temporal.SetActive (false);
		Gaster_Soul.SetActive (false);
		yield return new WaitForSeconds (0.07f);

		black_background.SetActive (true);
		temporal.SetActive (true);
		Gaster_Soul.SetActive (true);
		battle2.Play ();
		yield return new WaitForSeconds (0.07f);

		black_background.SetActive (false);
		temporal.SetActive (false);
		Gaster_Soul.SetActive (false);
		yield return new WaitForSeconds (0.07f);

		black_background.SetActive (true);
		temporal.SetActive (true);
		Gaster_Soul.SetActive (true);
		battle2.Play ();
		yield return new WaitForSeconds (0.07f);

		battle3.Play ();
		Vector3 SoulInitial = temporal.transform.position;
		Vector3 SoulFinal = Dialogue_box.transform.position;
		Vector3 GasterSoulInitial = Gaster_Soul.transform.position;
		Vector3 GasterSoulFinal = Battle_Canvas.GetComponentInChildren<GasterDog_Enemy> ().gameObject.transform.position;
        GasterSoulFinal.y -= 0.5f;
        GasterSoulFinal.x -= 0.013f;
        //GasterSoulFinal.z = 100;
        SoulFinal.z = soul.transform.position.z;
		GasterSoulFinal.z = Gaster_Soul.transform.position.z; 
		for (float i = 0; i < 1; i = i+0.04f) {
			Gaster_Soul.transform.position = Vector3.Lerp (GasterSoulInitial, GasterSoulFinal, i);
			temporal.transform.position = Vector3.Lerp (SoulInitial, SoulFinal, i);
			yield return new WaitForSeconds (0.000001f);
		}
		soul.transform.position = SoulFinal;
		yield return new WaitForSeconds (1.5f);
		for (int i = 0; i < memory_fragments.Length; i++) {
			battle2.Play ();
			float delay = 0.05f;
			Color c = memory_fragments[i].GetComponent<SpriteRenderer> ().color;
			Color d = memory_fragments[i].GetComponent<SpriteRenderer> ().color;
			d.a = 0.2f;
			memory_fragments [i].SetActive (true);
			memory_fragments[i].GetComponent<SpriteRenderer> ().color = d;
			yield return new WaitForSeconds (delay);
			memory_fragments[i].GetComponent<SpriteRenderer> ().color = c;
			yield return new WaitForSeconds (delay);
			memory_fragments[i].GetComponent<SpriteRenderer> ().color = d;
			yield return new WaitForSeconds (delay);
			memory_fragments[i].GetComponent<SpriteRenderer> ().color = c;
			yield return new WaitForSeconds (delay);

		}


		yield return new WaitForSeconds (1.5f);
		battle3.Play ();
        
		yield return new WaitForSeconds (0.3f);
		Battle_Canvas.SetActive (true);



		GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Play ();
		for (float i = 1; i > 0; i = i - 0.05f) {
			Color d = Gaster_Soul.GetComponent<SpriteRenderer> ().color;
			Color c = black_background.GetComponent<Image> ().color;
			c.a = i;
			d.a = i;
			Gaster_Soul.GetComponent<SpriteRenderer> ().color = d;
			black_background.GetComponent<Image> ().color = c;
			yield return new WaitForSeconds (0.01f);
		}
		black_background.SetActive (false);
		Gaster_Soul.SetActive (false);
		GameObject.Find ("Canvas").GetComponent<battle_manager> ().StartEnemyTurn ();
		Pumpkin.SetActive (false);
		gameObject.SetActive (false);
        foreach (BoxCollider2D box in boxlist)
        {
            box.enabled = true;
        }
        //temporal.SetActive (false);
    }
}
