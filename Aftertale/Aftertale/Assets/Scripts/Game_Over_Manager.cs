﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Game_Over_Manager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(temporal_restart_game());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator temporal_restart_game()
    {
        yield return new WaitForSeconds(1.5f);
        player_data.playerdata.Load();
        SceneManager.LoadScene("Overworld_Movement_Test");
        
    }
}
