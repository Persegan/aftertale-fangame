﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Menu_Manager : MonoBehaviour {

    public Button New_Game;
    public Button Exit;
    public Image Cursor;
    public Image Highlight_Bar;
    public Image Black_Background;
    public string Initial_Scene;

    private Coroutine auxiliar_coroutine;
    private Coroutine auxiliar_coroutine_2;
    private bool newgameiscontinue = false;


    // Use this for initialization
    void Start() {
        auxiliar_coroutine_2 = StartCoroutine(initial_transition_Coroutine());
         if (File.Exists(Application.persistentDataPath + "/player_data.dat") == true)
        {
            player_data.playerdata.Load();

            switch (player_data.playerdata.scene_to_load)
            {
                case "Demo_Ravine_5":
                    player_data.playerdata.position_to_load = new Vector3(-0.044f, 0.378f, 8.6f);
                    break;

            }
            newgameiscontinue = true;
            New_Game.GetComponentInChildren<Text>().text = "Continue";
            //player_data.playerdata.LoadScene();
        }

        else
        {
            player_data.playerdata.Reset();
            player_data.playerdata.scene_to_load = "Demo_Ravine_0";
        }


    }

    // Update is called once per frame
    void Update() {

    }

    public void submit_New_Game_Continue()
    {
        StartCoroutine(submit_New_Game_Continue_coroutine());
    }

    IEnumerator submit_New_Game_Continue_coroutine()
    {
        //Cursor.gameObject.SetActive(false);
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        StopCoroutine(auxiliar_coroutine_2);
        yield return StartCoroutine(final_transition_Coroutine());
        if (newgameiscontinue == true)
            player_data.playerdata.LoadScene();
        else
            SceneManager.LoadScene(Initial_Scene);
    }

    public void submit_Exit()
    {

        StartCoroutine(submit_Exit_coroutine());
    }

    IEnumerator submit_Exit_coroutine()
    {
        Cursor.gameObject.SetActive(false);
        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        StopCoroutine(auxiliar_coroutine_2);
        yield return StartCoroutine(final_transition_Coroutine());
        Application.Quit();
    }
    public void select_Exit()
    {
        if (auxiliar_coroutine != null) StopCoroutine(auxiliar_coroutine);
        Cursor.rectTransform.anchoredPosition = new Vector3(Cursor.rectTransform.anchoredPosition.x, -243, 0);
        auxiliar_coroutine = StartCoroutine(move_Highlight_Bar_lerping(-242));
    }

    public void select_New_Game_Continue()
    {
        if (auxiliar_coroutine != null)  StopCoroutine(auxiliar_coroutine);
        Cursor.rectTransform.anchoredPosition = new Vector3(Cursor.rectTransform.anchoredPosition.x, -161, 0);
        auxiliar_coroutine = StartCoroutine(move_Highlight_Bar_lerping(-162));
    }

    IEnumerator move_Highlight_Bar_lerping(float destination)
    {
        Vector3 auxiliar = Highlight_Bar.rectTransform.anchoredPosition;
        auxiliar.y = destination;
        while (Mathf.Abs(Mathf.Abs(destination) - Mathf.Abs(Highlight_Bar.rectTransform.anchoredPosition.y)) > 0.5f)
        {
            Highlight_Bar.rectTransform.anchoredPosition = Vector3.Lerp(Highlight_Bar.rectTransform.anchoredPosition, auxiliar, Time.deltaTime * 7);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForEndOfFrame();
            //speed += 0.02f;
        }
        yield break;
    }

    IEnumerator initial_transition_Coroutine()
    {
        New_Game.Select();
        Black_Background.gameObject.SetActive(true);
        Black_Background.canvasRenderer.SetAlpha(1.0f);
        //Color temp = black_background.color;
        //temp.a = 255;
        //Black_Background.color = temp;
        Black_Background.CrossFadeAlpha(0, 0.5f, false);
        yield return new WaitForEndOfFrame();
    }

    IEnumerator final_transition_Coroutine()
    {
        Black_Background.gameObject.SetActive(true);
        Black_Background.canvasRenderer.SetAlpha(0.0f);
        Black_Background.CrossFadeAlpha(1, 2.1f, false);
        AudioManager.Audio_Manager.GetComponent<AudioManager>().StopAllCoroutines();
        StartCoroutine(AudioManager.FadeOut(AudioManager.Audio_Manager.GetComponent<AudioSource>(), 2.0f));
        yield return new WaitForSeconds(2.1f);
    }
}
