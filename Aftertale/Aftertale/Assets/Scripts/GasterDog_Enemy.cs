﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GasterDog_Enemy : MonoBehaviour
{

    public GameObject speech_bubble;
    public Text speech_bubble_text;

    public GameObject memory_fragment;
    public GameObject soul_afterimage;
    public GameObject dialogue_box;
    public GameObject black_background;
    public GameObject Warning_signs;
    public GameObject bone_attack;
    public GameObject lock_on_indicator;
    public GameObject snowflake_attack;
    public GameObject impact_zone;
    public int memory_cooldown;

    public AudioSource speech_sound;
    public AudioSource attack_sound;
    public AudioSource finger_snap;

    public string dialogue_message;

    private Animator anim;
    private GameObject player;
    private bool spawnafterimage;
    private bool finishedspeaking;
    private bool isspeaking;
    private string speakingmessage;
    private bool attack_relies_on_turn;
    private bool Gaster_Was_Pet;
    private bool detect_if_player_dashes;
    private float phase;

    private bool genocide = false;
    private bool genocide_warning = false;
    private int lastrandomnumber = -1;




}