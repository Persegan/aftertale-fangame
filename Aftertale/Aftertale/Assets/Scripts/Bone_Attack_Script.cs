﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bone_Attack_Script : MonoBehaviour {

    public Vector3 direction;
    public float lifetime;
    public float speed;
    public int behaviour;
    public float super_speed_time;
    public float super_speed_wait;
    public float rotationspeed;
    public float elevator_speed; // how fast they  go
    public float elevator_distance;// how far away from the point they are
    public float elevator_angle;// the angle at which will start
    public float fade_time;

    public Vector3 center;
    public float elevator_speed_2;


    private Vector3 originalposition;
	// Use this for initialization
	void Start () {
        /*gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        StartCoroutine(FadeIn(10, fade_time));
        StartCoroutine(FadeOut(10, fade_time, lifetime - 1.0f));*/
        Destroy(gameObject, lifetime);
        originalposition = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (behaviour == 1)
        {
            transform.position += direction * Time.deltaTime * speed;
        }
        if (behaviour == 2)
        {
            StartCoroutine(superspeed());
        }
        if (behaviour == 3)
        {
            transform.position += direction * Time.deltaTime * speed;
            transform.Rotate(new Vector3(0, 0, 1) * (rotationspeed * Time.deltaTime));
        }
        if (behaviour == 4)
        {
            transform.position += direction * Time.deltaTime * speed;
            transform.position = new Vector3(gameObject.transform.position.x,  originalposition.y + Mathf.Sin(elevator_angle * Mathf.Deg2Rad) * elevator_distance, gameObject.transform.position.z);
            elevator_angle = elevator_angle % 360 + elevator_speed;
        }    
        if (behaviour == 5)
        {
            transform.Rotate(new Vector3(0, 0, 1) * (rotationspeed * Time.deltaTime));
            transform.localPosition = new Vector3(center.x + Mathf.Cos(elevator_angle * Mathf.Deg2Rad) * elevator_distance, center.y + Mathf.Sin(elevator_angle * Mathf.Deg2Rad) * elevator_distance, -1);
            elevator_angle = elevator_angle % 360 + elevator_speed;
            elevator_distance -= Time.deltaTime * elevator_speed_2;
        }
	}

    IEnumerator superspeed()
    {
        behaviour = 1;
        yield return new WaitForSeconds(super_speed_time);
        behaviour = 0;
        yield return new WaitForSeconds(super_speed_wait);
        behaviour = 1;
    }

    IEnumerator FadeIn(int iterations, float frequency)
    {
        for (float i = 0; i <= iterations; i++)
        {
            gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0+ (i/iterations));
            yield return new WaitForSeconds(frequency);
        }
    }

    IEnumerator FadeOut(int iterations, float frequency, float delay)
    {
        yield return new WaitForSeconds(delay);
        for (float i = 0; i <= iterations; i++)
        {
            gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1 - (i / iterations));
            yield return new WaitForSeconds(frequency);
        }
    }
}
