﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class popup_text_choices : MonoBehaviour {

    public string[] messages;
    public string[] choicemessages;
    public string[] messageschoice1;
    public string[] messageschoice2;
    public Image dialoguebox;
    public Text displaytext;
    public GameObject player;
    public AudioClip sound;
    public GameObject soul;
    public Button Choice1;
    public Button Choice2;

    private int state = -1;

    private int choice_level = 0;

    private bool caninteract = false;

    // Use this for initialization
    void Start()
    {
        GetComponent<AudioSource>().clip = sound;
    }

    IEnumerator PresentChoices()
    {
        AutoTextScript.isinteracting = true;
        while (AutoTextScript.isinteracting == true)
        {
            yield return new WaitForSeconds(0.2f);
        }
        AutoTextScript.TypeText(Choice1.GetComponentInChildren<Text>(), choicemessages[1], 0.03f, GetComponent<AudioSource>());
        AutoTextScript.TypeText(Choice2.GetComponentInChildren<Text>(), choicemessages[2], 0.03f, GetComponent<AudioSource>());
        while (AutoTextScript.isinteracting == true)
        {
            yield return new WaitForSeconds(0.2f);
        }
        soul.SetActive(true);
        Choice1.Select();
        choice_level = 1;
        state = -1;

    }
    // Update is called once per frame
    void Update()
    {
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.000000000000000000001f, 0);
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-0.000000000000000000001f, 0);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (choice_level == 1)
            {
                if (choice_button_script.choice == 0)
                {
                    if (state == messageschoice1.Length - 1 && AutoTextScript.isinteracting == false)
                    {

                        player.GetComponent<movement>().can_move = true;
                        dialoguebox.gameObject.SetActive(false);
                        displaytext.gameObject.SetActive(false);
                        AutoTextScript.isfinished = false;
                        AutoTextScript.isinteracting = false;
                        state = -1;
                        choice_level = 0;
                        choice_button_script.choice = 1;
                    }
                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        soul.SetActive(false);
                        GameObject myEventSystem = GameObject.Find("EventSystem");
                        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                        Choice1.GetComponentInChildren<Text>().text = "";
                        Choice2.GetComponentInChildren<Text>().text = "";
                        Choice1.gameObject.SetActive(false);
                        Choice2.gameObject.SetActive(false);
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messageschoice1[state], 0.03f, GetComponent<AudioSource>());
                    }
                }
                else if (choice_button_script.choice == 1)
                {
                    if (state == messageschoice2.Length - 1 && AutoTextScript.isinteracting == false)
                    {
                        choice_level = 0;
                        player.GetComponent<movement>().can_move = true;
                        dialoguebox.gameObject.SetActive(false);
                        displaytext.gameObject.SetActive(false);
                        AutoTextScript.isfinished = false;
                        AutoTextScript.isinteracting = false;
                        state = -1;
                        choice_button_script.choice = 1;
                    }
                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        soul.SetActive(false);
                        GameObject myEventSystem = GameObject.Find("EventSystem");
                        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                        Choice1.GetComponentInChildren<Text>().text = "";
                        Choice2.GetComponentInChildren<Text>().text = "";
                        Choice1.gameObject.SetActive(false);
                        Choice2.gameObject.SetActive(false);
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messageschoice2[state], 0.03f, GetComponent<AudioSource>());
                    }

                }
            }
            else if (choice_level == 0)
            {
                if (state == messages.Length - 1 && AutoTextScript.isinteracting == false)
                {
                    choice_level = -1;
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, choicemessages[0], 0.03f, GetComponent<AudioSource>());
                    Choice1.gameObject.SetActive(true);
                    Choice2.gameObject.SetActive(true);
                    Choice1.GetComponentInChildren<Text>().text = "";
                    Choice2.GetComponentInChildren<Text>().text = "";
                    StartCoroutine(PresentChoices());
                    /*
                    player.GetComponent<movement>().can_move = true;
                    dialoguebox.gameObject.SetActive(false);
                    displaytext.gameObject.SetActive(false);
                    AutoTextScript.isfinished = false;
                    AutoTextScript.isinteracting = false;
                    state = -1;*/
                }
                else if (caninteract == true && AutoTextScript.isinteracting == false)
                {
                    state++;
                    AutoTextScript.isinteracting = true;
                    player.GetComponent<movement>().can_move = false;
                    player.GetComponent<Animator>().SetBool("moving", false);
                    dialoguebox.gameObject.SetActive(true);
                    displaytext.gameObject.SetActive(true);
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, GetComponent<AudioSource>());
                } /*else if (state == 3) {
				player.GetComponent<movement> ().can_move = true;
				dialoguebox.gameObject.SetActive (false);
				displaytext.gameObject.SetActive (false);
				AutoTextScript.isfinished = false;
				AutoTextScript.isinteracting = false;
				state = 0;
			}*/


            }


        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (choice_level == 0)
            {
                if (caninteract == true)
                {
                    if (state >= 0)
                    {
                        AutoTextScript.StopText(displaytext);
                        messages[state] = messages[state].Replace("ç", "\n");
                        displaytext.text = messages[state];
                        AutoTextScript.isfinished = true;
                        AutoTextScript.isinteracting = false;

                    }
                }
            }

            if (choice_level == 1 && state != -1)
            {
                if (choice_button_script.choice == 0)
                {
                    AutoTextScript.StopText(displaytext);
                    messageschoice1[state] = messageschoice1[state].Replace("ç", "\n");
                    displaytext.text = messageschoice1[state];
                    AutoTextScript.isfinished = true;
                    AutoTextScript.isinteracting = false;
                }
                else if (choice_button_script.choice == 1)
                {
                    AutoTextScript.StopText(displaytext);
                    messageschoice2[state] = messageschoice2[state].Replace("ç", "\n");
                    displaytext.text = messageschoice2[state];
                    AutoTextScript.isfinished = true;
                    AutoTextScript.isfinished = true;
                    AutoTextScript.isinteracting = false;
                }
            }
           
        }
    }
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player_Collision")
        {

            if (collision.gameObject.GetComponentInParent<movement>().menu_open == false && collision.gameObject.GetComponentInParent<movement>().in_menu == false && collision.gameObject.GetComponentInParent<movement>().can_interact == true)
            {
                caninteract = true;
                //AutoTextScript.isfinished = true;
            }
            else
            {
                caninteract = false;
                //AutoTextScript.isfinished = false;
            }
            /*
			if (AutoTextScript.isinteracting == false) {				
				state = 1;
			} else if (AutoTextScript.isinteracting == true) {
					state = 2;	
			}
			if (AutoTextScript.isfinished == true) {
				state = 3;
			}*/
        }
    }

    void OnTriggerExit2D()
    {
        caninteract = false;
        AutoTextScript.isfinished = false;
    }
}
