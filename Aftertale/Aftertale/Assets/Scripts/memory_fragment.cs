﻿using UnityEngine;
using System.Collections;

public class memory_fragment : MonoBehaviour {

    public AudioSource pickup_fragment;
    public int behaviour;
    public Vector3 center;
    public float speed;

    private Vector3 v;

    void Start()
    {
        if (behaviour == 1)
        {
            v = transform.position - center;
        }

    }

    void Update()
    {
        if (behaviour == 1)
        {
            v = Quaternion.AngleAxis(speed * Time.deltaTime, Vector3.forward) * v;
            transform.position = center + v;
        }

    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player_Soul_Collision")
        {
            GameObject.Find("Heal_Sound").GetComponent<AudioSource>().Play();
            Destroy(gameObject);         
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        OnTriggerEnter2D(collider);
    }


}
