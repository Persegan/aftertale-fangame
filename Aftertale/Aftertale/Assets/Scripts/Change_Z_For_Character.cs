﻿using UnityEngine;
using System.Collections;

public class Change_Z_For_Character : MonoBehaviour {
    public GameObject character;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (character != null)
        {
            if (character.transform.position.y < gameObject.transform.position.y)
            {
                gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, 5);
            }
            else
            {
                gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
            }
        }              

    }
}
