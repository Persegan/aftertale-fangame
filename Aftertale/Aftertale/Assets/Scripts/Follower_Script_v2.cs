﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Follower_Script_v2 : MonoBehaviour {

    public GameObject target;
    public float distance_permitted;
    //public float speed;

    private Vector2 behind;
    private Vector3 previous_target_position;
    private float auxiliar_time;


    // Use this for initialization
    void Start()
    {
        auxiliar_time = 0.33f;
        previous_target_position = target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(gameObject.GetComponent<Rigidbody2D>().velocity);
        behind = (Vector2)target.transform.position + (((target.GetComponent<Rigidbody2D>().velocity * -1).normalized) * distance_permitted);
        if (target.GetComponent<Rigidbody2D>().velocity.magnitude >= 1 && !V3Equal(previous_target_position, target.transform.position))
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = (behind - (Vector2)gameObject.transform.position).normalized * target.GetComponent<Rigidbody2D>().velocity.magnitude;
            gameObject.GetComponent<Animator>().SetBool("moving", true);
            if (gameObject.GetComponent<Rigidbody2D>().velocity.x > 0.5f)
            {
                gameObject.GetComponent<Animator>().SetInteger("direction", 2);
            }
            else if (gameObject.GetComponent<Rigidbody2D>().velocity.x < -0.5f)
            {
                gameObject.GetComponent<Animator>().SetInteger("direction", 4);
            }
            else if (gameObject.GetComponent<Rigidbody2D>().velocity.y > 0.5f)
            {
                gameObject.GetComponent<Animator>().SetInteger("direction", 1);
            }
            else if (gameObject.GetComponent<Rigidbody2D>().velocity.y < -0.5f)
            {
                gameObject.GetComponent<Animator>().SetInteger("direction", 3);
            }

        }
        else
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            gameObject.GetComponent<Animator>().SetBool("moving", false);
        }

        if (auxiliar_time < 0)
        {
            previous_target_position = target.transform.position;
            auxiliar_time = 0.33f;
        }
        else
        {
            auxiliar_time -= Time.deltaTime;
            
        }

    }



    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }
}
