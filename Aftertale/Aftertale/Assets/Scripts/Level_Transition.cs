﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level_Transition : MonoBehaviour {

    public Image black_background;
    public GameObject Player;

	// Use this for initialization
	public virtual void Start () {
        black_background.gameObject.SetActive(true);
        initial_transition();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void initial_transition()
    {
        StartCoroutine(initial_transition_Coroutine());
    }

    public void final_transition(string scene)
    {
        StartCoroutine(final_transition_Coroutine(scene));
    }



    IEnumerator initial_transition_Coroutine()
    {
        Vector3 auxiliar = player_data.playerdata.position_to_load;
        auxiliar.z = 8.6f;
        Player.transform.position = auxiliar;
        Player.GetComponent<Animator>().SetInteger("direction", player_data.playerdata.direction_to_load);
        Camera.main.gameObject.GetComponent<camera>().Reposition();
        //yield return new WaitForEndOfFrame();
        if (GameObject.Find("_Parallaxing"))
        {
            GameObject.Find("_Parallaxing").SetActive(true);
        }
        /*Player.GetComponent<movement>().can_dash = false;
        Player.GetComponent<movement>().can_move = false;
        Player.GetComponent<movement>().can_interact = false;*/
        black_background.gameObject.SetActive(true);
        Color temp = black_background.color;
        temp.a = 255;
        black_background.color = temp;
        black_background.CrossFadeAlpha(0, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        /*Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<movement>().can_interact = true;*/
    }

    IEnumerator final_transition_Coroutine(string scene)
    {
        Player.GetComponent<movement>().can_dash = false;
        Player.GetComponent<movement>().can_move = false;
        Player.GetComponent<movement>().can_interact = false;
        black_background.gameObject.SetActive(true);

        black_background.canvasRenderer.SetAlpha(0.0f);
        black_background.CrossFadeAlpha(1.0f, 0.5f, false);
        SceneManager.LoadScene(scene);
        yield return 0;
    }

    public static IEnumerator move_lerping(GameObject target, Vector3 destination, float speed, float ease_out_speed)
    {
        while (Vector3.Distance(target.transform.localPosition, destination) > 0.001f)
        {
            target.transform.localPosition = Vector3.Lerp(target.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForEndOfFrame();
            speed +=  ease_out_speed;
        }
        yield break;
    }
}
