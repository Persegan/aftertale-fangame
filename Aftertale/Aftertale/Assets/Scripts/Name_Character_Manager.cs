﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Name_Character_Manager : MonoBehaviour {

    public InputField character_name;
    public string name_to_load;

	// Use this for initialization
	void Start () {
        character_name.Select();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void name_is_done()
    {
        if(character_name.text == "")
        {
            player_data.playerdata.player_name = "Pumpkin";
        }
        else
        {
            player_data.playerdata.player_name = character_name.text;
        }
        player_data.playerdata.Save();
        SceneManager.LoadScene(name_to_load);
    }
}
