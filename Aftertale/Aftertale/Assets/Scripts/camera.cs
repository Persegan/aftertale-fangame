﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour {
	public Transform player;

	public float maxX;
	public float minX;
	public float maxY;
	public float minY;


    public float speed;


	// Use this for initialization
	void Awake () {
        Reposition();
    }

    public void Reposition()
    {
        Vector3 original = player.transform.position;
        original.z = gameObject.transform.position.z;
        gameObject.transform.position = original;

        if (gameObject.transform.position.x < minX)
        {
            gameObject.transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
        if (gameObject.transform.position.x > maxX)
        {
            gameObject.transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }
        if (gameObject.transform.position.y < minY)
        {
            gameObject.transform.position = new Vector3(transform.position.x, minY, transform.position.z);
        }

        if (gameObject.transform.position.y < maxY)
        {
            gameObject.transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        }
    }

    // Update is called once per frame
    void LateUpdate () {

		if (player != null) {
            /*if (player.position.x > minX && player.position.x < maxX) {
                Vector3 auxiliar = new Vector3(player.position.x, transform.position.y, transform.position.z);
                //gameObject.transform.position = new Vector3 (player.position.x, transform.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);

			}*/
            if (player.transform.position.x <= minX)
            {
                Vector3 auxiliar = new Vector3(minX, transform.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            }
            else if (player.transform.position.x >= maxX)
            {
                Vector3 auxiliar = new Vector3(maxX, transform.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            }
            else {
                Vector3 auxiliar = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            }


            
            if (player.transform.position.y <= minY)
            {
                Vector3 auxiliar = new Vector3(transform.position.x, minY, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            }
            else if (player.transform.position.y >= maxY)
            {
                Vector3 auxiliar = new Vector3(transform.position.x, maxY, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            }
            else
            {
                Vector3 auxiliar = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            }


            /*if (player.position.y > minY && player.position.y < maxY) {
                Vector3 auxiliar = new Vector3(transform.position.x, player.position.y, transform.position.z);
                //gameObject.transform.position = new Vector3 (transform.position.x, player.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, auxiliar, speed * Time.deltaTime);
            } else {
                gameObject.transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
            }*/
        }

    }

    public void transition_function(float MinX, float MaxX, float MinY, float MaxY, float speed1, float speed2, float time)
    {
        StopAllCoroutines();
        StartCoroutine(transition(MinX, MaxX, MinY, MaxY, speed1, speed2, time));
    }


    IEnumerator transition(float MinX, float MaxX, float MinY, float MaxY, float speed1, float speed2, float time)
    {
       minX = MinX;
       maxX = MaxX;
       minY = MinY;
       maxY = MaxY;
       speed = speed1;

       /*while (gameObject.transform.position.x != player.transform.position.x && gameObject.transform.position.y != player.transform.position.y && gameObject.transform.position.x != minX
            && gameObject.transform.position.x != maxX && gameObject.transform.position.y != minY && gameObject.transform.position.y != maxY)
        {
            yield return new WaitForEndOfFrame();
        }*/
        
       yield return new WaitForSeconds(time);
       speed = speed2;
    }
}
