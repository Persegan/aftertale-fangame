﻿using UnityEngine;
using System.Collections;

public class Snowflake_Attack_Script : MonoBehaviour {
    public float rotation_strength;
    public Vector3 target;
    public float speed_1;
    public float time_phase_1;
    public float lifetime;
    public float speed_2;
    public float zig_zag_strength;
    public float zig_zag_time;
    public int zig_zag_direction;


    private float ellapsed_time;
    private bool auxiliar;
    void Awake()
    {

    }

    void Start()
    {
        auxiliar = false;
        ellapsed_time = 0.0f;
        GetComponent<Rigidbody2D>().AddTorque(rotation_strength);
        Destroy(gameObject, lifetime);
    }

    void Update()
    {
       if (ellapsed_time <= time_phase_1)
        {
            Vector3 direction = target - gameObject.transform.localPosition;
            gameObject.transform.localPosition += direction * Time.deltaTime * speed_1;
            ellapsed_time += Time.deltaTime;
        }
       else if (auxiliar == false)
        {
            if (zig_zag_direction == 1)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-zig_zag_strength / 10, 0);
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(zig_zag_strength / 10, 0);
            }

            StartCoroutine(Zig_Zag());
            auxiliar = true;
        }
       else
        {
            gameObject.transform.localPosition += Vector3.down * Time.deltaTime * speed_2;
        }

    }

    IEnumerator Zig_Zag()
    {
        if (GetComponent<Rigidbody2D>().velocity.x > 0)
        {
            for (int i = 0; i < 10; i++)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(-zig_zag_strength, 0));
                yield return new WaitForSeconds(zig_zag_time);
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(zig_zag_strength, 0));
                yield return new WaitForSeconds(zig_zag_time);
            }
        }
        StartCoroutine(Zig_Zag());
    }


}
