﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_7_Snowlder_Snowflake_Attack_2 : MonoBehaviour {

    public float gravity = -1.5f;

	// Use this for initialization
	void Start () {

        //GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1.5f, 1.5f), 1.0f));
        GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-0.12f, 0.12f), 0.3f);
        GetComponent<Rigidbody2D>().AddTorque(150.0f);
		
	}
	
	// Update is called once per frame
	void Update () {

        if (transform.localPosition.y <= -215)
        {
            Destroy(gameObject);
        }
        GetComponent<Rigidbody2D>().velocity += new Vector2(0, gravity);
    }

}
