﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Demo_Ravine_6_Ball_Minigame : MonoBehaviour {



    public GameObject player;

    public GameObject whiteball;

    public Text Combo_Counter;

    public AudioSource dialogue_sound;

    private Sprite FreezePlayerSprite;

    public SpriteRenderer background;

    public GameObject Foreground;

    private Vector3 velocity;
    private float angularvelocity;

    public Image black_background;

    public GameObject narrator_box;
    public Text narrator_text;

    public ParticleSystem ball_hit_particles;

    public ParticleSystem ball_travelling_particles;


    public ParticleSystem Fireworks_Particles;

    public AudioClip Drumroll;

    public AudioClip Launching_Sound;

    public AudioClip Bouncing_Sound;

    public AudioClip Victory_Sound;

    public AudioClip Fireworks_1;
    public AudioClip Fireworks_2;

    public AudioClip Gaster_Bark;

    public BoxCollider2D Ball_Bottom_Limit;
    public BoxCollider2D Ball_Top_Limit;
    public PolygonCollider2D Out_Of_Bounds_Trigger;

    public GameObject Leaderboard;

    private bool ball_fired;
    private bool ball_stopped;
    private int combo;
    private Vector3 original_combo_counter_position;
    private string message;

	// Use this for initialization
	void Start () {
        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), Ball_Bottom_Limit, true);
        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), Ball_Top_Limit, true);
        ball_stopped = false;
        combo = 0;
        original_combo_counter_position = Combo_Counter.rectTransform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {

        if (ball_fired == true)
        {
            if (gameObject.GetComponent<Rigidbody2D>().velocity.magnitude < 0.05f)
            {
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                //StartCoroutine(Finish_Ball());
                StartCoroutine(ShowText());
            }
        }
        else if (ball_stopped == true && AutoTextScript.isinteracting == false)
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                StartCoroutine(Finish_Ball());
            }
        }
        else if (ball_stopped == true && AutoTextScript.isinteracting == true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
               
                AutoTextScript.StopText(narrator_text);
                message = message.Replace("ç", "\n");
                message = message.Replace("ñ", player_data.playerdata.player_name);
                narrator_text.text = message;
                AutoTextScript.isfinished = true;
                AutoTextScript.isinteracting = false;

            }
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player = collision.gameObject;

            if (player.GetComponent<movement>().is_dashing == true)
            {
                StartCoroutine(PlayerKickedBall());
            }
        }

        if (ball_fired == true)
        {
            GetComponent<AudioSource>().PlayOneShot(Bouncing_Sound);
            combo++;
            Combo_Counter.text = "COMBO:" + '\n' + combo.ToString();
            StartCoroutine(Combo_Counter_Effect());
        }

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider == Out_Of_Bounds_Trigger)
        {
            Destroy(collider.gameObject);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            ball_fired = false;
            combo = 0;
            StartCoroutine(ShowOutOfBoundsText());
        }
    }

    IEnumerator PlayerKickedBall()
    {
        //We set the camera to follow the ball
        Camera.main.GetComponent<camera>().player = gameObject.transform;

        //We set the combo to 0
        combo = 0;

        //We freeze the player
        player.GetComponent<movement>().StopAllCoroutines();
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;
        player.GetComponent<movement>().is_dashing = false;
        FreezePlayerSprite = player.GetComponent<SpriteRenderer>().sprite;
        player.GetComponent<Animator>().enabled = false;
        player.transform.GetChild(1).GetComponent<Animator>().enabled = false;
        player.GetComponent<SpriteRenderer>().sprite = FreezePlayerSprite;
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        //We save the velocity of the ball and set it to zero
        velocity = gameObject.GetComponent<Rigidbody2D>().velocity;
        angularvelocity = gameObject.GetComponent<Rigidbody2D>().angularVelocity;
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = 5525.0f;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        gameObject.GetComponent<Rigidbody2D>().drag = 5000;
        gameObject.GetComponent<Rigidbody2D>().mass = 0.0f;

        //We fade to black objects

        Coroutine lastroutine2 = StartCoroutine(DarkenObjects());
        /*black_background.gameObject.SetActive(true);
        black_background.canvasRenderer.SetAlpha(0.0f);
        black_background.CrossFadeAlpha(0.55f, 0.5f, false);*/



        //We squash the ball depending on where the player is at the moment of impact
        Vector3 temporal = gameObject.transform.InverseTransformPoint(player.transform.position);

        //We're above or below
        if (Mathf.Abs(temporal.x) > Mathf.Abs(temporal.y))
        {
            gameObject.transform.localScale = new Vector3(0.5f, gameObject.transform.localScale.y);
        }

        //We're to the left or to the right
        else
        {
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 0.5f);   

        }

        //We enable the particle system
        ball_hit_particles.gameObject.SetActive(true);

        //We whiten the ball
        Coroutine lastroutine = StartCoroutine(FadeBall());

        //We play the drumroll
        GetComponent<AudioSource>().PlayOneShot(Drumroll);


        //we wait for a bit
        yield return new WaitForSeconds(1.6f);
        GetComponent<AudioSource>().PlayOneShot(Launching_Sound);
        yield return new WaitForSeconds(0.4f);

        //We dewhite the ball
        StopCoroutine(lastroutine);
        Color temporal2 = whiteball.GetComponent<SpriteRenderer>().color;
        temporal2.a = 0;
        whiteball.GetComponent<SpriteRenderer>().color = temporal2;
        Color temporal3 = gameObject.GetComponent<SpriteRenderer>().color;
        temporal3.a = 1;
        gameObject.GetComponent<SpriteRenderer>().color = temporal3;

        //We shoot the ball and restore the colors of everything
        gameObject.GetComponent<Rigidbody2D>().velocity = velocity*16;
        gameObject.GetComponent<Rigidbody2D>().drag = 0.25f;
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = angularvelocity;
        gameObject.transform.localScale = new Vector3(0.8f, 0.8f);
        //black_background.CrossFadeAlpha(0.0f, 0.0f, false);
        StopCoroutine(lastroutine2);
        background.color = new Color(1.0f, 1.0f, 1.0f);
        foreach (SpriteRenderer s in Foreground.GetComponentsInChildren<SpriteRenderer>())
        {
            s.color = new Color(1.0f, 1.0f, 1.0f);
        }

        //We stop the particles
        ball_hit_particles.gameObject.SetActive(false);

        ball_travelling_particles.gameObject.SetActive(true);

        //We reset the player's animations and values
        player.GetComponent<Animator>().enabled = true;
        player.transform.GetChild(1).GetComponent<Animator>().enabled = true;
        player.GetComponent<Animator>().SetBool("Dashing", false);
        player.GetComponent<Animator>().SetBool("moving", false);

        player.GetComponent<movement>().is_dashing = false;
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
        //player.GetComponent<movement>().can_move = true;
        //player.GetComponent<movement>().can_interact = true;


        //We tell the script the ball has been fired!
        ball_fired = true;

        //We initiate the combo counter
        Combo_Counter.gameObject.SetActive(true);

    }

    IEnumerator Finish_Ball()
    {
        GetComponent<AudioSource>().pitch = 1.0f;
        if (combo > player_data.playerdata.global_variables[19])
            player_data.playerdata.global_variables[19] = combo;      
        if (combo > 103)
        {
            player_data.playerdata.global_variables[20] = 1;
            Leaderboard.GetComponent<popup_text>().messages[1] = "* #1 ñ.ç" + player_data.playerdata.global_variables[19].ToString() + " bounces.çNice job, ñ. You balled.";
            Leaderboard.GetComponent<popup_text>().messages[2] = "* #2 Undyne.ç103 bounces.çNice job, Undyne. You balled.";
            Leaderboard.GetComponent<popup_text>().messages[3] = "* #3 Jimmy Hotpants.ç36 bounces.çNice job, Jimmy Hotpants. You balled.";
        }

        else if (combo > 50)
        {
            player_data.playerdata.global_variables[45] = 1;
            Leaderboard.GetComponent<popup_text>().messages[1] = "* #1 Undyne.ç103 bounces.çNice job, Undyne. You balled.";
            Leaderboard.GetComponent<popup_text>().messages[2] = "* #2 ñ.ç" + player_data.playerdata.global_variables[19].ToString() + " bounces.çNice job, ñ. You balled.";
            Leaderboard.GetComponent<popup_text>().messages[3] = "* #3 Jimmy Hotpants.ç36 bounces.çNice job, Jimmy Hotpants. You balled.";
        }
        else if (combo > 5)
        {
            player_data.playerdata.global_variables[46] = 1;
            Leaderboard.GetComponent<popup_text>().messages[1] = "* #1 Undyne.ç103 bounces.çNice job, Undyne. You balled.";
            Leaderboard.GetComponent<popup_text>().messages[2] = "* #2 Jimmy Hotpants.ç36 bounces.çNice job, Jimmy Hotpants. You balled.";
            Leaderboard.GetComponent<popup_text>().messages[3] = "* #3 ñ.ç" + player_data.playerdata.global_variables[19].ToString() + " bounces.çNice job, ñ. You balled.";
        }
        ball_stopped = false;
        Fireworks_Particles.Stop();
        yield return new WaitForEndOfFrame();
        gameObject.GetComponent<Rigidbody2D>().drag = 3.0f;
        gameObject.GetComponent<Rigidbody2D>().mass = 0.03f;
        combo = 0;
        Combo_Counter.gameObject.SetActive(false);
        Camera.main.GetComponent<camera>().player = player.transform;
        player.GetComponent<movement>().can_move = true;
        player.GetComponent<movement>().can_interact = true;
        player.GetComponent<movement>().can_dash = true;

        narrator_text.gameObject.SetActive(false);
        narrator_box.SetActive(false);
    }

    IEnumerator ShowText()
    {
        ball_fired = false;
        ball_travelling_particles.gameObject.SetActive(false);
        yield return new WaitForEndOfFrame();
        narrator_text.gameObject.SetActive(true);
        narrator_box.SetActive(true);
        narrator_text.text = "";

        //We set the message
        if (player_data.playerdata.global_variables[20] == 1)
        {
            if (combo > player_data.playerdata.global_variables[19])
            {
                message = "* New top score! Congratulations, you balled!çMake sure to visit the Snowdin kiosk to redeem your commemorative pair of socks.";
                GetComponent<AudioSource>().PlayOneShot(Victory_Sound);
                StartCoroutine(Fireworks());
            }
            else
            {
                message = "* You balled. Nice job!";
            }

        }
        else if (combo > 103)
        {
            message = "* New top score! Congratulations, you balled!çMake sure to visit the Snowdin kiosk to redeem your commemorative pair of socks.";
            GetComponent<AudioSource>().PlayOneShot(Victory_Sound);
            StartCoroutine(Fireworks());
        }
        else
        {
            message = "* You balled. Nice job!";
        }
        
        AutoTextScript.TypeText(narrator_text, message, 0.03f, dialogue_sound);
        ball_stopped = true;
    }

    IEnumerator ShowOutOfBoundsText()
    {
        ball_fired = false;
        ball_travelling_particles.gameObject.SetActive(false);
        GetComponent<AudioSource>().pitch = 0.5f;
        GetComponent<AudioSource>().PlayOneShot(Gaster_Bark);
        yield return new WaitForSeconds(0.25f);
        GetComponent<AudioSource>().PlayOneShot(Gaster_Bark);
        yield return new WaitForSeconds(0.75f);
        narrator_text.gameObject.SetActive(true);
        narrator_box.SetActive(true);
        narrator_text.text = "";
        message = "* OUCH.";
        AutoTextScript.TypeText(narrator_text, message, 0.03f, GetComponent<AudioSource>());
        ball_stopped = true;
    }


    //Coroutine to handle fireworks and playing their sounds. It uses a check with combo != 0 to see if the narrator text is still up or not, since FinishBall() resets it to 0 and Fireworks can't proc with
    //combo 0
    IEnumerator Fireworks()
    {
        yield return new WaitForSeconds(0.75f);
        Fireworks_Particles.Play();

        while (combo != 0)
        {
            GetComponent<AudioSource>().PlayOneShot(Fireworks_1);
            yield return new WaitForSeconds(Fireworks_Particles.main.startLifetime.constant);
            GetComponent<AudioSource>().PlayOneShot(Fireworks_2);
            yield return new WaitForSeconds(Fireworks_Particles.subEmitters.GetSubEmitterSystem(1).main.startLifetime.constant);
        }

    }

    IEnumerator FadeBall()
    {
        while (whiteball.GetComponent<SpriteRenderer>().color.a < 1)
        {
            Color temporal = whiteball.GetComponent<SpriteRenderer>().color;
            Color temporal2 = gameObject.GetComponent<SpriteRenderer>().color;
            temporal2.a -= 0.01f;
            temporal.a += 0.01f;
            whiteball.GetComponent<SpriteRenderer>().color = temporal;
            gameObject.GetComponent<SpriteRenderer>().color = temporal2;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Combo_Counter_Effect()
    {
        float shakeAmount = 25.0f;
        float delay = 0.03f;
        Combo_Counter.rectTransform.localPosition = original_combo_counter_position + Random.insideUnitSphere * shakeAmount;
        yield return new WaitForSeconds(delay);
        Combo_Counter.rectTransform.localPosition = original_combo_counter_position + Random.insideUnitSphere * shakeAmount;
        yield return new WaitForSeconds(delay);
        Combo_Counter.rectTransform.localPosition = original_combo_counter_position + Random.insideUnitSphere * shakeAmount;
        yield return new WaitForSeconds(delay);
        Combo_Counter.rectTransform.localPosition = original_combo_counter_position;
    }

    IEnumerator DarkenObjects()
    {

        float speed = 1.5f;
        while (background.color.r > 0.5f)
        {
            float temporal = background.color.r;
            float temporal2 = Mathf.Lerp(temporal, 0.5f, Time.deltaTime * speed);

            background.color = new Color(temporal2, temporal2, temporal2);
            foreach (SpriteRenderer s in Foreground.GetComponentsInChildren<SpriteRenderer>())
            {
                s.color = new Color(temporal2, temporal2, temporal2);
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
