﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Demo_Ravine_2_Conversation_1_Manager : MonoBehaviour {

    public AudioSource Gaster_Barks;

    public int state = 0;

    public GameObject Standing_Still_Conversation_Object;

    private GameObject player;
    public bool caninteract = false;

    public GameObject Conversation1_2;
    public GameObject Conversation2;

	// Use this for initialization
	void Start () {
        state = 0;
        caninteract = false;
	
	}
	
	// Update is called once per frame
	void Update () {


        if (state == 1)
        {
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 0) * player.GetComponent<movement>().movespeed;
        }

        else if (state == 2)
        {
            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && caninteract == true)
                {
                    BroadcastMessage("Execute_Dialogue");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (caninteract == true)
                    {
                        BroadcastMessage("Skip_Dialogue");
                    }
                }
            }
            if (gameObject.GetComponent<Demo_Ravine_2_Conversation_1>().complete == true)
            {
                state = 3;
                caninteract = false;
                StartCoroutine(ScriptedMovement2());
            }
        }


        else if (state == 3)
        {
            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && caninteract == true)
                {
                    Conversation1_2.BroadcastMessage("Execute_Dialogue");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (caninteract == true)
                    {
                        Conversation1_2.BroadcastMessage("Skip_Dialogue");
                    }
                }
            }
            if (Conversation1_2.GetComponent<Demo_Ravine_2_Conversation_1_2>().complete == true)
            {
                state = 4;
                caninteract = false;
            }
        }

        else if (state == 4)
        {
            Conversation2.SetActive(true);
            Standing_Still_Conversation_Object.SetActive(true);
            Destroy(gameObject);
        }


     }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            player = collider.gameObject;
            StartCoroutine(ScriptedMovement1());

        }
    }

    IEnumerator ScriptedMovement1()
    {
        if (player.GetComponent<movement>().is_dashing == true)
        {
            player.GetComponent<movement>().StopAllCoroutines();
            player.GetComponent<movement>().is_dashing = false;
            player.GetComponent<Animator>().SetBool("Dashing", false);
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
            player.GetComponent<Animator>().SetBool("moving", false);
        }
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;

        state = 1;
        player.GetComponent<Animator>().SetBool("moving", true);
        player.GetComponent<Animator>().SetInteger("direction", 2);

        yield return new WaitForSeconds(0.675f);
        state = 0;
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
        player.GetComponent<Animator>().SetBool("moving", false);

        Gaster_Barks.Play();
        yield return new WaitForSeconds(0.25f);
        Gaster_Barks.Play();
        yield return new WaitForSeconds(0.75f);

        BroadcastMessage("Execute_Dialogue");
        caninteract = true;
        state = 2;
    }

    IEnumerator ScriptedMovement2()
    {
        player.GetComponent<Animator>().SetInteger("direction", 1);
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<Animator>().SetInteger("direction", 2);
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<Animator>().SetInteger("direction", 3);
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<Animator>().SetInteger("direction", 4);
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<Animator>().SetInteger("direction", 3);
        Conversation1_2.BroadcastMessage("Execute_Dialogue");
        caninteract = true;
    }

}
