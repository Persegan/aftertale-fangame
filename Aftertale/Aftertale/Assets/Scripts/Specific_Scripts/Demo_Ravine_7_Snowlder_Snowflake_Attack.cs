﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_7_Snowlder_Snowflake_Attack : MonoBehaviour {

    public float gravity = -1.5f;
    public GameObject snowflake_2;

    public static int spawn_position;

	// Use this for initialization
	void Start () {
        
        GetComponent<Rigidbody2D>().AddTorque(150.0f);
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, gravity);

    }
	
	// Update is called once per frame
	void Update () {
        if (transform.localPosition.y <= -114)
        {
            for (int i = 0; i < 3; i++)
            {
                GameObject temp = GameObject.Instantiate(snowflake_2, new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y), Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
                temp.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 1);
            } 


            Destroy(gameObject);
        }

    }
}
