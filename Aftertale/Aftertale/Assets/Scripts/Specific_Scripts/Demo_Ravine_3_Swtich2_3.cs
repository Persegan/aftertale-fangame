﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Swtich2_3 : MonoBehaviour {


    public GameObject manager;
    public GameObject pressedswitch;
    public GameObject unpressedswitch;

    private bool reset;

    // Use this for initialization
    void Start () {
        reset = false;	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player" && manager.GetComponent<Demo_Ravine_3_Switch2>().switch1activated == true && manager.GetComponent<Demo_Ravine_3_Switch2>().switch2activated==true)
        {
            unpressedswitch.SetActive(false);
            manager.GetComponent<Demo_Ravine_3_Switch2>().switch3activated = true;
            pressedswitch.GetComponent<SpriteRenderer>().color = new Color(69/255.0f, 218 / 255.0f, 131 / 255.0f);
            manager.GetComponent<Demo_Ravine_3_Switch2>().complete();
        }

        else if (collider.tag == "Player" && manager.GetComponent<Demo_Ravine_3_Switch2>().switch2activated == false)
        {
            unpressedswitch.SetActive(false);
            pressedswitch.GetComponent<SpriteRenderer>().color = new Color(210 / 255.0f, 65 / 255.0f, 65 / 255.0f);
            reset = true;
        }
    }




    void OnTriggerExit2D(Collider2D collider)
    {
        if (reset == true && collider.tag == "Player")
        {
            manager.GetComponent<Demo_Ravine_3_Switch2>().switch1activated = false;
            manager.GetComponent<Demo_Ravine_3_Switch2>().switch2activated = false;
            manager.GetComponent<Demo_Ravine_3_Switch2>().switch3activated = false;
            manager.GetComponent<Demo_Ravine_3_Switch2>().resetswitches();
            reset = false;
        }
    }
}
