﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_6_Event_Starter_Destroyer : MonoBehaviour {

    public GameObject Switch;
    public GameObject Sackboy;
    public GameObject Conversation_1;
    public GameObject Ball_Leaderboard;

	// Use this for initialization
	void Start () {

        if (player_data.playerdata.global_variables[15] == 1)
        {
            Switch.GetComponent<Demo_Ravine_6_Timed_Switches_Puzzle>().EndPuzzleFast();
        }

        if (player_data.playerdata.global_variables[43] == 1)
        {
            Destroy(Sackboy);
        }

        if (player_data.playerdata.global_variables[44] == 1)
        {
            Destroy(Conversation_1);
        }

        if (player_data.playerdata.global_variables[20] == 1)
        {
            Ball_Leaderboard.GetComponent<popup_text>().messages[1] = "* #1 ñ.ç" + player_data.playerdata.global_variables[19].ToString() + " bounces.çNice job, ñ. You balled.";
            Ball_Leaderboard.GetComponent<popup_text>().messages[2] = "* #2 Undyne.ç103 bounces.çNice job, Undyne. You balled.";
            Ball_Leaderboard.GetComponent<popup_text>().messages[3] = "* #3 Jimmy Hotpants.ç36 bounces.çNice job, Jimmy Hotpants. You balled.";
        }
        else if (player_data.playerdata.global_variables[45] == 1)
        {
            Ball_Leaderboard.GetComponent<popup_text>().messages[1] = "* #1 Undyne.ç103 bounces.çNice job, Undyne. You balled.";
            Ball_Leaderboard.GetComponent<popup_text>().messages[2] = "* #2 ñ.ç" + player_data.playerdata.global_variables[19].ToString() + " bounces.çNice job, ñ. You balled.";
            Ball_Leaderboard.GetComponent<popup_text>().messages[3] = "* #3 Jimmy Hotpants.ç36 bounces.çNice job, Jimmy Hotpants. You balled.";
        }
        else if (player_data.playerdata.global_variables[46] == 1)
        {
            Ball_Leaderboard.GetComponent<popup_text>().messages[1] = "* #1 Undyne.ç103 bounces.çNice job, Undyne. You balled.";
            Ball_Leaderboard.GetComponent<popup_text>().messages[2] = "* #2 Jimmy Hotpants.ç36 bounces.çNice job, Jimmy Hotpants. You balled.";
            Ball_Leaderboard.GetComponent<popup_text>().messages[3] = "* #3 ñ.ç" + player_data.playerdata.global_variables[19].ToString() + " bounces.çNice job, ñ. You balled.";
        }


    }

    // Update is called once per frame
    void Update () {
	
	}
}
