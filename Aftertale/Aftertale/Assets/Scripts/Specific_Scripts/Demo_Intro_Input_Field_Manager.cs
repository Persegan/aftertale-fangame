﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Intro_Input_Field_Manager : MonoBehaviour {

    public Text shown_text;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnEditString(string text)
    {
        shown_text.text = gameObject.GetComponent<InputField>().text;
        while (shown_text.text.Length < gameObject.GetComponent<InputField>().characterLimit)
        {
            shown_text.text += '_';
        }
    }

    public void OnFinishEditString()
    {
        player_data.playerdata.player_name = gameObject.GetComponent<InputField>().text;
    }
}
