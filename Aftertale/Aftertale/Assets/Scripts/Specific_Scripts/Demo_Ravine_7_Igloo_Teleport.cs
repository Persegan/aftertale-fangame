﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_7_Igloo_Teleport : MonoBehaviour {

    public GameObject Igloo_Start;
    public GameObject Igloo_Finish;

    public float camera_moving_speed = 2.5f;

    public float player_moving_speed = 2.0f;

    private static GameObject Player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player = collision.gameObject;
            if (gameObject == Igloo_Start)
            {
                StopAllCoroutines();
                StartCoroutine(Teleport_Start_Finish());
            }

            else if (gameObject == Igloo_Finish)
            {
                StopAllCoroutines();
                StartCoroutine(Teleport_Finish_Start());
            }
        }
    }



    IEnumerator Teleport_Start_Finish()
    {
        if (player_data.playerdata.global_variables[47] == 1)
        {
            player_data.playerdata.global_variables[49] = 1;
        }
        Camera.main.GetComponent<camera>().enabled = false;
        if (Player.GetComponent<movement>().is_dashing == true)
        {
            Player.GetComponent<movement>().StopAllCoroutines();
            Player.GetComponent<movement>().is_dashing = false;
            Player.GetComponent<Animator>().SetBool("Dashing", false);
            Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            Player.GetComponent<Rigidbody2D>().drag = Player.GetComponent<movement>().previousdrag;
            Player.GetComponent<Animator>().SetBool("moving", false);
        }
        Player.GetComponent<movement>().StopAllCoroutines();
        Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        Player.SetActive(false);

        Vector3 temporal = Igloo_Finish.transform.position;
        temporal.z = Camera.main.transform.position.z;
        temporal.x = -4.4f;
        while (Camera.main.transform.position != temporal)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, temporal, Time.deltaTime * camera_moving_speed);
            yield return new WaitForEndOfFrame();
        }

        Player.transform.position = Igloo_Finish.transform.position;
        Player.GetComponent<BoxCollider2D>().enabled = false;
        Player.SetActive(true);
        Player.GetComponent<movement>().enabled = false;
        Player.GetComponent<Animator>().SetBool("moving", true);
        Player.GetComponent<Animator>().SetInteger("direction", 3);
        Camera.main.GetComponent<camera>().enabled = true;
        Camera.main.GetComponent<camera>().maxX = -1.68f;
        Camera.main.GetComponent<camera>().minX = -4.4f;
        Camera.main.GetComponent<camera>().maxY = 5.27f;
        Camera.main.GetComponent<camera>().minY = -2.257f;


        while (Player.transform.position.y != Igloo_Finish.transform.position.y - 0.5f)
        {
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, new Vector3(Player.transform.position.x, Igloo_Finish.transform.position.y - 0.5f, Player.transform.position.z), Time.deltaTime * player_moving_speed);
            yield return new WaitForEndOfFrame();
        }
        Player.GetComponent<BoxCollider2D>().enabled = true;
        Player.GetComponent<Animator>().SetBool("moving", false);
        Player.GetComponent<movement>().enabled = true;
        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_interact = true;


    }

    IEnumerator Teleport_Finish_Start()
    {
        Camera.main.GetComponent<camera>().enabled = false;
        if (Player.GetComponent<movement>().is_dashing == true)
        {
            Player.GetComponent<movement>().StopAllCoroutines();
            Player.GetComponent<movement>().is_dashing = false;
            Player.GetComponent<Animator>().SetBool("Dashing", false);
            Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            Player.GetComponent<Rigidbody2D>().drag = Player.GetComponent<movement>().previousdrag;
            Player.GetComponent<Animator>().SetBool("moving", false);
        }
        Player.GetComponent<movement>().StopAllCoroutines();
        Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        Player.SetActive(false);
        Vector3 temporal = Igloo_Start.transform.position;
        temporal.z = Camera.main.transform.position.z;
        while (Camera.main.transform.position != temporal)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, temporal, Time.deltaTime * camera_moving_speed);
            yield return new WaitForEndOfFrame();
        }
        Player.SetActive(true);
        Player.transform.position = Igloo_Start.transform.position;
        Player.GetComponent<BoxCollider2D>().enabled = false;
        Player.GetComponent<Animator>().SetBool("moving", true);
        Player.GetComponent<Animator>().SetInteger("direction", 3);
        Player.GetComponent<movement>().enabled = false;
        Camera.main.GetComponent<camera>().enabled = true;
        Camera.main.GetComponent<camera>().maxX = 4.38f;
        Camera.main.GetComponent<camera>().minX = -4.4f;
        Camera.main.GetComponent<camera>().maxY = 0.53f;
        Camera.main.GetComponent<camera>().minY = -2.257f;

        while (Player.transform.position.y != Igloo_Start.transform.position.y - 0.5f)
        {
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, new Vector3(Player.transform.position.x, Igloo_Start.transform.position.y - 0.5f, Player.transform.position.z), Time.deltaTime * player_moving_speed);
            yield return new WaitForEndOfFrame();
        }
        Player.GetComponent<BoxCollider2D>().enabled = true;
        Player.GetComponent<Animator>().SetBool("moving", false);
        Player.GetComponent<movement>().enabled = true;
        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_interact = true;
    }
}
