﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Switch2 : MonoBehaviour {

    public GameObject switch1pressed;
    public GameObject switch2pressed;
    public GameObject switch3pressed;

    public GameObject switch1unpressed;
    public GameObject switch2unpressed;
    public GameObject switch3unpressed;

    public bool switch1activated;
    public bool switch2activated;
    public bool switch3activated;

    public GameObject spikes;

	// Use this for initialization
	void Start () {
        switch1activated = false;
        switch2activated = false;
        switch3activated = false;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void resetswitches()
    {
        switch1unpressed.SetActive(true);
        switch2unpressed.SetActive(true);
        switch3unpressed.SetActive(true);
    }

    public void complete()
    {
        switch1unpressed.SetActive(false);
        switch2unpressed.SetActive(false);
        switch3unpressed.SetActive(false);
        spikes.SetActive(false);
        player_data.playerdata.global_variables[10] = 1;
    }
}
