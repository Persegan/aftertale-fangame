﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_12_Spikes_Reactivation_Zone : MonoBehaviour {

    public List<GameObject> Spikes;
    public List<GameObject> Reactivation_Zones;
    public int GlobalVariable;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player" && player_data.playerdata.global_variables[GlobalVariable]== 0)
        {
            player_data.playerdata.global_variables[GlobalVariable] = 1;
            for (int i = 0; i < Spikes.Count; i++)
            {
                Spikes[i].SetActive(true);
            }
            for (int i = 0; i < Reactivation_Zones.Count; i++)
            {
                Reactivation_Zones[i].SetActive(false);
            }
        }
    }
}
