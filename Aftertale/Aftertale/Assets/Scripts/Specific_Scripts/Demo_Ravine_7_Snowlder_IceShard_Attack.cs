﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_7_Snowlder_IceShard_Attack : MonoBehaviour {

    public float waittime = 1.2f;
    public float scalespeed = 0.05f;

    private bool should_explode = false;
    bool should_particle = true;

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        should_explode = false;
        StartCoroutine(WaitForExplosion());
        gameObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.5f);
    }
	
	// Update is called once per frame
	void Update () {
        if (should_explode == false)
        {
    
        }
        else
        {

            gameObject.GetComponent<Image>().CrossFadeAlpha(1.0f, 0.25f, false);
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            if  (transform.localScale.x < 5)
            {
                transform.localScale += new Vector3(75.0f, 75.0f, 0) * Time.deltaTime;

            }
           else
            {
                if (should_particle)
                {
                    GetComponent<ParticleSystem>().Play();
                    should_particle = false;
                }

                Destroy(gameObject, 1.2f);
            }

        }
		
	}

    IEnumerator WaitForExplosion()
    {
        yield return new WaitForSeconds(waittime);
        should_explode = true;
    }


}
