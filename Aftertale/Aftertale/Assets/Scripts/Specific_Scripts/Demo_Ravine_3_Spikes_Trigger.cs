﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Spikes_Trigger : MonoBehaviour {

    public GameObject Switch;
    public GameObject Spikes;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player" && player_data.playerdata.global_variables[1] == 0 && Switch.GetComponent<SpriteRenderer>().enabled == false)
        {
            RaiseSpikes();
        }
    }


    void RaiseSpikes()
    {
        Spikes.SetActive(true);
        player_data.playerdata.global_variables[1] = 1;
    }
}
