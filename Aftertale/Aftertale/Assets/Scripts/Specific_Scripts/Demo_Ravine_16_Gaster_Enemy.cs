﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_16_Gaster_Enemy : MonoBehaviour {


    public GameObject speech_bubble;
    public Text speech_bubble_text;

    public GameObject memory_fragment;
    public GameObject soul_afterimage;
    public GameObject dialogue_box;
    public GameObject black_background;
    public GameObject Warning_signs;
    public GameObject bone_attack;
    public GameObject lock_on_indicator;
    public GameObject snowflake_attack;
    public GameObject impact_zone;
    public int memory_cooldown;

    public AudioSource speech_sound;
    public AudioSource attack_sound;
    public AudioSource finger_snap;

    public string dialogue_message;

    private Animator anim;
    private GameObject player;
    private bool spawnafterimage;
    private bool finishedspeaking;
    private bool isspeaking;
    private string speakingmessage;
    private bool attack_relies_on_turn;
    private bool Gaster_Was_Pet;
    private bool detect_if_player_dashes;
    private float phase;

    private bool genocide = false;
    private bool genocide_warning = false;
    private int lastrandomnumber = -1;

    // Use this for initalization
    void Start()
    {
        player = GameObject.Find("Soul");
        phase = 0.0f;
        memory_cooldown = 1;
        attack_relies_on_turn = true;
        detect_if_player_dashes = false;
        anim = gameObject.GetComponent<Animator>();
        dialogue_box = GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box.gameObject;
        finishedspeaking = false;
        lastrandomnumber = -1;
    }

    // Update is called once per frame
    void Update()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (finishedspeaking == true)
            {
                finishedspeaking = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (isspeaking == true)
            {
                finishedspeaking = true;
                AutoTextScript.isfinished = true;
                speech_bubble_text.StopAllCoroutines();
                speakingmessage = speakingmessage.Replace("c", "\n");
                speech_bubble_text.text = speakingmessage;
            }

        }
        if (detect_if_player_dashes == true)
        {
            if (Input.GetKeyDown(KeyCode.X) && player.GetComponent<Rigidbody2D>().velocity.magnitude != 0)
            {
                phase = 1.0f;
                detect_if_player_dashes = false;
            }
        }

    }











    ///Auxiliar methods
    IEnumerator ShowDialogue(string message)
    {
        speakingmessage = message;
        isspeaking = true;
        speech_bubble.SetActive(true);
        speech_bubble_text.gameObject.SetActive(true);
        speech_bubble_text.text = "";
        AutoTextScript.TypeText(speech_bubble_text, message, 0.03f, null);
        while (AutoTextScript.isfinished == false)
        {
            //speech_sound.pitch = Random.Range (0.3f, 1.9f);
            speech_sound.Play();
            yield return new WaitForSeconds(0.1f);
        }
        finishedspeaking = true;
        while (finishedspeaking == true)
        {
            yield return new WaitForSeconds(0.1f);
        }
        //yield return new WaitForSeconds (2.0f);
        speech_bubble.SetActive(false);
        speech_bubble_text.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.0f);
    }

    public void attack(int turn)
    {
        StartCoroutine(attack_coroutine(turn));
    }

    public void dialogue(int turn)
    {
        StartCoroutine(dialogue_coroutine(turn));
    }

    public void ExecuteInteraction(int number)
    {
        StartCoroutine(ExecuteInteraction_coroutine(number));
    }

    public void ExecuteMemory(int number)
    {
        StartCoroutine(ExecuteMemory_coroutine(number));
    }

    IEnumerator spawn_afterimage(GameObject goal)
    {
        while (spawnafterimage == true)
        {
            GameObject temporal = Instantiate(soul_afterimage, player.transform.position, player.transform.rotation) as GameObject;
            temporal.GetComponent<Soul_Afterimage>().target = goal;
            yield return new WaitForSeconds(1.0f);
        }
        yield return new WaitForSeconds(0.01f);
        foreach (GameObject afterimage in GameObject.FindGameObjectsWithTag("soul_afterimage"))
        {
            Destroy(afterimage);
        }
    }

    IEnumerator attack_coroutine(int turn)
    {
        if (gameObject.GetComponent<enemy_data>().health <= gameObject.GetComponent<enemy_data>().max_health * 90 / 100 && genocide_warning == false && genocide == false)
        {
            genocide_warning = true;
        }

        if (genocide_warning == true && player.GetComponent<soul_movement>().fight_chosen == true)
        {
            StartCoroutine(Start_Genocide());
            yield break;
        }

        //Genocide AI
        if (genocide == true)
        {
            if (phase == 0.0f)
            {
                StartCoroutine(genocide_attack_0());
            }

            else if (phase == 1.0f)
            {
                StartCoroutine(genocide_attack_1());
            }

            else if (phase == 2.0f)
            {
                StartCoroutine(genocide_attack_2());
            }

            else if (phase == 3.0f)
            {
                StartCoroutine(genocide_attack_3());
            }

            else if (phase == 4.0f)
            {
                StartCoroutine(genocide_attack_4());
            }
            else
            {
                if (GetComponent<enemy_data>().health <= 47)
                {
                    StartCoroutine(genocide_final_attack());
                    yield break;
                }
                else
                {
                    int random_number = Random.Range(1, 4);
                    if (lastrandomnumber == -1)
                    {
                        lastrandomnumber = random_number;
                    }
                    else
                    {
                        while (random_number == lastrandomnumber)
                        {
                            random_number = Random.Range(1, 4);
                        }
                        lastrandomnumber = random_number;
                    }
                    switch (random_number)
                    {
                        case 0:
                            StartCoroutine(genocide_random_attack_0());
                            break;

                        case 1:
                            StartCoroutine(genocide_random_attack_1());
                            break;

                        case 2:
                            StartCoroutine(genocide_random_attack_2());
                            break;

                        case 3:
                            StartCoroutine(genocide_random_attack_3());
                            break;
                    }
                }

            }
        }

        //Pacifist AI
        if (genocide == false)
        {
            if (attack_relies_on_turn == true)
            {
                if (turn == 0)
                {
                    StartCoroutine(attack0());
                }
                else if (turn == 1)
                {
                    yield return ShowDialogue("OKAY, SORRY. THAT WAS UNFAIR. IT'S JUST THAT,THIS GAME IS STILL UNDER DEVELOPMENT.");
                    yield return ShowDialogue("AND IT FALLS ON ME, THE LOVABLE W.D.GASTER TO SAFEGUARD THIS PRECIOUS TIMELINE UNTIL IT CAN ADVANCE.");
                    string message = "SO, PLEASE COOPERATE WITH ME HERE. DON'T MOVE. AND DON'T <color=red>DASH</color> EITHER. YOU KNOW, USING THE <color=red>X KEY</color>.";
                    StartCoroutine(attack_dash_failed(message));
                }
                else
                {
                    StartCoroutine(attack_placeholder());
                }
            }
            else
            {
                if (phase == 0.0f)
                {
                    string message = "WOWIE! HOW CONSIDERATE. YOU DIDN'T <color=red>DASH</color> AT ALL!GREAT, JUST KEEP YOURSELF FROM <color=red>HITTING X</color>, AND I'LL MAKE THIS QUICK.";
                    StartCoroutine(attack_dash_failed(message));

                }
                else if (phase == 0.1f)
                {
                    string message = "WOWIE! HOW CONSIDERATE. YOU DIDN'T <color=red>DASH</color> AT ALL!GREAT, JUST KEEP YOURSELF FROM <color=red>HITTING X</color>, AND I'LL MAKE THIS QUICK.";
                    StartCoroutine(attack_dash_failed(message));
                }
                else if (phase == 0.2f || phase > 0.2f && phase < 1.1f)
                {
                    string message = ". . .";
                    StartCoroutine(attack_dash_failed(message));
                }
                else if (phase == 1.1f)
                {
                    StartCoroutine(attack1());
                }
                else if (phase == 1.2f)
                {
                    StartCoroutine(attack2());
                }
                else if (phase == 1.3f)
                {
                    StartCoroutine(attack3());
                }
                else if (phase == 1.4f)
                {
                    StartCoroutine(attack4());
                }
                else if (phase == 1.5f)
                {
                    StartCoroutine(attack5());
                }
                else if (phase == 1.6f)
                {
                    StartCoroutine(attack6());
                }
                else if (phase == 1.7f)
                {
                    StartCoroutine(attack7());
                }
                else if (phase == 1.8f)
                {
                    StartCoroutine(attack8());
                }
                else if (phase == 2.0) //the new patterns that will be seen in phase 2 are introduced
                {
                    StartCoroutine(attack9());
                }
                else if (phase == 2.1f)
                {
                    StartCoroutine(attack10());
                }
                else if (phase >= 3.0f && phase <= 4.9f)
                {
                    if (memory_cooldown == 0 && player.GetComponent<soul_movement>().memory_chosen == true)
                    {
                        StartCoroutine(final_attack());
                    }
                    else if (memory_cooldown == 0 && player.GetComponent<soul_movement>().memory_chosen == true)
                    {
                        int random_number = Random.Range(1, 5);
                        if (lastrandomnumber == -1)
                        {
                            lastrandomnumber = random_number;
                        }
                        else
                        {
                            while (random_number == lastrandomnumber)
                            {
                                random_number = Random.Range(1, 5);
                            }
                            lastrandomnumber = random_number;
                        }

                        switch (random_number)
                        {
                            case 1:
                                StartCoroutine(random_memory_attack_1());
                                break;
                            case 2:
                                StartCoroutine(random_memory_attack_2());
                                break;
                            case 3:
                                StartCoroutine(random_memory_attack_3());
                                break;
                            case 4:
                                StartCoroutine(random_memory_attack_4());
                                break;
                            default:

                                break;
                        }
                    }
                    else
                    {
                        int random_number = Random.Range(1, 5);
                        if (lastrandomnumber == -1)
                        {
                            lastrandomnumber = random_number;
                        }
                        else
                        {
                            while (random_number == lastrandomnumber)
                            {
                                random_number = Random.Range(1, 5);
                            }
                            lastrandomnumber = random_number;
                        }
                        switch (random_number)
                        {
                            case 1:
                                StartCoroutine(random_attack_1());
                                break;
                            case 2:
                                StartCoroutine(random_attack_2());
                                break;
                            case 3:
                                StartCoroutine(random_attack_3());
                                break;
                            case 4:
                                StartCoroutine(random_attack_4());
                                break;
                            default:

                                break;
                        }

                    }
                }
                else if (phase == 5.0f)
                {
                    StartCoroutine(MemoryCompletedAttack());
                }
                else
                {
                    yield return new WaitForSeconds(2.5f);
                    GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
                }
            }
        }

        else if (genocide == true)
        {

        }

        yield return new WaitForSeconds(0.0f);
    }

    IEnumerator dialogue_coroutine(int turn)
    {
        if (genocide == false)
        {
            if (phase == 2.0)
            {
                GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "Tastes like hot dog.";
            }
            else if (phase < 2.0f || phase > 2.0f)
            {
                int random_number = Random.Range(0, 10);
                switch (random_number)
                {
                    case 0:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "What an adograble enemy.";
                        break;
                    case 1:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You ask Gaster for a paws to catch your breath.";
                        break;
                    case 2:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You wonder how good Gaster is at making woofles.";
                        break;
                    case 3:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "It looks like Gaster's been having it ruff.";
                        break;
                    case 4:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You feel like you could go for a pupsicle right now.";
                        break;
                    case 5:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You wonder if Gaster's taking care of his canines.";
                        break;
                    case 6:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "Something about Gaster's stance makes him a bit terrierfying.";
                        break;
                    case 7:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You ask Gaster about his knowledge in barkeology.";
                        break;
                    case 8:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You get the sudden urge of eating some pupperoni.";
                        break;
                    case 9:
                        GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You wonder how far Gaster's shar peis can see.";
                        break;

                    default:

                        break;
                }
            }
            else
            {
                GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "BARK!";
            }
        }

        if (genocide == true)
        {
            GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "The true battle has begun.";
        }
        yield return new WaitForSeconds(0.0f);

    }

    IEnumerator ExecuteInteraction_coroutine(int number)
    {
        yield return new WaitForSeconds(0.0f);

        if (genocide == false)
        {
            switch (number)
            {
                case 0:
                    GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "ATK 6  DEF 10 \n He's big, he's fluffy, he's loud.";
                    GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("ATK 6  DEF 10 \n He's big, he's fluffy, he's loud.");
                    break;
                case 1:
                    GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "It's a critical pet! \n" + "Gaster Dog's attack fell for this turn! \n" + "You recover 1 HP!";
                    GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("It's a critical pet! \n" + "Gaster Dog's attack fell for this turn! \n" + "You recover 1 HP!");
                    gameObject.GetComponent<enemy_data>().attack -= 1;
                    Gaster_Was_Pet = true;
                    player.GetComponent<soul_movement>().Recover_health(1);
                    break;
                case 2:
                    GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You decide to wait and see how events unfold.";
                    GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("You decide to wait and see how events unfold.");
                    break;


                default:
                    break;
            }
        }

        else if (genocide == true)
        {
            switch (number)
            {
                case 0:
                    GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "ATK 66  DEF 666 \n You have a feeling it's Gaster time.";
                    GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("ATK 66  DEF 666 \n You have a feeling it's Gaster time.");
                    break;
                case 1:
                    GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "Gaster refuses to be pet.";
                    GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("Gaster refuses to be pet.");
                    break;
                case 2:
                    GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "You decide to wait and see how events unfold. Which taking into account your situation may not be the best idea.";
                    GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("You decide to wait and see how events unfold. Which taking into account your situation may not be the best idea.");
                    break;


                default:
                    break;
            }
        }


    }

    IEnumerator ExecuteMemory_coroutine(int number)
    {
        yield return new WaitForSeconds(0.0f);
        switch (number)
        {
            case 0:
                GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "Bit by bit, pieces of your past begin to fall back into place. You remember your promise to Gaster.";
                GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("Bit by bit, pieces of your past begin to fall back into place. You remember your promise to Gaster.");
                phase = 5.0f;
                break;
            case 1:
                GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "Bit by bit, pieces of your past begin to fall back into place. You remember the events before the last reset.";
                GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("Bit by bit, pieces of your past begin to fall back into place. You remember the events before the last reset.");
                phase = 5.0f;
                break;
            case 2:
                GameObject.Find("Canvas").GetComponent<battle_manager>().dialogue_box_message = "Bit by bit, pieces of your past begin to fall back into place. You remember the events about last time you fought.";
                GameObject.Find("Canvas").GetComponent<battle_manager>().SetDialogueBox("Bit by bit, pieces of your past begin to fall back into place. You remember the events about last time you fought.");
                phase = 5.0f;
                break;


            default:
                break;
        }
    }
    //-
    //This attack spawns slow moving bones from right to left
    IEnumerator attack0()
    {
        yield return StartCoroutine(ShowDialogue("HELLO THERE, HUMAN! IT IS I, THE LOVABLE W. D. GASTER. FANCY MEETING YOU HERE, OUTSIDE OF TIME AND SPACE."));
        yield return StartCoroutine(ShowDialogue("THAT LOOK ON YOUR FACE. DON'T TELL ME. YOU DON'T REMEMBER THE LAST TIME WE PLAYTESTED TOGETHER?"));
        yield return StartCoroutine(ShowDialogue("MAYBE I'VE MISTAKEN YOU FOR SOMEONE ELSE. MY BAD. IN THAT CASE, JUST STAND STILL FOR ME.TAKE YOUR FINGERS OFF THE <color=red>ARROW KEYS</color> AND JUST RELAX."));
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        //yield return new WaitForSeconds(0.5f);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        Vector3 spawn_helper = new Vector3(249, -14, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's instantiate more bones
        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(249, -34, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(249, -54, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.5f);
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(249, -182, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(249, -162, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate bones from the left
        yield return new WaitForSeconds(2.0f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(-249, -34, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(-249, -54, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.5f);
        spawn_helper = new Vector3(-249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(-249, -182, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.0f);
        spawn_helper = new Vector3(-249, -162, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(5.0f);
        yield return new WaitForSeconds(1.0f);
        anim.Play("Gaster_enemy_still", 0);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns the first elevator bones!
    IEnumerator attack1()
    {
        yield return StartCoroutine(ShowDialogue("OKAY. YOU'RE A SUPER BAD LISTENER. BUT TRY FOR ME. IF YOU BEAT ME, THERE ISN'T ANYTHING AFTER ME. BARK BARK."));
        yield return StartCoroutine(ShowDialogue("DO YOU UNDERSTAND WHAT I'M SAYING? YOU'LL BE DESTROYING THE TIMELINE IF YOU FINISH THIS PLAYTEST."));
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        //Bones from prior pattern for conditioning.
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.22f;
        Vector3 spawn_helper = new Vector3(249, -64, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(249, -152, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(249, -64, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //First Elevators
        yield return new WaitForSeconds(1.4f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 4;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.4f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.18f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, 6, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 0.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, -224, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Wave 2
        yield return new WaitForSeconds(2.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, 46, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 0.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, -184, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Wave 3
        yield return new WaitForSeconds(2.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, -44, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 0.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, -274, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Wave 4
        yield return new WaitForSeconds(2.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, 6, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 0.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        spawn_helper = new Vector3(249, -224, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(5.0f);
        anim.Play("Gaster_enemy_still", 0);
        phase += 0.1f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns a zipper of fast moving bones from top to bottom
    IEnumerator attack2()
    {

        yield return ShowDialogue(" REALLY? YOU STILL INTEND TO TRY AND DEFEAT ME? WHAT DO YOU STAND TO GAIN? WHAT ARE YOU HOPING FOR? ");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);
        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.28f;
        Vector3 spawn_helper = new Vector3(95.9f, 46.0f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate the rest of the bones
        yield return new WaitForSeconds(1.2f);
        spawn_helper = new Vector3(-100.9f, 46.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.2f);
        spawn_helper = new Vector3(95.9f, 46.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.2f);
        spawn_helper = new Vector3(-100.9f, 46.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        /*
               //Foreshadow Next Pattern
               yield return new WaitForSeconds(1.2f);
               spawn_helper = new Vector3(150.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               spawn_helper = new Vector3(-90.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               //wave2
               yield return new WaitForSeconds(1.0f);
               spawn_helper = new Vector3(160.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               spawn_helper = new Vector3(-80.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               //wave3
               yield return new WaitForSeconds(0.8f);
               spawn_helper = new Vector3(170.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               spawn_helper = new Vector3(-70.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               //wave4
               yield return new WaitForSeconds(0.8f);
               spawn_helper = new Vector3(160.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               spawn_helper = new Vector3(-80.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               //wave5
               yield return new WaitForSeconds(0.8f);
               spawn_helper = new Vector3(150.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
 
               spawn_helper = new Vector3(-90.9f, 46.0f, -2021.14f);
               temp = Instantiate(bone_attack) as GameObject;
               temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
               temp.GetComponent<RectTransform>().localPosition = spawn_helper;
               temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
               temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
               */
        yield return new WaitForSeconds(3.0f);

        anim.Play("Gaster_enemy_still", 0);
        phase = 1.3f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }

    }

    //This attack spawns a zipper of slow moving bones from bottom right to top right, and a zipper of fast moving bones from top left to bottom left
    IEnumerator attack3()
    {
        yield return ShowDialogue("OH. I THINK I UNDERSTAND. YOU MUST JUST REALLY LIKE SPENDING TIME WITH ME, ISN'T THAT IT?");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        //Let's instantiate Bone 1 Right
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.22f;
        Vector3 spawn_helper = new Vector3(90.9f, -274.4f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Bone 1 Left
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        spawn_helper = new Vector3(-95.9f, 54.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Left Right Left Right from here on.
        yield return new WaitForSeconds(1.6f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        spawn_helper = new Vector3(90.9f, -274.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        spawn_helper = new Vector3(-95.9f, 54.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //wave3
        yield return new WaitForSeconds(1.6f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        spawn_helper = new Vector3(90.9f, -274.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        spawn_helper = new Vector3(-95.9f, 54.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //wave4
        yield return new WaitForSeconds(1.6f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        spawn_helper = new Vector3(90.9f, -274.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        spawn_helper = new Vector3(-95.9f, 54.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //wave5
        yield return new WaitForSeconds(1.6f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        spawn_helper = new Vector3(90.9f, -274.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 3.0f;
        spawn_helper = new Vector3(-95.9f, 54.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(3.1f);

        anim.Play("Gaster_enemy_still", 0);
        phase = 1.4f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack introduces super speed bones. Generally speaking, spawn them between -22 and 215.
    IEnumerator attack4()
    {
        yield return ShowDialogue("HUMAN... I KNOW HOW YOU MUST FEEL. BUT PLEASE,YOU JUST HAVE TO WAIT FOR THE FULL DEMO.I DON'T WANT TO HAVETO HURT YOU, YOU KNOW.");
        finger_snap.Play();
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        yield return new WaitForSeconds(0.5f);
        //We set up general parameters for spawning super speed bones
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

        //We set up specific parameters for spawning a super speed bone
        Vector3 spawn_helper = new Vector3(413.6f, -81.8f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We spawn the rest of the bones
        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -202.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        spawn_helper = new Vector3(413.6f, -125.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -45.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        spawn_helper = new Vector3(413.6f, -195.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -73.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(3.1f);

        anim.Play("Gaster_enemy_still", 0);
        phase = 1.5f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns a zipper of bones from top to bottom plus super speed bones
    IEnumerator attack5()
    {

        yield return ShowDialogue("NO. NOT EVEN FOR ME, YOU WON'T LEAVE?");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);
        //We instantiate the first top to bottom bone
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;
        Vector3 spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate the second top to bottom bone
        yield return new WaitForSeconds(1.5f);
        spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We set up generic values to instantiate two super speed bones
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

        //We set up specific parameters for spawning a first super speed bone
        yield return new WaitForSeconds(0.2f);
        spawn_helper = new Vector3(413.6f, -111.8f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We set up specific parameters for spawning a second super speed bone
        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -70.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We set up general parameters to instantiate two more top to bottom bones
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;

        //We set up specific parameteres to spawn a first top to bottom bone
        yield return new WaitForSeconds(1.1f);
        spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        //We set up specific parameters to spawn a second top to bottom bone
        yield return new WaitForSeconds(1.5f);
        spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We set up generic values to instantiate two super speed bones
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

        //We set up specific parameters for spawning a first super speed bone
        yield return new WaitForSeconds(0.2f);
        spawn_helper = new Vector3(413.6f, -30.8f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We set up specific parameters for spawning a second super speed bone
        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -200.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We set up general parameters to instantiate the final two top to bottom bones
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;

        //We set up specific parameteres to spawn a first top to bottom bone
        yield return new WaitForSeconds(1.1f);
        spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        //We set up specific parameters to spawn a second top to bottom bone
        yield return new WaitForSeconds(1.5f);
        spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We set up generic values to instantiate two super speed bones
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

        //We set up specific parameters for spawning a first super speed bone
        yield return new WaitForSeconds(0.2f);
        spawn_helper = new Vector3(413.6f, -130.8f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We set up specific parameters for spawning a second super speed bone
        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -55.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);


        yield return new WaitForSeconds(4.1f);

        anim.Play("Gaster_enemy_still", 0);

        phase = 1.6f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns the first rotating bones
    IEnumerator attack6()
    {
        yield return ShowDialogue("HUMAN, IF YOU KEEP THIS UP, I'M GOING TO BE FORCED TO USE EXTREME MEASURES TO ENSURE THE SAFETY OF THIS TIMELINE..");
        //Let's instantiate both bones

        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 3;
        bone_attack.GetComponent<Bone_Attack_Script>().rotationspeed = 100.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.5f;
        Vector3 spawn_helper = new Vector3(249, -22, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's instantiate more bones
        spawn_helper = new Vector3(-249, -211, -2021.14f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        GameObject temp2 = Instantiate(bone_attack) as GameObject;
        temp2.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp2.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp2.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's make it change directions
        yield return new WaitForSeconds(1.0f);
        temp.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        temp2.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

        yield return new WaitForSeconds(2.0f);
        temp.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
        temp2.GetComponent<Bone_Attack_Script>().direction = Vector3.down;

        yield return new WaitForSeconds(1.0f);
        temp.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        temp2.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

        yield return new WaitForSeconds(2.0f);
        temp.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        temp2.GetComponent<Bone_Attack_Script>().direction = Vector3.up;


        yield return new WaitForSeconds(1.0f);
        anim.Play("Gaster_enemy_still", 0);
        phase = 1.7f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns a rotating bone, has zipper coming from right and left, and has super speed bones (SFB from now on, Super Fast Bones). This attack also sets up beginning of phase 2. So, disables all buttons except memory.
    IEnumerator attack7()
    {
        yield return ShowDialogue("I'M GETTING IMPATIENT, HUMAN. THIS IS YOUR LAST CHANCE.");
        yield return ShowDialogue("ALT F4.");
        yield return ShowDialogue("THE X AT THE TOP OF THE WINDOW.");
        yield return ShowDialogue("TASK MANAGER.");
        yield return ShowDialogue("YOU HAVE A CHOICE HERE.YOU DON'T HAVE TO FIGHT ME.");

        yield return new WaitForSeconds(0.5f);
        StartCoroutine(auxiliar_rotating_bones(11.0f, new Vector3(190, -10, -2021.14f), 2, 3, true));
        // StartCoroutine(auxiliar_rotating_bones(11.0f, new Vector3(-190, -217, -2021.14f), 0, 3));
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(attack7_auxiliar_sidecoming_bones());
        yield return new WaitForSeconds(7.0f);
        StartCoroutine(attack7_auxiliar_superfastbones());
        yield return new WaitForSeconds(10.0f);
        phase = 1.8f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        yield return new WaitForSeconds(0.501f);
        GameObject.Find("Canvas").GetComponent<battle_manager>().fight.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().act.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().item.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        GameObject.Find("Memory").GetComponent<Button>().Select();
        anim.Play("Gaster_enemy_still", 0);
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }

    }

    //This attack begins phase 2, so it introduces the memory option
    IEnumerator attack8()
    {
        player.GetComponent<soul_movement>().can_move = false;
        //We make Gaster talk
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(ShowDialogue("WOWIE. SUPER LAME. ALRIGHT, I HAVE NO OTHER CHOICE."));
        yield return ShowDialogue("FOR THE SAFETY OF THE TIMELINE, I HAVE LOCKED YOUR MOMENT. I WILL FORCIBLY END THIS ENCOUNTER.");
        yield return ShowDialogue("SAY GOODBYE, HUMAN.");
        yield return new WaitForSeconds(3.0f);

        //Gaster gets ready to strike
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        //yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);

        //Blacken screen, save coordinates to instantiate memory fragment
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += 0.25f;
        spawn_helper.y -= 0.1f;
        spawn_helper.z = 0;
        AudioSource music = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        black_background.SetActive(true);

        for (float i = 0; i < 10; i++)
        {
            music.volume -= 0.05f;
            black_background.GetComponent<Image>().color = new Color(0, 0, 0, i / 20);
            yield return new WaitForSeconds(0.05f);
        }
        music.volume = 0.1f;

        //Instantiate memory fragment and make it blink. Then activate its box collider 2D.
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;

        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);

        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;
        spawnafterimage = true;
        StartCoroutine(spawn_afterimage(memory_temporal));

        player.GetComponent<soul_movement>().can_move = true;
        //We wait until the player picks up the fragment
        /*while (player.GetComponent<soul_movement>().memoryfragments == 0)
        {
            yield return new WaitForSeconds(0.01f);
        }*/


        //Once the fragment has been picked up
        spawnafterimage = false;
        foreach (GameObject afterimage in GameObject.FindGameObjectsWithTag("soul_afterimage"))
        {
            Destroy(afterimage);
        }
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        player.GetComponent<soul_movement>().StopAllCoroutines();
        player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        player.GetComponent<soul_movement>().can_move = false;
        //memory_temporal.GetComponent<BoxCollider2D>().enabled = false;
        music.volume = 1.0f;
        black_background.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);
        attack_sound.Play();
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        yield return new WaitForSeconds(2.0f);
        //anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        //yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);

        for (float i = 0; i < 10; i++)
        {
            music.volume -= 0.05f;
            black_background.GetComponent<Image>().color = new Color(0, 0, 0, i / 20);
            yield return new WaitForSeconds(0.05f);
        }
        music.Stop();
        yield return ShowDialogue("OKAY.");
        yield return ShowDialogue("NOT SURE HOW YOU DID THAT...");
        yield return ShowDialogue("SORT OF MAKING OLD GASTER HERE FEEL LIKE A FOOL.");
        yield return ShowDialogue("NO MORE PLAYING AROUND.");

        anim.Play("Gaster_enemy_still", 0);
        GameObject.Find("Canvas").GetComponent<battle_manager>().fight.gameObject.SetActive(true);
        GameObject.Find("Canvas").GetComponent<battle_manager>().act.gameObject.SetActive(true);
        GameObject.Find("Canvas").GetComponent<battle_manager>().item.gameObject.SetActive(true);
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(false);
        phase = 2.0f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        GameObject.Find("Second_Music").GetComponent<AudioSource>().Play();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack introduces claw swipe warning signs attacks and locking onto the soul attacks
    IEnumerator attack9()
    {
        yield return ShowDialogue("I WON'T HOLD BACK ANY LONGER, HUMAN. YOU MUST BE STOPPED.");
        //We start spawning bones, instantiate warning signs and make Gaster ready to strike once
        StartCoroutine(attack9_auxiliar_sidecoming_bones());
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.y += 0.00f;
        GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);

        //Gaster strikes once
        yield return new WaitForSeconds(1.5f);
        Destroy(temp);
        temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster gets ready to strike a second time, and does so
        yield return new WaitForSeconds(0.5f);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        Destroy(temp);
        spawn_helper.x += 0.3f;
        temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        yield return new WaitForSeconds(1.5f);
        Destroy(temp);
        temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);
        Vector3 Vector_temporal = gameObject.transform.position;
        gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(130.4008f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster locks onto the soul
        yield return new WaitForSeconds(0.5f);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        Destroy(temp);
        yield return new WaitForSeconds(0.5f);

        gameObject.transform.position = Vector_temporal;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().speed = 0.2f;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().target = player;
        temp = Instantiate(lock_on_indicator) as GameObject;
        //temp.GetComponent<Transform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        for (float i = 0; i < 1.5; i = i + 0.01f)
        {
            gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            yield return new WaitForSeconds(0.01f);

        }
        temp.GetComponent<Lock_On_Indicator_Script>().target = null;

        //Gaster strikes
        //temp.GetComponent<Animator>().Stop();
        temp.GetComponent<AudioSource>().Stop();
        yield return new WaitForSeconds(0.5f);

        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);

        gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        temp.GetComponent<BoxCollider2D>().enabled = true;
        temp.GetComponent<Animator>().Play("lock_on_strike", 0);
        //temp.GetComponent<Animator>().enabled = false;
        //temp.GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        gameObject.transform.position = Vector_temporal;
        Destroy(temp);

        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);
        phase = 2.1f;
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack introduces snow flakes
    IEnumerator attack10()
    {
        /* yield return ShowDialogue("Impressive. You've made it this far.");
         yield return ShowDialogue("Unfortunately, the programmer was lazy and didn't program any more attacks...");
         yield return ShowDialogue("Oh, this doesn't mean you bested me.");
         yield return ShowDialogue("This just means I'm gonna have to use my ultra secret attack.");
         yield return ShowDialogue("Are you ready, human?");
         yield return ShowDialogue("I'm just gonna close your game. Bye!");
         yield return ShowDialogue("Nah, just kidding, there are more attacks now. Let's go!");*/

        //We execute the animation
        anim.Play("Gaster_enemy_frost_slam", 0);
        attack_sound.Play();

        //We set up the SpawnHelper to instantiate the snowflakes
        Vector3 SpawnHelper = dialogue_box.transform.localPosition;
        snowflake_attack.GetComponent<enemy_data>().attack = GetComponent<enemy_data>().attack;

        //We instantiate a snowflake near the dialogue box, and set its parent to the canvas.
        GameObject temp = Instantiate(snowflake_attack);
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        SpawnHelper.x += dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
        temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);

        //We set up the snowflake variables: Direction, speed, etc.
        SpawnHelper.x = dialogue_box.GetComponent<RectTransform>().localPosition.x;
        SpawnHelper.y += 150;
        temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
        temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
        temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
        temp.GetComponent<Snowflake_Attack_Script>().speed_2 = 70.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.1f;

        //Let's summon more snowflakes!
        //Snowflake 2
        SpawnHelper = dialogue_box.transform.localPosition;
        temp = Instantiate(snowflake_attack);
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        SpawnHelper.x += dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
        temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);
        SpawnHelper.x = dialogue_box.GetComponent<RectTransform>().localPosition.x + 50;
        SpawnHelper.y += 170;
        temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
        temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
        temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
        temp.GetComponent<Snowflake_Attack_Script>().speed_2 = 60.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.15f;

        //Snowflake 3
        SpawnHelper = dialogue_box.transform.localPosition;
        temp = Instantiate(snowflake_attack);
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        SpawnHelper.x += dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
        temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);
        SpawnHelper.x = dialogue_box.GetComponent<RectTransform>().localPosition.x - 150;
        SpawnHelper.y += 120;
        temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
        temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
        temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
        temp.GetComponent<Snowflake_Attack_Script>().speed_2 = 80.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1.65f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.08f;

        //Snowflake 4
        SpawnHelper = dialogue_box.transform.localPosition;
        temp = Instantiate(snowflake_attack);
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        SpawnHelper.x -= dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
        temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);
        SpawnHelper.x = dialogue_box.GetComponent<RectTransform>().localPosition.x - 30;
        SpawnHelper.y += 150;
        temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
        temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
        temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
        temp.GetComponent<Snowflake_Attack_Script>().speed_2 = 76.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1.55f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.125f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_direction = 1;

        //Snowflake 5
        SpawnHelper = dialogue_box.transform.localPosition;
        temp = Instantiate(snowflake_attack);
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        SpawnHelper.x -= dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
        temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);
        SpawnHelper.x = dialogue_box.GetComponent<RectTransform>().localPosition.x + 35;
        SpawnHelper.y += 150;
        temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
        temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
        temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
        temp.GetComponent<Snowflake_Attack_Script>().speed_2 = 70.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.1f;

        //Snowflake 6
        SpawnHelper = dialogue_box.transform.localPosition;
        temp = Instantiate(snowflake_attack);
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        SpawnHelper.x -= dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
        temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);
        SpawnHelper.x = dialogue_box.GetComponent<RectTransform>().localPosition.x - 80;
        SpawnHelper.y += 150;
        temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
        temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
        temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
        temp.GetComponent<Snowflake_Attack_Script>().speed_2 = 70.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1.5f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.1f;
        temp.GetComponent<Snowflake_Attack_Script>().zig_zag_direction = 1;

        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_still", 0);
        yield return new WaitForSeconds(7.0f);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        phase = 3.0f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        memory_cooldown = 0;
        //player.GetComponent<soul_movement>().memory_chosen = false;
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }



    //Here are the attacks of Phase 2! This attack spawns two rounds of snowflakes with a zipper on the bottom of the screen, and also super fast bones.
    IEnumerator random_attack_1()
    {
        StartCoroutine(random_attack_1_auxiliar_zipper());
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_still", 0);
        yield return new WaitForSeconds(5.0f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(auxiliar_superfastbones(2, 0.2f, true));
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(auxiliar_superfastbones(2, 0.2f, false));
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(auxiliar_superfastbones(2, 0.2f, false));
        yield return new WaitForSeconds(4.0f);
        if (memory_cooldown > 0)
        {
            memory_cooldown--;
        }

        if (memory_cooldown == 0)
        {
            GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        }
        anim.Play("Gaster_enemy_still", 0);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns two rotating bones + the locking in claw attack from Gaster twice.
    IEnumerator random_attack_2()
    {
        StartCoroutine(auxiliar_rotating_bones(10.0f, new Vector3(190, -10, -2021.14f), 2, 3, false));
        yield return new WaitForSeconds(0.001f);
        StartCoroutine(auxiliar_rotating_bones(10.0f, new Vector3(-190, -217, -2021.14f), 0, 3, false));

        //This part of the code manages the lock in attacks
        GameObject temp;
        Vector3 Vector_temporal = gameObject.transform.position;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().speed = 0.2f;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().target = player;
        temp = Instantiate(lock_on_indicator) as GameObject;
        //temp.GetComponent<Transform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        for (float i = 0; i < 1.5; i = i + 0.01f)
        {
            gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            yield return new WaitForSeconds(0.01f);

        }
        temp.GetComponent<Lock_On_Indicator_Script>().target = null;

        //Gaster strikes
        //temp.GetComponent<Animator>().Stop();
        temp.GetComponent<AudioSource>().Stop();
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);

        gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        temp.GetComponent<BoxCollider2D>().enabled = true;
        temp.GetComponent<Animator>().Play("lock_on_strike", 0);
        // temp.GetComponent<Animator>().enabled = false;
        // temp.GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        gameObject.transform.position = Vector_temporal;
        Destroy(temp);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        anim.Play("Gaster_enemy_still", 0);


        yield return new WaitForSeconds(1.5f);

        //We execute the second lock in attack
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().speed = 0.2f;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().target = player;
        temp = Instantiate(lock_on_indicator) as GameObject;
        //temp.GetComponent<Transform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        for (float i = 0; i < 1.5; i = i + 0.01f)
        {
            gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            yield return new WaitForSeconds(0.01f);

        }
        temp.GetComponent<Lock_On_Indicator_Script>().target = null;

        //Gaster strikes
        //temp.GetComponent<Animator>().Stop();
        temp.GetComponent<AudioSource>().Stop();
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);

        gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        temp.GetComponent<BoxCollider2D>().enabled = true;
        temp.GetComponent<Animator>().Play("lock_on_strike", 0);
        //temp.GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        gameObject.transform.position = Vector_temporal;
        Destroy(temp);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        anim.Play("Gaster_enemy_still", 0);


        yield return new WaitForSeconds(1.5f);
        if (memory_cooldown > 0)
        {
            memory_cooldown--;
        }

        if (memory_cooldown == 0)
        {
            GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        }
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack uses warning signs strikes, super fast bones and a very slow zipper from top to bottom
    IEnumerator random_attack_3()
    {

        StartCoroutine(random_attack_3_auxiliar_zipper());



        //This code handles the warning signs. The zipper comes from the coroutines above this line.
        StartCoroutine(auxiliar_warning_sign(0.5f));
        yield return new WaitForSeconds(2.1f);
        StartCoroutine(auxiliar_warning_sign(0.5f));
        yield return new WaitForSeconds(2.1f);


        //We start the second round of attacks, and also instantiate super fast bones
        StartCoroutine(auxiliar_warning_sign(0.5f));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(auxiliar_superfastbones(2, 0.2f, false));
        yield return new WaitForSeconds(1.6f);
        StartCoroutine(auxiliar_warning_sign(0.5f));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(auxiliar_superfastbones(2, 0.2f, false));
        yield return new WaitForSeconds(1.6f);
        yield return new WaitForSeconds(1.5f);
        //anim.Play("Gaster_enemy_still", 0);

        if (memory_cooldown > 0)
        {
            memory_cooldown--;
        }

        if (memory_cooldown == 0)
        {
            GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        }
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns 3 rounds of snowflakes + rotating bones
    IEnumerator random_attack_4()
    {
        StartCoroutine(auxiliar_rotating_bones(15.0f, new Vector3(190, -10, -2021.14f), 2, 3, true));
        StartCoroutine(auxiliar_rotating_bones(15.0f, new Vector3(-190, -217, -2021.14f), 0, 3, false));
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_still", 0);
        yield return new WaitForSeconds(3.0f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_still", 0);
        yield return new WaitForSeconds(3.0f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_still", 0);
        yield return new WaitForSeconds(5.0f);
        if (memory_cooldown > 0)
        {
            memory_cooldown--;
        }

        if (memory_cooldown == 0)
        {
            GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        }
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }


    //This attack spawns a zipper from right to left, and a memory fragment at the end of it
    IEnumerator random_memory_attack_1()
    {
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        int numberofbones = 5;
        bool spawn_direction = false;
        float waittime = 1.5f;
        Vector3 spawn_helper;
        GameObject temp;
        for (int i = 0; i < numberofbones; i++)
        {

            if (spawn_direction == false)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                spawn_helper = new Vector3(249, -14, -2021.14f);
                spawn_direction = true;
            }
            else
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                spawn_helper = new Vector3(249, -202, -2021.14f);
                spawn_direction = false;
            }
            bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
            bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
            bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 4.25f;
            bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.6f;

            temp = Instantiate(bone_attack) as GameObject;
            temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
            temp.GetComponent<RectTransform>().localPosition = spawn_helper;
            temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waittime);
            anim.Play("Gaster_enemy_still", 0);
        }

        //spawn the memory fragment
        spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += 0.6f;
        spawn_helper.z = 0.1f;
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;
        Destroy(memory_temporal, 5.0f);
        Vector2 Force = new Vector2(-30, 0);
        memory_temporal.GetComponent<Rigidbody2D>().AddForce(Force);

        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);

        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;

        yield return new WaitForSeconds(5.0f);
        //player.GetComponent<soul_movement>().memory_chosen = false;
        memory_cooldown = 2;
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
       /* if (player.GetComponent<soul_movement>().memoryfragments == gameObject.GetComponent<enemy_data>().max_memory_fragments)
        {
            player.GetComponent<soul_movement>().all_memory_fragments_collected = true;
        }*/
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack spawns snowflakes, and a memory fragment at the top
    IEnumerator random_memory_attack_2()
    {
        //We call the snowflakes coroutines
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(2.5f);

        anim.Play("Gaster_enemy_still", 0);

        //We instantiate the fragment
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += Random.Range(-0.3f, 0.3f);
        spawn_helper.y += 0.5f;
        spawn_helper.z = 0.1f;
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;
        Destroy(memory_temporal, 5.0f);
        Vector2 Force = new Vector2(0, -15);
        memory_temporal.GetComponent<Rigidbody2D>().AddForce(Force);

        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);

        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;

        yield return new WaitForSeconds(5.0f);

        memory_cooldown = 2;
        //player.GetComponent<soul_movement>().memory_chosen = false;
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        /*if (Mathf.FloorToInt(player.GetComponent<soul_movement>().memoryfragments) == gameObject.GetComponent<enemy_data>().max_memory_fragments)
        {
            player.GetComponent<soul_movement>().all_memory_fragments_collected = true;
        }*/
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }

    //This attack has two warning claw sign attacks, then a memory fragment
    IEnumerator random_memory_attack_3()
    {
        int randomnumber = Random.Range(1, 3);
        Vector3 spawn_helper = dialogue_box.transform.position;
        if (randomnumber == 1)
        {
            Vector3 spawn_helper2 = dialogue_box.transform.position;
            spawn_helper.x += 0.3f;
            spawn_helper.y += 0.00f;
            GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
            anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);

            //Gaster strikes twice
            yield return new WaitForSeconds(0.5f);
            spawn_helper2 = dialogue_box.transform.position;
            spawn_helper2.y += 0.00f;
            GameObject temp2 = Instantiate(Warning_signs, spawn_helper2, transform.rotation) as GameObject;
            yield return new WaitForSeconds(1.0f);

            Destroy(temp);

            Vector3 Vector_temporal = gameObject.transform.position;
            gameObject.transform.position = new Vector3(1.167f, gameObject.transform.position.y, gameObject.transform.position.z);
            anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
            attack_sound.Play();
            temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);
            yield return new WaitForSeconds(0.5f);
            Destroy(temp2);
            Destroy(temp);
            temp = Instantiate(impact_zone, spawn_helper2, transform.rotation) as GameObject;
            gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
            anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
            attack_sound.Play();
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

            //Gaster waits, then returns to his normal position.

            yield return new WaitForSeconds(0.5f);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Destroy(temp);
            yield return new WaitForSeconds(0.5f);
            gameObject.transform.position = Vector_temporal;
            anim.Play("Gaster_enemy_still", 0, 0f);


            //Gaster gets ready to strike on middle, memory fragment spawns on right
            spawn_helper = dialogue_box.transform.position;
            spawn_helper2 = dialogue_box.transform.position;
            spawn_helper2.x += 0.25f;
            spawn_helper2.z = 0.1f;
            spawn_helper.y += 0.00f;
            temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
            anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);
            GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper2, transform.rotation) as GameObject;
            Destroy(memory_temporal, 5.0f);
            float delay = 0.05f;
            Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
            Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
            d.a = 0.2f;
            memory_temporal.GetComponent<SpriteRenderer>().color = d;
            yield return new WaitForSeconds(delay);
            memory_temporal.GetComponent<SpriteRenderer>().color = c;
            yield return new WaitForSeconds(delay);
            memory_temporal.GetComponent<SpriteRenderer>().color = d;
            yield return new WaitForSeconds(delay);
            memory_temporal.GetComponent<SpriteRenderer>().color = c;
            yield return new WaitForSeconds(delay);

            memory_temporal.GetComponent<BoxCollider2D>().enabled = true;


            yield return new WaitForSeconds(1.5f);
            Destroy(temp);
            gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
            anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
            temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
            attack_sound.Play();
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

            yield return new WaitForSeconds(0.5f);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Destroy(temp);
            yield return new WaitForSeconds(0.5f);
            anim.Play("Gaster_enemy_still", 0, 0f);
            gameObject.transform.position = Vector_temporal;


            yield return new WaitForSeconds(2.0f);
        }
        else if (randomnumber == 2)
        {
            Vector3 spawn_helper2 = dialogue_box.transform.position;
            spawn_helper.x -= 0.3f;
            spawn_helper.y += 0.00f;
            GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
            anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);

            //Gaster strikes twice
            yield return new WaitForSeconds(0.5f);
            spawn_helper2 = dialogue_box.transform.position;
            spawn_helper2.y += 0.00f;
            GameObject temp2 = Instantiate(Warning_signs, spawn_helper2, transform.rotation) as GameObject;
            yield return new WaitForSeconds(1.0f);

            Destroy(temp);
            Vector3 Vector_temporal = gameObject.transform.position;
            gameObject.transform.position = new Vector3(0.57f, gameObject.transform.position.y, gameObject.transform.position.z);
            anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
            attack_sound.Play();
            temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);
            yield return new WaitForSeconds(0.5f);
            Destroy(temp);
            Destroy(temp2);
            gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
            anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
            temp = Instantiate(impact_zone, spawn_helper2, transform.rotation) as GameObject;
            attack_sound.Play();
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

            //Gaster waits, then returns to his normal position.
            yield return new WaitForSeconds(0.5f);
            Destroy(temp);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            yield return new WaitForSeconds(0.5f);
            gameObject.transform.position = Vector_temporal;
            anim.Play("Gaster_enemy_still", 0, 0f);


            //Gaster gets ready to strike on middle, memory fragment spawns on right
            spawn_helper = dialogue_box.transform.position;
            spawn_helper2 = dialogue_box.transform.position;
            spawn_helper2.x -= 0.25f;
            spawn_helper2.z = 0.1f;
            spawn_helper.y += 0.00f;
            temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
            anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);
            GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper2, transform.rotation) as GameObject;
            Destroy(memory_temporal, 5.0f);
            float delay = 0.05f;
            Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
            Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
            d.a = 0.2f;
            memory_temporal.GetComponent<SpriteRenderer>().color = d;
            yield return new WaitForSeconds(delay);
            memory_temporal.GetComponent<SpriteRenderer>().color = c;
            yield return new WaitForSeconds(delay);
            memory_temporal.GetComponent<SpriteRenderer>().color = d;
            yield return new WaitForSeconds(delay);
            memory_temporal.GetComponent<SpriteRenderer>().color = c;
            yield return new WaitForSeconds(delay);

            memory_temporal.GetComponent<BoxCollider2D>().enabled = true;


            yield return new WaitForSeconds(1.5f);
            Destroy(temp);
            temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
            gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
            anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
            attack_sound.Play();
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

            yield return new WaitForSeconds(0.5f);
            Destroy(temp);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;

            yield return new WaitForSeconds(0.5f);
            gameObject.transform.position = Vector_temporal;
            anim.Play("Gaster_enemy_still", 0, 0f);

            yield return new WaitForSeconds(2.0f);
        }


        memory_cooldown = 2;
        //player.GetComponent<soul_movement>().memory_chosen = false;
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        /*if (Mathf.FloorToInt(player.GetComponent<soul_movement>().memoryfragments) == gameObject.GetComponent<enemy_data>().max_memory_fragments)
        {
            player.GetComponent<soul_movement>().all_memory_fragments_collected = true;
        }*/
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }


    //This attack spawns rotating bones, and also has a memory fragment rotating around
    IEnumerator random_memory_attack_4()
    {
        //We start the rotating bones coroutine
        StartCoroutine(auxiliar_rotating_bones(6.0f, new Vector3(190, -10, -2021.14f), 2, 2, true));
        StartCoroutine(auxiliar_rotating_bones(6.0f, new Vector3(-190, -217, -2021.14f), 0, 2, false));

        //We instantiate the fragment
        yield return new WaitForSeconds(1.0f);
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += Random.Range(0.1f, 0.3f);
        spawn_helper.y -= 0.07f;
        spawn_helper.z = 0.1f;
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;
        memory_temporal.GetComponent<memory_fragment>().speed = -150;
        memory_temporal.GetComponent<memory_fragment>().behaviour = 1;
        memory_temporal.GetComponent<memory_fragment>().center = dialogue_box.transform.position;
        Destroy(memory_temporal, 5.0f);

        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);

        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;

        yield return new WaitForSeconds(4.0f);
        memory_cooldown = 2;
        //player.GetComponent<soul_movement>().memory_chosen = false;
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        /*if (Mathf.FloorToInt(player.GetComponent<soul_movement>().memoryfragments) == gameObject.GetComponent<enemy_data>().max_memory_fragments)
        {
            player.GetComponent<soul_movement>().all_memory_fragments_collected = true;
        }*/
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
        anim.Play("Gaster_enemy_still", 0, 0f);
    }



    IEnumerator final_attack()
    {
        yield return ShowDialogue("WILL YOU STOP AT NOTHING TO DESTROY THIS TIMELINE?");
        yield return ShowDialogue("THIS IS ALL I HAVE LEFT. MY FINAL ATTACK! I CANNOT FAIL!");

        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_final_attack_zipper_1());
        yield return new WaitForSeconds(1.0f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
        StartCoroutine(auxiliar_final_attack_zipper_2());
        yield return new WaitForSeconds(5.0f);
        //StartCoroutine(auxiliar_superfastbones(14, 3.0f, true));
        StartCoroutine(auxiliar_superfastbones(11, 3.0f, true));
        yield return new WaitForSeconds(5.0f);
        StartCoroutine(auxiliar_rotating_bones(5.0f, new Vector3(190, -10, -2021.14f), 2, 2, true));
        StartCoroutine(auxiliar_rotating_bones(5.0f, new Vector3(-190, -217, -2021.14f), 0, 2, false));
        yield return new WaitForSeconds(5.0f);
        //StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(auxiliar_final_attack_lock_in());
        yield return new WaitForSeconds(10.0f);
        yield return StartCoroutine(auxiliar_final_attack_warning_signs());
        StartCoroutine(auxiliar_rotating_bones(5.0f, new Vector3(190, -10, -2021.14f), 2, 2, true));
        StartCoroutine(auxiliar_rotating_bones(5.0f, new Vector3(-190, -217, -2021.14f), 0, 2, false));
        yield return new WaitForSeconds(1.0f);
        //StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(auxiliar_final_attack_lock_in());
        // yield return StartCoroutine(auxiliar_final_attack_lock_in());

        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.z = 0.1f;
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;

        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);

        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;

        /*while (player.GetComponent<soul_movement>().memoryfragments != gameObject.GetComponent<enemy_data>().max_memory_fragments)
        {
            yield return new WaitForSeconds(0.1f);
        }*/
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        player.GetComponent<soul_movement>().StopAllCoroutines();
        player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        player.GetComponent<soul_movement>().can_move = false;

        yield return ShowDialogue("END OF THE LINE, HUMAN");

        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        yield return new WaitForSeconds(3.0f);
        anim.Play("Gaster_enemy_still", 0);
        yield return ShowDialogue(". . .");
        /*if (player.GetComponent<soul_movement>().memoryfragments == gameObject.GetComponent<enemy_data>().max_memory_fragments)
        {
            player.GetComponent<soul_movement>().all_memory_fragments_collected = true;
        }*/
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
        phase = 5.0f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        yield return new WaitForSeconds(0.501f);
        GameObject.Find("Canvas").GetComponent<battle_manager>().fight.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().act.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().item.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(true);
        GameObject.Find("Memory").GetComponent<Button>().Select();
    }

    IEnumerator MemoryCompletedAttack()
    {
        yield return ShowDialogue("BARK BARK. GUESS THAT IS IT THEN. THERE IS NOTHING BEYOND THIS POINT...");
        yield return ShowDialogue("BUT... IT WAS FUN IN A WAY, WASN'T IT? EVEN IF THIS IS THE END. IT'S NOT REALLY GOODBYE, IS IT?");
        yield return ShowDialogue("WE'LL MEET AGAIN, WON'T WE? AND NEXT TIME, WE REALLY WILL BE FRIENDS.");
        yield return ShowDialogue("THANKS FOR PLAYING, BARK BARK");
        Application.Quit();
    }


    //This is crazy rollercoaster attack.
    IEnumerator genocide_attack_0()
    {
        yield return ShowDialogue("I HAD THOUGHT MAYBE YOU WERE AN OLD FRIEND, COME BACK TO PLAY WITH ME AGAIN.");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(resizedialoguebox(new Vector3(0.2f, 1.0f, 1.0f), 5.0f));
        yield return ShowDialogue("AT LEAST NOW I CAN KILL YOU WITH A CLEANER CONSCIENCE.");
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        Vector3 box_original_position = dialogue_box.transform.localPosition;

        //upperbones, right side.
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7.5f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.0f;
        Vector3 spawn_helper = new Vector3(122, -33, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(174, -8, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(226, 17, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(278, 42, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //bottombones
        spawn_helper = new Vector3(174, -278, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(226, -253, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(278, -228, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(330, -203, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(382, -178, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(0.5f);
        /*Vector3 original_position = gameObject.transform.localPosition;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x-0.42f, gameObject.transform.position.y, gameObject.transform.position.z);
        (USE THIS LINE TO GO LEFT )gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, 1);*/
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
        attack_sound.Play();
        //yield return new WaitForSeconds(0.8f);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        anim.Play("Gaster_enemy_still", 0);

        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(379f, -114f, -1), 1.00f));
        //yield return new WaitForSeconds(4.0f);
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.8f);
        gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, 1);
        anim.Play("Gaster_enemy_still", 0);

        //LeftBonefield Begin
        //upperbones, leftside.
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7.5f;
        spawn_helper = new Vector3(-162, 42, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-214, 17, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-266, 42, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        /*spawn_helper = new Vector3(-318, 42, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);*/

        //bottombones
        spawn_helper = new Vector3(-6, -203, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-58, -178, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-110, -203, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-162, -228, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-214, -253, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-266, -228, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-318, -203, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(-370, -178, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
        attack_sound.Play();
        yield return new WaitForSeconds(0.8f);
        anim.Play("Gaster_enemy_still", 0);
        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(-370f, -114f, -1), 0.19f));
        //yield return new WaitForSeconds(4.0f);
        gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, 1);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
        attack_sound.Play();
        yield return new WaitForSeconds(0.8f);
        anim.Play("Gaster_enemy_still", 0);

        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(2f, -114f, -1), 0.89f));

        //gameObject.transform.localPosition = original_position;
        //gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, 1);
        yield return new WaitForSeconds(2.0f);
        yield return StartCoroutine(resizedialoguebox(new Vector3(1.0f, 1.0f, 1.0f), 5.0f));
        dialogue_box.transform.localPosition = box_original_position;
        phase = 1.0f;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        yield break;
    }

    //This is the mash potatoes attack.
    IEnumerator genocide_attack_1()
    {
        yield return ShowDialogue("STILL STANDING? MY MISTAKE.");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(resizedialoguebox(new Vector3(0.4f, 0.5f, 1.0f), 5.0f));
        yield return ShowDialogue("WORD OF ADVICE. STAND IN THE CENTER.");
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 4;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 6.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.00f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.16f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 2.5f;
        Vector3 spawn_helper = new Vector3(6, 6, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.16f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 2.4f;
        spawn_helper = new Vector3(-31, 6, -2021.14f);
        GameObject temp2 = Instantiate(bone_attack) as GameObject;
        temp2.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp2.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp2.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.16f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 2.6f;
        spawn_helper = new Vector3(43, 6, -2021.14f);
        GameObject temp3 = Instantiate(bone_attack) as GameObject;
        temp3.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp3.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp3.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        for (int i = 0; i < (int)bone_attack.GetComponent<Bone_Attack_Script>().lifetime; i++)
        {
            if (temp != null)
            {
                temp.GetComponent<Bone_Attack_Script>().elevator_speed += 0.1f;
                temp2.GetComponent<Bone_Attack_Script>().elevator_speed += 0.1f;
                temp3.GetComponent<Bone_Attack_Script>().elevator_speed += 0.1f;
                yield return new WaitForSeconds(1.0f);
            }
        }

        yield return new WaitForSeconds(0.8f);
        anim.Play("Gaster_enemy_still", 0);
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(resizedialoguebox(new Vector3(1.0f, 1.0f, 1.0f), 5.0f));
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        phase = 2.0f;
        yield break;

    }

    //This attack still has basically no purpose.
    IEnumerator genocide_attack_2()
    {

        Vector3 box_original_position = dialogue_box.transform.localPosition;

        yield return ShowDialogue("THAT LOOK. FRUSTRATION? HOW UNBECOMING OF A PLAYTESTER.");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(resizedialoguebox(new Vector3(0.4f, 0.5f, 1.0f), 5.0f));
        yield return ShowDialogue("WORD OF ADVICE. JUST GIVE UP.");
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 4;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.00f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = 0.16f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 3.0f;
        Vector3 spawn_helper = new Vector3(6, 6, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.6f);
        anim.Play("Gaster_enemy_still", 0);

        spawn_helper = new Vector3(-31, 6, -2021.14f);
        GameObject temp2 = Instantiate(bone_attack) as GameObject;
        temp2.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp2.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp2.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        spawn_helper = new Vector3(43, 6, -2021.14f);
        GameObject temp3 = Instantiate(bone_attack) as GameObject;
        temp3.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp3.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp3.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(4.2f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
        attack_sound.Play();
        yield return new WaitForSeconds(0.7f);
        anim.Play("Gaster_enemy_still", 0);
        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(130.0f, -114f, -1), 4.20f));

        yield return new WaitForSeconds(1.0f);
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_still", 0);

        //SFB BEGIN!
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 0.8f;

        yield return new WaitForSeconds(0.4f);
        //bottomrowbone
        spawn_helper = new Vector3(465f, -153f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //middlerowbone
        spawn_helper = new Vector3(465f, -119f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(.4f);
        //toprowbone
        spawn_helper = new Vector3(465f, -85f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.0f);
        //bottomrowbone
        spawn_helper = new Vector3(465f, -153f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //toprowbone
        spawn_helper = new Vector3(465f, -85f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.5f);
        //middlerowbone
        spawn_helper = new Vector3(465f, -119f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.6f);
        //bottomrowbone
        spawn_helper = new Vector3(465f, -153f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.6f);
        //middlerowbone
        spawn_helper = new Vector3(465f, -119f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //toprowbone
        spawn_helper = new Vector3(465f, -85f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.0f);
        //bottomrowbone
        spawn_helper = new Vector3(465f, -153f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //toprowbone
        spawn_helper = new Vector3(465f, -85f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.5f);
        //middlerowbone
        spawn_helper = new Vector3(465f, -119f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.4f);
        //bottomrowbone
        spawn_helper = new Vector3(465f, -153f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(.6f);
        //toprowbone
        spawn_helper = new Vector3(465f, -85f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //middlerowbone
        spawn_helper = new Vector3(465f, -119f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.5f);
        gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, 1);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
        attack_sound.Play();
        yield return new WaitForSeconds(0.7f);
        gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, 1);
        anim.Play("Gaster_enemy_still", 0);
        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(2.0f, -114f, -1), 4.20f));

        dialogue_box.transform.localPosition = box_original_position;
        dialogue_box.transform.localScale = new Vector3(1, 1, 1);

        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);

        phase = 3.0f;
        yield break;
    }

    //This attack is a SFB dodging minigame thing?
    IEnumerator genocide_attack_3()
    {

        Vector3 box_original_position = dialogue_box.transform.localPosition;

        yield return ShowDialogue("I CAN DO THIS ALL DAY.");
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(resizedialoguebox(new Vector3(0.2f, 1.0f, 1.0f), 5.0f));
        yield return ShowDialogue("HOW ABOUT YOU? RUNNING OUT OF HEALING ITEMS?");
        anim.Play("Gaster_enemy_Bone_Snap", 0, 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 0.5f;

        //Right Bone Wave
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

        //Right Bone 1 Position
        Vector3 spawn_helper = new Vector3(340f, -33.0f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 2 Position
        /*spawn_helper = new Vector3(340f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Right Bone 3 Position
        spawn_helper = new Vector3(340f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 4 Position
        /*spawn_helper = new Vector3(340f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Right Bone 5 Position
        spawn_helper = new Vector3(340f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 6 Position
        /*spawn_helper = new Vector3(340f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        yield return new WaitForSeconds(1.3f);

        // Left Wave
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

        //Left Bone 1 Position
        /*spawn_helper = new Vector3(-337f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Left Bone 2 Position
        spawn_helper = new Vector3(-337f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 3 Position
        /*spawn_helper = new Vector3(-337f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Left Bone 4 Position
        spawn_helper = new Vector3(-337f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 5 Position
        /*spawn_helper = new Vector3(-337f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Left Bone 6 Position
        spawn_helper = new Vector3(-337f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.3f);

        //Right Bone Wave 2
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

        //Right Bone 1 Position
        spawn_helper = new Vector3(340f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 2 Position
        /*spawn_helper = new Vector3(340f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Right Bone 3 Position
        spawn_helper = new Vector3(340f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 4 Position
        /*spawn_helper = new Vector3(340f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Right Bone 5 Position
        spawn_helper = new Vector3(340f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 6 Position
        spawn_helper = new Vector3(340f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.3f);

        // Left Wave 2
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

        //Left Bone 1 Position
        /*spawn_helper = new Vector3(-337f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Left Bone 2 Position
        spawn_helper = new Vector3(-337f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 3 Position
        spawn_helper = new Vector3(-337f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 4 Position
        spawn_helper = new Vector3(-337f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 5 Position
        spawn_helper = new Vector3(-337f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 6 Position
        /*spawn_helper = new Vector3(-337f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        yield return new WaitForSeconds(1.3f);

        //Right Bone Wave 3
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

        //Right Bone 1 Position
        spawn_helper = new Vector3(340f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 2 Position
        spawn_helper = new Vector3(340f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 3 Position
        spawn_helper = new Vector3(340f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 4 Position
        /*spawn_helper = new Vector3(340f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Right Bone 5 Position
        spawn_helper = new Vector3(340f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 6 Position
        spawn_helper = new Vector3(340f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.3f);

        // Left Wave
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

        //Left Bone 1 Position
        spawn_helper = new Vector3(-337f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 2 Position
        /*spawn_helper = new Vector3(-337f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Left Bone 3 Position
        spawn_helper = new Vector3(-337f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 4 Position
        spawn_helper = new Vector3(-337f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 5 Position
        spawn_helper = new Vector3(-337f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 6 Position
        spawn_helper = new Vector3(-337f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.3f);

        //Right Bone Wave 4
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

        //Right Bone 1 Position
        spawn_helper = new Vector3(340f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 2 Position
        spawn_helper = new Vector3(340f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 3 Position
        spawn_helper = new Vector3(340f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 4 Position
        spawn_helper = new Vector3(340f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 5 Position
        spawn_helper = new Vector3(340f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Right Bone 6 Position
        /*spawn_helper = new Vector3(340f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        yield return new WaitForSeconds(1.3f);

        // Left Wave
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

        //Left Bone 1 Position
        /*spawn_helper = new Vector3(-337f, -33.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);*/

        //Left Bone 2 Position
        spawn_helper = new Vector3(-337f, -67.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 3 Position
        spawn_helper = new Vector3(-337f, -101.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 4 Position
        spawn_helper = new Vector3(-337f, -135.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 5 Position
        spawn_helper = new Vector3(-337f, -169.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //Left Bone 6 Position
        spawn_helper = new Vector3(-337f, -203.0f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(1.3f);

        dialogue_box.transform.localPosition = box_original_position;
        dialogue_box.transform.localScale = new Vector3(1, 1, 1);

        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);

        phase = 5.0f;
        yield break;
    }

    IEnumerator genocide_attack_4()
    {
        phase = 5.0f;
        yield break;
    }

    //POSSIBLE IMPROVEMENT: Change vertical bones into sequences of smaller bones with breaks in them.
    //This attack moves the box to the left, then instantiates zippers of vertical bones  and elevator bones and starts moving the box to the right. Optional: Random SFBs in the process.
    IEnumerator genocide_random_attack_0()
    {
        //We move the box
        Vector3 box_original_position = dialogue_box.transform.localPosition;
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_move_dialogue_box(new Vector3(-463, dialogue_box.transform.localPosition.y, dialogue_box.transform.localPosition.z), 0.6f));
        yield return new WaitForSeconds(2.5f);

        //We instantiate the zippers and elevators
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        if (gameObject.GetComponent<enemy_data>().health > (GetComponent<enemy_data>().max_health * 30 / 100))
        {
            StartCoroutine(auxiliar_zipper(new Vector3(-361, -342, -1), Vector3.up, new Vector3(0, 0, 90), 0.8f, 2.0f, 1.5f, 8));
            StartCoroutine(auxiliar_zipper(new Vector3(-159, 179, -1), Vector3.down, new Vector3(0, 0, 90), 0.8f, 2.0f, 1.5f, 8));
            StartCoroutine(auxiliar_zipper(new Vector3(43, -342, -1), Vector3.up, new Vector3(0, 0, 90), 0.8f, 2.0f, 1.5f, 8));
            StartCoroutine(auxiliar_zipper(new Vector3(245, 179, -1), Vector3.down, new Vector3(0, 0, 90), 0.8f, 2.0f, 1.5f, 8));
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(5, 1.6f));
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(auxiliar_genocide_random_attack_3_AloofBones(5, 1.6f));
            //StartCoroutine(auxiliar_genocide_random_attack_0_elevatorbones(new Vector3(12f, 6, -1), new Vector3(12f, -224, -1), 3.0f, 9f, 0.05f));
        }

        else
        {
            StartCoroutine(auxiliar_zipper(new Vector3(-361, -342, -1), Vector3.up, new Vector3(0, 0, 90), 0.5f, 2.7f, 1.5f, 8));
            StartCoroutine(auxiliar_zipper(new Vector3(-159, 179, -1), Vector3.down, new Vector3(0, 0, 90), 0.5f, 2.7f, 1.5f, 8));
            StartCoroutine(auxiliar_zipper(new Vector3(43, -342, -1), Vector3.up, new Vector3(0, 0, 90), 0.5f, 2.7f, 1.5f, 8));
            StartCoroutine(auxiliar_zipper(new Vector3(245, 179, -1), Vector3.down, new Vector3(0, 0, 90), 0.5f, 2.7f, 1.5f, 8));
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(9, 1.0f));
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(auxiliar_genocide_random_attack_3_AloofBones(9, 1.0f));
            //StartCoroutine(auxiliar_genocide_random_attack_0_elevatorbones(new Vector3(12f, 6, -1), new Vector3(12f, -224, -1),6.0f, 9f, 0.05f));
        }


        //We start moving the box again
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_move_dialogue_box_constantly(new Vector3(238, dialogue_box.transform.localPosition.y, -1), 75f));

        //We instantiate super fast bones
        /*yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_genocide_random_attack_0_sfb(5, 1.5f, false));*/

        yield return new WaitForSeconds(10.5f);
        dialogue_box.transform.localPosition = box_original_position;
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);

        yield break;
    }

    //This attack is the "Asgore's fireballs" attacks. Waves of two patterns: Circles of bones converging on the middle, and circles of bones doing circular motions to converge on the middle, + lock in claw attacks.
    IEnumerator genocide_random_attack_1()
    {
        //We make the box bigger and move it upwards a bit
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        Vector3 original_position = dialogue_box.transform.localPosition;
        StartCoroutine(auxiliar_move_dialogue_box(new Vector3(original_position.x, 36, original_position.z), 2.5f));
        StartCoroutine(resizedialoguebox(new Vector3(1.6f, 1.6f, 1.2f), 1.6f));
        yield return new WaitForSeconds(1.5f);

        //We begin instantiating waves of bones
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        int waves = 3;
        float wait_time = 2.0f;
        float time_between_waves = 4.0f;
        int bones_per_wave = 4;
        float lifetime = 2.3f;
        float rotationspeed = 600f;
        float circlingspeed = 1.6f;
        float reducingradiusspeed = 220f;
        float initialradius = 500f;
        if (gameObject.GetComponent<enemy_data>().health > (GetComponent<enemy_data>().max_health * 30 / 100))
        {

        }
        else
        {
            waves = 3;
            wait_time = 2.0f;
            time_between_waves = 4.0f;
            bones_per_wave = 5;
            lifetime = 2.3f;
            rotationspeed = 600f;
            circlingspeed = 1.75f;
            reducingradiusspeed = 220;
            initialradius = 500;
        }
        StartCoroutine(auxiliar_genocide_circular_bones(false, dialogue_box.transform.localPosition, waves, wait_time, time_between_waves, bones_per_wave, lifetime, rotationspeed, circlingspeed, reducingradiusspeed, initialradius));
        yield return new WaitForSeconds(1.5f);

        //We begin shrinking the box
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_resize_dialoguebox_constantly(new Vector3(0.8f, 0.8f, 0.8f), 0.05f));
        yield return new WaitForSeconds(1.5f);

        //We begin claw lock in attacks
        yield return StartCoroutine(auxiliar_lock_in_attack(1.0f));
        yield return new WaitForSeconds(2.5f);
        yield return StartCoroutine(auxiliar_lock_in_attack(1.0f));
        yield return new WaitForSeconds(2.5f);
        yield return StartCoroutine(auxiliar_lock_in_attack(1.0f));
        yield return new WaitForSeconds(2.5f);
        yield return StartCoroutine(auxiliar_lock_in_attack(1.0f));
        yield return new WaitForSeconds(2.5f);

        //End of Gaster's turn, we reset everything
        yield return new WaitForSeconds(0.6f);
        dialogue_box.transform.localPosition = original_position;
        dialogue_box.transform.localScale = new Vector3(1, 1, 1);

        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);




    }

    //This attacks spawns snowflakes on a box  that limits vertical movement, then sfb on a box that limits horizontal movement, then sfb + snowflakes on a box that gets smaller over time
    IEnumerator genocide_random_attack_2()
    {
        //We scale the box
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(resizedialoguebox(new Vector3(1.0f, 0.6f, 1.0f), 2.0f));

        yield return new WaitForSeconds(1.5f);
        //We instantiate two rounds of snowflakes
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());

        //We resize the box again
        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(resizedialoguebox(new Vector3(0.6f, 1.0f, 1.0f), 2.0f));

        //We instantiate SFB
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();

        if (gameObject.GetComponent<enemy_data>().health > (GetComponent<enemy_data>().max_health * 30 / 100))
        {
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(6, 1.0f));
        }
        else
        {
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(6, 1.0f));
        }

        //We resize the box, start shrinking it down, and have both snowflakes and sfb
        yield return new WaitForSeconds(5.0f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(resizedialoguebox(new Vector3(1.2f, 1.2f, 1.0f), 2.0f));
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        if (gameObject.GetComponent<enemy_data>().health > (GetComponent<enemy_data>().max_health * 30 / 100))
        {
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(9, 1.0f));
        }
        else
        {
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(9, 1.0f));
        }

        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_resize_dialoguebox_constantly(new Vector3(0.6f, 0.6f, 1.0f), 0.1f));
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());

        yield return new WaitForSeconds(7.0f);
        dialogue_box.transform.localScale = new Vector3(1, 1, 1);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);
    }

    IEnumerator genocide_random_attack_3()
    {
        if (gameObject.GetComponent<enemy_data>().health > (GetComponent<enemy_data>().max_health * 30 / 100))
        {

            StartCoroutine(auxiliar_genocide_random_attack_3_warning(0.5f, 4, 2.2f));
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(7, 1.0f));
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(auxiliar_genocide_random_attack_3_AloofBones(7, 1.0f));
        }

        else
        {
            StartCoroutine(auxiliar_genocide_random_attack_3_warning(0.5f, 8, 1.0f));
            StartCoroutine(auxiliar_genocide_random_attack_3_bones(7, 1.0f));
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(auxiliar_genocide_random_attack_3_AloofBones(7, 1.0f));
        }
        yield return new WaitForSeconds(8.0f);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        anim.Play("Gaster_enemy_still", 0);

        yield break;
    }


    //This is the genocide final attack. Asgore type bones circling on the player and going further than them;tracking claw attacks. Horizontal zippers going across the screen slowly and considerably separated.
    //Then Box resize to horizontal line, in the middle we start spawning rounds of snowflakes and in the sides, top and bottom zippers that increase steadily in speed.
    IEnumerator genocide_final_attack()
    {
        //We resize the box to be super big
        yield return ShowDialogue("WHAT BROUGHT YOU THIS FAR?..");
        yield return ShowDialogue("YOUR FRUSTRATION?");
        yield return ShowDialogue("SADNESS? ANGER?..");
        yield return ShowDialogue("WHAT COMPELS SOMEONE TO SO THOROUGHLY DESTROY THAT WHICH ONCE MADE THEM HAPPY?");
        yield return ShowDialogue("ALLOW ME TO PUT AN END TO THE DESPAIR WHICH PLAGUES YOUR SOUL.");
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        Vector3 original_position = dialogue_box.transform.localPosition;
        StartCoroutine(auxiliar_move_dialogue_box(new Vector3(original_position.x, 10, original_position.z), 2.5f));
        StartCoroutine(resizedialoguebox(new Vector3(1.8f, 1.8f, 1.8f), 1.6f));
        yield return new WaitForSeconds(1.5f);

        //We begin instantiating asgore type bones
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        int waves = 3;
        float wait_time = 3.0f;
        float time_between_waves = 4.0f;
        int bones_per_wave = 5;
        float lifetime = 5.0f;
        float rotationspeed = 300f;
        float circlingspeed = 0.2f;
        float reducingradiusspeed = 75f;
        float initialradius = 250f;
        StartCoroutine(auxiliar_genocide_circular_bones(true, player.transform.localPosition, waves, wait_time, time_between_waves, bones_per_wave, lifetime, rotationspeed, circlingspeed, reducingradiusspeed, initialradius));

        //We instantiate the zippers
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        float speed = 0.1f;
        lifetime = 16.0f;
        wait_time = 5.0f;
        int numberofbones = 6;
        StartCoroutine(auxiliar_zipper(new Vector3(-361, +164, -1), Vector3.right, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        StartCoroutine(auxiliar_zipper(new Vector3(361, 0, -1), Vector3.left, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        StartCoroutine(auxiliar_zipper(new Vector3(-361, -164, -1), Vector3.right, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));

        yield return new WaitForSeconds(2.0f);
        yield return StartCoroutine(auxiliar_lock_in_attack(2.0f));
        yield return new WaitForSeconds(4.0f);
        yield return StartCoroutine(auxiliar_lock_in_attack(2.0f));

        yield return new WaitForSeconds(4.5f);
        //StartCoroutine(auxiliar_zipper(new Vector3(361, +120, -1), Vector3.left, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        //StartCoroutine(auxiliar_zipper(new Vector3(-361, 0, -1), Vector3.right, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        //StartCoroutine(auxiliar_zipper(new Vector3(361, -120, -1), Vector3.left, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        //StartCoroutine(auxiliar_lock_in_attack(2.0f));

        yield return new WaitForSeconds(4.0f);
        StartCoroutine(auxiliar_lock_in_attack(2.0f));

        yield return new WaitForSeconds(5.0f);

        //we destroy all bones and resize again
        GameObject[] bones = GameObject.FindGameObjectsWithTag("enemy");
        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i].name == "Bone_Attack(Clone)")
            {
                Destroy(bones[i]);
            }
        }
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_move_dialogue_box(new Vector3(original_position.x, original_position.y, original_position.z), 2.5f));
        StartCoroutine(resizedialoguebox(new Vector3(2.2f, 1.0f, 1.0f), 1.6f));


        //snowflakes and zippers
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        lifetime = 16.00f;
        numberofbones = 4;
        StartCoroutine(auxiliar_zipper(new Vector3(361, 0, -1), Vector3.left, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        StartCoroutine(auxiliar_zipper(new Vector3(-361, 0, -1), Vector3.right, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));

        yield return new WaitForSeconds(2.0f);
        StartCoroutine(auxiliar_zipper(new Vector3(361, -200, -1), Vector3.left, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));
        StartCoroutine(auxiliar_zipper(new Vector3(-361, -200, -1), Vector3.right, new Vector3(0, 0, 90), speed, lifetime, wait_time, numberofbones));

        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(auxiliar_snowflakes());


        //End of the fight
        yield return new WaitForSeconds(7.0f);
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        StartCoroutine(auxiliar_move_dialogue_box(new Vector3(original_position.x, original_position.y, original_position.z), 2.5f));
        StartCoroutine(resizedialoguebox(new Vector3(1.0f, 1.0f, 1.0f), 1.6f));
        yield return new WaitForSeconds(3.0f);

        bones = GameObject.FindGameObjectsWithTag("enemy");
        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i].name == "Bone_Attack(Clone)")
            {
                Destroy(bones[i]);
            }
        }
        anim.Play("Gaster_enemy_still", 0);
        yield return StartCoroutine(ShowDialogue("WELL."));
        yield return ShowDialogue("THIS IS AS FAR AS THINGS GO.");
        yield return ShowDialogue("YOU'RE NOT CONTENT TO DIE ALONE, IT SEEMS.");
        yield return ShowDialogue("... ");
        yield return ShowDialogue("WE'LL MEET AGAIN. I PROMISE. I WILL END YOUR SUFFERING THEN.");
        yield break;
    }







    IEnumerator attack_dash_failed(string message)
    {
        yield return StartCoroutine(ShowDialogue(message));
        detect_if_player_dashes = true;
        yield return new WaitForSeconds(1.5f);


        anim.Play("Gaster_enemy_Bone_Snap", 0);
        finger_snap.Play();
        yield return new WaitForSeconds(0.5f);
        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.22f;
        Vector3 spawn_helper = new Vector3(249, -64, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's instantiate more bones
        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(249, -152, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(249, -64, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(249, -152, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate bones from the left
        yield return new WaitForSeconds(2.0f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-249, -64, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(-249, -152, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(-249, -64, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(1.4f);
        spawn_helper = new Vector3(-249, -152, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield return new WaitForSeconds(4.5f);
        detect_if_player_dashes = false;
        attack_relies_on_turn = false;
        phase = phase + 0.1f;
        anim.Play("Gaster_enemy_still", 0);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        if (Gaster_Was_Pet == true)
        {
            gameObject.GetComponent<enemy_data>().attack += 1;
            Gaster_Was_Pet = false;
        }
    }
    IEnumerator attack_division_box()
    {
        yield return new WaitForSeconds(0.7f);
        //Gaster gets ready to strike
        anim.Play("Gaster_enemy_dual_claw_swipe", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        //We instantiate the warning signs
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.y += 0.00f;
        GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        yield return new WaitForSeconds(1.5f);

        //Gaster Strikes
        Destroy(temp, 0.0f);
        anim.Play("Gaster_enemy_dual_claw_swipe_2", 0);
        attack_sound.Play();

        // We divide the dialogue box
        Vector3 dialoguebox_original_position = dialogue_box.GetComponent<RectTransform>().localPosition;
        Vector2 dialoguebox_original_size = dialogue_box.GetComponent<RectTransform>().sizeDelta;
        dialogue_box.GetComponent<RectTransform>().localPosition = new Vector3(-102.8f, -117.7f, -1);
        dialogue_box.GetComponent<RectTransform>().sizeDelta = new Vector2(101.1f, 199.7f);

        spawn_helper = dialogue_box.GetComponent<RectTransform>().localPosition;
        GameObject dialoguebox2 = Instantiate(dialogue_box, spawn_helper, transform.rotation) as GameObject;
        dialoguebox2.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        dialoguebox2.GetComponent<RectTransform>().localPosition = dialogue_box.GetComponent<RectTransform>().localPosition;
        dialoguebox2.GetComponent<RectTransform>().localPosition = new Vector3(128.7f, -117.7f, -1);
        dialoguebox2.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1);

        //We correct the colliders
        BoxCollider2D[] temparray = dialogue_box.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in temparray)
        {
            box.enabled = false;
        }

        temparray = dialoguebox2.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in temparray)
        {
            box.enabled = false;
        }

        //Gaster prepares for 2nd strike
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_dual_claw_swipe_3", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        //We instantiate warning signs again
        spawn_helper = dialogue_box.transform.position;
        //spawn_helper.y += 0.00f;
        //spawn_helper.x -= 0.3009881f;
        temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;

        //We also instantiate the memory fragment
        spawn_helper = dialoguebox2.transform.position;
        // spawn_helper.x += 0.2f;
        spawn_helper.z = 0;
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;
        spawn_helper = dialogue_box.transform.position;
        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        yield return new WaitForSeconds(1.5f);
        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;

        //Gaster strikes once more
        Destroy(temp);
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-98, gameObject.GetComponent<RectTransform>().localPosition.y, gameObject.GetComponent<RectTransform>().localPosition.z);
        anim.Play("Gaster_enemy_dual_claw_swipe_4", 0);
        attack_sound.Play();
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        yield return new WaitForSeconds(0.5f);

        //The attack ends
        anim.Play("Gaster_enemy_dual_claw_swipe_5", 0);
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-1, gameObject.GetComponent<RectTransform>().localPosition.y, gameObject.GetComponent<RectTransform>().localPosition.z);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_still", 0);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        Destroy(dialoguebox2);
        dialogue_box.GetComponent<RectTransform>().localPosition = dialoguebox_original_position;
        dialogue_box.GetComponent<RectTransform>().sizeDelta = dialoguebox_original_size;


    }
    IEnumerator attack_placeholder()
    {

        int memory_fragments = 0;
        yield return new WaitForSeconds(0.7f);
        //Gaster gets ready to strike
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);


        //Blacken screen, save coordinates to instantiate memory fragment
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += 0.2f;
        spawn_helper.z = 0;
        AudioSource music = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        black_background.SetActive(true);

        for (float i = 0; i < 10; i++)
        {
            music.volume -= 0.05f;
            black_background.GetComponent<Image>().color = new Color(0, 0, 0, i / 20);
            yield return new WaitForSeconds(0.05f);
        }
        music.volume = 0.1f;

        //Instantiate memory fragment and make it blink. Then activate its box collider 2D.
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;

        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);

        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;
        spawnafterimage = true;
        StartCoroutine(spawn_afterimage(memory_temporal));

        //We wait until the player picks up the fragment
        while (memory_fragments == 0)
        {
            yield return new WaitForSeconds(0.01f);
        }


        //Once the fragment has been picked up
        spawnafterimage = false;
        foreach (GameObject afterimage in GameObject.FindGameObjectsWithTag("soul_afterimage"))
        {
            Destroy(afterimage);
        }
        music.volume = 1.0f;
        black_background.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);
        attack_sound.Play();
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        yield return new WaitForSeconds(2.0f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);

        anim.Play("Gaster_enemy_still", 0);

        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();







        /*
        yield return new WaitForSeconds(0.7f);
        //Gaster gets ready to strike
        anim.Play("Gaster_enemy_dual_claw_swipe", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
 
        //We instantiate the warning signs
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.y += 0.00f;
        GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        yield return new WaitForSeconds(1.5f);
 
        //Gaster Strikes
        Destroy(temp, 0.0f);
        anim.Play("Gaster_enemy_dual_claw_swipe_2", 0);
        attack_sound.Play();
 
        // We divide the dialogue box
        Vector3 dialoguebox_original_position = dialogue_box.GetComponent<RectTransform>().localPosition;
        Vector2 dialoguebox_original_size = dialogue_box.GetComponent<RectTransform>().sizeDelta;
        dialogue_box.GetComponent<RectTransform>().localPosition = new Vector3(-102.8f, -117.7f, -1);
        dialogue_box.GetComponent<RectTransform>().sizeDelta = new Vector2(101.1f, 199.7f);
 
        spawn_helper = dialogue_box.GetComponent<RectTransform>().localPosition;
        GameObject dialoguebox2 = Instantiate(dialogue_box, spawn_helper, transform.rotation) as GameObject;
        dialoguebox2.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        dialoguebox2.GetComponent<RectTransform>().localPosition = dialogue_box.GetComponent<RectTransform>().localPosition;
        dialoguebox2.GetComponent<RectTransform>().localPosition = new Vector3(128.7f, -117.7f, -1);
        dialoguebox2.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1);
 
        //We correct the colliders
        BoxCollider2D[] temparray = dialogue_box.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in temparray)
        {
            box.enabled = false;
        }
 
        temparray = dialoguebox2.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in temparray)
        {
            box.enabled = false;
        }
 
        //Gaster prepares for 2nd strike
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_dual_claw_swipe_3", 0);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
 
        //We instantiate warning signs again
        spawn_helper = dialogue_box.transform.position;
        //spawn_helper.y += 0.00f;
        //spawn_helper.x -= 0.3009881f;
        temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
 
        //We also instantiate the memory fragment
        spawn_helper = dialoguebox2.transform.position;
        // spawn_helper.x += 0.2f;
        spawn_helper.z = 0;
        GameObject memory_temporal = Instantiate(memory_fragment, spawn_helper, transform.rotation) as GameObject;
        spawn_helper = dialogue_box.transform.position;
        float delay = 0.05f;
        Color c = memory_fragment.GetComponent<SpriteRenderer>().color;
        Color d = memory_fragment.GetComponent<SpriteRenderer>().color;
        d.a = 0.2f;
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = d;
        yield return new WaitForSeconds(delay);
        memory_temporal.GetComponent<SpriteRenderer>().color = c;
        yield return new WaitForSeconds(delay);
        yield return new WaitForSeconds(1.5f);
        memory_temporal.GetComponent<BoxCollider2D>().enabled = true;
 
        //Gaster strikes once more
        Destroy(temp);
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-98, gameObject.GetComponent<RectTransform>().localPosition.y, gameObject.GetComponent<RectTransform>().localPosition.z);
        anim.Play("Gaster_enemy_dual_claw_swipe_4", 0);
        attack_sound.Play();
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        yield return new WaitForSeconds(0.5f);
 
        //The attack ends
        anim.Play("Gaster_enemy_dual_claw_swipe_5", 0);
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-1, gameObject.GetComponent<RectTransform>().localPosition.y, gameObject.GetComponent<RectTransform>().localPosition.z);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        yield return new WaitForSeconds(1.5f);
        anim.Play("Gaster_enemy_still", 0);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        Destroy(dialoguebox2);
        dialogue_box.GetComponent<RectTransform>().localPosition = dialoguebox_original_position;
        dialogue_box.GetComponent<RectTransform>().sizeDelta = dialoguebox_original_size;*/

    }



    IEnumerator auxiliar_rotating_bones(float lifetime, Vector3 position, int direction, float number_of_rounds, bool animation)
    {
        //For harder bones, try 1.33f and 2.0f for times between changing the directions
        if ((animation == true))
        {
            anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
            attack_sound.Play();
        }

        //We instantiate the rotating bone
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        switch (direction)
        {
            case 0:
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
                break;
            case 1:
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                break;
            case 2:
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
                break;
            case 3:
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                break;

        }
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 3;
        bone_attack.GetComponent<Bone_Attack_Script>().rotationspeed = 125.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.35f;
        Vector3 spawn_helper = position;
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's make it change directions
        for (int i = 0; i < number_of_rounds; i++)
        {


            if (direction == 0)
            {
                yield return new WaitForSeconds(1.65f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.down;

                yield return new WaitForSeconds(1.63f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.up;
            }
            else if (direction == 1)
            {

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.down;

                yield return new WaitForSeconds(1.63f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.up;

                yield return new WaitForSeconds(1.65f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

            }
            else if (direction == 2)
            {
                yield return new WaitForSeconds(1.63f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.left;

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.up;

                yield return new WaitForSeconds(1.65f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
            }
            else if (direction == 3)
            {
                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.up;

                yield return new WaitForSeconds(1.65f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.right;

                yield return new WaitForSeconds(2.8f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
                yield return new WaitForSeconds(1.63f);
                if (temp != null) temp.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
            }



        }
    }
    IEnumerator attack7_auxiliar_sidecoming_bones()
    {
        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        Vector3 spawn_helper = new Vector3(249, -14, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a second bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        //We instantiate a third bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a fourth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a fifth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a sixth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a seventh bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate an eight bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }
    IEnumerator attack7_auxiliar_superfastbones()
    {
        //We spawn one super speed bone
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        Vector3 spawn_helper = new Vector3(413.6f, -81.8f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We wait, then spawn one other SFB
        yield return new WaitForSeconds(3.0f);

        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        spawn_helper = new Vector3(-413.6f, -132.8f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We wait, then sapwn two SFB
        yield return new WaitForSeconds(1.75f);

        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        spawn_helper = new Vector3(413.6f, -150.8f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        spawn_helper = new Vector3(-413.6f, -60.8f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
    }

    IEnumerator attack9_auxiliar_sidecoming_bones()
    {
        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.5f;
        Vector3 spawn_helper = new Vector3(249, -14, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a second bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.5f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        //We instantiate a third bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.5f;
        spawn_helper = new Vector3(-249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a fourth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.5f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(-249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a fifth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 2.5f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.5f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }

    IEnumerator random_attack_1_auxiliar_zipper()
    {
        anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0);
        attack_sound.Play();
        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 4.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        Vector3 spawn_helper = new Vector3(-249, -254, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's instantiate more bones
        for (int i = 0; i < 5; i++)
        {
            if (i != 5)
            {
                yield return new WaitForSeconds(2.0f);
                bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
                bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
                spawn_helper = new Vector3(-249, -254, -2021.14f);
                temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localPosition = spawn_helper;
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
            else
            {
                yield return new WaitForSeconds(2.0f);
                bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 0.5f;
                bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
                spawn_helper = new Vector3(-249, -254, -2021.14f);
                temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localPosition = spawn_helper;
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }


        }
    }

    IEnumerator auxiliar_snowflakes()
    {
        //We execute the animation
        anim.Play("Gaster_enemy_frost_slam", 0, 0f);
        attack_sound.Play();

        //We create a loop that spawns 6 random snowflakes from the right
        for (int i = 0; i < 6; i++)
        {
            Vector3 SpawnHelper = dialogue_box.transform.localPosition;
            snowflake_attack.GetComponent<enemy_data>().attack = GetComponent<enemy_data>().attack;

            //We instantiate a snowflake near the dialogue box, and set its parent to the canvas.
            GameObject temp = Instantiate(snowflake_attack);
            temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
            if (i < 3)
            {
                SpawnHelper.x += dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
            }
            else
            {
                SpawnHelper.x -= dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2;
            }

            temp.GetComponent<RectTransform>().localPosition = SpawnHelper;
            temp.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 1);

            //We set up the snowflake variables: Direction, speed, etc.
            SpawnHelper.y += 250;
            SpawnHelper.x = Random.Range(-124.7f, 137.1f);
            temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
            temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
            temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
            temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
            temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
            temp.GetComponent<Snowflake_Attack_Script>().speed_2 = Random.Range(65.5f, 115.75f);
            temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = 1.5f;
            temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = 0.1f;
            /*
            temp.GetComponent<Snowflake_Attack_Script>().rotation_strength = 500;
            temp.GetComponent<Snowflake_Attack_Script>().target = SpawnHelper;
            temp.GetComponent<Snowflake_Attack_Script>().speed_1 = 1.5f;
            temp.GetComponent<Snowflake_Attack_Script>().time_phase_1 = 1.5f;
            temp.GetComponent<Snowflake_Attack_Script>().lifetime = 7.0f;
            temp.GetComponent<Snowflake_Attack_Script>().speed_2 = Random.Range(65.5f, 215.75f);
            temp.GetComponent<Snowflake_Attack_Script>().zig_zag_strength = Random.Range(0.8f, 4.5f);
            temp.GetComponent<Snowflake_Attack_Script>().zig_zag_time = Random.Range(0.08f, 0.75f);*/

        }
        yield return new WaitForSeconds(2.0f);
        //anim.Play("Gaster_enemy_still", 0);
    }
    IEnumerator auxiliar_superfastbones(int bones, float wait, bool animation)
    {
        if (animation == true)
        {
            anim.Play("Gaster_enemy_Desperation_Bone_Snap", 0, 0f);
            attack_sound.Play();
        }

        float vector_x;
        bool direction = false;
        for (int i = 0; i < bones; i++)
        {
            if (direction == false)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                vector_x = 413.6f;
                direction = true;
            }
            else
            {

                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                vector_x = -413.6f;
                direction = false;
            }

            bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
            bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
            bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
            bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
            bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

            //We set up parameters for spawning a super speed bone
            Vector3 spawn_helper = new Vector3(vector_x, Random.Range(-22.0f, -202f), -2021.14f);
            GameObject temp = Instantiate(bone_attack) as GameObject;
            temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
            temp.GetComponent<RectTransform>().localPosition = spawn_helper;
            temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
            temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
            temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
            temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
            yield return new WaitForSeconds(wait);
        }

    }
    IEnumerator auxiliar_warning_sign(float duration)
    {
        float randomnumber = Random.Range(-0.3f, 0.3f);//Random.Range(1, 4);
        Vector3 spawn_helper = dialogue_box.transform.position;
        /* if (randomnumber == 1)
         {
 
         }
         else if (randomnumber == 2)
         {
             spawn_helper.x = 1.167f;
         }
         else if (randomnumber == 3)
         {
             spawn_helper.x = 0.57f;
         }*/
        spawn_helper.x += randomnumber;
        spawn_helper.y += 0.00f;
        GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);

        //Gaster strikes once
        yield return new WaitForSeconds(1.5f);
        Destroy(temp);
        temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
        Vector3 Vector_temporal = gameObject.transform.position;
        gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster waits, then returns to his normal position.
        yield return new WaitForSeconds(0.5f);
        Destroy(temp);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;

        yield return new WaitForSeconds(Mathf.Abs(duration - 0.5f));
        gameObject.transform.position = Vector_temporal;
        anim.Play("Gaster_enemy_still", 0);
    }
    IEnumerator auxiliar_lock_in_attack(float time)
    {
        //This part of the code manages the lock in attacks
        GameObject temp;
        Vector3 Vector_temporal = gameObject.transform.position;
        player = GameObject.Find("Soul");
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().speed = 0.2f;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().target = player;
        temp = Instantiate(lock_on_indicator) as GameObject;
        //temp.GetComponent<Transform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        for (float i = 0; i < time; i = i + 0.01f)
        {
            gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            yield return new WaitForSeconds(0.01f);

        }
        temp.GetComponent<Lock_On_Indicator_Script>().target = null;

        //Gaster strikes
        //temp.GetComponent<Animator>().enabled = false;
        temp.GetComponent<AudioSource>().Stop();
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);

        gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        temp.GetComponent<BoxCollider2D>().enabled = true;
        temp.GetComponent<Animator>().Play("lock_on_strike", 0);
        //temp.GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        gameObject.transform.position = Vector_temporal;
        Destroy(temp);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        anim.Play("Gaster_enemy_still", 0);
    }
    IEnumerator auxiliar_zipper(Vector3 position, Vector3 direction, Vector3 rotation, float speed, float lifetime, float wait_time, int number_of_bones)
    {
        for (int i = 0; i < number_of_bones; i++)
        {
            bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
            bone_attack.GetComponent<Bone_Attack_Script>().direction = direction;
            bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
            bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
            bone_attack.GetComponent<Bone_Attack_Script>().speed = speed;
            Vector3 spawn_helper = position;
            GameObject temp = Instantiate(bone_attack) as GameObject;
            temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
            temp.GetComponent<RectTransform>().localPosition = spawn_helper;
            temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(rotation);
            temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

            yield return new WaitForSeconds(wait_time);
        }
    }

    IEnumerator random_attack_3_auxiliar_zipper()
    {
        for (int i = 0; i < 3; i++)
        {
            if (i != 2)
            {
                //We instantiate the first top to bottom bone
                bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 4.0f;
                bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.25f;
                Vector3 spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
                GameObject temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localPosition = spawn_helper;
                temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

                //We instantiate the second top to bottom bone
                yield return new WaitForSeconds(1.5f);
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 4.0f;
                bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.25f;
                spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
                temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localPosition = spawn_helper;
                temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                yield return new WaitForSeconds(1.5f);
            }
            //Control for the last bone
            else
            {
                //We instantiate the first top to bottom bone
                bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 4.0f;
                bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.25f;
                Vector3 spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
                GameObject temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localPosition = spawn_helper;
                temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

                //We instantiate the second top to bottom bone
                yield return new WaitForSeconds(1.5f);
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 2.5f;
                bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.25f;
                spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
                temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localPosition = spawn_helper;
                temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                yield return new WaitForSeconds(1.5f);
            }

        }


    }
    IEnumerator random_attack_3_auxiliar_superfastbones()
    {
        //We set up general parameters for spawning super speed bones
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

        //We set up specific parameters for spawning a super speed bone
        Vector3 spawn_helper = new Vector3(413.6f, -41.8f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        //We spawn the rest of the bones
        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -132.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(2.0f);
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        spawn_helper = new Vector3(413.6f, -125.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(0.2f);
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        spawn_helper = new Vector3(-413.6f, -80.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);

        yield return new WaitForSeconds(2.0f);
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        spawn_helper = new Vector3(413.6f, -195.5f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
    }


    IEnumerator auxiliar_final_attack_zipper_1()
    {
        //Let's instantiate bone 1
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        Vector3 spawn_helper = new Vector3(249, -14, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a second bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        //We instantiate a third bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a fourth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a fifth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a sixth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a seventh bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate an eight bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //Let's instantiate bone 9
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a 10
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);


        //We instantiate a 11
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a 12
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a 13
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a 14
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a 15
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        spawn_helper = new Vector3(249, -14, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate an 16
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        spawn_helper = new Vector3(249, -202, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }
    IEnumerator auxiliar_final_attack_zipper_2()
    {
        //We instantiate the first top to bottom bone
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;
        Vector3 spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate the second top to bottom bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;
        spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiate a third bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;
        spawn_helper = new Vector3(95.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //We instantiatea fourth bone
        yield return new WaitForSeconds(1.5f);
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.down;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 1;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 5.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.45f;
        spawn_helper = new Vector3(-100.9f, 114.4f, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }
    IEnumerator auxiliar_final_attack_lock_in()
    {
        //This part of the code manages the lock in attacks
        GameObject temp;
        Vector3 Vector_temporal = gameObject.transform.position;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().speed = 0.2f;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().target = player;
        temp = Instantiate(lock_on_indicator) as GameObject;
        //temp.GetComponent<Transform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        for (float i = 0; i < 1.0; i = i + 0.01f)
        {
            gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            yield return new WaitForSeconds(0.01f);

        }
        temp.GetComponent<Lock_On_Indicator_Script>().target = null;

        //Gaster strikes
        //temp.GetComponent<Animator>().Stop();
        temp.GetComponent<AudioSource>().Stop();
        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);

        gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        temp.GetComponent<BoxCollider2D>().enabled = true;
        temp.GetComponent<Animator>().Play("lock_on_strike", 0);
        //temp.GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        gameObject.transform.position = Vector_temporal;
        Destroy(temp);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        anim.Play("Gaster_enemy_still", 0);


        yield return new WaitForSeconds(1.5f);

        //We execute the second lock in attack
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().speed = 0.2f;
        lock_on_indicator.GetComponent<Lock_On_Indicator_Script>().target = player;
        temp = Instantiate(lock_on_indicator) as GameObject;
        //temp.GetComponent<Transform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        anim.Play("Gaster_enemy_Claw_Swipe", 0);
        for (float i = 0; i < 1.5; i = i + 0.01f)
        {
            gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            yield return new WaitForSeconds(0.01f);

        }
        temp.GetComponent<Lock_On_Indicator_Script>().target = null;

        //Gaster strikes
        yield return new WaitForSeconds(0.25f);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0);

        gameObject.transform.position = new Vector3(temp.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        attack_sound.Play();
        temp.GetComponent<BoxCollider2D>().enabled = true;
        //temp.GetComponent<Animator>().Stop();
        temp.GetComponent<Animator>().Play("lock_on_strike", 0);
        //temp.GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(0.5f);
        anim.Play("Gaster_enemy_Claw_Swipe_3", 0);
        gameObject.transform.position = Vector_temporal;
        Destroy(temp);
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speedMultiplier);
        anim.Play("Gaster_enemy_still", 0);

    }
    IEnumerator auxiliar_final_attack_warning_signs()
    {
        //We set up spawn helpers and temps for the first 3 warning boxes
        Vector3 spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += 0.3f;
        spawn_helper.y += 0.00f;
        spawn_helper.z = 0.01f;
        GameObject temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);

        yield return new WaitForSeconds(0.2f);
        Vector3 spawn_helper2 = dialogue_box.transform.position;
        spawn_helper2.y += 0.00f;
        spawn_helper2.z = 0.01f;
        GameObject temp2 = Instantiate(Warning_signs, spawn_helper2, transform.rotation) as GameObject;
        temp2.GetComponent<AudioSource>().Stop();

        yield return new WaitForSeconds(0.2f);
        Vector3 spawn_helper3 = dialogue_box.transform.position;
        spawn_helper3.x -= 0.3f;
        spawn_helper3.y += 0.00f;
        spawn_helper3.z = 0.01f;
        GameObject temp3 = Instantiate(Warning_signs, spawn_helper3, transform.rotation) as GameObject;
        temp3.GetComponent<AudioSource>().Stop();


        //Gaster strikes once
        yield return new WaitForSeconds(1.5f);
        Destroy(temp);
        Vector3 Vector_temporal = gameObject.transform.position;
        gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster strikes twice
        yield return new WaitForSeconds(0.2f);
        Destroy(temp);
        Destroy(temp2);
        temp = Instantiate(impact_zone, spawn_helper2, transform.rotation) as GameObject;
        gameObject.transform.position = new Vector3(spawn_helper2.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster strikes three times
        yield return new WaitForSeconds(0.2f);
        Destroy(temp3);
        Destroy(temp);
        temp = Instantiate(impact_zone, spawn_helper3, transform.rotation) as GameObject;
        gameObject.transform.position = new Vector3(spawn_helper3.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);


        //Gaster waits, then we prepare the 2 remaining warning boxes
        yield return new WaitForSeconds(0.5f);
        Destroy(temp);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(1.5f);
        gameObject.transform.position = Vector_temporal;
        anim.Play("Gaster_enemy_still", 0, 0f);



        spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += 0.185f;
        spawn_helper.y += 0.00f;
        spawn_helper.z = 0.01f;
        temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);


        yield return new WaitForSeconds(0.2f);
        spawn_helper2 = dialogue_box.transform.position;
        spawn_helper2.x -= 0.185f;
        spawn_helper2.y += 0.00f;
        spawn_helper2.z = 0.01f;
        temp2 = Instantiate(Warning_signs, spawn_helper2, transform.rotation) as GameObject;
        temp2.GetComponent<AudioSource>().Stop();

        yield return new WaitForSeconds(1.5f);
        Destroy(temp);
        temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
        gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster strikes twice
        yield return new WaitForSeconds(0.2f);
        Destroy(temp);
        Destroy(temp2);
        temp = Instantiate(impact_zone, spawn_helper2, transform.rotation) as GameObject;
        gameObject.transform.position = new Vector3(spawn_helper2.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster return to his normal position
        yield return new WaitForSeconds(0.5f);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        Destroy(temp);

        yield return new WaitForSeconds(1.5f);
        gameObject.transform.position = Vector_temporal;
        anim.Play("Gaster_enemy_still", 0, 0f);

        //We set up spawn helpers and temps for the first 3 warning boxes again
        spawn_helper = dialogue_box.transform.position;
        spawn_helper.x += 0.3f;
        spawn_helper.y += 0.00f;
        spawn_helper.z = 0.01f;
        temp = Instantiate(Warning_signs, spawn_helper, transform.rotation) as GameObject;
        anim.Play("Gaster_enemy_Claw_Swipe", 0, 0f);

        yield return new WaitForSeconds(0.2f);
        spawn_helper2 = dialogue_box.transform.position;
        spawn_helper2.y += 0.00f;
        spawn_helper2.z = 0.01f;
        temp2 = Instantiate(Warning_signs, spawn_helper2, transform.rotation) as GameObject;
        temp2.GetComponent<AudioSource>().Stop();

        yield return new WaitForSeconds(0.2f);
        spawn_helper3 = dialogue_box.transform.position;
        spawn_helper3.x -= 0.3f;
        spawn_helper3.y += 0.00f;
        spawn_helper3.z = 0.01f;
        temp3 = Instantiate(Warning_signs, spawn_helper3, transform.rotation) as GameObject;
        temp3.GetComponent<AudioSource>().Stop();


        //Gaster strikes once
        yield return new WaitForSeconds(1.5f);
        Destroy(temp);
        temp = Instantiate(impact_zone, spawn_helper, transform.rotation) as GameObject;
        Vector_temporal = gameObject.transform.position;
        gameObject.transform.position = new Vector3(spawn_helper.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster strikes twice
        yield return new WaitForSeconds(0.2f);
        Destroy(temp);
        Destroy(temp2);
        temp = Instantiate(impact_zone, spawn_helper2, transform.rotation) as GameObject;
        gameObject.transform.position = new Vector3(spawn_helper2.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);

        //Gaster strikes for the third time
        yield return new WaitForSeconds(0.2f);
        Destroy(temp);
        Destroy(temp3);
        temp = Instantiate(impact_zone, spawn_helper3, transform.rotation) as GameObject;
        gameObject.transform.position = new Vector3(spawn_helper3.x, gameObject.transform.position.y, gameObject.transform.position.z);
        anim.Play("Gaster_enemy_Claw_Swipe_2", 0, 0f);
        attack_sound.Play();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(-0.6f, -394.5f);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(60.76974f, 265.6f);


        //Gaster waits, then returns to his normal position
        yield return new WaitForSeconds(0.5f);
        Destroy(temp);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(1.5f);
        gameObject.transform.position = Vector_temporal;
        anim.Play("Gaster_enemy_still", 0);


    }


    IEnumerator auxiliar_genocide_random_attack_0_elevatorbones(Vector3 position1, Vector3 position2, float elevator_speed, float lifetime, float elevator_distance)
    {
        bone_attack.GetComponent<enemy_data>().attack = gameObject.GetComponent<enemy_data>().attack;
        bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 4;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = 0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 180.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = elevator_distance;//0.05f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = elevator_speed;//3.0f;
        Vector3 spawn_helper = position1;//new Vector3(249, 6, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = 0.0f;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = elevator_distance;
        bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = elevator_speed;
        spawn_helper = position2; //new Vector3(249, -224, -2021.14f);
        temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

        yield break;
    }
    IEnumerator auxiliar_genocide_random_attack_0_sfb(int number_of_bones, float wait_time, bool initial_direction)
    {
        bool direction = initial_direction;
        Vector3 spawn_helper;
        for (int i = 0; i < number_of_bones; i++)
        {
            if (direction == false)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                spawn_helper = new Vector3(dialogue_box.transform.localPosition.x - 425, Random.Range(-22.0f, -202f), -2021.14f);
            }
            else
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                spawn_helper = new Vector3(dialogue_box.transform.localPosition.x + 425, Random.Range(-22.0f, -202f), -2021.14f);
            }
            bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
            bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 7f;
            bone_attack.GetComponent<Bone_Attack_Script>().speed = 2.8f;
            bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.2f;
            bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 2.0f;

            //We set up parameters for spawning a super speed bone
            GameObject temp = Instantiate(bone_attack) as GameObject;
            temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
            temp.GetComponent<RectTransform>().localPosition = spawn_helper;
            temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
            temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
            temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
            temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
            direction = !direction;
            yield return new WaitForSeconds(wait_time);
        }
    }

    IEnumerator auxiliar_genocide_circular_bones(bool trackplayer, Vector3 center, int waves, float wait_time, float time_between_waves, int bones_per_wave, float lifetime, float rotationspeed, float elevatorspeed, float elevator_speed_2, float elevator_distance)
    {
        float angle = 0;
        for (int i = 0; i < waves; i++)
        {
            //Wave 1 of bones
            for (int j = 0; j < bones_per_wave; j++)
            {

                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 5;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
                bone_attack.GetComponent<Bone_Attack_Script>().rotationspeed = rotationspeed;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = 0f;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed_2 = elevator_speed_2;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = elevator_distance;
                if (trackplayer == false)
                {
                    bone_attack.GetComponent<Bone_Attack_Script>().center = center;
                }
                else
                {
                    bone_attack.GetComponent<Bone_Attack_Script>().center = player.transform.localPosition;
                }

                bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = angle;
                GameObject temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                temp.GetComponent<RectTransform>().sizeDelta = new Vector2(90, 60);
                temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.263494f, 3.599425f);
                temp.GetComponent<BoxCollider2D>().size = new Vector2(41.55807f, 8.140159f);
                angle += 360 / bones_per_wave;
            }
            yield return new WaitForSeconds(wait_time);
            for (int j = 0; j < bones_per_wave; j++)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 5;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
                bone_attack.GetComponent<Bone_Attack_Script>().rotationspeed = rotationspeed;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = elevatorspeed;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed_2 = elevator_speed_2;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = elevator_distance;
                if (trackplayer == false)
                {
                    bone_attack.GetComponent<Bone_Attack_Script>().center = center;
                }
                else
                {
                    bone_attack.GetComponent<Bone_Attack_Script>().center = player.transform.localPosition;
                }
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = angle;
                GameObject temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                temp.GetComponent<RectTransform>().sizeDelta = new Vector2(90, 60);
                temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.263494f, 3.599425f);
                temp.GetComponent<BoxCollider2D>().size = new Vector2(41.55807f, 8.140159f);
                angle += 360 / bones_per_wave;
            }

            yield return new WaitForSeconds(wait_time);

            for (int j = 0; j < bones_per_wave; j++)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 5;
                bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
                bone_attack.GetComponent<Bone_Attack_Script>().rotationspeed = rotationspeed;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed = -elevatorspeed;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_speed_2 = elevator_speed_2;
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_distance = elevator_distance;
                if (trackplayer == false)
                {
                    bone_attack.GetComponent<Bone_Attack_Script>().center = center;
                }
                else
                {
                    bone_attack.GetComponent<Bone_Attack_Script>().center = player.transform.localPosition;
                }
                bone_attack.GetComponent<Bone_Attack_Script>().elevator_angle = angle;
                GameObject temp = Instantiate(bone_attack) as GameObject;
                temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
                temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                temp.GetComponent<RectTransform>().sizeDelta = new Vector2(90, 60);
                temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.263494f, 3.599425f);
                temp.GetComponent<BoxCollider2D>().size = new Vector2(41.55807f, 8.140159f);
                angle += 360 / bones_per_wave;
            }

            yield return new WaitForSeconds(time_between_waves);
        }
    }


    IEnumerator auxiliar_genocide_random_attack_3_warning(float duration, int amount, float wait_time)
    {
        for (int i = 0; i < amount; i++)
        {
            StartCoroutine(auxiliar_warning_sign(duration));
            yield return new WaitForSeconds(wait_time);
        }

    }

    IEnumerator auxiliar_genocide_random_attack_3_bones(int bones_number, float wait_time)
    {
        float vector_x;
        bool direction = false;
        for (int i = 0; i < bones_number; i++)
        {
            if (direction == false)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                vector_x = dialogue_box.transform.localPosition.x + 300;
                direction = true;
            }
            else
            {

                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                vector_x = dialogue_box.transform.localPosition.x - 300;
                direction = false;
            }

            bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
            bone_attack.GetComponent<Bone_Attack_Script>().lifetime = 4.2f;
            bone_attack.GetComponent<Bone_Attack_Script>().speed = 0.3f;
            bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = 0.0f;
            bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = 0.0f;

            //We set up parameters for spawning a super speed bone
            Vector3 spawn_helper = new Vector3(vector_x, Random.Range(-22.0f, -202f), -2021.14f);
            GameObject temp = Instantiate(bone_attack) as GameObject;
            temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
            temp.GetComponent<RectTransform>().localPosition = spawn_helper;
            temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
            temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
            temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
            temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
            yield return new WaitForSeconds(wait_time);
        }
    }

    IEnumerator auxiliar_genocide_random_attack_3_AloofBones(int bones_number, float wait_time)
    {
        bool direction = false;
        for (int i = 0; i < bones_number; i++)
        {
            if (direction == false)
            {
                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
                PlayerSensitiveBone(false, 300.45f, 4.2f, 0.3f, 0.0f, 0.0f);
                direction = true;
            }
            else
            {

                bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
                PlayerSensitiveBone(true, -300.45f, 4.2f, 0.3f, 0.0f, 0.0f);
                direction = false;
            }

            yield return new WaitForSeconds(wait_time);
        }
    }


    //Genocide auxiliar functions

    IEnumerator Start_Genocide()
    {
        GameObject.Find("Main Camera").GetComponent<AudioSource>().Stop();
        GameObject.Find("Second_Music").GetComponent<AudioSource>().Stop();
        yield return StartCoroutine(ShowDialogue("..."));
        yield return ShowDialogue("I SEE.");
        yield return ShowDialogue("THIS ISN'T ABOUT THE TIMELINE. THIS IS PERSONAL.");
        yield return ShowDialogue("YOUR DEATH DATE HAS BEEN SET. I WILL CARRY OUT YOUR EXECUTION.");

        GameObject.Find("Third_Music").GetComponent<AudioSource>().Play();
        genocide = true;
        genocide_warning = false;
        yield return new WaitForSeconds(1.5f);
        GameObject.Find("Canvas").GetComponent<battle_manager>().memory.gameObject.SetActive(false);
        GameObject.Find("Canvas").GetComponent<battle_manager>().StartPlayerTurn();
        phase = 0.0f;
    }

    IEnumerator auxiliar_move_dialogue_box(Vector3 destination, float speed)
    {

        /*
        example code:
        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(309f, 97f, -1), 700.0f));
         */
        while (Vector3.Distance(dialogue_box.transform.localPosition, destination) > 10.5f)
        {
            dialogue_box.transform.localPosition = Vector3.Lerp(dialogue_box.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForSeconds(0.00001f);
            speed += 0.01f;
        }
        yield break;
    }

    IEnumerator resizedialoguebox(Vector3 newscale, float speed)
    {
        //Remember to return the scale to Vector3(1,1,1) at the end of each attack!
        //Example code:
        //At the beginning of an attack:
        /*
         yield return new WaitForSeconds(1.0f) //So that we wait until the dialogue box has been resized once
         resizedialoguebox(Vector3 (0.2f, 0.3f, 1.0F); //We input the desired new size
         resizedialoguebox(Vector3 (1.0f,1.0f,1.0f); //We reset the scale of the box at the end of the turn;
          */
        while (Vector3.Distance(dialogue_box.transform.localScale, newscale) > 0.05f)
        {
            dialogue_box.transform.localScale = Vector3.Lerp(dialogue_box.transform.localScale, newscale, speed * Time.deltaTime);

            // If you don't want an eased scaling, replace the above line with the following line
            //   and change speed to suit:
            // transform.localScale = Vector3.MoveTowards (transform.localScale, newscale, speed * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }

    }
    //
    void PlayerSensitiveBone(bool direction, float distance, float lifetime, float speed, float super_speed_time, float super_speed_wait)
    {
        /*
         * Theoretically this functions instantiates a single bone on the players position. It receives 6 parameters:
         *
         * - direction is the direction the bone will move towards. Direction must be either true or false. False is left, true is right. This is done this way
         * for memory efficiency purposes.
         *
         * - distance means the horizontal distance the bone will be instantiated from the player. Use negative values if you want it to be instantiated to the left of the player.  
         *
         * - lifetime is the lifetime of the bone
         *
         * - speed is the speed of the bone
         *
         * - super_speed_time is the time the bone spends moving the moment it's instantiated. You know, when you create the bone immediately starts moving, then stops, then moves again.
         * This is the time it moves the moment it appears.
         *
         * - Super_speed_wait is the time it remains stopped between the two movements.
         *
         *
         * Example of calling the function:        PlayerSensitiveBone(true, -100.45f, 7f, 2.8f, 0.3f, 1.5f);
          */
        player = GameObject.Find("Soul");
        //We set up general parameters for spawning super speed bones
        if (direction == false)
        {
            bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.left;
        }
        else
        {
            bone_attack.GetComponent<Bone_Attack_Script>().direction = Vector3.right;
        }

        bone_attack.GetComponent<Bone_Attack_Script>().behaviour = 2;
        bone_attack.GetComponent<Bone_Attack_Script>().lifetime = lifetime;
        bone_attack.GetComponent<Bone_Attack_Script>().speed = speed;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_time = super_speed_time;
        bone_attack.GetComponent<Bone_Attack_Script>().super_speed_wait = super_speed_wait;

        //We set up specific parameters for spawning a super speed bone
        Vector3 spawn_helper = new Vector3(dialogue_box.transform.localPosition.x + distance, player.transform.localPosition.y, -2021.14f);
        GameObject temp = Instantiate(bone_attack) as GameObject;
        temp.GetComponent<RectTransform>().SetParent(GameObject.Find("Battle_Canvas").GetComponent<Transform>());
        temp.GetComponent<RectTransform>().localPosition = spawn_helper;
        temp.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        temp.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(192, 108);
        temp.GetComponent<BoxCollider2D>().offset = new Vector2(5.329605f, 7.056124f);
        temp.GetComponent<BoxCollider2D>().size = new Vector2(119.2439f, 27.90309f);
    }

    IEnumerator auxiliar_move_dialogue_box_constantly(Vector3 destination, float speed)
    {

        /*
        example code:
        yield return StartCoroutine(auxiliar_move_dialogue_box(new Vector3(309f, 97f, -1), 700.0f));
         */
        while (dialogue_box.transform.localPosition != destination)
        {
            dialogue_box.transform.localPosition = Vector3.MoveTowards(dialogue_box.transform.localPosition, destination, speed * Time.deltaTime);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForSeconds(0.00001f);
        }
        yield break;
    }

    IEnumerator auxiliar_resize_dialoguebox_constantly(Vector3 newscale, float speed)
    {
        while (dialogue_box.transform.localScale != newscale)
        {
            dialogue_box.transform.localScale = Vector3.MoveTowards(dialogue_box.transform.localScale, newscale, speed * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }
    }










    //Auxiliar functions
    //this one doesn't really work??
    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 1.0f;
    }




}
