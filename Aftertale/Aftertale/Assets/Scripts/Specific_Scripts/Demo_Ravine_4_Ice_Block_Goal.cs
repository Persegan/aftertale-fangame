﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_4_Ice_Block_Goal : MonoBehaviour {

    public GameObject IceBlock;
    public GameObject Player;
    public float transition_speed;

    public GameObject Freezing_River;
    public GameObject Freezing_Particle_System;

    public GameObject River_Collisions_1;
    public GameObject River_Collisions_2;

    public GameObject Frozen_River;
    public GameObject Normal_River;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.name == "Ice_Block")
        {
            IceBlock = collider.gameObject;
            StartCoroutine(EndIceBlockPuzzle());
        }
    }


    IEnumerator EndIceBlockPuzzle()
    {
        IceBlock.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        GetComponent<BoxCollider2D>().enabled = false;

        Player.GetComponent<Animator>().SetBool("moving", false);
        Player.GetComponent<movement>().Close_Menu();
        Player.GetComponent<movement>().StopAllCoroutines();
        Player.GetComponent<movement>().can_move = false;
        Player.GetComponent<movement>().can_dash = false;
        Player.GetComponent<movement>().can_interact = false;

        if (Player.GetComponent<movement>().is_dashing == true)
        {
            Player.GetComponent<movement>().is_dashing = false;
            Player.GetComponent<movement>().gameObject.GetComponent<Animator>().SetBool("Dashing", false);
            Player.GetComponent<movement>().gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            Player.GetComponent<movement>().gameObject.GetComponent<Rigidbody2D>().drag = Player.GetComponent<movement>().previousdrag;
            Player.GetComponent<Animator>().SetBool("moving", false);
        }


        Camera.main.GetComponent<camera>().enabled = false;
        Vector3 auxiliar = IceBlock.transform.position;
        auxiliar.z = -10;
        while (Mathf.Abs(Camera.main.transform.position.x - IceBlock.transform.position.x) > 0.2f || Mathf.Abs(Camera.main.transform.position.y - IceBlock.transform.position.y) > 0.2f)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, auxiliar, transition_speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.75f);

        Destroy(IceBlock);
        Freezing_Particle_System.SetActive(true);
        GetComponent<AudioSource>().Play();
        Normal_River.SetActive(false);
        Freezing_River.GetComponent<Animator>().enabled = true;

        yield return new WaitForSeconds(Freezing_River.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length / Freezing_River.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speedMultiplier);

        Frozen_River.SetActive(true);
        Destroy(Freezing_River);
        Freezing_Particle_System.GetComponent<ParticleSystem>().Stop();

        yield return new WaitForSeconds(2.5f);

        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_interact = true;
        Camera.main.GetComponent<camera>().enabled = true;

        player_data.playerdata.global_variables[16] = 1;
        Destroy(River_Collisions_1);
        Destroy(River_Collisions_2);

    }
}
