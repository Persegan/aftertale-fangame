﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_0_Fade_OST_Manager : MonoBehaviour {

    public AudioSource OST;
    public float minX = -1.525f;
    public float maxX = 2.475f;
    public GameObject Player;

    private float currentvalue;
    private float normalizedvalue;

    // Use this for initialization
    void Start () {
        OST = GameObject.Find("_AudioManager").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        currentvalue = Player.transform.position.x;

        //Calculate the normalized float;
        normalizedvalue = (currentvalue - minX) / (maxX - minX);


        OST.volume = normalizedvalue;

    }
}
