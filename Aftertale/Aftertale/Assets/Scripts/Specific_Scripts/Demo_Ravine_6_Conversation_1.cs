﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
 * Script that handles the initial conversation of Ravine 6. It's like the scripts that handle conversations
 * in Ravines but like the intro conversation it doesn't need a manager.
 */


public class Demo_Ravine_6_Conversation_1 : MonoBehaviour {


    public int state;
    public string[] messages;
    public GameObject character_portrait;
    public Sprite[] character_sprites;
    public Image dialoguebox;
    public Text displaytext;
    public Button message_owner_box;
    public AudioSource[] character_sounds;
    public Font[] character_fonts;
    public GameObject black_background;
    public GameObject player;
    public List<GameObject> private_characters = new List<GameObject>();

    public GameObject Overworld_Gaster;


    const int GASTER_ANNOYED = 1;
    const int GASTER_CONFIDENT = 2;
    const int GASTER_DOGFACE = 3;
    const int GASTER_FRUSTRATED = 4;
    const int GASTER_NEUTRAL = 5;
    const int GASTER_SYMPATHETIC = 6;
    const int GASTER_THINKING = 7;
    const int GASTER_PUPPY = 8;

    private bool can_skip;
    private bool can_interact;
    // Use this for initialization
    void Start()
    {
        state = 0;
        can_skip = false;
        can_interact = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) && can_interact == true && can_skip == true)
        {
            Execute_Dialogue();
        }

        if (Input.GetKeyDown(KeyCode.X) && can_interact == true && can_skip == true)
        {
            Skip_Dialogue();
        }
    }

    public void Execute_Dialogue()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Coroutine());
        }
    }


    public void Skip_Dialogue()
    {
        if (can_interact == true)
        {
            if (state >= 0 && can_skip == true)
            {
                AutoTextScript.StopText(displaytext);
                messages[state] = messages[state].Replace("ç", "\n");
                messages[state] = messages[state].Replace("ñ", player_data.playerdata.player_name);
                displaytext.text = messages[state];
                AutoTextScript.isfinished = true;
                AutoTextScript.isinteracting = false;
            }
        }

    }


    IEnumerator Execute_Dialogue_Coroutine()
    {
        if (can_interact == true)
        {
            if (AutoTextScript.isinteracting == false && can_skip == true)
            {
                state++;
            }
            switch (state)
            {
                case 0:
                    can_interact = false;
                    can_skip = false;
                    GetComponent<AudioSource>().Play();
                    yield return new WaitForSeconds(0.25f);
                    GetComponent<AudioSource>().Play();
                    yield return new WaitForSeconds(0.75f);
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().TTF_Overworld;
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Standing_Left", 0);

                    Camera.main.GetComponent<camera>().enabled = false;
                    yield return StartCoroutine(move_without_lerping(Camera.main.gameObject, new Vector3(-1.72f, 3.626f, -10f), 2.0f));

                    //Dialogue begins
                    AutoTextScript.isinteracting = true;
                    yield return StartCoroutine(Place_Objects());
                    message_owner_box.gameObject.SetActive(true);
                    message_owner_box.transform.Find("Text").GetComponent<Text>().text = "Gaster Dog";
                    private_characters[1].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                    message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    can_interact = true;
                    can_skip = true;
                    break;

                case 1:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                    break;

                case 2:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                    break;

                case 3:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                    break;
                case 4:
                    can_skip = false;
                    can_interact = false;
                    Camera.main.GetComponent<camera>().enabled = true;
                    StartCoroutine(FinishEverything());
                    break;
                default:

                    break;
            }
        }

    }



    IEnumerator Fade_Out_AudioSource(float FadeTime)
    {
        float startVolume = GetComponent<AudioSource>().volume;
        while (GetComponent<AudioSource>().volume > 0)
        {
            GetComponent<AudioSource>().volume -= startVolume * Time.deltaTime / FadeTime;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Play_Audiosource_Randomly()
    {
        while (GetComponent<AudioSource>().volume > 0)
        {
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(Random.Range(0.05f, 0.3f));
        }

    }





    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            player = collider.gameObject;
            player.GetComponent<movement>().can_move = false;
            player.GetComponent<movement>().can_dash = false;
            player.GetComponent<movement>().can_interact = false;
            if (player.GetComponent<movement>().is_dashing == true)
            {
                player.GetComponent<movement>().StopAllCoroutines();
                player.GetComponent<movement>().is_dashing = false;
                player.GetComponent<Animator>().SetBool("Dashing", false);
                player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
                player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
                player.GetComponent<Animator>().SetBool("moving", false);
            }


            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
            player.GetComponent<Animator>().SetBool("moving", false);

            Execute_Dialogue();
        }
    }


    IEnumerator Place_Objects()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);
        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);

        yield return new WaitForSeconds(0.5f);


        private_characters.Add(GameObject.Instantiate(character_portrait));
        private_characters.Add(GameObject.Instantiate(character_portrait));

        private_characters[0].transform.SetParent(Camera.main.transform.GetChild(0));
        private_characters[1].transform.SetParent(Camera.main.transform.GetChild(0));

        private_characters[0].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);
        private_characters[1].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);

        private_characters[0].transform.localPosition = new Vector3(-777, 72.5f, 0);
        private_characters[1].transform.localPosition = new Vector3(777, 72.5f, 0);


        private_characters[0].GetComponent<Image>().sprite = character_sprites[0];
        private_characters[1].GetComponent<Image>().sprite = character_sprites[1];

        private_characters[0].transform.localScale = new Vector3(1, 1, 1);
        private_characters[1].transform.localScale = new Vector3(1, 1, 1);

        private_characters[0].transform.SetAsFirstSibling();
        private_characters[1].transform.SetAsFirstSibling();
        black_background.transform.SetAsFirstSibling();

        private_characters[0].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
        private_characters[1].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);

        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);
        displaytext.gameObject.transform.localPosition = new Vector3(11, -220, -1);
        private_characters[0].transform.localPosition = new Vector3(-300, 72.5f, -1);
        private_characters[1].transform.localPosition = new Vector3(235, 72.5f, -1);
        can_interact = true;
    }

    IEnumerator move_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (Vector3.Distance(target.transform.localPosition, destination) > 1.5f)
        {
            target.transform.localPosition = Vector3.Lerp(target.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForEndOfFrame();
            speed += 0.02f;
        }
        yield break;
    }


    IEnumerator move_without_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (target.transform.position != destination)
        {
            target.transform.position = Vector3.MoveTowards(target.transform.position, destination, Time.deltaTime * speed);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FinishEverything()
    {
        //player.GetComponent<movement>().can_move = true;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Count; i++)
        {
            Destroy(private_characters[i]);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        player_data.playerdata.global_variables[44] = 1;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
        player.GetComponent<movement>().can_move = true;
        player.GetComponent<movement>().can_dash = true;
        player.GetComponent<movement>().can_interact = true;
        Destroy(gameObject);
    }

    IEnumerator PauseExecution()
    {
        can_interact = false;
        can_skip = false;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Count; i++)
        {
            private_characters[i].SetActive(false);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator ResumeExecution()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        message_owner_box.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);

        for (int i = 0; i < private_characters.Count; i++)
        {
            private_characters[i].SetActive(true);
        }

        yield return new WaitForSeconds(0.5f);
        can_interact = true;
        can_skip = true;
    }
}
