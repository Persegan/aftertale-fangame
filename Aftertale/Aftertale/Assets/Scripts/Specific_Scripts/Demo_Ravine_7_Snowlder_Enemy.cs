﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Demo_Ravine_7_Snowlder_Enemy : MonoBehaviour
{



    public battle_manager battle_manager;
    public GameObject speech_bubble;
    private bool finishedspeaking;
    private bool isspeaking;
    private string speakingmessage;
    public Text speech_bubble_text;

    public AudioClip speech_sound;

    public GameObject dialogue_box;

    //Stuff for the attack patterns
    public GameObject snowflake_attack;
    public GameObject snowball_attack;
    public GameObject iceshard_attack;
    public GameObject wind_attack;
    public GameObject wiretrap_attack;
    public GameObject spike_object;

    public GameObject soul;


    //Stuff for building the snowman
    private int head = 0;
    private int body = 0;
    private int arms = 0;
    private bool head1 = false;
    private bool head2 = false;
    private bool head3 = false;
    private bool body1 = false;
    private bool body2 = false;
    private bool body3 = false;
    private bool arms1 = false;
    private bool arms2 = false;
    private bool arms3 = false;

    public GameObject Snowlder_Arm_Left;
    public GameObject Snowlder_Arm_Right;
    public GameObject Snowlder_Body;
    public GameObject Snowlder_Head;
    public GameObject Snowlder_Snowball;

    public Sprite head0_sprite;
    public Sprite head1_sprite;
    public Sprite head2_sprite;
    public Sprite head3_sprite;

    public Sprite body0_sprite;
    public Sprite body1_sprite;
    public Sprite body2_sprite;
    public Sprite body3_sprite;

    public Sprite arms1_sprite_left;
    public Sprite arms1_sprite_right;
    public Sprite arms2_sprite_left;
    public Sprite arms2_sprite_right;
    public Sprite arms3_sprite_left;
    public Sprite arms3_sprite_right;

    private bool snowperson_complete = false;
    private bool weirdhead_dialogue = false;
    private bool temmiehead_musclebody = false;

    private int fight_phase = 0;
    private int phase_snowball_attack = 0;

    private int lastrandom = -1;

    //Colors for the fight
    Color light_blue_color = new Color(74 / 255f, 238 / 255f, 225 / 255f);


    // Use this for initialization
    void Start()
    {
        player_data.playerdata.global_variables[22] = 1;
        player_data.playerdata.global_variables[20] = 1;



        battle_manager = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<battle_manager>();

        //We add a memory if Signy was completed with pacifist
        if (player_data.playerdata.global_variables[22] == 1)
        {
            Color[] colors = new Color[3];
            colors[0] = light_blue_color;
            colors[1] = light_blue_color;
            colors[2] = light_blue_color;
            gameObject.GetComponent<enemy_data>().memories.Add(new Memory("Sign white", colors, "You heal when dashing!"));
        }

        //We add a memory if Signy was completed with genocide
        if (player_data.playerdata.global_variables[22] == 2)
        {
            Color[] colors = new Color[3];
            colors[0] = Color.red;
            colors[1] = Color.red;
            colors[2] = Color.red;
            gameObject.GetComponent<enemy_data>().memories.Add(new Memory("Sign black", colors, "You deal more damage until you get hit (scrub!)"));
        }

        //We add a memory if ball high score was achieved
        if (player_data.playerdata.global_variables[20] == 1)
        {
            Color[] colors = new Color[3];
            colors[0] = light_blue_color;
            colors[1] = light_blue_color;
            colors[2] = light_blue_color;
            gameObject.GetComponent<enemy_data>().memories.Add(new Memory("Ball high score", colors, "Reduced dash cooldown until hit!"));
        }

        //We add a memory if ball high score wasn't achieved but interaction occurred. 
        else if (player_data.playerdata.global_variables[20] == 0 || player_data.playerdata.global_variables[19] != 0)
        {
            Color[] colors = new Color[3];
            colors[0] = light_blue_color;
            colors[1] = light_blue_color;
            colors[2] = light_blue_color;
            gameObject.GetComponent<enemy_data>().memories.Add(new Memory("Ball high score", colors, "Reduced dash cooldown for 3 turns!"));
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (finishedspeaking == true)
            {
                finishedspeaking = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (isspeaking == true)
            {
                finishedspeaking = true;
                AutoTextScript.isfinished = true;
                speech_bubble_text.StopAllCoroutines();
                speakingmessage = speakingmessage.Replace("ç", "\n");
                speech_bubble_text.text = speakingmessage;
            }

        }

    }



    ///Auxiliar methods
    IEnumerator ShowDialogue(string message)
    {
        speakingmessage = message;
        isspeaking = true;
        speech_bubble.SetActive(true);
        speech_bubble_text.gameObject.SetActive(true);
        speech_bubble_text.text = "";
        AutoTextScript.TypeText(speech_bubble_text, message, 0.03f, null);
        while (AutoTextScript.isfinished == false)
        {
            //speech_sound.pitch = Random.Range (0.3f, 1.9f);
            GetComponent<AudioSource>().clip = speech_sound;
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.1f);
        }
        finishedspeaking = true;
        while (finishedspeaking == true)
        {
            yield return new WaitForEndOfFrame();
        }
        //yield return new WaitForSeconds (2.0f);
        speech_bubble.SetActive(false);
        speech_bubble_text.gameObject.SetActive(false);
    }

    public void attack(int turn)
    {
        StartCoroutine(attack_coroutine(turn));
    }

    public void dialogue(int turn)
    {
        StartCoroutine(dialogue_coroutine(turn));
    }

    public void ExecuteInteraction(string name)
    {
        StartCoroutine(ExecuteInteraction_coroutine(name));
    }

    public void ExecuteMemory(string name)
    {
        StartCoroutine(ExecuteMemory_coroutine(name));
    }



    IEnumerator attack_coroutine(int turn)
    {
        if (snowperson_complete == true)
        {
            if (head == 1)
            {
                yield return (ShowDialogue("MY SINCERE APOLOGIES,çHUMAN."));
                yield return (ShowDialogue("I'VE MADE MYSELF QUITEçAN INCONVENIENCE."));
                snowperson_complete = false;
            }
            else if (head == 2)
            {
                yield return (ShowDialogue("!!!wher tEMMIE mannrs??"));
                yield return (ShowDialogue("tEMMIE go nOW!!!"));
                snowperson_complete = false;
            }
            else if (head == 3)
            {
                yield return (ShowDialogue("... Hmm..."));
                yield return (ShowDialogue(".... K."));
                snowperson_complete = false;
            }
        }

        if (weirdhead_dialogue == true)
        {
            yield return (ShowDialogue("... Hey."));
            weirdhead_dialogue = false;
        }

        if (temmiehead_musclebody == true)
        {
            yield return (ShowDialogue("!!!nNO!MUSCLES r NOtçCUTE!!"));
            temmiehead_musclebody = false;
        }

        if (phase_snowball_attack == 1)
        {
            yield return StartCoroutine(attack_random_0());
            phase_snowball_attack++;
        }

        else if (fight_phase == 0)
        {
            yield return StartCoroutine(attack_0());
            fight_phase++;
        }
        else if (fight_phase == 1)
        {
            yield return StartCoroutine(attack_1());
            fight_phase++;
        }
        else if (fight_phase == 2)
        {
            yield return StartCoroutine(attack_2());
            fight_phase++;
        }
        else if (fight_phase == 3)
        {
            int random = Random.Range(0, 9);
            if (phase_snowball_attack != 2)
            {
                while (random == 2)
                {
                    random = Random.Range(0, 9);
                }
            }
           
            while (random == lastrandom)
            {
                random = Random.Range(0, 9);
            }
            lastrandom = random;

            switch (random)
            {
                case 0:
                    yield return StartCoroutine(attack_1());
                    break;

                case 1:
                    yield return StartCoroutine(attack_2());
                    break;

                case 2:
                    yield return StartCoroutine(attack_random_0());
                    break;

                case 3:
                    yield return StartCoroutine(attack_random_1());
                    break;

                case 4:
                    yield return StartCoroutine(attack_random_2());
                    break;

                case 5:
                    yield return StartCoroutine(attack_random_3());
                    break;

                case 6:
                    yield return StartCoroutine(attack_random_4());
                    break;

                case 7:
                    yield return StartCoroutine(attack_random_5());
                    break;

                case 8:
                    yield return StartCoroutine(attack_random_6());
                    break;

                default:

                    break;
            }
        }

        //yield return new WaitForSeconds(10.0f);
        battle_manager.StartPlayerTurn();
    }

    IEnumerator dialogue_coroutine(int turn)
    {
        int temporal = Random.Range(1, 4);
        switch (temporal)
        {
            case 1:
                battle_manager.dialogue_box_message = "* ( It's a Snowlder. That is to say, a boulder madeçout of snow.)";
                break;
            case 2:
                battle_manager.dialogue_box_message = "* Snowlder blocks the way!";
                break;
            case 3:
                battle_manager.dialogue_box_message = "* Snowlder is totally ignoring you!";
                break;

        }

        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteInteraction_coroutine(string name)
    {
        for (int i = 0; i < GetComponent<enemy_data>().interactions.Count; i++)
        {
            if (name == GetComponent<enemy_data>().interactions[i].name)
            {
                GetComponent<enemy_data>().interactions[i].unread = false;
                battle_manager.dialogue_box_message = GetComponent<enemy_data>().interactions[i].message;
                battle_manager.SetDialogueBox(GetComponent<enemy_data>().interactions[i].message);
            }
        }
        switch (name)
        {
            //First we account for the original options in phase 1
            case "> Ask Snowlder to Move":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Ask Snowlder to Move", "* You impatiently ask the Snowlder to move.ç çBut it refused.", false);
                
                battle_manager.GiveMemoryPortion(this.gameObject, light_blue_color, light_blue_color);
                break;

            case "> Tell the Snowlder a Joke":
                battle_manager.GiveMemoryPortion(this.gameObject, light_blue_color, light_blue_color);
                break;


            case "> Play in the Snow":

                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Roll a Snowball", "* ( Snow is no problem, you decide to just rollçwith it.)", true);
                battle_manager.GiveMemoryPortion(this.gameObject, light_blue_color, light_blue_color);

                break;

            case "> Roll a Snowball":
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Build a Snowperson", "* ( Inspiration strikes! This Snowlder is beggingçto be sculpted into a Snowperson.)", true);
                Snowlder_Snowball.SetActive(true);
                phase_snowball_attack = 1;
                break;


            //This option procs phase 2
            case "> Build a Snowperson":

                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You can sense a beautiful body hiding insideçthis lump of snow.)", true);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", "* ( Without a proper face, there is no Snowperson.)", true);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", "* ( No Snowperson could be complete without a setçof friendly arms.)", true);
                Snowlder_Snowball.SetActive(false);
                Snowlder_Head.SetActive(true);


                break;



            #region Options for phase 2

            case "> Sculpt the Body":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Muscular Physique", "* You're feeling generous, and chisel out someçsweet abs for the Snowperson.", (body1 == false) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Tiny and Cute", "* Small. Adorable. The body of an intellectual.", (body2 == false) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Keep it Traditional", "* You decide to go the extra mile and carve someçbuttons.", (body3 == false) ? true : false);
                break;

            case "> Work on the Head":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> A Friendly Face", "* You carve out Gaster Dog's face from memory.ç çNot half bad!", (head1 == false) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> A Heroic Visage", "* You vaguely recall an epic mural, and model theçface after the hero.", (head2 == false) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Keep it Plain", "* You try for a traditional Snowperson face, but...çuh...", (head3 == false) ? true : false);
                break;

            case "> Give it some Arms":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Strong Arms", "* Strong arms. Powerful arms. Friendly arms.", (arms1 == false) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Sexy Arms", "* Because this Snowperson deserves to feelçfabulous!", (arms2 == false) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Sticks Will Do", "* Wood is, afterall, a trustworthy and reliableçsource of arms.", (arms3 == false) ? true : false);
                break;
            #endregion





            #region Options for when you sculpt the body

            case "> Muscular Physique":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You decide to take another pass at the body.)", false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", (head == 0) ? "* ( Without a proper face, there is no Snowperson.)" : "* ( Perhaps a different face is in order.)", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", (arms == 0) ? "* ( No Snowperson could be complete without a setçof friendly arms.)" : "* ( You decide to go with a different set of arms.)", (arms == 0) ? true : false);
                body = 1;
                body1 = true;

                if (head == 2)
                {
                    temmiehead_musclebody = true;
                }

                if (head == 0 || head == 3)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(10, 145, 0);
                }

                else if (head == 1)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(18, 135, 0);
                }

                else if (head == 2)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(-5, 145, 0);
                }


                if (arms == 1)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-190, 60, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(180, 60, 0);
                }

                else if (arms == 2)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-180, 80, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(250, 60, 0);
                }

                else if (arms == 3)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-215, 80, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(215, 80, 0);
                }

                Snowlder_Body.GetComponent<Image>().sprite = body1_sprite;
                Snowlder_Body.GetComponent<Image>().SetNativeSize();
                break;

            case "> Tiny and Cute":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You decide to take another pass at the body.)", false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", (head == 0) ? "* ( Without a proper face, there is no Snowperson.)" : "* ( Perhaps a different face is in order.)", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", (arms == 0) ? "* ( No Snowperson could be complete without a setçof friendly arms.)" : "* ( You decide to go with a different set of arms.)", (arms == 0) ? true : false);
                body = 2;
                body2 = true;
                Snowlder_Body.GetComponent<Image>().sprite = body2_sprite;
                Snowlder_Body.GetComponent<Image>().SetNativeSize();

                if (head == 0 || head == 3)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(15, 135, 0);
                }

                else if (head == 1)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(26, 55, 0);
                }

                else if (head == 2)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(0, 135, 0);
                }


                if (arms == 1)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-160, 40, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(160, 40, 0);
                }

                else if (arms == 2)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-125, 80, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(205, 60, 0);
                }

                else if (arms == 3)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-165, 50, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(180, 70, 0);
                }
                break;

            case "> Keep it Traditional":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", "* ( You decide to take another pass at the body.)", false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", (head == 0) ? "* ( Without a proper face, there is no Snowperson.)" : "* ( Perhaps a different face is in order.)", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", (arms == 0) ? "* ( No Snowperson could be complete without a setçof friendly arms.)" : "* ( You decide to go with a different set of arms.)", (arms == 0) ? true : false);
                body = 3;
                body3 = true;
                Snowlder_Body.GetComponent<Image>().sprite = body3_sprite;
                Snowlder_Body.GetComponent<Image>().SetNativeSize();

                if (head == 0 || head == 3)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(20, 125, 0);
                }

                else if (head == 1)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(22, 109, 0);
                }

                else if (head == 2)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(0, 125, 0);
                }


                if (arms == 1)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-215, 10, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(205, 10, 0);
                }

                else if (arms == 2)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-180, 55, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(230, 55, 0);
                }

                else if (arms == 3)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-215, 10, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(205, 10, 0);
                }
                break;
            #endregion



            #region Options for when you work on the head
            case "> A Friendly Face":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding insideçthis lump of snow.)" : "* ( You decide to take another pass at the body.)", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", "* ( Perhaps a different face is in order.)", false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", (arms == 0) ? "* ( No Snowperson could be complete without a setçof friendly arms.)" : "* ( You decide to go with a different set of arms.)", (arms == 0) ? true : false);
                head = 1;
                head1 = true;
                Snowlder_Head.GetComponent<Image>().sprite = head1_sprite;
                Snowlder_Head.GetComponent<Image>().SetNativeSize();
                if (body == 0 || body == 3)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(22, 109, 0);
                }
                else if (body == 1)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(18, 135, 0);
                }
                else if (body == 2)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(26, 55, 0);
                }
                break;

            case "> A Heroic Visage":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding insideçthis lump of snow.)" : "* ( You decide to take another pass at the body.)", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", "* ( Perhaps a different face is in order.)", false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", (arms == 0) ? "* ( No Snowperson could be complete without a setçof friendly arms.)" : "* ( You decide to go with a different set of arms.)", (arms == 0) ? true : false);
                head = 2;
                head2 = true;
                if (body == 1)
                {
                    temmiehead_musclebody = true;
                }
                Snowlder_Head.GetComponent<Image>().sprite = head2_sprite;
                Snowlder_Head.GetComponent<Image>().SetNativeSize();
                if (body == 0 || body == 3)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(0, 125, 0);
                }
                else if (body == 1)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(-5, 145, 0);
                }
                else if (body == 2)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(0, 135, 0);
                }
                break;

            case "> Keep it Plain":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding insideçthis lump of snow.)" : "* ( You decide to take another pass at the body.)", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", "* ( Perhaps a different face is in order.)", false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", (arms == 0) ? "* ( No Snowperson could be complete without a setçof friendly arms.)" : "* ( You decide to go with a different set of arms.)", (arms == 0) ? true : false);
                head = 3;
                head3 = true;
                weirdhead_dialogue = true;
                Snowlder_Head.GetComponent<Image>().sprite = head3_sprite;
                Snowlder_Head.GetComponent<Image>().SetNativeSize();
                if (body == 0 || body == 3)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(20, 125, 0);
                }
                else if (body == 1)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(10, 145, 0);
                }
                else if (body == 2)
                {
                    Snowlder_Head.transform.localPosition = new Vector3(15, 135, 0);
                }
                break;
            #endregion


            #region Options for when you give it some arms
            case "> Strong Arms":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding insideçthis lump of snow.)" : "* ( You decide to take another pass at the body.)", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", (head == 0) ? "* ( Without a proper face, there is no Snowperson.)" : "* ( Perhaps a different face is in order.)", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", "* ( You decide to go with a different set of arms.)", false);
                arms = 1;
                arms1 = true;
                Snowlder_Arm_Left.SetActive(true);
                Snowlder_Arm_Left.GetComponent<Image>().sprite = arms1_sprite_left;
                Snowlder_Arm_Left.GetComponent<Image>().SetNativeSize();
                Snowlder_Arm_Right.SetActive(true);
                Snowlder_Arm_Right.GetComponent<Image>().sprite = arms1_sprite_right;
                Snowlder_Arm_Right.GetComponent<Image>().SetNativeSize();
                if (body == 0 || body == 3)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-215, 10, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(205, 10, 0);
                }
                else if (body == 1)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-190, 60, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(180, 60, 0);
                }
                else if (body == 2)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-160, 40, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(160, 40, 0);
                }
                break;

            case "> Sexy Arms":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding insideçthis lump of snow.)" : "* ( You decide to take another pass at the body.)", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", (head == 0) ? "* ( Without a proper face, there is no Snowperson.)" : "* ( Perhaps a different face is in order.)", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", "* ( You decide to go with a different set of arms.)", false);
                arms = 2;
                arms2 = true;
                Snowlder_Arm_Left.SetActive(true);
                Snowlder_Arm_Left.GetComponent<Image>().sprite = arms2_sprite_left;
                Snowlder_Arm_Left.GetComponent<Image>().SetNativeSize();
                Snowlder_Arm_Right.SetActive(true);
                Snowlder_Arm_Right.GetComponent<Image>().sprite = arms2_sprite_right;
                Snowlder_Arm_Right.GetComponent<Image>().SetNativeSize();
                if (body == 0 || body == 3)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-180, 55, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(230, 55, 0);
                }
                else if (body == 1)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-180, 80, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(250, 60, 0);
                }
                else if (body == 2)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-125, 80, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(205, 60, 0);
                }
                break;

            case "> Sticks Will Do":
                GetComponent<enemy_data>().interactions[0] = new enemy_interaction("> Sculpt the Body", (body == 0) ? "* ( You can sense a beautiful body hiding insideçthis lump of snow.)" : "* ( You decide to take another pass at the body.)", (body == 0) ? true : false);
                GetComponent<enemy_data>().interactions[1] = new enemy_interaction("> Work on the Head", (head == 0) ? "* ( Without a proper face, there is no Snowperson.)" : "* ( Perhaps a different face is in order.)", (head == 0) ? true : false);
                GetComponent<enemy_data>().interactions[2] = new enemy_interaction("> Give it some Arms", "* ( You decide to go with a different set of arms.)", false);
                arms = 3;
                arms3 = true;
                Snowlder_Arm_Left.SetActive(true);
                Snowlder_Arm_Left.GetComponent<Image>().sprite = arms3_sprite_left;
                Snowlder_Arm_Left.GetComponent<Image>().SetNativeSize();
                Snowlder_Arm_Right.SetActive(true);
                Snowlder_Arm_Right.GetComponent<Image>().sprite = arms3_sprite_right;
                Snowlder_Arm_Right.GetComponent<Image>().SetNativeSize();
                if (body == 0 || body == 3)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-215, 10, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(205, 10, 0);
                }
                else if (body == 1)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-215, 80, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(215, 80, 0);
                }
                else if (body == 2)
                {
                    Snowlder_Arm_Left.transform.localPosition = new Vector3(-165, 50, 0);
                    Snowlder_Arm_Right.transform.localPosition = new Vector3(180, 70, 0);
                }
                break;
            #endregion

            case "> Ask Snowperson to Move":
                snowperson_complete = true;
                break;
        }

        if (head != 0 && body != 0 && arms != 0)
        {
            if (GetComponent<enemy_data>().interactions.Count == 4)
            {
                GetComponent<enemy_data>().interactions[3] = new enemy_interaction("> Ask Snowperson to Move", "* You politely ask the Snowperson to move.", true);
            }
            else
            {
                GetComponent<enemy_data>().interactions.Add(new enemy_interaction("> Ask Snowperson to Move", "* You politely ask the Snowperson to move.", true));
            }
        }

        yield return new WaitForEndOfFrame();
    }

    IEnumerator ExecuteMemory_coroutine(string name)
    {
        yield return new WaitForEndOfFrame();
        Memory auxiliar;

        for (int i = 0; i < GetComponent<enemy_data>().memories.Count; i++)
        {
            if (GetComponent<enemy_data>().memories[i].name == name)
            {
                auxiliar = GetComponent<enemy_data>().memories[i];
                battle_manager.dialogue_box_message = auxiliar.message;
                battle_manager.SetDialogueBox(auxiliar.message);
                break;
            }
        }



        switch (name)
        {
            case "Sign white": //Signy white memory
                battle_manager.Buff_HealOnDash(1, 3, 2, Color.white);
                break;

            case "Sign black": //Signy black memory
                battle_manager.Buff_HealOnDash(0, 3, 2, Color.white);
                break;

            case "Ball high score": //ball high score memory
                battle_manager.Buff_DashCooldownReduction(2, 0, 0.1f, Color.white);
                break;

            case "Ball no high score": //ball interaction memory
                battle_manager.Buff_DashCooldownReduction(2, 0, 0.1f, Color.white);
                break;

            case "White ending": //white interaction memory

                break;

            case "Black ending": //black interaction memory

                break;

            default:
                break;
        }
    }

    public void ExecutePacifistEnding()
    {
        /*Overworld_Signy.transform.parent.gameObject.SetActive(true);
        Overworld_Signy.SetActive(true);
        Overworld_Signy.GetComponent<Demo_Ravine_6_Signey_Overworld_Fight>().Finish_Battle_Pacifist();
        soul.SetActive(false);*/
    }

    public void ExecuteGenocideEnding()
    {

    }



    //Pattern attack coroutines. First the hard coded ones, then the random ones

    //Attack 0 is just wind that pushes to one side with spikes in a resized box, and then wind pushes to the other side for a bit.
    IEnumerator attack_0()
    {
        yield return StartCoroutine(resizedialoguebox(new Vector3(1.5f, 1.0f, 1.0f), 9.0f));
        StartCoroutine(spiky_boys(false, 16.2f));
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(attack_wind(2, 0.05f, 0.52f, 0.0035f, 6.5f));
        yield return new WaitForSeconds(7.2f);
        StartCoroutine(attack_wind(6, 0.05f, 0.52f, 0.0035f, 6.5f));
        yield return new WaitForSeconds(7.5f);
        dialogue_box.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
     
    //Attack 1 is two sets of snowflakes patterns with small pause in between
    IEnumerator attack_1()
    {
        StartCoroutine(attack_snowflakes(5));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_snowflakes(5));
        yield return new WaitForSeconds(9.0f);
    }

    //Attack 2 is vertical wind and snowflakes
    IEnumerator attack_2()
    {
        //StartCoroutine(spiky_boys(true, 15.0f));
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(attack_wind(4, 0.05f, 0.27f, 0.0035f, 9.0f));
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(attack_snowflakes(5));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_snowflakes(5));
        yield return new WaitForSeconds(10.0f);
        //StartCoroutine(attack_wind(0, 0.05f, 0.2f, 0.005f, 6.0f));
        //yield return new WaitForSeconds(7.0f);
    }




    //Random patterns


    //attack 0 is the snowballs pattern, considered a "break" pattern
    IEnumerator attack_random_0()
    {
        StartCoroutine(attack_snowballs());
        yield return new WaitForSeconds(5.5f);
    }

    //attack 1 does two sets of snowflakes + wind that pulls from a random direction)
    IEnumerator attack_random_1()
    {
        int random = Random.Range(0, 8);
        StartCoroutine(attack_wind(random, 0.05f, 0.27f, 0.0035f, 5.3f));
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(6.3f);
    }

    //Attack 2 does shards + 1 set of snowflakes
    IEnumerator attack_random_2()
    {
        StartCoroutine(attack_snowflakes(3));
        StartCoroutine(attack_shards(6, 0.7f));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(6.3f);
    }

    //attack 3 does 2 set of snowflakes and horizontal icicles
    IEnumerator attack_random_3()
    {
        StartCoroutine(spiky_boys(false, 7.2f));
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(6.3f);
    }

    //attack 4 does 2 sets of snowflakes and vertical icicles
    IEnumerator attack_random_4()
    {
       StartCoroutine(spiky_boys(true, 8.2f));
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_snowflakes(3));
        yield return new WaitForSeconds(6.3f);
    }

    //attack 5 does shards + wind that pulls on a random direction
    IEnumerator attack_random_5()
    {
        int random = Random.Range(0, 8);
        StartCoroutine(attack_wind(random, 0.05f, 0.18f, 0.005f, 6.4f));
        StartCoroutine(attack_shards(7, 0.7f));
        yield return new WaitForSeconds(7.2f);
    }

    /*attack 6 does icicles on borders + wind, it's kind of a break pattern
    IEnumerator attack_random6()
    {
        int random = Random.Range(0, 8);
        yield return StartCoroutine(resizedialoguebox(new Vector3(0.8f, 0.8f, 1.0f), 6.6f));
        spiky_boys(true, 6.5f);
        spiky_boys(false, 6.5f);
        StartCoroutine(attack_wind(random, 0.05f, 0.32f, 0.005f, 6.1f));
        yield return new WaitForSeconds(6.5f);
        dialogue_box.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }*/

    //attack 6 is just attack 1 again, but slightly stronger.
    IEnumerator attack_random_6()
    {
        yield return StartCoroutine(resizedialoguebox(new Vector3(1.6f, 1.0f, 1.0f), 9.0f));
        StartCoroutine(spiky_boys(false, 9.1f));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(attack_wind(2, 0.05f, 0.58f, 0.0085f, 3.5f));
        yield return new WaitForSeconds(4.2f);
        StartCoroutine(attack_wind(6, 0.05f, 0.58f, 0.0085f, 3.5f));
        yield return new WaitForSeconds(4.0f);
        dialogue_box.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }


    //Auxiliary attack coroutines

    IEnumerator attack_snowflakes(int number_of_snowflakes)
    {
        Vector3 spawn_helper = new Vector3(0, 101, 1);
        for (int i = 0; i < number_of_snowflakes; i++)
        {
            int repetition_safe = Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position;
            while (Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position == repetition_safe)
            {
                Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position = Random.Range(1, 6);
            }
            switch (Demo_Ravine_7_Snowlder_Snowflake_Attack.spawn_position)
            {
                case 1:
                    spawn_helper = new Vector3(-117, spawn_helper.y, 1);
                    break;

                case 2:
                    spawn_helper = new Vector3(-57f, spawn_helper.y, 1);
                    break;

                case 3:
                    spawn_helper = new Vector3(3, spawn_helper.y, 1);
                    break;

                case 4:
                    spawn_helper = new Vector3(60f, spawn_helper.y, 1);
                    break;

                case 5:
                    spawn_helper = new Vector3(120, spawn_helper.y, 1);
                    break;

                default:
                    break;
            }
            GameObject temp = Instantiate(snowflake_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localPosition = spawn_helper;
            yield return new WaitForSeconds(1.5f);
        }
    }

    IEnumerator attack_snowballs()
    {
        Vector3 spawn_helper = new Vector3(127, -180, 1);
        GameObject temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = false;

        spawn_helper = new Vector3(127, -90, 1);
        temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = false;

        spawn_helper = new Vector3(-118, -135, 1);
        temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = true;

        spawn_helper = new Vector3(-118, -45, 1);
        temp = Instantiate(snowball_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.transform.localPosition = spawn_helper;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Snowball_Attack>().behaviour = true;

        yield return new WaitForEndOfFrame();
    }

    IEnumerator attack_shards(int number_of_shards, float time_between_shards)
    {
        float range = 50f;

        for (int i = 0; i < number_of_shards; i++)
        {
            Vector3 randomPos = Random.insideUnitSphere * range;
            randomPos.z = 0;
            Vector3 spawn_helper = GameObject.Find("Battle_Canvas").transform.InverseTransformPoint(soul.transform.position) + randomPos;
            GameObject temp = Instantiate(iceshard_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localPosition = spawn_helper;
            yield return new WaitForSeconds(time_between_shards);
        }

    }


    //Direction goes from 0 to 7 and indicates the direction you're pushing the player in, starting in "down" and going clockwise..
    //initial strength is the initial strength of the wind, the one it has upon instantiation
    //final strength is th maximum strength it can achieve during its lifetime
    //increment is how fast it gains strength from intial strength to final strength per second
    //duration is how long the wind lasts. 
    IEnumerator attack_wind(int direction, float initial_strength, float final_strength, float increment, float duration)
    {
        Vector3 spawn_helper = new Vector3();
        Vector2 push_direction = new Vector2();

        switch (direction)
        {
            case 0:
                spawn_helper = new Vector3(14, 74, 0);
                push_direction = Vector2.down;
                break;
            case 1:
                spawn_helper = new Vector3(239, 105, 0);
                push_direction = new Vector2(-0.5f, -0.5f);
                break;
            case 2:
                spawn_helper = new Vector3(239, -112, 0);
                push_direction = Vector2.left;
                break;
            case 3:
                spawn_helper = new Vector3(194.5f, -286, 0);
                push_direction = new Vector2(-0.5f, 0.5f);
                break;
            case 4:
                spawn_helper = new Vector3(0, -286, 0);
                push_direction = Vector2.up;
                break;
            case 5:
                spawn_helper = new Vector3(-175, -286, 0);
                push_direction = new Vector2(0.5f, 0.5f);
                break;
            case 6:
                spawn_helper = new Vector3(-239, -112, 0);
                push_direction = Vector2.right;
                break;
            case 7:
                spawn_helper = new Vector3(-159, 58, 0);
                push_direction = new Vector2(0.5f, -0.5f);
                break;
            default:
                break;
        }

        GameObject temp = Instantiate(wind_attack, spawn_helper, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().push_direction = push_direction;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().initial_strength = initial_strength;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().final_strength = final_strength;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().increment = increment;
        temp.GetComponent<Demo_Ravine_7_Snowlder_Wind_Attack>().duration = duration;
        temp.transform.localPosition = spawn_helper;

        yield return new WaitForEndOfFrame();
    }



    //State is wether the wire is vertical or horizontal. False for vertical, true for horizontal. Position is the position the wire is instantiated in.
    //Wires are hard coded, and they're always the same. if you want to change their behaviour you need to go to their own script.
    IEnumerator attack_wiretrap(bool state, Vector3 position)
    {
        if (state == false)
        {
            GameObject temp = Instantiate(wiretrap_attack, position, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localPosition = position;
        }

        else
        {
            GameObject temp = Instantiate(wiretrap_attack, position, Quaternion.identity, GameObject.Find("Battle_Canvas").transform);
            temp.transform.localRotation = Quaternion.Euler(0, 0, 90);
            temp.GetComponent<RectTransform>().sizeDelta = new Vector2(temp.GetComponent<RectTransform>().sizeDelta.x, 315);
            temp.transform.localPosition = position;
        }
        yield return new WaitForEndOfFrame();
    }

    //This function spawns spikes along the edges of the dialogue box. It receives two parameters, orientation and duration.
    //For orientiation, if false it'll spawn the spikes in the sides of the box but if true it'll spawn them on the upper and bottom part of the box.
    //For duration, that determines the amount of time the spikes will stick around before being destroyed.
    IEnumerator spiky_boys(bool orientation, float duration)
    {
        List<GameObject> spikygameobjects = new List<GameObject>();

        //We spawn shards on the sides of the box
        if (orientation == false)
        {
            //We spawn from bottom to top on the left side of the box
            for (float i = dialogue_box.transform.localPosition.y - (dialogue_box.GetComponent<RectTransform>().sizeDelta.y/2 * dialogue_box.transform.localScale.y); i < dialogue_box.transform.localPosition.y + dialogue_box.GetComponent<RectTransform>().sizeDelta.y / 2 * dialogue_box.transform.localScale.y; i += spike_object.GetComponent<RectTransform>().sizeDelta.x)
            {
                GameObject temporal = GameObject.Instantiate(spike_object, GameObject.Find("Battle_Canvas").transform);
                spikygameobjects.Add(temporal);
                temporal.transform.localPosition = new Vector3(dialogue_box.transform.localPosition.x - dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2 * dialogue_box.transform.localScale.x, i, 0);
                temporal.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                //Destroy(temporal, duration);
                temporal.transform.localRotation = Quaternion.Euler(0, 0, -90);
                yield return new WaitForSeconds(0.1f);

            }

            //We spawn from bottom to top on the right side of the box
            for (float i = dialogue_box.transform.localPosition.y - dialogue_box.GetComponent<RectTransform>().sizeDelta.y / 2 * dialogue_box.transform.localScale.y; i < dialogue_box.transform.localPosition.y + dialogue_box.GetComponent<RectTransform>().sizeDelta.y / 2 * dialogue_box.transform.localScale.y; i += spike_object.GetComponent<RectTransform>().sizeDelta.x)
            {
                GameObject temporal = GameObject.Instantiate(spike_object, GameObject.Find("Battle_Canvas").transform);
                spikygameobjects.Add(temporal);
                temporal.transform.localPosition = new Vector3(dialogue_box.transform.localPosition.x + dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2 * dialogue_box.transform.localScale.x, i, 0);
                temporal.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                //Destroy(temporal, duration);
                temporal.transform.localRotation = Quaternion.Euler(0, 0, 90);
                duration -= 0.1f;
                yield return new WaitForSeconds(0.1f);
            }
        }
        else if (orientation == true)
        {
            //We spawn from left to right on the bottom of the box
            for (float i = dialogue_box.transform.localPosition.x - dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2 * dialogue_box.transform.localScale.x; i < dialogue_box.transform.localPosition.x + dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2 * dialogue_box.transform.localScale.x; i += spike_object.GetComponent<RectTransform>().sizeDelta.y)
            {
                GameObject temporal = GameObject.Instantiate(spike_object, GameObject.Find("Battle_Canvas").transform);
                spikygameobjects.Add(temporal);
                temporal.transform.localPosition = new Vector3(i, dialogue_box.transform.localPosition.y - dialogue_box.GetComponent<RectTransform>().sizeDelta.y / 2 * dialogue_box.transform.localScale.y, 0);
                temporal.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                Destroy(temporal, duration);
                yield return new WaitForSeconds(0.1f);
                duration -= 0.1f;
            }

            for (float i = dialogue_box.transform.localPosition.x - dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2 * dialogue_box.transform.localScale.x; i < dialogue_box.transform.localPosition.x + dialogue_box.GetComponent<RectTransform>().sizeDelta.x / 2 * dialogue_box.transform.localScale.x; i += spike_object.GetComponent<RectTransform>().sizeDelta.y)
            {
                GameObject temporal = GameObject.Instantiate(spike_object, GameObject.Find("Battle_Canvas").transform);
                spikygameobjects.Add(temporal);
                temporal.transform.localPosition = new Vector3(i, dialogue_box.transform.localPosition.y + dialogue_box.GetComponent<RectTransform>().sizeDelta.y / 2 * dialogue_box.transform.localScale.y, 0);
                temporal.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                Destroy(temporal, duration);
                temporal.transform.localRotation = Quaternion.Euler(0, 0, 180);
                yield return new WaitForSeconds(0.1f);
                duration -= 0.1f;
            }
        }
        for (int i = 0; i < spikygameobjects.Count; i++)
        {
            Destroy(spikygameobjects[i], duration - spikygameobjects.Count*0.1f);
        }
    }



    IEnumerator resizedialoguebox(Vector3 newscale, float speed)
    {
        //Remember to return the scale to Vector3(1,1,1) at the end of each attack!
        //Example code:
        //At the beginning of an attack:
        /*
         yield return new WaitForSeconds(1.0f) //So that we wait until the dialogue box has been resized once
         resizedialoguebox(Vector3 (0.2f, 0.3f, 1.0F); //We input the desired new size
         resizedialoguebox(Vector3 (1.0f,1.0f,1.0f); //We reset the scale of the box at the end of the turn;
          */
        while (Vector3.Distance(dialogue_box.transform.localScale, newscale) > 0.05f)
        {
            dialogue_box.transform.localScale = Vector3.Lerp(dialogue_box.transform.localScale, newscale, speed * Time.deltaTime);

            // If you don't want an eased scaling, replace the above line with the following line
            //   and change speed to suit:
            // transform.localScale = Vector3.MoveTowards (transform.localScale, newscale, speed * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }

    }
}
