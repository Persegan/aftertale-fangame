﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_4_Ice_Puzzle_Collisions : MonoBehaviour {

    public GameObject River_Collision;
    public GameObject Ice_Block;

	// Use this for initialization
	void Awake () {
        GameObject Player = GameObject.Find("Pumpkin");

        BoxCollider2D[] BoxArray = gameObject.GetComponents<BoxCollider2D>();

        foreach (BoxCollider2D b in BoxArray)
        {
            Physics2D.IgnoreCollision(Player.GetComponent<BoxCollider2D>(), b, true);
        }
        Physics2D.IgnoreCollision(River_Collision.GetComponent<PolygonCollider2D>(), Ice_Block.GetComponent<BoxCollider2D>());

     
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
