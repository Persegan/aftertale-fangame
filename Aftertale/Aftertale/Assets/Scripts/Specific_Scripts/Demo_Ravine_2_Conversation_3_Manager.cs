﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Demo_Ravine_2_Conversation_3_Manager : MonoBehaviour {

    public int state = 0;

    private GameObject player;
    public bool caninteract = false;
    public GameObject Standing_Still_Conversation_Object;

    // Use this for initialization
    void Start()
    {
        state = 0;
        caninteract = false;

    }

    // Update is called once per frame
    void Update()
    {


        if (state == 1)
        {

            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && caninteract == true)
                {
                    BroadcastMessage("Execute_Dialogue");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (caninteract == true)
                    {
                        BroadcastMessage("Skip_Dialogue");
                    }
                }
            }
            if (gameObject.GetComponent<Demo_Ravine_2_Conversation_3>().complete == true)
            {
                state = 2;
                caninteract = false;
            }

        }

        else if (state == 2)
        {
            Standing_Still_Conversation_Object.SetActive(true);
            Standing_Still_Conversation_Object.GetComponent<Demo_Ravine_3_Conversation_4_Manager>().Restart();
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            player = collider.gameObject;
            StartCoroutine(ScriptedMovement1());

        }
    }

    IEnumerator ScriptedMovement1()
    {
        if (player.GetComponent<movement>().is_dashing == true)
        {
            player.GetComponent<movement>().StopAllCoroutines();
            player.GetComponent<movement>().is_dashing = false;
            player.GetComponent<Animator>().SetBool("Dashing", false);
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
            player.GetComponent<Animator>().SetBool("moving", false);
        }
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;

        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
        player.GetComponent<Animator>().SetBool("moving", false);

        BroadcastMessage("Execute_Dialogue");
        caninteract = true;
        state = 1;
        Standing_Still_Conversation_Object.SetActive(false);

        yield return 0;
    }
}
