﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_6_Ramp_Collision_Change_2 : MonoBehaviour {
    public PolygonCollider2D NewCollisions;
    public PolygonCollider2D OldCollisions;

    public GameObject[] Foregrounds;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            NewCollisions.gameObject.SetActive(true);
            OldCollisions.gameObject.SetActive(false);

            for (int i = 0; i < Foregrounds.Length; i++)
            {
                if (Foregrounds[i].GetComponent<Change_Layer_For_Character>() == true)
                {
                    Foregrounds[i].GetComponent<Change_Layer_For_Character>().enabled = true;
                }
                Foregrounds[i].GetComponent<SpriteRenderer>().sortingOrder = 2;
            }
        }
    }
}
