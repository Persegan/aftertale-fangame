﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_7_Ramp : MonoBehaviour {

    public float force;
    public float dashforce;

    private bool activated;

    private GameObject Player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {

        if (activated == true)
        {
            if (Player.GetComponent<movement>().is_dashing == true)
            {
                if (Player.GetComponent<Animator>().GetInteger("direction") == 4)
                {
                    Player.GetComponent<Rigidbody2D>().velocity = new Vector2(Player.GetComponent<Rigidbody2D>().velocity.x, Player.GetComponent<Rigidbody2D>().velocity.y - dashforce);
                }
                else if (Player.GetComponent<Animator>().GetInteger("direction") == 2)
                {
                    Player.GetComponent<Rigidbody2D>().velocity = new Vector2(Player.GetComponent<Rigidbody2D>().velocity.x, Player.GetComponent<Rigidbody2D>().velocity.y + dashforce);
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    Player.GetComponent<Rigidbody2D>().velocity = new Vector2(Player.GetComponent<Rigidbody2D>().velocity.x, Player.GetComponent<Rigidbody2D>().velocity.y - force);
                    //Player.transform.Translate(Vector3.up* Time.deltaTime * force);
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    Player.GetComponent<Rigidbody2D>().velocity = new Vector2(Player.GetComponent<Rigidbody2D>().velocity.x, Player.GetComponent<Rigidbody2D>().velocity.y + force);
                    //Player.transform.Translate(Vector3.down * Time.deltaTime * force);
                }
            }
        }

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            Player = collider.gameObject;
            activated = true;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            activated = false;
        }
    }

}
