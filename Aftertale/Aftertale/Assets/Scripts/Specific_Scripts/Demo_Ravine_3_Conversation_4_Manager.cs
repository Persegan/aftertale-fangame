﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Conversation_4_Manager : MonoBehaviour {


    public int state = 0;

    public GameObject player;
    public bool caninteract = false;

    public float time_to_wait;

    public GameObject Standing_Still_Conversation1;
    public GameObject Standing_Still_Conversation2;
    public GameObject Standing_Still_Conversation3;


    // Use this for initialization
   void Start()
    {
        state = 0;
        caninteract = false;
        StartCoroutine(StartCountdown());
    }

    public void Restart()
    {
        state = 0;
        caninteract = false;
        StartCoroutine(StartCountdown());
    }

    // Update is called once per frame
    void Update()
    {


        if (state == 1)
        {

            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && caninteract == true)
                {
                    if (player_data.playerdata.global_variables[4] == 0)
                    {
                        Standing_Still_Conversation1.BroadcastMessage("Execute_Dialogue");
                    }
                    else if (player_data.playerdata.global_variables[4] == 1)
                    {
                        Standing_Still_Conversation2.BroadcastMessage("Execute_Dialogue");
                    }
                    else if (player_data.playerdata.global_variables[4] == 2)
                    {
                        Standing_Still_Conversation3.BroadcastMessage("Execute_Dialogue");
                    }
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (caninteract == true)
                    {
                        if (player_data.playerdata.global_variables[4] == 0)
                        {
                            Standing_Still_Conversation1.BroadcastMessage("Skip_Dialogue");
                        }
                        else if (player_data.playerdata.global_variables[4] == 1)
                        {
                            Standing_Still_Conversation2.BroadcastMessage("Skip_Dialogue");
                        }
                        else if (player_data.playerdata.global_variables[4] == 2)
                        {
                            Standing_Still_Conversation3.BroadcastMessage("Skip_Dialogue");
                        }
                    }
                }
            }
            /*if (gameObject.GetComponent<Demo_Ravine_2_Conversation_2>().complete == true)
            {
                state = 2;
                caninteract = false;
            }*/

        }

        else if (state == 2)
        {
            caninteract = false;
            StartCoroutine(StartCountdown());
            state = 0;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {

        if (collider.tag == "Player")
        {
            StopAllCoroutines();
            gameObject.SetActive(false);
        }
    }

    /*void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            StartCoroutine(StartCountdown());
        }
    }*/

    IEnumerator ScriptedMovement1()
    {
        if (player.GetComponent<movement>().is_dashing == true)
        {
            player.GetComponent<movement>().StopAllCoroutines();
            player.GetComponent<movement>().is_dashing = false;
            player.GetComponent<Animator>().SetBool("Dashing", false);
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
            player.GetComponent<Animator>().SetBool("moving", false);
        }
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;

        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
        player.GetComponent<Animator>().SetBool("moving", false);

        if (player_data.playerdata.global_variables[4] == 0)
        {
            Standing_Still_Conversation1.BroadcastMessage("Execute_Dialogue");
        }
        else if (player_data.playerdata.global_variables[4] == 1)
        {
            Standing_Still_Conversation2.BroadcastMessage("Execute_Dialogue");
        }
        else if (player_data.playerdata.global_variables[4] == 2)
        {
            Standing_Still_Conversation3.BroadcastMessage("Execute_Dialogue");
        }
        caninteract = true;
        state = 1;

        yield return 0;
    }


    IEnumerator StartCountdown()
    {
        yield return new WaitForSeconds(time_to_wait);
        StartCoroutine(ScriptedMovement1());
    }
}
