﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_4_Event_StarterDestroyer : MonoBehaviour {

    public GameObject switches_puzzle;
    public GameObject crossing_bridge;
    public GameObject river_animation;

    public GameObject Freezing_River;
    public GameObject Normal_River;
    public GameObject Frozen_River;
    public GameObject Ice_Block;
    public GameObject Bridge_Collisions;

    public GameObject River_Collisions_1;
    public GameObject River_Collisions_2;
    public GameObject Sackboy;

	// Use this for initialization
	void Start () {
        if (player_data.playerdata.global_variables[7] == 1)
        {
            switches_puzzle.SetActive(false);
            crossing_bridge.SetActive(true);
            river_animation.SetActive(false);
            Bridge_Collisions.SetActive(false);
        }
        if (player_data.playerdata.global_variables[21] == 1)
        {
            Destroy(Sackboy);
        }
        if (player_data.playerdata.global_variables[16] == 1)
        {
            Destroy(Freezing_River);
            Destroy(Normal_River);
            Destroy(Ice_Block);
            Frozen_River.SetActive(true);
            Destroy(River_Collisions_1);
            Destroy(River_Collisions_2);
        }


    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
