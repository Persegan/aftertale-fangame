﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * Script that handles the initial conversation of the game at Ravine 0. It's like the scripts that handle conversations
 * during Ravines, but it only features Silas and it offers players the possibility of naming their character.
 * The "Place_Objects" function is modified because in this conversation there's only one character portrait, Silas'.
 */

public class Demo_Intro_Conversation : MonoBehaviour {

    public int state;
    public string[] messages;
    public GameObject character_portrait;
    public Sprite[] character_sprites;
    public Image dialoguebox;
    public Text displaytext;
    public Button message_owner_box;
    public AudioSource[] character_sounds;
    public Font[] character_fonts;
    public GameObject black_background;
    public GameObject white_background;
    public ParticleSystem[] stars_particle_systems;

    public List<GameObject> private_characters = new List<GameObject>();

    public GameObject Overworld_Silas;

    public Image Background_Image;

    public InputField favorite_food_input_field;
    public Text favorite_food_shown_text;
    public Text favorite_food_question_text;

    public bool complete = false;

    private int SILAS_CONFIDENT = 0;
    private int SILAS_CRY = 1;
    private int SILAS_EMBARRASS = 2;
    private int SILAS_EXPLAIN = 3;
    private int SILAS_HAPPY = 4;
    private int SILAS_MISCHIEF = 5;
    private int SILAS_NEUTRAL = 6;
    private int SILAS_SHRUG = 7;
    private int SILAS_SOMBER = 8;

    private bool can_skip;
    private bool can_interact;
    // Use this for initialization
    void Start()
    {
        state = 0;
        can_skip = false;
        can_interact = true;
        complete = false;
        Execute_Dialogue();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) && can_interact == true && can_skip == true)
        {
            Execute_Dialogue();
        }

        if (Input.GetKeyDown(KeyCode.X) && can_interact == true && can_skip == true)
        {
            Skip_Dialogue();
        }
    }

    public void Execute_Dialogue()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Coroutine());
        }
    }


    public void Skip_Dialogue()
    {
        if (can_interact == true)
        {
            if (state >= 0 && can_skip == true)
            {
                AutoTextScript.StopText(displaytext);
                messages[state] = messages[state].Replace("ç", "\n");
                messages[state] = messages[state].Replace("ñ", player_data.playerdata.player_name);
                displaytext.text = messages[state];
                AutoTextScript.isfinished = true;
                AutoTextScript.isinteracting = false;
            }
        }

    }


    IEnumerator Execute_Dialogue_Coroutine()
    {
        if (can_interact == true)
        {
            if (AutoTextScript.isinteracting == false && can_skip == true)
            {
                state++;
            }
            switch (state)
            {
                case 0:
                    can_interact = false;
                    can_skip = false;

                    //The background color fades in
                    Background_Image.canvasRenderer.SetAlpha(0.0f);
                    Background_Image.CrossFadeAlpha(1.0f, 3.0f, false);
                    
                    //Music begins
                    yield return new WaitForSeconds(2.0f);
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().volume = 0.0f;
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.OST_Stand_Again;
                    StartCoroutine(AudioManager.FadeIn(AudioManager.Audio_Manager.GetComponent<AudioSource>(), 2.0f));

                    //Silas begins moving
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Moving_Left");
                    yield return StartCoroutine(move_without_lerping(Overworld_Silas, new Vector3(0, 0, 0), 0.43f));
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.5f);

                    //Dialogue begins
                    AutoTextScript.isinteracting = true;
                    yield return StartCoroutine(Place_Objects());
                    message_owner_box.gameObject.SetActive(true);
                    message_owner_box.transform.Find("Text").GetComponent<Text>().text = "Boss Monster";
                    private_characters[0].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    break;

                case 1:
                //Silas faces in every direction
                yield return StartCoroutine(PauseExecution());
                Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Left");
                yield return new WaitForSeconds(1.0f);
                Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                yield return new WaitForSeconds(0.25f);
                Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Right");
                yield return new WaitForSeconds(1.0f);
                Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                yield return new WaitForSeconds(0.5f);

                yield return StartCoroutine(ResumeExecution());
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EMBARRASS];
                break;

                case 2:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 3:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_SOMBER];
                    break;

                case 4:
                    //Silas does a head scratch
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_HeadScratch");
                    yield return new WaitForEndOfFrame();
                    while (Overworld_Silas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.9f)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    yield return new WaitForSeconds(1.0f);

                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.25f);

                    //dialogue continues
                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EMBARRASS];
                    break;

                case 5:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 6:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_HAPPY];
                    break;

                case 7:
                    //Silas does a cute animation
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_CutePose");
                    yield return new WaitForEndOfFrame();
                    while (Overworld_Silas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.9f)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    yield return new WaitForSeconds(0.5f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.25f);

                    //dialogue continues
                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 8:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 9:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_SOMBER];
                    break;

                case 10:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_SOMBER];
                    break;

                case 11:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 12:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 13:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_CONFIDENT];
                    break;

                case 14:
                    message_owner_box.transform.Find("Text").GetComponent<Text>().text = "Silas";
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_CONFIDENT];
                    break;

                case 15:
                    //Silas poses
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_CoolPose");
                    yield return new WaitForEndOfFrame();
                    while (Overworld_Silas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.9f)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    yield return new WaitForSeconds(1.0f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.25f);

                    //dialogue continues
                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_MISCHIEF];
                    break;

                case 16:

                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 17:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EMBARRASS];
                    break;

                case 18:
                    /*//Silas does a head scratch
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_HeadScratch");
                    yield return new WaitForEndOfFrame();
                    while (Overworld_Silas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.9f)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    yield return new WaitForSeconds(1.0f);

                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.25f);

                    //dialogue continues
                    yield return StartCoroutine(ResumeExecution());*/
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 19:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 20:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 21:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_SOMBER];
                    break;

                case 22:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 23:
                    //Silas shrugs
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Shrug");
                    yield return new WaitForEndOfFrame();
                    while (Overworld_Silas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.9f)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.25f);

                    //dialogue continues

                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EMBARRASS];
                    break;

                case 24:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_SOMBER];
                    break;

                case 25:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 26:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 27:
                    //Silas turns back
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Right");
                    yield return new WaitForSeconds(0.5f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Back");

                    //We fade in the favorite food question and space to type in
                    favorite_food_question_text.gameObject.SetActive(true);
                    favorite_food_shown_text.gameObject.SetActive(true);
                    favorite_food_question_text.canvasRenderer.SetAlpha(0.0f);
                    favorite_food_shown_text.canvasRenderer.SetAlpha(0.0f);
                    favorite_food_question_text.CrossFadeAlpha(1.0f, 2.0f, false);
                    favorite_food_shown_text.CrossFadeAlpha(1.0f, 2.0f, false);
                    favorite_food_input_field.Select();

                    //We wait until the player decides on a name
                    while (player_data.playerdata.player_name == "")
                    {
                        yield return new WaitForEndOfFrame();
                    }

                    //We fade out things
                    favorite_food_input_field.gameObject.SetActive(false);
                    favorite_food_question_text.canvasRenderer.SetAlpha(1.0f);
                    favorite_food_shown_text.canvasRenderer.SetAlpha(1.0f);
                    favorite_food_question_text.CrossFadeAlpha(0.0f, 2.0f, false);
                    favorite_food_shown_text.CrossFadeAlpha(0.0f, 2.0f, false);
                    yield return new WaitForSeconds(2.0f);

                    //silas turns again
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Right");
                    yield return new WaitForSeconds(0.5f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");



                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_MISCHIEF];
                    break;

                case 28:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;

                case 29:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_MISCHIEF];
                    break;

                case 30:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 31:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EMBARRASS];
                    break;

                case 32:
                    //Silas does a head scratch
                    yield return StartCoroutine(PauseExecution());
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_HeadScratch");
                    yield return new WaitForEndOfFrame();
                    while (Overworld_Silas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.9f)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    yield return new WaitForSeconds(1.0f);

                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(0.25f);

                    //dialogue continues
                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_SOMBER];
                    break;

                case 33:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_MISCHIEF];
                    break;
                case 34:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EXPLAIN];
                    break;


                case 35:
                    //We start the stars particle systems
                    yield return StartCoroutine(PauseExecution());
                    for (int i = 0; i < stars_particle_systems.Length; i++)
                    {
                        stars_particle_systems[i].Play();
                    }
                    yield return new WaitForSeconds(2.0f);
                    //Silas looks around
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Left");
                    yield return new WaitForSeconds(2.0f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return new WaitForSeconds(1.0f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Right");
                    yield return new WaitForSeconds(2.0f);
                    Overworld_Silas.GetComponent<Animator>().Play("Silas_Overworld_Standing_Front");
                    yield return StartCoroutine(ResumeExecution());
                    //Silas Nonsense
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_NEUTRAL];
                    break;

                case 36:
                    can_skip = false;
                    can_interact = false;
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[0].GetComponent<Image>().sprite = character_sprites[SILAS_EMBARRASS];
                    //Silas begins talking and we fade out his voice
                    StartCoroutine(AudioManager.FadeOut(AudioManager.Audio_Manager.GetComponent<AudioSource>(), 3.0f));
                    StartCoroutine(Fade_Out_AudioSource(5.0f));
                    StartCoroutine(Play_Audiosource_Randomly());
                    yield return new WaitForSeconds(0.2f);
                    //The stars particle systems begin emitting more stars
                    for (int i = 0; i < stars_particle_systems.Length; i++)
                    {
                        var em = stars_particle_systems[i].emission;
                        var rate = em.rateOverTime;
                        rate.constant = 230f;
                        em.rateOverTime = rate;

                    }
                    yield return new WaitForSeconds(2.0f);
                    //We fade in everything to white and fade out the music
                    white_background.SetActive(true);
                    white_background.gameObject.transform.SetAsLastSibling();
                    white_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                    white_background.GetComponent<Image>().CrossFadeAlpha(1.0f, 2.2f, false);
                    yield return new WaitForSeconds(2.5f);
                    //We fade in everything to black
                    black_background.SetActive(true);
                    black_background.gameObject.transform.SetAsLastSibling();
                    black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                    Color c = black_background.GetComponent<Image>().color;
                    c.a = 255;
                    black_background.GetComponent<Image>().color = c;
                    black_background.GetComponent<Image>().CrossFadeAlpha(1.0f, 2.0f, false);
                    yield return new WaitForSeconds(2.0f);
                    SceneManager.LoadScene("Demo_Ravine_-1");

                    break;


                default:

                    break;
            }
        }

    }



    IEnumerator Fade_Out_AudioSource(float FadeTime)
    {
        float startVolume = GetComponent<AudioSource>().volume;
        while (GetComponent<AudioSource>().volume > 0)
        {
            GetComponent<AudioSource>().volume -= startVolume * Time.deltaTime / FadeTime;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Play_Audiosource_Randomly()
    {
        while (GetComponent<AudioSource>().volume > 0)
        {
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(Random.Range(0.05f, 0.3f));
        }

    }







    IEnumerator Place_Objects()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);
        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);

        yield return new WaitForSeconds(0.5f);


        private_characters.Add(GameObject.Instantiate(character_portrait));
        //private_characters[1] = GameObject.Instantiate(character_portrait);

        private_characters[0].transform.SetParent(GameObject.Find("Canvas").transform);
        //private_characters[1].transform.SetParent(GameObject.Find("Canvas").transform);

        private_characters[0].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);
        //private_characters[1].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);

        private_characters[0].GetComponent<Image>().sprite = character_sprites[0];
        //private_characters[1].GetComponent<Image>().sprite = character_sprites[1];

        private_characters[0].transform.localScale = new Vector3(1, 1, 1);
        //private_characters[1].transform.localScale = new Vector3(1, 1, 1);

        private_characters[0].transform.SetAsFirstSibling();
        //private_characters[1].transform.SetAsFirstSibling();
        black_background.transform.SetAsFirstSibling();

        private_characters[0].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
        //private_characters[1].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);

        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);
        private_characters[0].transform.localPosition = new Vector3(235, 72.5f, -1);
        //private_characters[1].transform.localPosition = new Vector3(235, 72.5f, -1);
        can_skip = true;
        can_interact = true;
    }

    IEnumerator move_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (Vector3.Distance(target.transform.localPosition, destination) > 1.5f)
        {
            target.transform.localPosition = Vector3.Lerp(target.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForEndOfFrame();
            speed += 0.02f;
        }
        yield break;
    }


    IEnumerator move_without_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (target.transform.position != destination)
        {
            target.transform.position = Vector3.MoveTowards(target.transform.position, destination, Time.deltaTime * speed);
            yield return new WaitForEndOfFrame();
        }
    }

    /*IEnumerator FinishEverything()
    {
        //player.GetComponent<movement>().can_move = true;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Count; i++)
        {
            Destroy(private_characters[i]);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        complete = true;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
    }*/

    IEnumerator PauseExecution()
    {
        can_interact = false;
        can_skip = false;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Count; i++)
        {
            private_characters[i].SetActive(false);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator ResumeExecution()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        message_owner_box.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);

        for (int i = 0; i < private_characters.Count; i++)
        {
            private_characters[i].SetActive(true);
        }

        yield return new WaitForSeconds(0.5f);
        can_interact = true;
        can_skip = true;
    }
}
