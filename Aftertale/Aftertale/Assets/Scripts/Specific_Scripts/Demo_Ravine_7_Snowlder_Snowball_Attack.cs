﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_7_Snowlder_Snowball_Attack : MonoBehaviour {

    public bool behaviour;
    public bool phase = false;

    public float speed;
    public float scale_speed;
    public float angular_speed;

	// Use this for initialization
	void Start () {
        speed = 10.5f;
        scale_speed = 0.5f;
        //GetComponent<Rigidbody2D>().AddTorque(250.0f);
        angular_speed = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {

        GetComponent<Rigidbody2D>().angularVelocity = speed * 5f;
        scale_speed = speed *0.002f;
        transform.localScale += new Vector3(scale_speed, scale_speed, 1) * Time.deltaTime;

		if (behaviour == false)
        {
            if (phase == false)
            {
                if (transform.localPosition.x >= -118)
                {
                    transform.localPosition -= new Vector3(speed, 0, 0) * Time.deltaTime;
                    if (transform.localPosition.x >= 3)
                    {
                        speed = speed * 1.03f;
                    }
                    else
                    {
                        speed = speed * 0.98f;
                    }
                }
                else
                {
                    phase = true;
                    speed = 10.5f;
                }
            }

            else if (phase == true)
            {
                if (transform.localPosition.x <= 126)
                {
                    transform.localPosition += new Vector3(speed, 0, 0) * Time.deltaTime;
                    if (transform.localPosition.x <= 3)
                    {
                        speed = speed * 1.03f;
                    }
                    else
                    {
                        speed = speed * 0.98f;
                    }
                }
                else
                {
                    Destroy(gameObject);
                }
      
            }
        }

        else if (behaviour == true)
        {
            if (phase == false)
            {
                if (transform.localPosition.x <= 126)
                {
                    transform.localPosition += new Vector3(speed, 0, 0) * Time.deltaTime;
                    if (transform.localPosition.x <= 3)
                    {
                        speed = speed * 1.03f;
                    }
                    else
                    {
                        speed = speed * 0.98f;
                    }
                }

                else
                {
                    phase = true;
                    speed = 10.5f;
                }
                
            }
            
            else if (phase == true)
            {
                if (transform.localPosition.x >= -118)
                {
                    transform.localPosition -= new Vector3(speed, 0, 0) * Time.deltaTime;
                    if (transform.localPosition.x >= 3)
                    {
                        speed = speed * 1.03f;
                    }
                    else
                    {
                        speed = speed * 0.98f;
                    }
                }

                else
                {
                    Destroy(gameObject);
                }                
            }
        }
	}
}
