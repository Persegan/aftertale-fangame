﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_6_Timed_Switches_Puzzle : MonoBehaviour {

    public static int activeswitches = 0;

    public int maxswitches;

    public GameObject[] switches;

    public float waittime;

    public GameObject Spikes;

	// Use this for initialization
	void Start () {
        activeswitches = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (activeswitches < maxswitches && this.transform.GetChild(0).gameObject.activeSelf == false)
            {
                activeswitches++;
                this.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(1).gameObject.SetActive(false);

            }

            if (activeswitches == maxswitches)
            {
                EndPuzzle();
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (activeswitches == 1)
            {
                StartCoroutine(StartCountdown());
            }
            if (activeswitches < maxswitches )
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = false;             
            }
        }
    }


    IEnumerator StartCountdown()
    {
        yield return new WaitForSeconds(waittime);


        if (activeswitches < maxswitches)
        {
            for (int i = 0; i < switches.Length; i++)
            {
                switches[i].GetComponent<BoxCollider2D>().enabled = true;
                switches[i].transform.GetChild(0).gameObject.SetActive(false);
                switches[i].transform.GetChild(1).gameObject.SetActive(true);
            }
            activeswitches = 0;
        }
    }

    public void EndPuzzle()
    {
        Spikes.SetActive(false);
        for (int i = 0; i < switches.Length; i++)
        {
            switches[i].GetComponent<BoxCollider2D>().enabled = false;
            switches[i].transform.GetChild(0).gameObject.SetActive(true);
            switches[i].transform.GetChild(1).gameObject.SetActive(false);

        }
       
        player_data.playerdata.global_variables[15] = 1;
        player_data.playerdata.global_variables[23] = 0;
    }

    public void EndPuzzleFast()
    {
        Spikes.SetActive(false);
        for (int i = 0; i < switches.Length; i++)
        {
            switches[i].GetComponent<BoxCollider2D>().enabled = false;
            switches[i].transform.GetChild(0).gameObject.SetActive(true);
            switches[i].transform.GetChild(1).gameObject.SetActive(false);
        }
    }

}
