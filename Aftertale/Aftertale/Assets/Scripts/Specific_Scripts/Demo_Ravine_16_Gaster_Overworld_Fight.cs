﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_16_Gaster_Overworld_Fight : MonoBehaviour {
    public int state;
    public int state2
    {
        get;
        set;
    }
    public string[] messages;
    public string[] messages_option_1;
    public string[] messages_option_2;
    public string option1_message;
    public string option2_message;
    public Button option1_button;
    public Button option2_button;
    public GameObject character_portrait;
    public Sprite[] character_sprites;
    public Image dialoguebox;
    public Text displaytext;
    public Button message_owner_box;
    public GameObject player;
    public AudioSource[] character_sounds;
    public Font[] character_fonts;
    public GameObject black_background;

    public GameObject Overworld_Gaster;
    public AudioSource Gaster_Barks;
    public Image player_soul;
    public AudioClip menu_select;
    public AudioClip menu_choose;
    public GameObject button1;
    public GameObject button2;
    const int GASTER_ANNOYED = 1;
    const int GASTER_CONFIDENT = 2;
    const int GASTER_DOGFACE = 3;
    const int GASTER_FRUSTRATED = 4;
    const int GASTER_NEUTRAL = 5;
    const int GASTER_SYMPATHETIC = 6;
    const int GASTER_THINKING = 7;
    const int GASTER_PUPPY = 8;

    public GameObject[] private_characters;

    public bool complete = false;

    private bool can_skip;
    private bool can_interact;
    // Use this for initialization
    void Start()
    {
        state = 0;
        can_skip = false;
        complete = false;
        can_interact = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (state2 == 1)
        {

            if (can_interact == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && can_interact == true)
                {
                    Execute_Dialogue();
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (can_interact == true)
                    {
                        Skip_Dialogue();
                    }
                }
            }

        }

        else if (state2 == 2)
        {
            if (can_interact == true)
            {

                if (Input.GetKeyDown(KeyCode.Z))
                {
                    Execute_Dialogue_Option_1();
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    Skip_Dialogue_Option_1();
                }
            }
        }

        else if (state2 == 3)
        {
            if (can_interact == true)
            {

                if (Input.GetKeyDown(KeyCode.Z))
                {
                    Execute_Dialogue_Option_2();
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    Skip_Dialogue_Option_2();
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            player = collider.gameObject;
            //GetComponent<BoxCollider2D>().enabled = false;
            StartCoroutine(ScriptedMovement1());

        }
    }


    public void Execute_Dialogue()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Coroutine());
        }
    }

    public void Execute_Dialogue_Option_1()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Option_1_Coroutine());
        }
    }

    public void Execute_Dialogue_Option_2()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Option_2_Coroutine());
        }
    }


    public void Skip_Dialogue()
    {
        if (state >= 0 && can_skip == true)
        {
            if (option1_button.gameObject.activeSelf == true)
            {
                AutoTextScript.StopText(option1_button.GetComponentInChildren<Text>());
                AutoTextScript.StopText(option2_button.GetComponentInChildren<Text>());
                option1_button.GetComponentInChildren<Text>().text = option1_message;
                option2_button.GetComponentInChildren<Text>().text = option2_message;
                AutoTextScript.isfinished = true;
                AutoTextScript.isinteracting = false;
                option1_button.Select();
                complete = true;
            }
            else
            {
                AutoTextScript.StopText(displaytext);
                messages[state] = messages[state].Replace("ç", "\n");
                displaytext.text = messages[state];
                AutoTextScript.isfinished = true;
                AutoTextScript.isinteracting = false;
            }

        }
    }

    public void Skip_Dialogue_Option_1()
    {
        if (state >= 0 && can_skip == true)
        {
            AutoTextScript.StopText(displaytext);
            messages_option_1[state] = messages_option_1[state].Replace("ç", "\n");
            displaytext.text = messages_option_1[state];
            AutoTextScript.isfinished = true;
            AutoTextScript.isinteracting = false;
        }
    }

    public void Skip_Dialogue_Option_2()
    {
        if (state >= 0 && can_skip == true && AutoTextScript.isinteracting == true)
        {
            AutoTextScript.StopText(displaytext);
            messages_option_2[state] = messages_option_2[state].Replace("ç", "\n");
            displaytext.text = messages_option_2[state];
            AutoTextScript.isfinished = true;
            AutoTextScript.isinteracting = false;
        }
    }




    IEnumerator Execute_Dialogue_Coroutine()
    {
        if (can_interact == true)
        {
            if (AutoTextScript.isinteracting == false && can_skip == true)
            {
                state++;
            }
            switch (state)
            {
                case 0:
                    can_interact = false;
                    //Gaster Barks and Pumpkin Faces South
                    Gaster_Barks.Play();
                    yield return new WaitForSeconds(0.25f);
                    Gaster_Barks.Play();
                    yield return new WaitForSeconds(0.75f);
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().TTF_Overworld;
                    AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
                    player.GetComponent<Animator>().SetInteger("direction", 3);

                    //Camera pans down
                    Camera.main.GetComponent<camera>().enabled = false;
                    StartCoroutine(move_without_lerping(Camera.main.gameObject, new Vector3(-0.822f, -1.207037f, -10f), 2.0f));

                    //Gaster is walking into position and waves
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Walk_Right", 0);
                    yield return StartCoroutine(move_without_lerping(Overworld_Gaster, new Vector3(-0.822f, -1.107037f, 0f), 0.5f));
                    yield return new WaitForSeconds(0.16f);
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Rub_Chin_Front", 0);
                    yield return new WaitForSeconds(0.8f);

                    //Gaster faces Pumpkin
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Standing_Back", 0);
                    yield return new WaitForSeconds(0.5f);

                    player.GetComponent<movement>().can_move = false;
                    AutoTextScript.isinteracting = true;
                    yield return StartCoroutine(Place_Objects());
                    message_owner_box.gameObject.SetActive(true);
                    message_owner_box.transform.Find("Text").GetComponent<Text>().text = "???";
                    private_characters[1].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[8];
                    message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    can_interact = true;
                    break;

                case 1:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[7];
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    break;

                case 2:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[5];
                    break;

                case 3:
                    //We show options in here
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[2];
                    while (AutoTextScript.isinteracting == true)
                    {
                        yield return new WaitForSeconds(0.01f);
                    }
                    option1_button.gameObject.SetActive(true);
                    option2_button.gameObject.SetActive(true);
                    option1_button.GetComponentInChildren<Text>().text = "";
                    option2_button.GetComponentInChildren<Text>().text = "";
                    AutoTextScript.TypeText(option1_button.GetComponentInChildren<Text>(), option1_message, 0.03f, character_sounds[1]);
                    AutoTextScript.TypeText(option2_button.GetComponentInChildren<Text>(), option2_message, 0.03f, character_sounds[1]);
                    while (AutoTextScript.isinteracting == true)
                    {
                        yield return new WaitForSeconds(0.01f);
                    }
                    option1_button.Select();
                    state = 0;
                    can_skip = false;
                    can_interact = false;
                    complete = true;
                    break;

                default:

                    break;
            }
        }

    }

    IEnumerator Execute_Dialogue_Option_1_Coroutine()
    {
        if (AutoTextScript.isinteracting == false && can_skip == true)
        {
            state++;
        }
        switch (state)
        {
            case 0:

                AutoTextScript.isinteracting = true;
                option1_button.gameObject.SetActive(false);
                option2_button.gameObject.SetActive(false);
                message_owner_box.transform.Find("Text").GetComponent<Text>().text = "???";
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                can_skip = true;
                break;

            case 1:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                break;

            case 2:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                break;

            case 3:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                break;


            case 4:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                break;

            case 5:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_DOGFACE];
                break;

            case 6:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                break;


            case 7:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                break;

            case 8:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                break;

            case 9:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                break;

            case 10:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                break;

            case 11:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                break;

            case 12:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_PUPPY];
                break;

            case 13:
                can_skip = false;
                Camera.main.GetComponent<camera>().speed = 2;
                Camera.main.GetComponent<camera>().enabled = true;
                yield return new WaitForSeconds(0.4f);
                yield return StartCoroutine(move_without_lerping(Overworld_Gaster, new Vector3(-1.77f, -1.725f, 0f), 0.5f));
                StartCoroutine(FinishEverything());
                break;
            default:
                yield return 0;
                break;
        }

    }






    IEnumerator Execute_Dialogue_Option_2_Coroutine()
    {

        if (AutoTextScript.isinteracting == false && can_skip == true)
        {
            state++;
        }
        switch (state)
        {
            case 0:
                AutoTextScript.isinteracting = true;
                option1_button.gameObject.SetActive(false);
                option2_button.gameObject.SetActive(false);
                message_owner_box.transform.Find("Text").GetComponent<Text>().text = "???";
                private_characters[1].GetComponent<Image>().sprite = character_sprites[1];
                message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, character_sounds[0]);
                can_skip = true;
                break;

            case 1:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                private_characters[1].GetComponent<Image>().sprite = character_sprites[7];
                AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, character_sounds[0]);
                break;

            case 2:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[4];
                break;

            case 3:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[2];
                break;

            case 4:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                break;

            case 5:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_DOGFACE];
                break;

            case 6:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                break;


            case 7:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_NEUTRAL];
                break;

            case 8:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                break;

            case 9:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                break;

            case 10:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                break;

            case 11:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                break;

            case 12:
                AutoTextScript.isinteracting = true;
                displaytext.text = "";
                AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, character_sounds[0]);
                private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_ANNOYED];
                break;

            case 13:
                can_skip = false;
                Camera.main.GetComponent<camera>().speed = 2;
                Camera.main.GetComponent<camera>().enabled = true;
                yield return new WaitForSeconds(0.4f);
                yield return StartCoroutine(move_without_lerping(Overworld_Gaster, new Vector3(-1.77f, -1.725f, 0f), 0.5f));
                StartCoroutine(FinishEverything());
                break;
            default:
                yield return 0;
                break;
        }

    }











    IEnumerator Place_Objects()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);

        yield return new WaitForSeconds(0.5f);


        private_characters[0] = GameObject.Instantiate(character_portrait);
        private_characters[1] = GameObject.Instantiate(character_portrait);

        private_characters[0].transform.SetParent(GameObject.Find("Canvas").transform);
        private_characters[1].transform.SetParent(GameObject.Find("Canvas").transform);

        private_characters[0].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);
        private_characters[1].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);

        private_characters[0].transform.localPosition = new Vector3(-777, 72.5f, 0);
        private_characters[1].transform.localPosition = new Vector3(777, 72.5f, 0);


        private_characters[0].GetComponent<Image>().sprite = character_sprites[0];
        private_characters[1].GetComponent<Image>().sprite = character_sprites[1];

        private_characters[0].transform.localScale = new Vector3(1, 1, 1);
        private_characters[1].transform.localScale = new Vector3(1, 1, 1);

        private_characters[0].transform.SetAsFirstSibling();
        private_characters[1].transform.SetAsFirstSibling();
        black_background.transform.SetAsFirstSibling();

        private_characters[0].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
        private_characters[1].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);

        //StartCoroutine(move_lerping(dialoguebox.gameObject, new Vector3(0, -199, -1), 2.0f));
        //StartCoroutine(move_lerping(private_characters[0], new Vector3(-300, 23, -1), 2.0f));
        //StartCoroutine(move_lerping(private_characters[1], new Vector3(300, 23, -1), 2.0f));

        //yield return new WaitForSeconds(2.0f);

        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);
        displaytext.gameObject.transform.localPosition = new Vector3(11, -220, -1);
        private_characters[0].transform.localPosition = new Vector3(-300, 72.5f, -1);
        private_characters[1].transform.localPosition = new Vector3(235, 72.5f, -1);
        can_skip = true;
    }

    IEnumerator move_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (Vector3.Distance(target.transform.localPosition, destination) > 1.5f)
        {
            target.transform.localPosition = Vector3.Lerp(target.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForSeconds(0.00001f);
            speed += 0.02f;
        }
        yield break;
    }

    IEnumerator move_without_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (target.transform.position != destination)
        {
            target.transform.position = Vector3.MoveTowards(target.transform.position, destination, Time.deltaTime * speed);
            yield return new WaitForSeconds(0.001f);
        }
    }

    IEnumerator FinishEverything()
    {
        //player.GetComponent<movement>().can_move = true;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Length; i++)
        {
            Destroy(private_characters[i]);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        complete = true;
        player.GetComponent<movement>().can_move = true;
        player.GetComponent<movement>().can_dash = true;
        player.GetComponent<movement>().can_interact = true;
        //player_data.playerdata.global_variables[12] = 1;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
    }



    IEnumerator ScriptedMovement1()
    {
        if (player.GetComponent<movement>().is_dashing == true)
        {
            player.GetComponent<movement>().StopAllCoroutines();
            player.GetComponent<movement>().is_dashing = false;
            player.GetComponent<Animator>().SetBool("Dashing", false);
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
            player.GetComponent<Animator>().SetBool("moving", false);
        }
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;

        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
        player.GetComponent<Animator>().SetBool("moving", false);

        Execute_Dialogue();
        state2 = 1;

        yield return 0;
    }

    public void SelectButton1()
    {
        player_soul.gameObject.SetActive(true);
        //player_soul.transform.localPosition = new Vector3(button1.transform.localPosition.x - 300, button1.transform.localPosition.y, 0.0f);
        player_soul.transform.position = GetLetterPosition_Script.GetPos(button1.GetComponentInChildren<Text>(), 1, transform.GetChild(0).gameObject.GetComponent<Canvas>());
        Vector3 auxiliar = player_soul.transform.position;
        auxiliar.x -= 0.16f;
        player_soul.transform.position = auxiliar;
        GetComponent<AudioSource>().PlayOneShot(menu_select);
    }

    public void SelectButton2()
    {
        player_soul.gameObject.SetActive(true);
        player_soul.transform.localPosition = new Vector3(button2.transform.localPosition.x - 300, button2.transform.localPosition.y, 0.0f);
        player_soul.transform.position = GetLetterPosition_Script.GetPos(button2.GetComponentInChildren<Text>(), 1, transform.GetChild(0).gameObject.GetComponent<Canvas>());
        Vector3 auxiliar = player_soul.transform.position;
        auxiliar.x -= 0.16f;
        player_soul.transform.position = auxiliar;
        GetComponent<AudioSource>().PlayOneShot(menu_select);
    }

    public void ChooseButton1()
    {
        player_soul.gameObject.SetActive(false);
        Execute_Dialogue_Option_1();
        GetComponent<AudioSource>().PlayOneShot(menu_choose);
        state2 = 2;
        can_interact = true;
    }

    public void ChooseButton2()
    {
        player_soul.gameObject.SetActive(false);
        Execute_Dialogue_Option_2();
        GetComponent<AudioSource>().PlayOneShot(menu_choose);
        state2 = 3;
        can_interact = true;
    }

}
