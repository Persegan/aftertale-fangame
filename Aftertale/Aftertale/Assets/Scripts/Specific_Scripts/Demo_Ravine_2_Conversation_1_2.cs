﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Demo_Ravine_2_Conversation_1_2 : MonoBehaviour
{

    public int state;
    public string[] messages;
    public GameObject[] character_portraits;
    public Sprite[] character_sprites;
    public Image dialoguebox;
    public Text displaytext;
    public Button message_owner_box;
    public GameObject player;
    public AudioSource[] character_sounds;
    public Font[] character_fonts;
    public GameObject black_background;

    public GameObject[] private_characters;

    public GameObject Overworld_Gaster;

    public bool complete = false;

    private int GASTER_ANNOYED = 2;
    private int GASTER_CONFIDENT = 3;
    private int GASTER_TONGUE = 4;
    private int GASTER_FRUSTRATED = 5;
    private int GASTER_HAPPY = 6;
    private int GASTER_PUPPY = 7;
    private int GASTER_SYMPATHETIC = 8;
    private int GASTER_THINKING = 9;

    private bool can_skip;
    private bool can_interact;
    // Use this for initialization
    void Start()
    {
        state = 0;
        can_skip = false;
        can_interact = true;
        complete = false;
    }

    // Update is called once per frame
    void Update()
    {



    }

    public void Execute_Dialogue()
    {
        if (AutoTextScript.isinteracting == false)
        {
            StartCoroutine(Execute_Dialogue_Coroutine());
        }
    }


    public void Skip_Dialogue()
    {
        if (can_interact == true)
        {
            if (state >= 0 && can_skip == true)
            {
                AutoTextScript.StopText(displaytext);
                messages[state] = messages[state].Replace("ç", "\n");
                messages[state] = messages[state].Replace("ñ", player_data.playerdata.player_name);
                displaytext.text = messages[state];
                AutoTextScript.isfinished = true;
                AutoTextScript.isinteracting = false;
            }
        }
       
    }


    IEnumerator Execute_Dialogue_Coroutine()
    {
        if ( can_interact == true)
        {
            if (AutoTextScript.isinteracting == false && can_skip == true)
            {
                state++;
            }
            switch (state)
            {
                case 0:
                    can_interact = false;
                    //Camera pans down
                    Camera.main.GetComponent<camera>().enabled = false;
                    yield return StartCoroutine(move_without_lerping(Camera.main.gameObject, new Vector3(-0.954f, -2.590f, -10f), 2.0f));

                    //Gaster is waving
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Wave_Back", 0);
                    yield return new WaitForSeconds(1.9f);

                    //Gaster is facing upwards
                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Standing_Back", 0);

                    yield return new WaitForSeconds(1.0f);
                    //Dialogue resumes
                    can_interact = true;
                    player.GetComponent<movement>().can_move = false;
                    AutoTextScript.isinteracting = true;
                    yield return StartCoroutine(Place_Objects());
                    message_owner_box.gameObject.SetActive(true);
                    message_owner_box.transform.Find("Text").GetComponent<Text>().text = "???";
                    private_characters[1].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_HAPPY];
                    message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    break;

                case 1:
                    //message_owner_box.transform.FindChild("Text").GetComponent<Text>().text = "Pumpkin";
                    //private_characters[0].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
                    //private_characters[1].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
                    //message_owner_box.transform.localPosition = new Vector3(386, -82, -1);
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_PUPPY];
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    break;

                case 2:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 3:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 4:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                    break;

                case 5:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_TONGUE];
                    break;

                case 6:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_HAPPY];
                    break;

                case 7:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_PUPPY];
                    break;

                case 8:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 9:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_CONFIDENT];
                    break;

                case 10:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_PUPPY];
                    break;

                case 11:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_TONGUE];
                    break;

                case 12:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 13:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                    break;

                case 14:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_ANNOYED];
                    break;

                case 15:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 16:
        
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 17:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_ANNOYED];
                    break;

                case 18:
                    yield return StartCoroutine(PauseExecution());

                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Rub_Chin_Front", 0);

                    yield return new WaitForSeconds(2.0f);

                    Overworld_Gaster.GetComponent<Animator>().Play("Gaster_Overworld_Standing_Back", 0);

                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 19:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_HAPPY];
                    break;

                case 20:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 21:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 22:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                    break;

                case 23:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_THINKING];
                    break;

                case 24:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_CONFIDENT];
                    break;

                case 25:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_PUPPY];
                    break;

                case 26:
                    yield return StartCoroutine(PauseExecution());

                    player.GetComponent<Rigidbody2D>().drag = 0;
                    player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0.2f);
                    player.GetComponent<Animator>().SetBool("moving", true);
                    player.GetComponent<Animator>().SetInteger("direction", 1);

                    Camera.main.GetComponent<camera>().speed = 2;
                    Camera.main.GetComponent<camera>().enabled = true;

                    yield return new WaitForSeconds(2.5f);

                    Camera.main.GetComponent<camera>().speed = 100;
                    player.GetComponent<Rigidbody2D>().drag = 200;

                    player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    player.GetComponent<Animator>().SetBool("moving", false);

                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_ANNOYED];
                    break;

                case 27:
                    yield return StartCoroutine(PauseExecution());

                    player.GetComponent<Animator>().SetInteger("direction", 3);
                    yield return new WaitForSeconds(2.0f);

                    yield return StartCoroutine(ResumeExecution());
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 28:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_SYMPATHETIC];
                    break;

                case 29:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_HAPPY];
                    break;

                case 30:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_FRUSTRATED];
                    break;

                case 31:
                    AutoTextScript.isinteracting = true;
                    displaytext.text = "";
                    AutoTextScript.TypeText(displaytext, messages[state], 0.03f, character_sounds[0]);
                    private_characters[1].GetComponent<Image>().sprite = character_sprites[GASTER_HAPPY];
                    break;

                case 32:
                    can_skip = false;
                    can_interact = false;
                    StartCoroutine(FinishEverything());
                    break;

                default:

                    break;
            }
        }
       
    }











    IEnumerator Place_Objects()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);
        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);

        yield return new WaitForSeconds(0.5f);


        private_characters[0] = GameObject.Instantiate(character_portraits[0]);
        private_characters[1] = GameObject.Instantiate(character_portraits[0]);

        private_characters[0].transform.SetParent(GameObject.Find("Canvas").transform);
        private_characters[1].transform.SetParent(GameObject.Find("Canvas").transform);

        private_characters[0].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);
        private_characters[1].GetComponent<RectTransform>().sizeDelta = new Vector2(547, 547);

        private_characters[0].transform.localPosition = new Vector3(-777, 72.5f, 0);
        private_characters[1].transform.localPosition = new Vector3(777, 72.5f, 0);


        private_characters[0].GetComponent<Image>().sprite = character_sprites[0];
        private_characters[1].GetComponent<Image>().sprite = character_sprites[1];

        private_characters[0].transform.localScale = new Vector3(1, 1, 1);
        private_characters[1].transform.localScale = new Vector3(1, 1, 1);

        private_characters[0].transform.SetAsFirstSibling();
        private_characters[1].transform.SetAsFirstSibling();
        black_background.transform.SetAsFirstSibling();

        private_characters[0].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
        private_characters[1].GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);

        //StartCoroutine(move_lerping(dialoguebox.gameObject, new Vector3(0, -199, -1), 2.0f));
        //StartCoroutine(move_lerping(private_characters[0], new Vector3(-300, 23, -1), 2.0f));
        //StartCoroutine(move_lerping(private_characters[1], new Vector3(300, 23, -1), 2.0f));

        //yield return new WaitForSeconds(2.0f);

        dialoguebox.gameObject.transform.localPosition = new Vector3(0, -199, -1);
        private_characters[0].transform.localPosition = new Vector3(-300, 72.5f, -1);
        private_characters[1].transform.localPosition = new Vector3(235, 72.5f, -1);
        can_skip = true;
    }

    IEnumerator move_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (Vector3.Distance(target.transform.localPosition, destination) > 1.5f)
        {
            target.transform.localPosition = Vector3.Lerp(target.transform.localPosition, destination, Time.deltaTime * speed);
            //dialogue_box.GetComponent<Rigidbody2D>().velocity = (destination - dialogue_box.transform.localPosition).normalized * speed;
            yield return new WaitForSeconds(0.00001f);
            speed += 0.02f;
        }
        yield break;
    }


    IEnumerator move_without_lerping(GameObject target, Vector3 destination, float speed)
    {
        while (target.transform.position != destination)
        {
            target.transform.position = Vector3.MoveTowards(target.transform.position, destination, Time.deltaTime * speed);
            yield return new WaitForSeconds(0.001f);
        }
    }

    IEnumerator FinishEverything()
    {
        //player.GetComponent<movement>().can_move = true;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Length; i++)
        {
            Destroy(private_characters[i]);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        complete = true;
        player.GetComponent<movement>().can_move = true;
        player.GetComponent<movement>().can_dash = true;
        player.GetComponent<movement>().can_interact = true;
        player_data.playerdata.global_variables[2] = 1;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.GetComponent<AudioManager>().OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
    }

    IEnumerator PauseExecution()
    {
        can_interact = false;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        message_owner_box.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        for (int i = 0; i < private_characters.Length; i++)
        {
            private_characters[i].SetActive(false);
        }
        black_background.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator ResumeExecution()
    {
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0.8f);
        black_background.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        black_background.GetComponent<Image>().CrossFadeAlpha(0.75f, 0.5f, false);

        for (int i = 0; i < private_characters.Length; i++)
        {
            private_characters[i].SetActive(true);
        }

        yield return new WaitForSeconds(0.5f);
        can_interact = true;
    }

}