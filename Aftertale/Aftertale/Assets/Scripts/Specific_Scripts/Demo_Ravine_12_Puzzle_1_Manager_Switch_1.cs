﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_12_Puzzle_1_Manager_Switch_1 : MonoBehaviour {
    public int state = 0;

    private GameObject player;
    public bool caninteract = false;

    public GameObject Spikes1;
    public GameObject Spikes2;

    // Use this for initialization
    void Start()
    {
        state = 0;
        caninteract = false;

    }

    // Update is called once per frame
    void Update()
    {


        if (state == 1)
        {

            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && caninteract == true)
                {
                    BroadcastMessage("Execute_Dialogue");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (caninteract == true)
                    {
                        BroadcastMessage("Skip_Dialogue");
                    }
                }
            }
            if (gameObject.GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Switch_1>() != null)
            {
                if (gameObject.GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Switch_1>().complete == true)
                {
                    state = 2;
                    caninteract = false;
                }
            }
            if (gameObject.GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Second_Switch>()!= null)
            {
                if (gameObject.GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Second_Switch>().complete == true)
                {
                    state = 2;
                    caninteract = false;
                }
            }
            if (gameObject.GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Final_Switch>() != null)
            {
                if (gameObject.GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Final_Switch>().complete == true)
                {
                    state = 2;
                    caninteract = false;
                }
            }


        }

        else if (state == 2)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            player_data.playerdata.global_variables[25] = 1;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag == "Player")
        {
            if (player_data.playerdata.global_variables[24]== 0)
            {
                transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Switch_1>().gameObject.SetActive(true);
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Second_Switch>().gameObject.SetActive(false);
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Final_Switch>().gameObject.SetActive(false);
                player = collider.gameObject;
                StartCoroutine(ScriptedMovement1());
                player_data.playerdata.global_variables[24]++;
                Spikes1.SetActive(false);
                Spikes2.SetActive(false);
            }
            else if (player_data.playerdata.global_variables[24] == 1)
            {
                transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Switch_1>().gameObject.SetActive(false);
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Second_Switch>().gameObject.SetActive(true);
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Final_Switch>().gameObject.SetActive(false);
                player = collider.gameObject;
                StartCoroutine(ScriptedMovement1());
                player_data.playerdata.global_variables[24]++;
            }
            else if (player_data.playerdata.global_variables[24] == 7)
            {
                transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Switch_1>().gameObject.SetActive(false);
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Second_Switch>().gameObject.SetActive(false);
                GetComponentInChildren<Demo_Ravine_12_Puzzle_1_Interaction_Final_Switch>().gameObject.SetActive(true);
                player = collider.gameObject;
                StartCoroutine(ScriptedMovement1());
                player_data.playerdata.global_variables[24]++;
            }
            else
            {
                player_data.playerdata.global_variables[24]++;
                player_data.playerdata.global_variables[25] = 1;
                GetComponent<BoxCollider2D>().enabled = false;
                transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
            }



        }
    }

    IEnumerator ScriptedMovement1()
    {
        if (player.GetComponent<movement>().is_dashing == true)
        {
            player.GetComponent<movement>().StopAllCoroutines();
            player.GetComponent<movement>().is_dashing = false;
            player.GetComponent<Animator>().SetBool("Dashing", false);
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
            player.GetComponent<Animator>().SetBool("moving", false);
        }
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;

        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
        player.GetComponent<Animator>().SetBool("moving", false);

        BroadcastMessage("Execute_Dialogue");
        caninteract = true;
        state = 1;

        yield return 0;
    }
}
