﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Demo_Ravine_3_Conversation_1_Manager : MonoBehaviour {
    public int state = 0;

    public AudioSource menu_select;
    public AudioSource menu_choose;
    public Image player_soul;
    private GameObject player;
    public GameObject button1;
    public GameObject button2;
    public bool caninteract = false;

    // Use this for initialization
    void Start()
    {
        state = 0;
        caninteract = false;

    }

    // Update is called once per frame
    void Update()
    {


        if (state == 1)
        {

            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z) && caninteract == true)
                {
                    BroadcastMessage("Execute_Dialogue");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (caninteract == true)
                    {
                        BroadcastMessage("Skip_Dialogue");
                    }
                }
            }
            if (gameObject.GetComponent<Demo_Ravine_3_Conversation_1>().complete == true && state == 1)
            {
                caninteract = false;
            }

        }

        else if (state == 2)
        {
            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z))
                {
                    BroadcastMessage("Execute_Dialogue_Option_1");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                   BroadcastMessage("Skip_Dialogue_Option_1");
                }
            }
            if (gameObject.GetComponent<Demo_Ravine_3_Conversation_1>().complete == true)
            {
                caninteract = false;
                state = 4;
            }
        }

        else if (state == 3)
        {
            if (caninteract == true)
            {

                if (Input.GetKeyDown(KeyCode.Z))
                {
                    BroadcastMessage("Execute_Dialogue_Option_2");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                        BroadcastMessage("Skip_Dialogue_Option_2");
                }
            }
            if (gameObject.GetComponent<Demo_Ravine_3_Conversation_1>().complete == true)
            {
                caninteract = false;
                state = 4;
            }
        }

        else if (state == 4)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            player = collider.gameObject;
            StartCoroutine(ScriptedMovement1());

        }
    }



    public void SelectButton1()
    {
        player_soul.gameObject.SetActive(true);
        //player_soul.transform.localPosition = new Vector3(button1.transform.localPosition.x - 300, button1.transform.localPosition.y, 0.0f);
        player_soul.transform.position = GetLetterPosition_Script.GetPos(button1.GetComponentInChildren<Text>(),1,transform.GetChild(0).gameObject.GetComponent<Canvas>());
        Vector3 auxiliar = player_soul.transform.position;
        auxiliar.x -= 0.16f;
        player_soul.transform.position = auxiliar;
        menu_select.Play();
    }

    public void SelectButton2()
    {
        player_soul.gameObject.SetActive(true);
        player_soul.transform.localPosition = new Vector3(button2.transform.localPosition.x - 300, button2.transform.localPosition.y, 0.0f);
        player_soul.transform.position = GetLetterPosition_Script.GetPos(button2.GetComponentInChildren<Text>(), 1, transform.GetChild(0).gameObject.GetComponent<Canvas>());
        Vector3 auxiliar = player_soul.transform.position;
        auxiliar.x -= 0.16f;
        player_soul.transform.position = auxiliar;
        menu_select.Play();
    }

    public void ChooseButton1()
    {
        player_soul.gameObject.SetActive(false);
        BroadcastMessage("Execute_Dialogue_Option_1");
        menu_choose.Play();
        state = 2;
        caninteract = true;
        gameObject.GetComponent<Demo_Ravine_3_Conversation_1>().complete = false;
    }

    public void ChooseButton2()
    {
        player_soul.gameObject.SetActive(false);
        BroadcastMessage("Execute_Dialogue_Option_2");
        menu_choose.Play();
        state = 3;
        caninteract = true;
        gameObject.GetComponent<Demo_Ravine_3_Conversation_1>().complete = false;
    }




    IEnumerator ScriptedMovement1()
    {
        if (player.GetComponent<movement>().is_dashing == true)
        {
            player.GetComponent<movement>().StopAllCoroutines();
            player.GetComponent<movement>().is_dashing = false;
            player.GetComponent<Animator>().SetBool("Dashing", false);
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            player.GetComponent<Rigidbody2D>().drag = player.GetComponent<movement>().previousdrag;
            player.GetComponent<Animator>().SetBool("moving", false);
        }
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_dash = false;
        player.GetComponent<movement>().can_interact = false;
    
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * player.GetComponent<movement>().movespeed;
        player.GetComponent<Animator>().SetBool("moving", false);
    
        BroadcastMessage("Execute_Dialogue");
        caninteract = true;
        state = 1;
    
        yield return 0;
    }
}
