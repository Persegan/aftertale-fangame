﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_12_Puzzle_2_Switches : MonoBehaviour {

    public GameObject Self_Switch_Pressed;
    public GameObject Self_Switch_Unpressed;

    public List<GameObject> Spikes;


    public List<GameObject> Affected_Switches;

    public List<GameObject> All_Switches;

    public int global_variable;


    private Color light_red = new Color(210 / 255.0f, 65 / 255.0f, 65 / 255.0f);
    private Color light_green = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {

            Self_Switch_Unpressed.GetComponent<SpriteRenderer>().enabled = false;
            Self_Switch_Pressed.GetComponent<SpriteRenderer>().enabled = true;

            ChangeColor();
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            Self_Switch_Unpressed.GetComponent<SpriteRenderer>().enabled = true;
            Self_Switch_Pressed.GetComponent<SpriteRenderer>().enabled = false;
        }
    }


    public void ChangeColor()
    {
        for (int i = 0; i < Affected_Switches.Count; i++)
        {
            if (Affected_Switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color == light_red)
            {
                Affected_Switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = light_green;
                Affected_Switches[i].transform.GetChild(1).GetComponent<SpriteRenderer>().color = light_green;
            }
            else
            {
                Affected_Switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = light_red;
                Affected_Switches[i].transform.GetChild(1).GetComponent<SpriteRenderer>().color = light_red;
            }
        }
        int auxiliar_count = 0;
        for (int i = 0; i < All_Switches.Count; i++)
        {
            if (All_Switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color == light_green)
            {
                auxiliar_count++;
            }
        }
        if (auxiliar_count == All_Switches.Count)
        {
            StartCoroutine(EndPuzzle());
        }

    }

    public IEnumerator EndPuzzle()
    {

        for (int i = 0; i < All_Switches.Count; i++)
        {
            All_Switches[i].GetComponent<BoxCollider2D>().enabled = false;
            All_Switches[i].transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
            All_Switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            All_Switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
        }

        for (int i = 0; i < Spikes.Count; i++)
        {
            Spikes[i].SetActive(false);
        }

        player_data.playerdata.global_variables[global_variable] = 1;
        yield return 0;
    }
}
