﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_7_Racing_Puzzle_Timer : MonoBehaviour {

    public Text timer_text;
    public float startTime;

    public float time;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
		
	}
	
	// Update is called once per frame
	void Update () {
        time = Time.time - startTime;
        string minutes = ((int)time / 60).ToString();
        string seconds = (time % 60).ToString("f2");

        if (((int)time / 60) <= 9)
        {
            minutes = "0" + minutes;
        }

        if (time%60 <= 10)
        {
            seconds = "0" + seconds;
        }

        timer_text.text = minutes + ":" + seconds;

		
	}
}
