﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Event_StarterDestroyer : MonoBehaviour {

    public GameObject switch1;

    public GameObject switch2collider;
    public GameObject switch2unpressed;
    public GameObject switch2pressed;

    public GameObject switch3collider;
    public GameObject switch3unpressed;
    public GameObject switch3pressed;

    public GameObject switch4collider;
    public GameObject switch4unpressed;
    public GameObject switch4pressed;

    public GameObject spikes1;
    public GameObject spikes2;

    public GameObject Conversation1;
    public GameObject Conversation2;

    public GameObject Normal_River;
    public GameObject River_Animation;
    public GameObject Frozen_River;

    public GameObject Normal_River_Collision;

    public GameObject[] Iced_River_Collisions;

    //private Color light_red = new Color(210 / 255.0f, 65 / 255.0f, 65 / 255.0f);
    private Color light_green = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);

    // Use this for initialization
    void Start () {

        if (player_data.playerdata.global_variables[9] == 1)
        {
            switch1.SetActive(false);
            spikes1.SetActive(false);
        }


        if (player_data.playerdata.global_variables[10] == 1)
        {
            spikes2.SetActive(false);

            switch2collider.GetComponent<BoxCollider2D>().enabled = false;
            switch3collider.GetComponent<BoxCollider2D>().enabled = false;
            switch4collider.GetComponent<BoxCollider2D>().enabled = false;

            switch2unpressed.SetActive(false);
            switch3unpressed.SetActive(false);
            switch4unpressed.SetActive(false);

            switch2pressed.GetComponent<SpriteRenderer>().color = light_green;
            switch3pressed.GetComponent<SpriteRenderer>().color = light_green;
            switch4pressed.GetComponent<SpriteRenderer>().color = light_green;
        }

        if (player_data.playerdata.global_variables[12] == 1)
        {
            Destroy(Conversation1);
        }
        
        if (player_data.playerdata.global_variables[13] == 1)
        {
            Destroy(Conversation2);
        }


        if (player_data.playerdata.global_variables[16] == 1)
        {
            Destroy(Normal_River);
            Destroy(River_Animation);
            Frozen_River.SetActive(true);
            Normal_River_Collision.SetActive(false);
            for (int i = 0; i < Iced_River_Collisions.Length; i++)
            {
                Iced_River_Collisions[i].SetActive(true);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
