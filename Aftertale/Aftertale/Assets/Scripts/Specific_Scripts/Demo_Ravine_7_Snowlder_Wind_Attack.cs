﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_7_Snowlder_Wind_Attack : MonoBehaviour {

    public Vector2 push_direction;
    public float initial_strength;
    public float final_strength;
    public float increment;
    public float duration;


    private float current_strength;

    private GameObject Soul;
    private bool should_push = true;


	// Use this for initialization
	void Start () {
       
        Soul = GameObject.FindGameObjectWithTag("Player_Soul");
        StartCoroutine(Deactivate_After_Time(duration));
        current_strength = initial_strength;
        if (push_direction == Vector2.right)
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, 90);
        }
        if (push_direction == Vector2.left)
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, -90);
        }
        if (push_direction == Vector2.up)
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, 180);
        }
        if (push_direction == new Vector2(0.5f, 0.5f))
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, 135);
        }
        if (push_direction == new Vector2(-0.5f, 0.5f))
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, -135);
        }
        if (push_direction == new Vector2(0.5f, -0.5f))
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, 45);
        }
        if (push_direction == new Vector2(-0.5f, -0.5f))
        {
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, -45);
        }     
    }
	
	// Late Update is called once per frame after Update
	void FixedUpdate () {
        if (should_push == true)
        {
            Vector3 push = new Vector3(push_direction.x, push_direction.y);

            //Soul.transform.localPosition += push * current_strength * Time.deltaTime;
            Soul.transform.localPosition = Vector3.MoveTowards (Soul.transform.localPosition, Soul.transform.localPosition + push, current_strength * Time.deltaTime);
            if (current_strength < final_strength)
            {
                current_strength += increment;
            }
        }

        //increment = increment * 2;
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Soul == null)
        {
            if (collision.tag == "Player_Soul")
            {
                Soul = collision.gameObject;
            }
        }
    }

    IEnumerator Deactivate_After_Time(float time)
    {
        yield return new WaitForSeconds(time);
        should_push = false;
        GetComponentInChildren<ParticleSystem>().Stop();
        yield return new WaitForSeconds(2.0f);
        Destroy(gameObject);
    }
}
