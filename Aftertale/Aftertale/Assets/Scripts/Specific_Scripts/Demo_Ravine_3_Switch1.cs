﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Switch1 : MonoBehaviour {

    public GameObject Spikes_1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (player_data.playerdata.global_variables[1] == 0)
            {
                Press_Switch_1();
            }
            else if (player_data.playerdata.global_variables[1] == 1)
            {
                Press_Switch_2();
            }

        }
    }

    public void Press_Switch_1()
    {
        Spikes_1.SetActive(false);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void Press_Switch_2()
    {
        Spikes_1.SetActive(false);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        player_data.playerdata.global_variables[1] = 2;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        player_data.playerdata.global_variables[9] = 1;
    }
}
