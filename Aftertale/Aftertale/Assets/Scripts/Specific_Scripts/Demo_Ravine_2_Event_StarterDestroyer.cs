﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_2_Event_StarterDestroyer : MonoBehaviour {
    public GameObject Conversation1;
    public GameObject Conversation2;
    public GameObject Conversation3;
    public GameObject StandStillConversations;
    public GameObject Player;
    public GameObject River_Collider_0;
    public GameObject River_Collider_1;
    public GameObject River_Collider_2;
    public GameObject River_Collider_3;
    public GameObject Frozen_River;
    public GameObject Normal_River;

	// Use this for initialization
	void Start () {
	
        if (player_data.playerdata.global_variables[2] == 1)
        {
            Destroy(Conversation1);
            Destroy(Conversation2);
            StandStillConversations.SetActive(true);
        }


        if (player_data.playerdata.global_variables[3] == 1)
        {
            Destroy(Conversation3);
            StandStillConversations.SetActive(true);
        }

        if (player_data.playerdata.global_variables[4] == 3)
        {
            Destroy(StandStillConversations);
        }

        if (player_data.playerdata.global_variables[16] == 1)
        {
            Destroy(Normal_River);
            Frozen_River.SetActive(true);
            River_Collider_0.SetActive(false);
            River_Collider_1.SetActive(true);
            River_Collider_2.SetActive(true);
            River_Collider_3.SetActive(true);
        }


        player_data.playerdata.global_variables[14] = 1;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
