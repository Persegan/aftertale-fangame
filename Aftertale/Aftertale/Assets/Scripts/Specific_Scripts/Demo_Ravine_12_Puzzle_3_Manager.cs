﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_12_Puzzle_3_Manager : MonoBehaviour {

    public List<GameObject> pressed_switches;
    public List<GameObject> unpressed_switches;
    public List<bool> activated_switches;
    public List<GameObject> spikes;

    public int global_variable;


    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < activated_switches.Count; i++)
        {
            activated_switches[i] = false;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void resetswitches()
    {
        for (int i = 0; i < unpressed_switches.Count; i++)
        {
            unpressed_switches[i].SetActive(true);
        }
        for (int i = 0; i < activated_switches.Count; i++)
        {
            activated_switches[i] = false;
        }
    }

    public void complete()
    {
        for (int i = 0; i < unpressed_switches.Count; i++)
        {
            unpressed_switches[i].SetActive(false);
        }

        for (int i = 0; i < spikes.Count; i++)
        {
            spikes[i].SetActive(false);
        }
        player_data.playerdata.global_variables[global_variable] = 1;
    }
}
