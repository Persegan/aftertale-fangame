﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_5_Event_StarterDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(Save_Room_Coroutine());       
        		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Save_Room_Coroutine()
    {
        //We check whether we're loading from the menu or walking into the room
        if (player_data.playerdata.global_variables[41] == 0)
        {
            player_data.playerdata.scene_to_load = "Demo_Ravine_5";
            player_data.playerdata.position_from_load_x = 0.0f;
            player_data.playerdata.position_from_load_y = 0.37f;
            player_data.playerdata.Save();
        }


        else
        {
            player_data.playerdata.global_variables[41] = 0;
            Destroy(GameObject.Find("Save_Icon"));
            GameObject Player = GameObject.FindGameObjectWithTag("Player");
            Player.GetComponent<movement>().can_dash = false;
            Player.GetComponent<movement>().can_interact = false;
            Player.GetComponent<movement>().can_move = false;
            Player.GetComponent<Animator>().Play("Pumpkin_Getting_Up", 1);
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).length / Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).speedMultiplier);
            Player.GetComponent<movement>().can_dash = true;
            Player.GetComponent<movement>().can_interact = true;
            Player.GetComponent<movement>().can_move = true;
            Player.GetComponent<Animator>().Play("Blank_State", 1);
        }

    }
}
