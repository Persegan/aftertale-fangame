﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_7_Racing_Puzzle_Switch : MonoBehaviour {

    public static bool puzzle_is_active = false;

    public Sprite pressed_switch;
    public Sprite unpressed_switch;

    public GameObject Starting_Switch;
    public GameObject Ending_Switch;

    public GameObject Starting_Spikes;
    public GameObject Ending_Spikes;

    public GameObject Timer;

    public float camera_moving_speed;
    public float final_time;

    private static GameObject Player;
	// Use this for initialization
	void Start () {
        puzzle_is_active = false;		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player = collision.gameObject;
            if (gameObject == Starting_Switch)
            {
                if (puzzle_is_active == true)
                {
                    StartCoroutine(Finish_Puzzle());
                }
                else
                {
                    StartCoroutine(Begin_Start_Finish());
                }
            }

            else if (gameObject == Ending_Switch)
            {
                if (puzzle_is_active == true)
                {
                    StartCoroutine(Finish_Puzzle());
                }
                else
                {
                    StartCoroutine(Begin_Finish_Start());
                }
            }
        }
    }

    IEnumerator Begin_Start_Finish()
    {
        player_data.playerdata.global_variables[47] = 1;
        puzzle_is_active = true;
        Starting_Switch.GetComponent<SpriteRenderer>().sprite = pressed_switch;
        Starting_Switch.GetComponent<BoxCollider2D>().enabled = false;

        if (Player.GetComponent<movement>().is_dashing == true)
        {
            Player.GetComponent<movement>().StopAllCoroutines();
            Player.GetComponent<movement>().is_dashing = false;
            Player.GetComponent<Animator>().SetBool("Dashing", false);
            Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
            Player.GetComponent<Rigidbody2D>().drag = Player.GetComponent<movement>().previousdrag;
            Player.GetComponent<Animator>().SetBool("moving", false);
        }
        Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0) * Player.GetComponent<movement>().movespeed;
        Camera.main.GetComponent<camera>().enabled = false;
        Player.GetComponent<Animator>().SetBool("moving", false);
        Player.GetComponent<movement>().can_move = false;
        Player.GetComponent<movement>().can_dash = false;
        Player.GetComponent<movement>().can_interact = false;
        Timer.SetActive(true);

        AudioManager.Audio_Manager.gameObject.GetComponent<AudioSource>().Stop();
        AudioManager.Audio_Manager.gameObject.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.Thundernsail_Remix;
        AudioManager.Audio_Manager.gameObject.GetComponent<AudioSource>().Play();
        //Starting_Spikes.SetActive(true);
        Ending_Spikes.SetActive(false);
        
        while  (Camera.main.transform.position.x != Ending_Switch.transform.position.x)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(Ending_Switch.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z), Time.deltaTime * camera_moving_speed);
            yield return new WaitForEndOfFrame();
        }

        while (Camera.main.transform.position.y != Ending_Switch.transform.position.y)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(Camera.main.transform.position.x, Ending_Switch.transform.position.y, Camera.main.transform.position.z), Time.deltaTime * camera_moving_speed);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2.5f);
        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_interact = true;
        Camera.main.GetComponent<camera>().enabled = true;
        Timer.GetComponent<Demo_Ravine_7_Racing_Puzzle_Timer>().enabled = true;

    }


    IEnumerator Begin_Finish_Start()
    {
        player_data.playerdata.global_variables[47] = 1;
        puzzle_is_active = true;
        Ending_Switch.GetComponent<SpriteRenderer>().sprite = pressed_switch;
        Ending_Switch.GetComponent<BoxCollider2D>().enabled = false;
        Camera.main.GetComponent<camera>().enabled = false;
        Player.GetComponent<movement>().can_move = false;
        Player.GetComponent<movement>().can_dash = false;
        Player.GetComponent<movement>().can_interact = false;
        Timer.SetActive(true);
        Starting_Spikes.SetActive(true);
        Ending_Spikes.SetActive(true);

        while (Camera.main.transform.position.y != Starting_Switch.transform.position.y)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(Camera.main.transform.position.x, Starting_Switch.transform.position.y, Camera.main.transform.position.z), Time.deltaTime * camera_moving_speed);
            yield return new WaitForEndOfFrame();
        }

        while (Camera.main.transform.position.x != Starting_Switch.transform.position.x)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(Starting_Switch.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z), Time.deltaTime * camera_moving_speed);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2.5f);
        Player.GetComponent<movement>().can_move = true;
        Player.GetComponent<movement>().can_dash = true;
        Player.GetComponent<movement>().can_interact = true;
        Camera.main.GetComponent<camera>().enabled = true;
        Timer.GetComponent<Demo_Ravine_7_Racing_Puzzle_Timer>().enabled = true;
    }

    IEnumerator Finish_Puzzle()
    {
        player_data.playerdata.global_variables[47] = 2;
        //Ending_Spikes.SetActive(false);
        //Starting_Spikes.SetActive(false);
        Ending_Switch.GetComponent<SpriteRenderer>().sprite = pressed_switch;
        Ending_Switch.GetComponent<BoxCollider2D>().enabled = false;
        Starting_Switch.GetComponent<SpriteRenderer>().sprite = pressed_switch;
        Starting_Switch.GetComponent<BoxCollider2D>().enabled = false;
        Timer.GetComponent<Demo_Ravine_7_Racing_Puzzle_Timer>().enabled = false;

        final_time = Timer.GetComponent<Demo_Ravine_7_Racing_Puzzle_Timer>().time - Timer.GetComponent<Demo_Ravine_7_Racing_Puzzle_Timer>().startTime;
        AudioManager.Audio_Manager.gameObject.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.OST_Nostalgia;
        AudioManager.Audio_Manager.gameObject.GetComponent<AudioSource>().Play();
        yield return new WaitForEndOfFrame();
    }

}
