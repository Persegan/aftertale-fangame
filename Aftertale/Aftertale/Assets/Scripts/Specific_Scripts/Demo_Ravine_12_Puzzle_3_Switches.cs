﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_Ravine_12_Puzzle_3_Switches : MonoBehaviour {


    public GameObject manager;
    public GameObject pressedswitch;
    public GameObject unpressedswitch;

    public int requirednumber;
    public int changednumber;

    private bool reset;

    // Use this for initialization
    void Start()
    {
        reset = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (requirednumber == 0)
            {
                unpressedswitch.SetActive(false);
                pressedswitch.GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
                manager.GetComponent<Demo_Ravine_12_Puzzle_3_Manager>().activated_switches[changednumber] = true;
            }
            else if (manager.GetComponent<Demo_Ravine_12_Puzzle_3_Manager>().activated_switches[requirednumber] == true)
            {
                unpressedswitch.SetActive(false);
                pressedswitch.GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
                if (changednumber == manager.GetComponent<Demo_Ravine_12_Puzzle_3_Manager>().activated_switches.Count)
                {
                    manager.GetComponent<Demo_Ravine_12_Puzzle_3_Manager>().complete();
                }
                else
                {
                    manager.GetComponent<Demo_Ravine_12_Puzzle_3_Manager>().activated_switches[changednumber] = true;
                }

            }

            else
            {
                unpressedswitch.SetActive(false);
                pressedswitch.GetComponent<SpriteRenderer>().color = new Color(210 / 255.0f, 65 / 255.0f, 65 / 255.0f);
                reset = true;
            }
        }
        
    }




    void OnTriggerExit2D(Collider2D collider)
    {
        if (reset == true && collider.tag == "Player")
        {
            manager.GetComponent<Demo_Ravine_12_Puzzle_3_Manager>().resetswitches();
            reset = false;
        }
    }
}
