﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Demo_Ravine_7_Snowlder_Wiretrap : MonoBehaviour {

    private GameObject Soul;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Soul == null)
        {
            if (collision.tag == "Player_Soul_Collision")
            {
                Soul = collision.gameObject;
                StartCoroutine(Detonate());
            }
        }
 
    }


    IEnumerator Detonate()
    {
        GetComponent<Image>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        gameObject.tag = "enemy";
        Destroy(gameObject,0.021f);        
    }

}
