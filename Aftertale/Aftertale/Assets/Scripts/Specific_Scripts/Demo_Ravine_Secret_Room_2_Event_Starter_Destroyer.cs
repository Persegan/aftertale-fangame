﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_Secret_Room_2_Event_Starter_Destroyer : MonoBehaviour {
    public GameObject Sackboy;
	// Use this for initialization
	void Start () {

        if (player_data.playerdata.global_variables[42] == 1)
        {
            Destroy(Sackboy);
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
