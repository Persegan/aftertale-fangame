﻿using UnityEngine;
using System.Collections;

public class Demo_Ravine_3_Switch2_1 : MonoBehaviour {

    public GameObject manager;
    public GameObject pressedswitch;
    public GameObject unpressedswitch;

	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            unpressedswitch.SetActive(false);
            manager.GetComponent<Demo_Ravine_3_Switch2>().switch1activated = true;
            pressedswitch.GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
        }
    }
}
