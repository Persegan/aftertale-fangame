﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Demo_Ravine_6_Signey_Overworld_Fight : MonoBehaviour {
    //To make him do different stuff depending on the side you're on, see transform.inversetransformpoint. Search google for How to determine if enemy is on right or left side.
    public AudioClip battle1;
    public AudioClip battle2;
    public AudioClip battle3;

    public GameObject black_background;
    public GameObject soul;
    public GameObject Battle_Canvas;
    public GameObject collisions;

    public Button Choice1;
    public Button Choice2;
    public Image Choice_Soul;
    public AudioSource menu_select;
    public AudioSource menu_choose;


    public string[] messages;
    public string[] messages_option_1;
    public string[] messages_option_2;
    public string[] messages_after_fight_pacifist;
    public string[] messages_after_fight_genocide;
    public string option1_message;
    public string option2_message;

    public Image dialoguebox;
    public Image Battle_dialogue_box;
    public Text displaytext;
    public GameObject player;
    public GameObject Signy_Enemy;
    public Sprite Destroyed_Signy;
    public AudioSource sound;
    public int variable;
    public int value;

    public int state
    {
        get;
        set;
    }



    /*the manager state controls what messages are being displayed:
     * 0 is the base messages
     * 1 is for choice 1
     * 2 is for choice 2
     * 3 is for pacifist completion
     * 4 is for genocide completion*/
    public int manager_state
    {
        get;
        set;
    }


    protected bool caninteract = false;


    // Use this for initialization
    void Awake()
    {
        state = -1;
        manager_state = 0;
        collisions = GameObject.Find("Collisions");
}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (caninteract == true)
            {
                if (manager_state == 0)
                {
                    if (state == messages.Length - 2 && AutoTextScript.isinteracting == false)
                    {
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messages[state], 0.03f, sound);      
                        StartCoroutine(Present_Buttons());
                    }

                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messages[state], 0.03f, sound);
                    }
                }
                else if (manager_state == 1)
                {
                    if (state == messages_option_1.Length - 1 && AutoTextScript.isinteracting == false)
                    {
                        state = -1;
                        manager_state = 0;
                        Start_Battle();
                    }

                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, sound);
                    }
                }

                else if (manager_state == 2)
                {
                    if (state == messages_option_2.Length - 1 && AutoTextScript.isinteracting == false)
                    {
                        StartCoroutine(Finish_Conversation());

                    }

                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, sound);
                    }
                }

                else if (manager_state == 3)
                {
                    if (state == messages_after_fight_pacifist.Length - 1 && AutoTextScript.isinteracting == false)
                    {
                        StartCoroutine(Finish_Conversation());

                    }

                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messages_after_fight_pacifist[state], 0.03f, sound);
                    }
                }

                else if (manager_state == 4)
                {
                    if (state == messages_after_fight_genocide.Length - 1 && AutoTextScript.isinteracting == false)
                    {
                        StartCoroutine(Finish_Conversation());

                    }

                    else if (caninteract == true && AutoTextScript.isinteracting == false)
                    {
                        state++;
                        AutoTextScript.isinteracting = true;
                        player.GetComponent<movement>().can_move = false;
                        player.GetComponent<Animator>().SetBool("moving", false);
                        dialoguebox.gameObject.SetActive(true);
                        displaytext.gameObject.SetActive(true);
                        displaytext.text = "";
                        AutoTextScript.TypeText(displaytext, messages_after_fight_genocide[state], 0.03f, sound);
                    }
                }
            }          

        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            if (caninteract == true)
            {
                if (manager_state == 0)
                {
                    if (state >= 0)
                    {
                        AutoTextScript.StopText(displaytext);
                        messages[state] = messages[state].Replace("ç", "\n");
                        displaytext.text = messages[state];
                        AutoTextScript.isfinished = true;
                        AutoTextScript.isinteracting = false;
                    }
                }

                else if (manager_state == 1)
                {
                    if (state >= 0)
                    {
                        AutoTextScript.StopText(displaytext);
                        messages_option_1[state] = messages_option_1[state].Replace("ç", "\n");
                        displaytext.text = messages_option_1[state];
                        AutoTextScript.isfinished = true;
                        AutoTextScript.isinteracting = false;
                    }
                }

                else if (manager_state == 2)
                {
                    if (state >= 0)
                    {
                        AutoTextScript.StopText(displaytext);
                        messages_option_2[state] = messages_option_2[state].Replace("ç", "\n");
                        displaytext.text = messages_option_2[state];
                        AutoTextScript.isfinished = true;
                        AutoTextScript.isinteracting = false;
                    }
                }

                else if (manager_state == 3)
                {
                    if (state >= 0)
                    {
                        AutoTextScript.StopText(displaytext);
                        messages_after_fight_pacifist[state] = messages_after_fight_pacifist[state].Replace("ç", "\n");
                        displaytext.text = messages_after_fight_pacifist[state];
                        AutoTextScript.isfinished = true;
                        AutoTextScript.isinteracting = false;
                    }
                }

                else if (manager_state == 4)
                {
                    if (state >= 0)
                    {
                        AutoTextScript.StopText(displaytext);
                        messages_after_fight_genocide[state] = messages_after_fight_genocide[state].Replace("ç", "\n");
                        displaytext.text = messages_after_fight_genocide[state];
                        AutoTextScript.isfinished = true;
                        AutoTextScript.isinteracting = false;
                    }
                }
            }
        }

    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player_Collision")
        {
            if (collision.gameObject.GetComponentInParent<movement>().menu_open == false && collision.gameObject.GetComponentInParent<movement>().in_menu == false && collision.gameObject.GetComponentInParent<movement>().can_interact == true)
            {
                caninteract = true;
            }
            else
            {
                caninteract = false;
            }
        }
    }

    void OnTriggerExit2D()
    {
        caninteract = false;
        AutoTextScript.isfinished = false;
    }


   public void SelectButton1()
    {
        Choice_Soul.gameObject.SetActive(true);
        //player_soul.transform.localPosition = new Vector3(button1.transform.localPosition.x - 300, button1.transform.localPosition.y, 0.0f);
        Choice_Soul.transform.position = GetLetterPosition_Script.GetPos(Choice1.GetComponentInChildren<Text>(), 1, transform.GetChild(0).gameObject.GetComponent<Canvas>());
        Vector3 auxiliar = Choice_Soul.transform.position;
        auxiliar.x -= 0.16f;
        Choice_Soul.transform.position = auxiliar;
        menu_select.Play();
    }

    public void SelectButton2()
    {
        Choice_Soul.gameObject.SetActive(true);
        //player_soul.transform.localPosition = new Vector3(button1.transform.localPosition.x - 300, button1.transform.localPosition.y, 0.0f);
        Choice_Soul.transform.position = GetLetterPosition_Script.GetPos(Choice2.GetComponentInChildren<Text>(), 1, transform.GetChild(0).gameObject.GetComponent<Canvas>());
        Vector3 auxiliar = Choice_Soul.transform.position;
        auxiliar.x -= 0.16f;
        Choice_Soul.transform.position = auxiliar;
        menu_select.Play();
    }
   public void ChooseButton1()
    {
        Choice1.gameObject.SetActive(false);
        Choice2.gameObject.SetActive(false);
        Choice_Soul.gameObject.SetActive(false);
        manager_state = 1;
        caninteract = true;
        state++;
        AutoTextScript.isinteracting = true;
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<Animator>().SetBool("moving", false);
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        AutoTextScript.TypeText(displaytext, messages_option_1[state], 0.03f, sound);
    }
   public void ChooseButton2()
    {
        Choice1.gameObject.SetActive(false);
        Choice2.gameObject.SetActive(false);
        Choice_Soul.gameObject.SetActive(false);
        manager_state = 2;
        caninteract = true;
        state++;
        AutoTextScript.isinteracting = true;
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<Animator>().SetBool("moving", false);
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        AutoTextScript.TypeText(displaytext, messages_option_2[state], 0.03f, sound);
    }

    IEnumerator Present_Buttons()
    {
        while (AutoTextScript.isinteracting == true)
        {
            yield return new WaitForEndOfFrame();
        }
        caninteract = false;
        state = -1;
        Choice1.gameObject.SetActive(true);
        Choice2.gameObject.SetActive(true);
        Choice1.GetComponentInChildren<Text>().text = "";
        Choice2.GetComponentInChildren<Text>().text = "";
        AutoTextScript.TypeText(Choice1.GetComponentInChildren<Text>(), option1_message, 0.03f, sound);
        AutoTextScript.TypeText(Choice2.GetComponentInChildren<Text>(), option2_message, 0.03f, sound);
        while (AutoTextScript.isinteracting == true)
        {
            yield return new WaitForEndOfFrame();
        }
        Choice1.Select();
    }

    IEnumerator Finish_Conversation()
    {
        state = -1;
        if (manager_state == 2) //This means the player chose the option of not doing the tutorial
        {
            manager_state = 0;
            if (variable != 0 && value != 0)
            {
                player_data.playerdata.global_variables[variable] = value;
            }
        }

        else if (manager_state == 3) //This means the player completed the fight in pacifist
        {
            player_data.playerdata.global_variables[22] = 1;
            player_data.playerdata.global_variables[23] = 0;
        }

        else if (manager_state == 4) //This means the player completed the fight in genocide
        {
            player_data.playerdata.global_variables[22] = 2;
            player_data.playerdata.global_variables[23] = 0;
        }

        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        AutoTextScript.isfinished = false;
        AutoTextScript.isinteracting = false;
        player.GetComponent<movement>().can_move = true;
        player.GetComponent<movement>().can_dash = true;
        player.GetComponent<movement>().can_interact = true;

        yield return new WaitForEndOfFrame();
    }

    public void Start_Battle()
    {
        StartCoroutine(Start_Battle_coroutine());
    }


    IEnumerator Start_Battle_coroutine()
    {
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Stop();
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<movement>().can_interact = false;
        dialoguebox.gameObject.SetActive(false);
        displaytext.gameObject.SetActive(false);
        collisions.SetActive(false);
        BoxCollider2D[] boxlist;
        boxlist = soul.gameObject.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in boxlist)
        {
            box.enabled = false;
        }

        GetComponent<AudioSource>().clip = battle1;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.4f);

        black_background.SetActive(false);
        black_background.SetActive(true);
        black_background.transform.SetAsLastSibling();
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 1f);
        GameObject temporal = soul;
        temporal.transform.position = player.transform.position;
        temporal.SetActive(true);
        GetComponent<AudioSource>().clip = battle2;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.07f);

        black_background.SetActive(false);
        temporal.SetActive(false);
        yield return new WaitForSeconds(0.07f);

        black_background.SetActive(true);
        temporal.SetActive(true);
        GetComponent<AudioSource>().clip = battle2;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.07f);

        black_background.SetActive(false);
        temporal.SetActive(false);;
        yield return new WaitForSeconds(0.07f);

        black_background.SetActive(true);
        temporal.SetActive(true);
        GetComponent<AudioSource>().clip = battle2;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.07f);

        GetComponent<AudioSource>().clip = battle3;
        GetComponent<AudioSource>().Play();
        Vector3 SoulInitial = temporal.transform.position;
        Vector3 SoulFinal = Battle_dialogue_box.gameObject.transform.position;
        SoulFinal.z = soul.transform.position.z;
 
        for (float i = 0; i < 1; i = i + 0.04f)
        {
            temporal.transform.position = Vector3.Lerp(SoulInitial, SoulFinal, i);
            yield return new WaitForSeconds(0.000001f);
        }
        soul.transform.position = SoulFinal;
        Battle_Canvas.SetActive(true);



        for (float i = 1; i > 0; i = i - 0.05f)
        {
            Color c = black_background.GetComponent<Image>().color;
            c.a = i;
            black_background.GetComponent<Image>().color = c;
            yield return new WaitForSeconds(0.01f);
        }
        black_background.SetActive(false);
        Battle_Canvas.GetComponentInParent<battle_manager>().StartEnemyTurn();
        player.SetActive(false);
        Signy_Enemy.GetComponent<Demo_Ravine_6_Signey_Enemy>().Overworld_Signy = gameObject;
        gameObject.SetActive(false);
        foreach (BoxCollider2D box in boxlist)
        {
            box.enabled = true;
        }
        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.Signy_Battle_Theme;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();
        transform.parent.gameObject.SetActive(false);
        gameObject.SetActive(false);

        //temporal.SetActive (false);
    }


    public void Finish_Battle_Pacifist()
    {
        StartCoroutine(Finish_Battle_Coroutine_Pacifist());
    }

    public void Finish_Battle_Genocide()
    {
        StartCoroutine(Finish_Battle_Coroutine_Genocide());
    }

    //This function terminates the tutorial fight
    IEnumerator Finish_Battle_Coroutine_Pacifist()
    {
        player.SetActive(true);
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0f);
        for (float i = 0; i < 1; i = i + 0.05f)
        {
            Color c = black_background.GetComponent<Image>().color;
            c.a = i;
            black_background.GetComponent<Image>().color = c;
            yield return new WaitForSeconds(0.01f);
        }


        Battle_Canvas.SetActive(false);



        for (float i = 1; i > 0; i = i - 0.05f)
        {
            Color c = black_background.GetComponent<Image>().color;
            c.a = i;
            black_background.GetComponent<Image>().color = c;
            yield return new WaitForSeconds(0.01f);
        }
        collisions.SetActive(true);
        BoxCollider2D[] boxlist;
        boxlist = soul.gameObject.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in boxlist)
        {
            box.enabled = false;
        }


        black_background.SetActive(false);
        player.GetComponent<movement>().can_interact = true;
        caninteract = true;
        manager_state = 3;

        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();

        state++;
        AutoTextScript.isinteracting = true;
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<Animator>().SetBool("moving", false);
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        AutoTextScript.TypeText(displaytext, messages_after_fight_pacifist[state], 0.03f, sound);
        transform.parent.gameObject.SetActive(true);
    }


    IEnumerator Finish_Battle_Coroutine_Genocide()
    {
        transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite= Destroyed_Signy;
        transform.GetChild(1).localScale = new Vector3(1, 1, 1);
        player.SetActive(true);
        black_background.SetActive(true);
        black_background.GetComponent<Image>().color = new Color(0, 0, 0, 0f);
        for (float i = 0; i < 1; i = i + 0.05f)
        {
            Color c = black_background.GetComponent<Image>().color;
            c.a = i;
            black_background.GetComponent<Image>().color = c;
            yield return new WaitForSeconds(0.01f);
        }


        Battle_Canvas.SetActive(false);



        for (float i = 1; i > 0; i = i - 0.05f)
        {
            Color c = black_background.GetComponent<Image>().color;
            c.a = i;
            black_background.GetComponent<Image>().color = c;
            yield return new WaitForSeconds(0.01f);
        }
        collisions.SetActive(true);
        BoxCollider2D[] boxlist;
        boxlist = soul.gameObject.GetComponents<BoxCollider2D>();
        foreach (BoxCollider2D box in boxlist)
        {
            box.enabled = false;
        }


        black_background.SetActive(false);
        player.GetComponent<movement>().can_interact = true;
        caninteract = true;
        manager_state = 4;

        AudioManager.Audio_Manager.GetComponent<AudioSource>().clip = AudioManager.Audio_Manager.OST_Nostalgia;
        AudioManager.Audio_Manager.GetComponent<AudioSource>().Play();

        state++;
        AutoTextScript.isinteracting = true;
        player.GetComponent<movement>().can_move = false;
        player.GetComponent<Animator>().SetBool("moving", false);
        dialoguebox.gameObject.SetActive(true);
        displaytext.gameObject.SetActive(true);
        displaytext.text = "";
        AutoTextScript.TypeText(displaytext, messages_after_fight_genocide[state], 0.03f, sound);
        transform.parent.gameObject.SetActive(true);
    }
}
