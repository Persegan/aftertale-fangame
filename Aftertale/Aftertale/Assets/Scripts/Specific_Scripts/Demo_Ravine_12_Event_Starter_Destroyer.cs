﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_Ravine_12_Event_Starter_Destroyer : MonoBehaviour {

    public GameObject puzzle_1_spikes_1;
    public GameObject puzzle_1_spikes_2;
    public GameObject puzzle_1_switch_1;
    public GameObject puzzle_1_switch_2;
    public GameObject puzzle_1_switch_3;
    public GameObject puzzle_1_switch_4;
    public GameObject puzzle_1_switch_5;
    public GameObject puzzle_1_switch_6;
    public GameObject puzzle_1_switch_7;
    public GameObject puzzle_1_switch_8;

    public List<GameObject> puzzle_2_spikes;
    public List<GameObject> puzzle_2_reactivation_zones;
    public List<GameObject> puzzle_2_switches;

    public List<GameObject> puzzle_3_spikes;
    public List<GameObject> puzzle_3_reactivation_zones;
    public List<GameObject> puzzle_3_switches;

    public List<GameObject> puzzle_4_spikes;
    public List<GameObject> puzzle_4_reactivation_zones;
    public List<GameObject> puzzle_4_switches;

    public List<GameObject> puzzle_5_spikes;
    public List<GameObject> puzzle_5_reactivation_zones;
    public List<GameObject> puzzle_5_switches;

    public List<GameObject> puzzle_6_spikes;
    public List<GameObject> puzzle_6_reactivation_zones;
    public List<GameObject> puzzle_6_switches;

    public List<GameObject> puzzle_7_spikes;
    public List<GameObject> puzzle_7_reactivation_zones;
    public List<GameObject> puzzle_7_switches;

    public GameObject puzzle_secret_interactable;
    public GameObject puzzle_secret_spikes;



    // Use this for initialization
    void Start () {
#region Puzzle1
        //if Puzzle 1 was completed, we disable the spikes
        if (player_data.playerdata.global_variables[24] != 0)
        {
            player_data.playerdata.global_variables[24] = 9;
            puzzle_1_spikes_1.SetActive(false);
            puzzle_1_spikes_2.SetActive(false);
        }


        //We check for individual presses of each switch and modify them if necessary
        if (player_data.playerdata.global_variables[25] == 1)
        {
            puzzle_1_switch_1.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_1.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_1.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[26] == 1)
        {
            puzzle_1_switch_2.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_2.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_2.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[27] == 1)
        {
            puzzle_1_switch_3.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_3.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_3.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[28] == 1)
        {
            puzzle_1_switch_4.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_4.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_4.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[29] == 1)
        {
            puzzle_1_switch_5.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_5.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_5.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[30] == 1)
        {
            puzzle_1_switch_6.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_6.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_6.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[31] == 1)
        {
            puzzle_1_switch_7.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_7.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_7.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (player_data.playerdata.global_variables[32] == 1)
        {
            puzzle_1_switch_8.GetComponent<BoxCollider2D>().enabled = false;
            puzzle_1_switch_8.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            puzzle_1_switch_8.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        }
        #endregion
#region Puzzle2

        //if Puzzle2 was completed we disable the spikes, reactivation zones and switches 
        if (player_data.playerdata.global_variables[33] == 1)
        {
            for (int i = 0; i < puzzle_2_spikes.Count; i++)
            {
                puzzle_2_spikes[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_2_reactivation_zones.Count; i++)
            {
                puzzle_2_reactivation_zones[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_2_switches.Count; i++)
            {
                puzzle_2_switches[i].GetComponent<BoxCollider2D>().enabled = false;
                puzzle_2_switches[i].transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                puzzle_2_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                puzzle_2_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
            }
        }

        #endregion
#region Puzzle3

        //if Puzzle3 was completed we disable the spikes, reactivation zones and switches 
        if (player_data.playerdata.global_variables[34] == 1)
        {
            for (int i = 0; i < puzzle_3_spikes.Count; i++)
            {
                puzzle_3_spikes[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_3_reactivation_zones.Count; i++)
            {
                puzzle_3_reactivation_zones[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_3_switches.Count; i++)
            {
                puzzle_3_switches[i].GetComponent<BoxCollider2D>().enabled = false;
                puzzle_3_switches[i].transform.GetChild(1).gameObject.SetActive(false);
                puzzle_3_switches[i].transform.GetChild(0).gameObject.SetActive(true);
                puzzle_3_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
            }
        }

        #endregion
#region Puzzle4

        //if Puzzle4 was completed we disable the spikes, reactivation zones and switches 
        if (player_data.playerdata.global_variables[35] == 1)
        {
            for (int i = 0; i < puzzle_4_spikes.Count; i++)
            {
                puzzle_4_spikes[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_4_reactivation_zones.Count; i++)
            {
                puzzle_4_reactivation_zones[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_4_switches.Count; i++)
            {
                puzzle_4_switches[i].GetComponent<BoxCollider2D>().enabled = false;
                puzzle_4_switches[i].transform.GetChild(1).gameObject.SetActive(false);
                puzzle_4_switches[i].transform.GetChild(0).gameObject.SetActive(true);
                puzzle_4_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
            }
        }
        #endregion
#region Puzzle5

        //if Puzzle5 was completed we disable the spikes, reactivation zones and switches 
        if (player_data.playerdata.global_variables[36] == 1)
        {
            for (int i = 0; i < puzzle_5_spikes.Count; i++)
            {
                puzzle_5_spikes[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_5_reactivation_zones.Count; i++)
            {
                puzzle_5_reactivation_zones[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_5_switches.Count; i++)
            {
                puzzle_5_switches[i].GetComponent<BoxCollider2D>().enabled = false;
                puzzle_5_switches[i].transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                puzzle_5_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                puzzle_5_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
            }
        }

        #endregion
#region Puzzle6
        //if Puzzle6 was completed we disable the spikes, reactivation zones and switches 
        if (player_data.playerdata.global_variables[37] == 1)
        {
            for (int i = 0; i < puzzle_6_spikes.Count; i++)
            {
                puzzle_6_spikes[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_6_reactivation_zones.Count; i++)
            {
                puzzle_6_reactivation_zones[i].SetActive(false);
            }

            for (int i = 0; i < puzzle_6_switches.Count; i++)
            {
                puzzle_6_switches[i].GetComponent<BoxCollider2D>().enabled = false;
                puzzle_6_switches[i].transform.GetChild(1).gameObject.SetActive(false);
                puzzle_6_switches[i].transform.GetChild(0).gameObject.SetActive(true);
                puzzle_6_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
            }
        }
#endregion
#region Puzzle7

        //if Puzzle7 was completed we disable the spikes, reactivation zones and switches 
        if (player_data.playerdata.global_variables[38] == 1)
    {
        for (int i = 0; i < puzzle_7_spikes.Count; i++)
        {
            puzzle_7_spikes[i].SetActive(false);
        }

        for (int i = 0; i < puzzle_7_reactivation_zones.Count; i++)
        {
            puzzle_7_reactivation_zones[i].SetActive(false);
        }

        for (int i = 0; i < puzzle_7_switches.Count; i++)
        {
            puzzle_7_switches[i].GetComponent<BoxCollider2D>().enabled = false;
            puzzle_7_switches[i].transform.GetChild(1).gameObject.SetActive(false);
            puzzle_7_switches[i].transform.GetChild(0).gameObject.SetActive(true);
            puzzle_7_switches[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(69 / 255.0f, 218 / 255.0f, 131 / 255.0f);
        }
    }

        #endregion
#region PuzzleSecret
        if (player_data.playerdata.global_variables[39] == 1)
        {
            puzzle_secret_interactable.SetActive(false);
            puzzle_secret_spikes.SetActive(false);
        }
        #endregion


    }

    // Update is called once per frame
    void Update () {
		
	}
}
