﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff_Charges : MonoBehaviour {

    public int charges;
    public Color color;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>().color = color;
        }
    }
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < charges; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }		
	}
}
