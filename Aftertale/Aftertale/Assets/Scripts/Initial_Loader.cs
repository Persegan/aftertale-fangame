﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class Initial_Loader : MonoBehaviour {

    public bool reset = false;

	// Use this for initialization
	void Start () {
        UnityEngine.Cursor.visible = false;

        if (reset == true)
        {
            player_data.playerdata.Reset();
            player_data.playerdata.scene_to_load = "Demo_Ravine_0";
            SceneManager.LoadScene("Demo_Menu");
            File.Delete(Application.persistentDataPath + "/player_data.dat");
        }

        else if (File.Exists(Application.persistentDataPath + "/player_data.dat") == true)
        {
            player_data.playerdata.Load();


            switch (player_data.playerdata.scene_to_load)
            {
                case "Demo_Ravine_5":
                    player_data.playerdata.position_to_load = new Vector3(-0.044f, 0.378f, 8.6f);
                    break;

            }

            player_data.playerdata.LoadScene();
        }

        else
        {
            player_data.playerdata.Reset();
            player_data.playerdata.scene_to_load = "Demo_Ravine_0";
            SceneManager.LoadScene("Demo_Menu");
        }



    }
	
	// Update is called once per frame
	void Update () {
       

    }
}
