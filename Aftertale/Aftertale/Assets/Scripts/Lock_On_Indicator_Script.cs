﻿using UnityEngine;
using System.Collections;

public class Lock_On_Indicator_Script : MonoBehaviour {

    public GameObject target;
    public float speed;

	// Use this for initialization
	void Start () {
        gameObject.transform.position = target.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            Vector3 direction = target.transform.position - gameObject.transform.position;
            gameObject.transform.position += direction.normalized * Time.deltaTime * speed;
        }
	
	}
}
