﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class soul_movement : MonoBehaviour {
	public float movespeed = 0.4f;
	public bool can_move = false;
    public bool invulnerable = false;
	public GameObject HPnumber_UI;
	public GameObject HPbar_UI;
    public float dashspeed = 3.5f;
    public float dashduration;
    public float dashcooldown = 0.7f;
    public float invulnerability_duration = 1.3f;
    public bool memory_chosen;
    public bool fight_chosen;
    public bool act_chosen;
    public bool item_chosen;
    public bool is_dashing = false;
	
	public AudioSource damaged;
    public AudioSource dashready;
    public AudioSource heal_sound;

    public bool can_dash
    {
        get;
        set;
    }

    public float HealOnDash
    {
        get;
        set;
    }

    public bool should_heal_on_dash
    {
        get;
        set;
    }


	// Use this for initialization
	void Start () {
        player_data.playerdata.Player_Soul = gameObject;
        should_heal_on_dash = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (can_move == true) {
     
            if (Input.GetKey (KeyCode.UpArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, movespeed);

            }
			if (Input.GetKey (KeyCode.RightArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (movespeed, gameObject.GetComponent<Rigidbody2D> ().velocity.y);

			}

			if (Input.GetKey (KeyCode.DownArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, -movespeed);

			}

			if (Input.GetKey (KeyCode.LeftArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-movespeed, gameObject.GetComponent<Rigidbody2D> ().velocity.y);

			} 
			if (Input.GetKeyUp (KeyCode.UpArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, 0);
			}
			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (Input.GetKeyUp (KeyCode.LeftArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (Input.GetKeyUp (KeyCode.DownArrow)) {
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.GetComponent<Rigidbody2D> ().velocity.x, 0);
			}
            gameObject.GetComponent<Rigidbody2D>().velocity = gameObject.GetComponent<Rigidbody2D>().velocity.normalized * movespeed;
            if (Input.GetKeyDown (KeyCode.X)){
                   if (can_move == true && can_dash == true && GetComponent<Rigidbody2D>().velocity != new Vector2 (0.0f, 0.0f))
                {
                    StartCoroutine(Dash());
                }
            }

          
            /*if (Input.GetKeyDown (KeyCode.Y)) {
				Take_Damage (1);
			}*/
        }

	}
    
    /*void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag == "memory")
        {
            //gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 2, gameObject.transform.localScale.y + 2, gameObject.transform.localScale.z);
            memoryfragments++;
        }

       if (collider.tag == "enemy")
        {
            if (invulnerable == false)
            {
                Take_Damage(collider.GetComponent<enemy_data>().attack);
            }
        }

    }*/

    /*void OnTriggerStay2D(Collider2D collider)
    {

        OnTriggerEnter2D(collider);
    }*/











    ///Auxiliar methods


    public void Recover_health(float healthRecover)
    {
        if (player_data.playerdata.health == player_data.playerdata.maxhealth)
        {

        }
        else if ((player_data.playerdata.health + healthRecover) <= player_data.playerdata.maxhealth)
        {
            heal_sound.Play();
            player_data.playerdata.health += healthRecover;
        }

        else
        {
            heal_sound.Play();
            player_data.playerdata.health = player_data.playerdata.maxhealth;
        }       
        Text Temporal = HPnumber_UI.GetComponent<Text>();
        Temporal.text = player_data.playerdata.health.ToString();
        Temporal.text += "/";
        Temporal.text += player_data.playerdata.maxhealth.ToString();
        HPbar_UI.GetComponent<Slider>().maxValue = player_data.playerdata.maxhealth;
        HPbar_UI.GetComponent<Slider>().value = player_data.playerdata.health;

    }


    public void Take_Damage(float damage)
	{
        invulnerable = true;
        player_data.playerdata.health -= damage;
		Text Temporal = HPnumber_UI.GetComponent<Text> ();
		Temporal.text = player_data.playerdata.health.ToString ();
		Temporal.text += "/";
		Temporal.text += player_data.playerdata.maxhealth.ToString ();
		HPbar_UI.GetComponent<Slider> ().maxValue = player_data.playerdata.maxhealth;
		HPbar_UI.GetComponent<Slider> ().value = player_data.playerdata.health;
		if (player_data.playerdata.health > 0) {
			StartCoroutine (Take_Damage_coroutine ());
		} else {
			Game_Over ();
		}
	}

	IEnumerator Take_Damage_coroutine()
	{
        StopCoroutine(Dash_indicator());
		StartCoroutine (Camera_Shake ());
		damaged.Play ();
		float delay = 0.05f;
        Color c = new Color(1, 1, 1, 1);//gameObject.GetComponent<SpriteRenderer> ().color;
        Color d = new Color(1, 1, 1, 0.2f);//gameObject.GetComponent<SpriteRenderer> ().color;
        //d.a = 0.2f;
        //gameObject.GetComponent<BoxCollider2D> ().enabled = false;
        gameObject.GetComponent<SpriteRenderer> ().color = d;
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<SpriteRenderer> ().color = c;
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<SpriteRenderer> ().color = d;
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<SpriteRenderer> ().color = c;
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<SpriteRenderer> ().color = d;
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<SpriteRenderer> ().color = c;
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<SpriteRenderer> ().color = d;
		yield return new WaitForSeconds (delay);
        yield return new WaitForSeconds(invulnerability_duration);
        gameObject.GetComponent<SpriteRenderer> ().color = c;
        //gameObject.GetComponent<BoxCollider2D> ().enabled = true;
        invulnerable = false;
	}
	IEnumerator Camera_Shake()
	{
		GameObject temporal = GameObject.Find ("Battle_Canvas");
		Vector3 original = temporal.transform.localPosition;
		float shakeAmount = 5.0f;
		float delay = 0.03f;
		temporal.transform.localPosition = Random.insideUnitSphere * shakeAmount;
		yield return new WaitForSeconds (delay);
		temporal.transform.localPosition = Random.insideUnitSphere * shakeAmount;
		yield return new WaitForSeconds (delay);
		temporal.transform.localPosition = Random.insideUnitSphere * shakeAmount;
		yield return new WaitForSeconds (delay);
		temporal.transform.localPosition = original;
	}
	public void Game_Over()
	{
		player_data.playerdata.soul_local_position = gameObject.transform.localPosition;
		SceneManager.LoadScene ("Game Over");
	}


    IEnumerator Dash()
    {   
        if (should_heal_on_dash == true)
        {
            Recover_health(HealOnDash);
        }
        dashready.Play();
        can_move = false;
        can_dash = false;
        is_dashing = true;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(gameObject.GetComponent<Rigidbody2D>().velocity.x * dashspeed, gameObject.GetComponent<Rigidbody2D>().velocity.y * dashspeed);
        yield return new WaitForSeconds(dashduration);
        is_dashing = false;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        StartCoroutine(Dash_indicator());
        can_move = true;
        yield return new WaitForSeconds(dashcooldown);
        can_dash = true;
    }
   
    IEnumerator Dash_indicator()
    {
        Color d = new Color(0.5f, 0.5f, 0.5f, 1);
        gameObject.GetComponent<SpriteRenderer>().color = d;
        for (float i = 0; i < 12; i++)
        {
            d.r += 0.45f / 12;
            d.g += 0.45f / 12;
            d.b += 0.45f / 12;
            gameObject.GetComponent<SpriteRenderer>().color = d;
            yield return new WaitForSeconds(dashcooldown / 20);
        }
        gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7f, 0.7f, 0.7f, 1);
        yield return new WaitForSeconds(dashcooldown / 20);
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);     
    }

}
