﻿using UnityEngine;
using System.Collections;

public class Change_Camera_Limits : MonoBehaviour {

    public float maxX;
    public float minX;
    public float maxY;
    public float minY;

    public bool fast_transition;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {

            if (Camera.main.GetComponent<camera>().minX == minX &&
            Camera.main.GetComponent<camera>().maxX == maxX &&
            Camera.main.GetComponent<camera>().minY == minY &&
            Camera.main.GetComponent<camera>().maxY == maxY)
            {

            }
            else
            {
                if (fast_transition == false)
                {
                    Camera.main.GetComponent<camera>().transition_function(minX, maxX, minY, maxY, 2.0f, 100, 1.75f);
                }
                else
                {
                    Camera.main.GetComponent<camera>().transition_function(minX, maxX, minY, maxY, 100, 100, 0.0f);
                }

            }
        }
    }
}
