﻿using UnityEngine;
using System.Collections;

public class Wind_Zone : MonoBehaviour {

    public Vector2 direction;
    public float strength;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Rigidbody2D>().AddForce(direction * strength);
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {

    }
}
