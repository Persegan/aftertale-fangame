﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;


public class enemy_data : MonoBehaviour {
	
	public string  enemy_name;
	public float health;
	public float max_health;
	public float attack;
    public float defense;
	public Slider health_bar;
	public List <enemy_interaction> interactions;
    public List<Memory> memories;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	
	}

	public void attack_abstract (int turn)
	{
		BroadcastMessage ("attack", turn);
	}

	public void dialogue_abstract (int turn)
	{
		BroadcastMessage ("dialogue", turn);
	}

	public void ExecuteInteraction_abstract (string name)
	{
		BroadcastMessage ("ExecuteInteraction", name);
	}

    public void ExecuteMemory_abstract(string name)
    {
        BroadcastMessage("ExecuteMemory", name);
    }

    public void ExecutePacifistEnding_abstract()
    {
        BroadcastMessage("ExecutePacifistEnding");
    }

    public void ExecuteGenocideEnding_abstract()
    {
        BroadcastMessage("ExecuteGenocideEnding");
    }
}
	
[System.Serializable]
public class Memory
{
    public string name;
    public Color[] colors = new Color[3];
    public string message;

    public Memory(string _name, Color[] _colors, string _message)
    {
        this.name = _name;
        this.colors = _colors;
        this.message = _message;
    }
}


[System.Serializable]
public class enemy_interaction
{
    public string name;
    public string message;
    public bool unread;

    public enemy_interaction(string _name, string _message, bool _unread)
    {
        this.name = _name;
        this.message = _message;
        this.unread = _unread;
    }
}