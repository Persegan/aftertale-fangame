﻿using UnityEngine;
using System.Collections;

public class Soul_Afterimage : MonoBehaviour {

    public GameObject target;

    public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float step = speed * Time.deltaTime;
       if (gameObject != null)transform.position = Vector3.MoveTowards(transform.position, target.GetComponent<Transform>().position, step);
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "memory")
        {
            Destroy(gameObject);
        }
    }


    void OnTriggerStay2D(Collider2D collider)
    {
        OnTriggerEnter2D(collider);
    }
}
