﻿using UnityEngine;
using System.Collections;

public class Reflection_Movement : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (player.GetComponent<Animator>().enabled == true)
        {
            GetComponent<Animator>().enabled = true;
            GetComponent<Animator>().SetInteger("direction", player.GetComponent<Animator>().GetInteger("direction"));
            GetComponent<Animator>().SetBool("moving", player.GetComponent<Animator>().GetBool("moving"));
            GetComponent<Animator>().SetBool("Dashing", player.GetComponent<Animator>().GetBool("Dashing"));
        }
        else
        {
            GetComponent<Animator>().enabled = false;
        }

    }
}
