﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change_Box_Collider_For_Character : MonoBehaviour {

    public bool front_or_back;

    private GameObject character;

    // Use this for initialization
    void Start()
    {
        character = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        if (character.transform.position.y < gameObject.transform.position.y)
        {
            if (front_or_back)
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
            }
            else
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }

        }
        else
        {
            if (front_or_back)
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
            else
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
            }

        }

    }
}
