﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Popup_text_fonts : MonoBehaviour {

    public string[] messages;
    public AudioSource[] dialoguesounds;
    public Font[] fonts;
    public Image dialoguebox;
    public Text displaytext;
    public GameObject player;
    public int variable;
    public int value;

    protected int state = -1;

    protected bool caninteract = false;
    protected Font originalfont;

    // Use this for initialization
    void Start()
    {
        originalfont = displaytext.font;
    }

    // Update is called once per frame
    void Update()
    {
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0.000000000000000000001f, 0);
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (-0.000000000000000000001f, 0);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (state == messages.Length - 1 && AutoTextScript.isinteracting == false)
            {
                player.GetComponent<movement>().can_move = true;
                dialoguebox.gameObject.SetActive(false);
                displaytext.gameObject.SetActive(false);
                AutoTextScript.isfinished = false;
                AutoTextScript.isinteracting = false;
                displaytext.font = originalfont;
                if (variable != 0 || value != 0)
                {
                    player_data.playerdata.global_variables[variable] = value;
                }
                state = -1;
                //StartCoroutine(UpdateStatus_Coroutine());
            }
            else if (caninteract == true && AutoTextScript.isinteracting == false)
            {
                state++;
                AutoTextScript.isinteracting = true;
                player.GetComponent<movement>().can_move = false;
                dialoguebox.gameObject.SetActive(true);
                displaytext.gameObject.SetActive(true);
                displaytext.text = "";
                displaytext.font = fonts[state];           
                AutoTextScript.TypeText(displaytext, messages[state], 0.03f, dialoguesounds[state]);

            } /*else if (state == 3) {
				player.GetComponent<movement> ().can_move = true;
				dialoguebox.gameObject.SetActive (false);
				displaytext.gameObject.SetActive (false);
				AutoTextScript.isfinished = false;
				AutoTextScript.isinteracting = false;
				state = 0;
			}*/


        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (caninteract == true)
            {
                if (state >= 0)
                {
                    AutoTextScript.StopText(displaytext);
                    messages[state] = messages[state].Replace("ç", "\n");
                    displaytext.text = messages[state];
                    AutoTextScript.isfinished = true;
                    AutoTextScript.isinteracting = false;
                }

            }
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player_Collision")
        {
            if (collision.gameObject.GetComponentInParent<movement>().menu_open == false && collision.gameObject.GetComponentInParent<movement>().in_menu == false && collision.gameObject.GetComponentInParent<movement>().can_interact == true)
            {
                caninteract = true;
                // AutoTextScript.isfinished = true;
            }
            else
            {
                caninteract = false;
                // AutoTextScript.isfinished = false;
            }
            /*
			if (AutoTextScript.isinteracting == false) {				
				state = 1;
			} else if (AutoTextScript.isinteracting == true) {
					state = 2;	
			}
			if (AutoTextScript.isfinished == true) {
				state = 3;
			}*/
        }
    }

    void OnTriggerExit2D()
    {
        caninteract = false;
        AutoTextScript.isfinished = false;
    }

    /*IEnumerator UpdateStatus_Coroutine()
    {
        yield return 0;
        BroadcastMessage("UpdateStatus");
    }*/
}
