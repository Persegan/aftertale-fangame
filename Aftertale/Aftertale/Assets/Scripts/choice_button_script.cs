﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class choice_button_script : MonoBehaviour {

    public GameObject soul;
    public AudioSource menu_choose;
    public AudioSource menu_highlight;

    public static int choice = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void UpdateSoulPosition()
    {
        GameObject myEventSystem = GameObject.Find("EventSystem");
        GameObject currentbutton = myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().currentSelectedGameObject;
        soul.transform.position = new Vector3(currentbutton.transform.position.x - 0.25f, currentbutton.transform.position.y, soul.transform.position.z);
    }

    public void PlayMenuChoose()
    {
        transform.GetComponentInParent<AudioSource>().PlayOneShot(menu_choose.clip);
    }
    
    public void PlayMenuHighlight()
    {
        menu_highlight.Play();
    }

    public void choiceincrease()
    {
        choice++;
    }
    public void choicedecrease()
    {
        choice--;
    }
}
