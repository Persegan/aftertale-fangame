﻿using UnityEngine;
using System.Collections;

public class attack_indicator : MonoBehaviour {

	public bool active = true;
	public AudioSource hit;
	public GameObject battle_manager;
	public GameObject enemy;

	private bool miss;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (2f, 0f);
		StartCoroutine (miss_attack ());

	}
	
	// Update is called once per frame
	void Update () {
		if (active == true) {
			if (Input.GetKeyDown (KeyCode.Z)) {
				StopAllCoroutines ();
				//hit.Play ();
				//gameObject.GetComponent<Animator> ().SetBool ("attack", true);
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 0f);
                battle_manager.GetComponent<battle_manager>().damage = 520 - Mathf.Abs(gameObject.GetComponent<RectTransform>().localPosition.x);
                battle_manager.GetComponent<battle_manager>().damage = ((battle_manager.GetComponent<battle_manager>().damage - 0)) / (520 - 0) * (120 - 80) + 80;
                battle_manager.GetComponent<battle_manager>().damage = battle_manager.GetComponent<battle_manager>().damage / 100;
                battle_manager.GetComponent<battle_manager> ().damage_enemy (enemy);
				miss = false;
				active = false;
			}

		}
		if (miss == true) {
			battle_manager.GetComponent<battle_manager> ().miss_enemy (enemy);
			Destroy (gameObject);
		}

	
	}



	IEnumerator miss_attack()
	{
		yield return new WaitForSeconds (1.2f);
		{
			miss = true;
		}
	}
}
